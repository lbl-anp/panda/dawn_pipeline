#!/bin/bash
version=$1
#sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-l4t-ros-melodic:${version}
sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-l4t-ros-melodic:${version}
