#!/bin/bash

rm -rf yolov7
rm -rf TensorRT-For-YOLO-Series

cp -r ../../dependencies/yolov7 .                   # detached branch
cp -r ../../dependencies/TensorRT-For-YOLO-Series . # detached branch

version=$1
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-l4t-noetic-tensorrt7:${version}
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-l4t-noetic-tensorrt7:${version}
