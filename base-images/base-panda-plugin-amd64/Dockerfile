FROM registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-amd64-focal-noetic:v1.0.1

RUN ln -s /usr/bin/python3.8 /usr/bin/python

# Setup directory structure
RUN mkdir -p /home/panda
WORKDIR /home/panda
RUN mkdir tmp && mkdir -p panda_ws/src

# Install dependencies 
RUN apt-get update \
  && apt-get install -y python3-pip \
# For h5py
  && apt-get install -y libhdf5-dev \
# For numpy
  && apt-get install -y gfortran libopenblas-dev liblapack-dev \
# For USB devices
  && apt-get install -y libusb-1.0 \
# LLVM for numba
  && apt-get install -y llvm-10 \
  && ln -s /usr/bin/llvm-config-10 /usr/bin/llvm-config \
# For curie setup install
  && apt-get install -y libcfitsio-dev \
# For stark setup install
  && apt-get install -y libxml2-dev libxslt-dev \
# For panda_cal
  && apt-get install -y libnlopt-dev
RUN rm -rf /var/lib/apt/lists/*

# Install python packages
#RUN pip3 install cython
RUN rm -rf /usr/lib/python3/dist-packages/numpy
#RUN pip3 install --force-reinstall numpy==1.19
#RUN pip3 install --force-reinstall numpy==1.20.3
RUN pip3 install --force-reinstall numpy==1.21
RUN pip3 install matplotlib
RUN pip3 install numba
RUN pip3 install healpy
RUN pip3 install h5py
RUN pip3 install deprecated

RUN apt-get update \
  && apt-get install -y swig
RUN rm -rf /var/lib/apt/lists/*
RUN pip3 install nlopt

RUN pip3 install pytest-cov
RUN pip3 install numexpr
RUN pip3 install statsmodels

RUN pip3 install scikit-learn

RUN pip3 install python-dateutil

# Set up ROS workspace and environment
RUN echo "source /opt/ros/noetic/setup.bash" >> .bashrc
WORKDIR /home/panda/panda_ws
RUN /bin/bash -c '. /home/panda/.bashrc; \
  catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3'
