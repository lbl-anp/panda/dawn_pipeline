#!/bin/bash
version=$1
sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/base-amd64-focal-noetic:${version}
