
### Scope:

This application is part of the PANDA project, collaboration between the ANP Group at LBNL and ANL.  
The PANDA project leverages the DAWN nodes to deploy radiation sensors in the city of Chicago, IL.  
The goal of the depoyment of these so-called PANDAWN nodes is to demonstrate the capability to track  
radioactive sources through an urban enviroment using a sparse array of detectors, with additional  
contextual information, and the possibility to use the array in a network configuration.

### Plugin:

The panda-listmode-agg plugin is run to aggregate listmode data into buffers that can be queried by  
downstream plugins for specific time ranges. It comes in two flavors, one plugin to aggregate raw  
listmode data from the digibase, another to aggregate the calibrated listmode data.
