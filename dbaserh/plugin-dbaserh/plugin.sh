#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

source /home/panda/.bashrc
source /home/panda/panda_ws/devel/setup.bash

export ROS_LOG_DIR="/home/panda/mount"

echo -en "Digibase params:"
echo -en "HV: "${DBASERH_HV}'\n'
echo -en "FGN: "${DBASERH_FGN}'\n'
echo -en "LLD: "${DBASERH_LLD}'\n'

roslaunch dbaserh_ros listmode.launch 
