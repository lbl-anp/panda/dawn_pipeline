#!/bin/bash

rm -rf curie
rm -rf curie_ros
rm -rf dbaserh
rm -rf dbaserh_ros
rm -rf stark
rm -rf stark_ros

cp -r ../../dependencies/curie .        # detached branch, original commit
cp -r ../../dependencies/curie_ros .    # dev_panda branch
cp -r ../../dependencies/dbaserh .      # main branch
cp -r ../../dependencies/dbaserh_ros .  # dev_panda branch
cp -r ../../dependencies/stark .        # detached branch, original commit
cp -r ../../dependencies/stark_ros .    # dev_panda branch

version=$1
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:${version}
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:${version}
