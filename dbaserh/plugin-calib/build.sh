#!/bin/bash

rm -rf becquerel
rm -rf curie
rm -rf curie_ros
rm -rf panda_cal_ros
rm -rf poisson-gof
rm -rf stark
rm -rf stark_ros

cp -r ../../dependencies/becquerel .
cp -r ../../dependencies/curie .         # detached branch, original commit
cp -r ../../dependencies/curie_ros .     # dev_panda branch
cp -r ../../dependencies/panda_cal_ros . # main branch
cp -r ../../dependencies/poisson-gof .
cp -r ../../dependencies/stark .         # detached branch, original commit
cp -r ../../dependencies/stark_ros .     # dev_panda branch

version=$1
sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:${version}
