#!/bin/bash

# target node passed as argument
NODE=$1

# select jobs to submit
RAINGAUGE=false
METSENSE=false
CAMERA=false
LIDAR=false

DBASERH=true
DBASERH_AGG=true
CALIB=true
BKGMON=false

OBJDET=false
OBJDEPTH=false
OBJTRK=false

ROSBAG_RADENV=true
ROSBAG_CAM=false
ROSBAG_LID=false
ROSBAG_OBJ=false

if [ "$RAINGAUGE" = true ]; then
echo 'Submiting job for raingauge plugin'
$sesctl submit --file-path raingauge_$NODE.yaml
echo '... done'
fi
if [ "$METSENSE" = true ]; then
echo 'Submiting job for metsense plugin'
$sesctl submit --file-path metsense_$NODE.yaml
echo '... done'
fi
if [ "$CAMERA" = true ]; then
echo 'Submiting job for camera plugin'
$sesctl submit --file-path camera_$NODE.yaml
echo '... done'
fi
if [ "$LIDAR" = true ]; then
echo 'Submiting job for lidar plugin'
$sesctl submit --file-path lidar_$NODE.yaml
echo '... done'
fi

if [ "$DBASERH" = true ]; then
echo 'Submiting job for dbaserh plugin'
$sesctl submit --file-path dbaserh_$NODE.yaml
echo '... done'
fi
if [ "$DBASERH_AGG" = true ]; then
echo 'Submiting job for dbaserh-agg plugin'
$sesctl submit --file-path dbaserh-agg_$NODE.yaml
echo '...done'
fi
if [ "$CALIB" = true ]; then
echo 'Submiting job for calib plugin'
$sesctl submit --file-path calib_$NODE.yaml
echo '... done'
fi

if [ "$BKGMON" = true ]; then
echo 'Submiting job for calib-agg plugin'
$sesctl submit --file-path calib-agg_$NODE.yaml
echo '... done'
fi

if [ "$OBJDET" = true ]; then
echo 'Submiting job for obj-detection plugin'
$sesctl submit --file-path obj-detection_$NODE.yaml
echo '... done'
fi
if [ "$OBJDEPTH" = true ]; then
echo 'Submiting job for obj-depth plugin'
$sesctl submit --file-path obj-depth_$NODE.yaml
echo '... done'
fi
if [ "$OBJTRK" = true ]; then
echo 'Submiting job for obj-tracker plugin'
$sesctl submit --file-path obj-tracker_$NODE.yaml
echo '... done'
fi

if [ "$ROSBAG_RADENV" = true ]; then
echo 'Submiting job for rosbag-radenv plugin'
$sesctl submit --file-path rosbag-radenv_$NODE.yaml
echo '... done'
fi
if [ "$ROSBAG_CAM" = true ]; then
echo 'Submiting job for rosbag-cam plugin'
$sesctl submit --file-path rosbag-cam_$NODE.yaml
echo '... done'
fi
if [ "$ROSBAG_LID" = true ]; then
echo 'Submiting job for rosbag-lid plugin'
$sesctl submit --file-path rosbag-lidar_$NODE.yaml
echo '... done'
fi
if [ "$ROSBAG_OBJ" = true ]; then
echo 'Submiting job for rosbag-obj plugin'
$sesctl submit --file-path rosbag-obj_$NODE.yaml
echo '... done'
fi



