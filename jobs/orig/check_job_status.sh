#!/bin/bash

# job ID passed as argument
ID=$1

# select jobs to create
if [ "$ID" ]; then
	$sesctl stat --job-id $ID
else
	$sesctl stat
fi


