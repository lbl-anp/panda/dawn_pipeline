import argparse
import os
import h5py
import numpy as np
from datetime import datetime
from collections import defaultdict
import matplotlib.pyplot as plt

def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys

def sqrt_n_bins(nmin=0, nmax=999, nbins=128):
    """ Returns quadratically-spaced energy bins."""
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins+1)
    bin_edges = bin_edges**2
    bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2
    return bin_edges, bin_centers


# plot timelines in units of days
time_norm = 24 * 3600

# nominal rad/env dataset keys
env_keys_ = ["env/pressure_shield/ts", "env/pressure_shield/val",
            "env/pressure_enclosure/ts", "env/pressure_enclosure/val",
            "env/humidity_shield/ts", "env/humidity_shield/val",
            "env/humidity_enclosure/ts", "env/humidity_enclosure/val",
            "env/temp_shield/ts", "env/temp_shield/val",
            "env/temp_enclosure/ts", "env/temp_enclosure/val",
            "env/rain/ts", "env/rain/val"]
cal_keys_ = ["calib/ts", "calib/fmin", "calib/pval",
            "calib/gain", "calib/off", "calib/sat", "calib/plaw", "calib/res",
            "calib/w_K", "calib/w_U", "calib/w_Th", "calib/w_Rn",
            "calib/w_511", "calib/w_cosmics"]

parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()

save_dir = os.path.join(os.getcwd(), 'timelines/'+args.node)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
    
env_datasets = defaultdict(list)
rad_datasets = defaultdict(list)
cal_datasets = defaultdict(list)
env_keys = [args.node+'/'+key for key in env_keys_]
cal_keys = [args.node+'/'+key for key in cal_keys_]

# read data in
for f in args.files:
    print('.. parsing file:', f)
    with h5py.File(f, 'r') as h5in:
        keys = get_dataset_keys(h5in)
        # read env data
        for key in env_keys:
            if key not in keys:
                print("*** Missing env dataset:", key, "***")
            else:
                k = key.replace(args.node+'/', '')
                env_datasets[k].append(h5in[key][()])
        # read rad data
        if args.node+"/listmode/raw/ts_det" not in keys:
            print("*** Missing rad data ***")
            continue
        else:
            ts = h5in[args.node+"/listmode/raw/ts_det"][()]
            val = h5in[args.node+"/listmode/raw/channel"][()]
            rad_datasets["listmode/raw/ts_det"].append(ts)
            rad_datasets["listmode/raw/channel"].append(val)
        if args.node+"/listmode/calib/ts_det" not in keys:
            print("*** Missing calibrated data ***")
        else:
            ts = h5in[args.node+"/listmode/calib/ts_det"][()]
            val = h5in[args.node+"/listmode/calib/energy"][()]
            rad_datasets["listmode/calib/ts_det"].append(ts)
            rad_datasets["listmode/calib/energy"].append(val)
        # calibration params
        for key in cal_keys:
            if key not in keys:
                print("*** Missing env dataset:", key, "***")
            else:
                k = key.replace(args.node+'/', '')
                cal_datasets[k].append(h5in[key][1:]) # first entry is sometimes replicated
print('Concatenating data')
data = {}
print(".. env data")
for key in env_datasets:
    data[key] = np.concatenate(env_datasets[key])
print(".. rad data")
for key in rad_datasets:
    data[key] = np.concatenate(rad_datasets[key])
print(".. cal data")
for key in cal_datasets:
    data[key] = np.concatenate(cal_datasets[key])
print('.. Concatenating data done')

# plot timelines
# env data        
if "env/rain/ts" in data:
    t_beg = data["env/rain/ts"][0]
    t_end = data["env/rain/ts"][-1]
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    rain_ts_centers = 0.5 * (data["env/rain/ts"][:-1] + data["env/rain/ts"][1:])
    rain_val = data["env/rain/val"][1:] - data["env/rain/val"][:-1]
    lam_B = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
    rain_proxy = np.zeros_like(rain_val)
    rain_proxy[0] = rain_val[0]
    for i in range(1, rain_val.size):
        rain_proxy[i] = rain_proxy[i - 1] * np.exp(-lam_B * (rain_ts_centers[i] - rain_ts_centers[i - 1])) 
        rain_proxy[i] += rain_val[i]               
    fig, ax = plt.subplots(2, sharex=True)
    ts = (rain_ts_centers - rain_ts_centers[0])/time_norm
    ax[0].plot(ts, rain_val)
    ax[0].set_ylabel("Precipitation [?]")
    ax[1].plot(ts, rain_proxy)
    ax[1].hlines(0.05, ts[0], ts[-1], color='black', linestyle='--')
    ax[1].set_xlabel("Time [day]")
    ax[1].set_ylabel("Proxy [?]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/precipitation.png')
if "env/temp_shield/ts" in data:
    t_beg = min(data["env/temp_shield/ts"][0],
                data["env/pressure_shield/ts"][0],
                data["env/humidity_shield/ts"][0])
    t_end = max(data["env/temp_shield/ts"][-1],
                data["env/pressure_shield/ts"][-1],
                data["env/humidity_shield/ts"][-1])
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot((data["env/temp_shield/ts"] - t_beg)/time_norm, data["env/temp_shield/val"],
               label="T (shield)", color='orange', alpha=0.4)
    ax[0].set_ylabel("Temperature [C]")
    ax[0].legend(loc="best")
    ax[1].plot((data["env/pressure_shield/ts"] - t_beg)/time_norm, data["env/pressure_shield/val"],
               label="p (shield)", color='gray', alpha=0.4)
    ax[1].set_ylabel("Pressure [Pa]")
    ax[1].legend(loc="best")
    ax[2].plot((data["env/humidity_shield/ts"] - t_beg)/time_norm, data["env/humidity_shield/val"],
               label="H (shield)", color='blue', alpha=0.4)
    ax[2].set_ylabel("Humidity [%]")
    ax[2].legend(loc="best")
    ax[2].set_xlabel("Time [day]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/env_shield.png')
if "env/temp_enclosure/ts" in data:
    t_beg = min(data["env/temp_enclosure/ts"][0],
                data["env/pressure_enclosure/ts"][0],
                data["env/humidity_enclosure/ts"][0])
    t_end = max(data["env/temp_enclosure/ts"][-1],
                data["env/pressure_enclosure/ts"][-1],
                data["env/humidity_enclosure/ts"][-1])
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot((data["env/temp_enclosure/ts"] - t_beg)/time_norm, data["env/temp_enclosure/val"],
               label="T (enclosure)", color='orange', alpha=0.4)
    ax[0].set_ylabel("Temperature [C]")
    ax[0].legend(loc="best")
    ax[1].plot((data["env/pressure_enclosure/ts"] - t_beg)/time_norm, data["env/pressure_enclosure/val"],
               label="p (enclosure)", color='gray', alpha=0.4)
    ax[1].set_ylabel("Pressure [Pa]")
    ax[1].legend(loc="best")
    ax[2].plot((data["env/humidity_enclosure/ts"] - t_beg)/time_norm, data["env/humidity_enclosure/val"],
               label="H (enclosure)", color='blue', alpha=0.4)
    ax[2].set_ylabel("Humidity [%]")
    ax[2].legend(loc="best")
    ax[2].set_xlabel("Time [day]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/env_enclosure.png')
# rad data
if "listmode/raw/ts_det" in data:
    ts = data["listmode/raw/ts_det"]
    val = data["listmode/raw/channel"]
    beg = datetime.fromtimestamp(ts[0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(ts[-1]).strftime("%m/%d/%Y, %H:%M:%S")
    nchannels = 1024
    bin_edges = np.arange(nchannels)
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    spectrum = np.histogram(val, bins=bin_edges)[0]
    fig, ax = plt.subplots()
    ax.step(bin_centers, spectrum, where='mid', label="integrated raw spectrum")
    ax.set_xlabel("Channel")
    ax.set_ylabel("Entries")
    ax.set_yscale('log')
    ax.legend(loc="best")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/raw_spectrum.png')
if "listmode/calib/ts_det" in data:
    ts = data["listmode/calib/ts_det"]
    val = data["listmode/calib/energy"]
    beg = datetime.fromtimestamp(ts[0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(ts[-1]).strftime("%m/%d/%Y, %H:%M:%S")
    nbins = 128
    nmin, nmax = 50, 3000
    bin_edges, bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=nbins)
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    spectrum = np.histogram(val, bins=bin_edges)[0]
    fig, ax = plt.subplots()
    ax.step(bin_centers, spectrum, where='mid', label="integrated cal spectrum")
    ax.set_xlabel("Energy [MeV]")
    ax.set_ylabel("Entries")
    ax.set_yscale('log')
    ax.legend(loc="best")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/calib_spectrum.png')
    # compute gross count rate
    cps_ts, cps_val = [], []
    spectrum = np.zeros(nbins)
    tf_idx = 0
    tl_idx = tf_idx + 1
    max_lt = 1.
    max_idx = ts.size
    while not tl_idx == max_idx:
        tf = ts[tf_idx]
        tl = ts[tl_idx]
        lt = tl - tf
        while lt < max_lt and tl_idx < max_idx:
            tl = ts[tl_idx]
            lt = tl - tf
            tl_idx += 1
        if tl_idx == max_idx:
            break
        cps_ts.append(tf)
        spectrum += np.histogram(val[tf_idx:tl_idx], bins=bin_edges)[0]
        cps_val.append(spectrum.sum()/max_lt)
        spectrum = np.zeros(nbins)
        tf_idx = tl_idx
    fig, ax = plt.subplots()
    cps_ts = (cps_ts - cps_ts[0])/time_norm
    ax.plot(cps_ts, cps_val)
    ax.set_xlabel("Time [day]")
    ax.set_ylabel("Gross count rate [cps]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/calib_gross_count_rate.png')
# calib params
if "calib/ts" in data:
    beg = datetime.fromtimestamp(data["calib/ts"][0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(data["calib/ts"][-1]).strftime("%m/%d/%Y, %H:%M:%S")
    cal_ts = (data["calib/ts"] - data["calib/ts"][0])/time_norm
    fig, ax = plt.subplots(2, sharex=True)
    ax[0].plot(cal_ts, data["calib/fmin"])
    ax[0].set_ylabel("fmin")
    ax[1].plot(cal_ts, data["calib/pval"])
    ax[1].set_ylabel("p-value")
    ax[1].set_xlabel("Time [day]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/calib_fit.png')
    fig, ax = plt.subplots(5, sharex=True)
    ax[0].plot(cal_ts, data["calib/res"])
    ax[0].set_ylabel("res")
    ax[1].plot(cal_ts, data["calib/plaw"])
    ax[1].set_ylabel("power")
    ax[2].plot(cal_ts, data["calib/gain"])
    ax[2].set_ylabel("gain")
    ax[3].plot(cal_ts, data["calib/sat"])
    ax[3].set_ylabel("sat")
    ax[4].plot(cal_ts, data["calib/off"])
    ax[4].set_ylabel("off")
    ax[4].set_xlabel("Time [day]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/calib_params.png')             
    fig, ax = plt.subplots(6, sharex=True)
    ax[0].plot(cal_ts, data["calib/w_cosmics"])
    ax[0].set_ylabel("cosmics")
    ax[1].plot(cal_ts, data["calib/w_511"])
    ax[1].set_ylabel("511keV")
    ax[2].plot(cal_ts, data["calib/w_K"])
    ax[2].set_ylabel("K")
    ax[3].plot(cal_ts, data["calib/w_U"])
    ax[3].set_ylabel("U")
    ax[4].plot(cal_ts, data["calib/w_Th"])
    ax[4].set_ylabel("Th")
    ax[5].plot(cal_ts, data["calib/w_Rn"])
    ax[5].set_ylabel("Rn")
    ax[5].set_xlabel("Time [day]")
    fig.suptitle(beg+" - "+end)
    plt.savefig(save_dir+'/calib_templates.png')
