from rosbag import Bag
import cv2
from cv_bridge import CvBridge
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()

cvbr = CvBridge()

cam_msgs, cam_ts = [], []
for filename in args.files:
    with Bag(filename, 'r') as bag:
        print(filename)
        msgs = list(bag.read_messages(topics=['/bag/sensors/camera/image_color']))
        cam_msgs.extend(msgs)
        cam_ts.extend([(msg.message.header.stamp.secs + msg.message.header.stamp.nsecs * 1E-9) for msg in msgs])
print(len(cam_msgs), "cam msgs")
cam_ts = np.asarray(cam_ts)

ts = cam_msgs[0].message.header.stamp.secs + cam_msgs[0].message.header.stamp.nsecs * 1E-9
print("first ts:", ts)
fig, ax = plt.subplots()
data = cvbr.compressed_imgmsg_to_cv2(cam_msgs[0].message)
plt.imshow(data)
title = "ts: " + str(ts)
plt.suptitle(title)
plt.show()
