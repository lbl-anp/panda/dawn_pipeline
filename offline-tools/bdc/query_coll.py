from bdc_api import BdcApi
import time
import os
import argparse

USERNAME = 'nabgrall'
API_KEY = 'COPY_KEY_GENERATED_ON_BDC_PORTAL'
HOST = 'https://bdc.lbl.gov'

def run_query_progress_checks(query_id):
    """Run subsequent checks every 3 seconds to see if a query succeeded.

    Parameters:

        :query_id: Valid query ID.

    Returns:

        - Named tuple object QueryInfo where the `progress` field
          contains the percentage progress of the query and the `status`
          field includes the job status.

    Raises:

        - None.
    """
    print ('Checking query progress every 3 seconds for query {0}'.format(query_id))
    query_info = BdcApi.QueryInfo(progress='0%', status='in progress')
    while query_info.progress != BdcApi.COMPLETE_QUERY and \
            query_info.status not in BdcApi.COMPLETE_QUERY_STATUS:
        time.sleep(3)
        query_info = api.check_query_progress(query_id)
        print ('Current query progress: {0}'.format(query_info.progress))
        print ('Current job status: {0}'.format(query_info.status))
    return query_info

def get_task_id(search_task, task_numbers):
    """Get a task ID from a return of `BdcApi.get_task_numbers`.

    Parameters:

        :search_task: Name of task to search for.
        :task_numbers: return value of `BdcApi.get_task_numbers`.

    Returns:

        - Task ID of the task to search for or -1.

    Raises:

        - None.
    """
    for key in task_numbers:
        if search_task in key:
            task_id = task_numbers[key]['ID']
            print ('Found desired task ID {0} in task {1}. ID is: {2}'.format(
                search_task, key, task_id))
            return task_id
    return -1


if __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--list', action='store_true')
    parser.add_argument('--coll', type=str)
    args = parser.parse_args()  

    api = BdcApi(username=USERNAME, api_key=API_KEY,
        hostname=HOST)
    task_name_to_search = 'PANDA'
    task_numbers = api.get_task_numbers()
    task_id = get_task_id(task_name_to_search, task_numbers)
    domains_by_task = api.get_domains(task_ID=task_id)
    domain_list = []
    for task in domains_by_task:
        domain_list.extend(domains_by_task[task])
    print ('Using all domains of this task, i.e: {0}'.format(domain_list))
    collections = api.get_datacollections(task_numbers=task_name_to_search, domains=domain_list)

    if args.list is True:
        print(collections)
    
    if args.coll is not None:
        desired_coll = args.coll

        #print('Requesting data collection:', desired_coll)
        #coll_query_id = api.start_datacollection_query(datacollection=desired_coll)
        #print ('Submitted datacollection query request with query ID {0}'.format(coll_query_id))
        #run_query_progress_checks(coll_query_id)

        # we need to query files directly to filter out .bag files
        print('Requesting files from data collection:', desired_coll)
        files = [f for f in api.get_files(datacollections=desired_coll)[desired_coll] if os.path.splitext(f)[1] == '.h5']
        for f in files: print('..', f)
        file_query_id = api.start_files_query(files=files)
        print ('Submitted files query request with query ID {0}'.format(file_query_id))
        run_query_progress_checks(file_query_id)

        print ('Downloading query results to local disk')
        result = api.save_file(file_query_id, jupyterhub=False, local_path='/home/nabgrall/BDC/data')
        print (result['message'])
    else:
        print("No collection specified for download")
