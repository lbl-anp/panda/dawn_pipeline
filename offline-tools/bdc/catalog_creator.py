from copy import deepcopy
from datetime import datetime, timezone
import json
from h5_parse import get_time_extents

PROJECTS = {"PANDA / 8"}
DATA_TYPE = "Collection"
PRIMARY_DOMAIN = "Radiation"
DATA_FORMAT = "ROS bag, HDF5"

# missing: CollectionInfo.ID, TimeInfo.TimeExtents, FileInfo.FileList
catalog_template = {
    "SchemaVersion": "1.4bdc",
    "ProjectInfo": {"ID": "PANDA / 8"},
    "CollectionInfo": {"ID": ""},
    "SubmitterInfo": {"ID": "nabgrall"},
    "AuthorsList": [],
    "TimeInfo": {
        "TimeExtents": []
     },
    "LocationInfo": {
        "LongitudeList": [],
        "LatitudeList": [],
        "GeodeticDatum": "",
        "Description": ""
    },
    "DataInfo": {
        "POCInfo": {"Name": "Nico Abgrall", "Lab":"LBNL"}, 
        "DataType": DATA_TYPE,
        "DataLevel": "",
        "DomainList": {
            "Primary": [PRIMARY_DOMAIN],
            "Secondary": []
        },
        "Instrument": "",
        "Description": "",
        "DataFormat": DATA_FORMAT,
        "ParserCodeFile":"Null"
    },
    "Comment": "",
    "FileInfo": {"FileList": []}
}


def _make_timestamp(t: datetime):
    # return datetime.fromtimestamp(t).replace(tzinfo=timezone.utc)\
    return datetime.utcfromtimestamp(t)\
        .isoformat(timespec="microseconds")


def to_datetime(t_unix):
    datetime.utcfromtimestamp(t_unix)


def make_catalog(
    collection_id: str, t_i: float, t_f: float, file_list: list,
    project: str, long: float, lat: float
):
    assert project in PROJECTS
    catalog = deepcopy(catalog_template)
    catalog["CollectionInfo"]["ID"] = collection_id
    catalog["TimeInfo"]["TimeExtents"] = [t_i, t_f]
    catalog["FileInfo"]["FileList"] = file_list
    catalog["ProjectInfo"]["ID"] = project
    catalog["LocationInfo"]["LongitudeList"] = [long]
    catalog["LocationInfo"]["LatitudeList"] = [lat]
    return catalog


def write_catalog(
    filepath: str, collection_id: str, t_i: float, t_f: float,
    file_list: list, project: str, long: float, lat: float
):
    t_i = _make_timestamp(t_i)
    t_f = _make_timestamp(t_f)
    catalog = make_catalog(collection_id, t_i, t_f, file_list, project, long, lat)
    with open(filepath, 'w') as json_file:
        json.dump(catalog, json_file, indent=2)
