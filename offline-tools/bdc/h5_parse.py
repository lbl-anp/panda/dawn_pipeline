import h5py
import numpy as np

def get_time_extents(filepath, node=""):
    with h5py.File(filepath, 'r') as f:
        ts_f = f[node+"/gis/ts_f"][()] # timestamp first
        ts_l = f[node+"/gis/ts_l"][()] # timestamp last
    return ts_f, ts_l
