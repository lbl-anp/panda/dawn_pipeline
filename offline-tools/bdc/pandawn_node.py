import os
import argparse
import numpy as np
from catalog_creator import write_catalog
from h5_parse import get_time_extents

DIR="/Users/nico/Desktop/BDC/PANDAWN/"
PROJECT = "PANDA / 8"
PANDAWN_LATLON = {'W022': (41.866594421, -87.666126815),
                  'W01C': (41.874286544, -87.666404294),}

def __removeprefix(text: str, prefix: str):
    # can use str.removeprefix in py3.9
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--node', type=str)
    args = parser.parse_args()
    node = args.node.upper()
    DIR += '/'+node
    LAT = PANDAWN_LATLON[node][0]
    LONG = PANDAWN_LATLON[node][1]
    collections_ignored = []
    collections = []
    collection_dirs = [d for d in os.listdir(DIR)]
    print("Creating catalogs for collections in:", DIR)
    for collection_dir in collection_dirs:
        collection_id = "PANDAWN_" + node + '_' + collection_dir
        print(".. collection DIR:", collection_dir)
        print(".. collection ID:", collection_id)
        collection_fullpath = os.path.join(DIR, collection_dir)
        catalog_filepath = os.path.join(collection_fullpath, collection_id + ".json")
        # make file list
        filelist_fullpath = []
        for dir_tuple in os.walk(collection_fullpath):
            filelist_fullpath.extend([os.path.join(dir_tuple[0], f) for f in dir_tuple[2] if f != "Icon\r"])
        # check to see if there's HDF5 files in filelist
        h5_filelist = [f for f in filelist_fullpath if f.endswith(".h5") or f.endswith(".hdf5")]
        # Only make JSONs for the collections with HDF5s:
        if h5_filelist:
            collections.append(collection_dir)
            ts_i, ts_f = zip(*[get_time_extents(os.path.join(catalog_filepath, f), node=node) for f in h5_filelist])
            t_i = np.min(ts_i)
            t_f = np.max(ts_f)
            filelist = [__removeprefix(f, f"{collection_fullpath}/") for f in filelist_fullpath]
            write_catalog(catalog_filepath, collection_id, t_i, t_f, filelist, PROJECT, LONG, LAT)
        else:
            collections_ignored.append(collection_dir)
            print(f"{collection_dir} ignored.")
