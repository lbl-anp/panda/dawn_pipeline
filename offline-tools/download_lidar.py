import sage_data_client
import subprocess

df = sage_data_client.query(
    start="2023-08-29 00:00:00.000000000+00:0", 
    #end="2023-08-08 15:00:00.000000000+00:0",
    filter={
        "name": "upload",
        "vsn": "W01C",
        "task": "panda-rosbag-lid", 
    },
)

# write values (urls)
df.value.to_csv("urls.txt", index=False, header=False)

# download everything in urls file
subprocess.check_call(["wget", "--user=<usr>", "--password=<pwd>", "-r", "-N", "-i", "urls.txt"])

