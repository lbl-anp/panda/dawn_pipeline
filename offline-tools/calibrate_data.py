import argparse
from calib import Calibrator

"""
Usage:
 - python3 calibrate_data.py --node VSN --files <path-to-h5files>/*.h5
"""

print("Calibrating data")
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()


if not args.node:
    print("Need to specify the node VSN")
else:
    for f in args.files:
        Cal = Calibrator(args.node.upper())
        Cal.run_calibration(f)
print("Calibrating data done")
