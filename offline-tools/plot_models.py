import argparse
import h5py
import os
import matplotlib.pyplot as plt

def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys

parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()
for f in args.files:
    print('file:', f)
    with h5py.File(f, 'r') as h5in:
        keys = get_dataset_keys(h5in)
        if "V" not in keys:
            print(".. is not a model file")
            continue
        else:
            n_components = h5in.attrs["n_components"]
            basis = h5in["V"][()]
            bin_centers = h5in["bin_centers"][()]
            
            fig, ax = plt.subplots()
            ax.step(bin_centers, basis[0], where='mid', label="static")
            if n_components > 1:
                ax.step(bin_centers, basis[1], where='mid', label="rain")
            ax.set_ylabel("pdf")
            ax.set_yscale('log')
            ax.set_xlabel("Energy [keV]")
            ax.legend(loc="best")
            fig.suptitle(os.path.splitext(os.path.basename(f))[0])
plt.show()
        
