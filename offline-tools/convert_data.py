import os
import argparse
from bag2bdch5 import Converter
from sanity import Sanitizer
"""
Usage:
 Conversion from bag to BDC h5 + clean up + timelines:
 - python3 convert_data.py --bag --node VSN --files <path-to-bagfiles>/*.bag
 Timelines only:
 - python3 convert_data.py --h5 --node VSN --files <path-to-h5files>/*.h5
"""

print('Converting bags to BDC h5 data')
parser = argparse.ArgumentParser()
parser.add_argument('--bag', action='store_true')
parser.add_argument('--h5', action='store_true')
parser.add_argument('--node', type=str)
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()

h5files = []
if not args.node:
    print("Need to specify the node VSN")
else:
    node = args.node.upper()
    if args.bag is True:
        print('Running conversion to h5 files on node', node)
        converter = Converter(node=node)
        for bagfile in args.files:
            print('file:', bagfile)
            print('running conversion to BDC h5 file')
            f = converter.to_bdch5(bagfile)
            if f is not None: h5files.append(f)
        print('Running conversion to h5 files done')

    print('Running sanity checks on node', node)
    directory = os.path.join(os.getcwd(), r'timelines')
    if not os.path.exists(directory):
        os.makedirs(directory)
    if args.h5 is True:
        h5files = args.files
    for h5file in h5files:
        print('file:', h5file)
        sanitizer = Sanitizer(h5file, directory, node=node)
        sanitizer.check_calibration()
        sanitizer.check_rad_timestamps()
        sanitizer.set_timerange()
        sanitizer.build_timelines()
print('Running sanity checks done')  
print('Converting bags done')    

