from panda_cal.templates import Templates
import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots(2, sharex=True)
edges = np.arange(0, 3200)

templates = Templates.from_csv_path("/Users/nico/Desktop/dawn_pipeline/offline-tools/panda_cal/templates/new_air_2000mm_alu_1mm", edges)
for l, t in templates.get_templates(0.07, as_dict=True).items():
    if l in ['A','R']:
        t *= 1E6
        ax[0].step(templates.centers, t, label=l, where="mid")
        ax[0].set_yscale('log')
        ax[0].set_ylabel("Scaled pdf")
        ax[0].set_xlabel("Energy [keV]")
        ax[0].legend(loc='best')
        ax[0].title.set_text('new_air_2000mm_alu_1mm')

templates = Templates.from_csv_path("/Users/nico/Desktop/dawn_pipeline/offline-tools/panda_cal/templates/thin_air_2m", edges)
for l, t in templates.get_templates(0.07, as_dict=True).items():
    if l in ['A','R']:
        t *= 1E6
        ax[1].step(templates.centers, t, label=l, where="mid")
        ax[1].set_yscale('log')
        ax[1].set_ylabel("Scaled pdf")
        ax[1].set_xlabel("Energy [keV]")
        ax[1].legend(loc='best')
        ax[1].title.set_text('thin_air_2m')
plt.show()
