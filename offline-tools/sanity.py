import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
from datetime import datetime
from calib_sanity import Calibrator

def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys


def sqrt_n_bins(nmin=0, nmax=999, nbins=128):
    """ Returns quadratically-spaced energy bins."""
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins+1)
    bin_edges = bin_edges**2
    bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2
    return bin_edges, bin_centers


# plot timelines in units of days
time_norm = 24 * 3600


# nominal rad/env dataset keys
env_keys_ = ["env/pressure_shield/ts", "env/pressure_shield/val",
            "env/pressure_enclosure/ts", "env/pressure_enclosure/val",
            "env/humidity_shield/ts", "env/humidity_shield/val",
            "env/humidity_enclosure/ts", "env/humidity_enclosure/val",
            "env/temp_shield/ts", "env/temp_shield/val",
            "env/temp_enclosure/ts", "env/temp_enclosure/val",
            "env/rain/ts", "env/rain/val"]
cal_keys_ = ["calib/ts", "calib/fmin", "calib/pval",
            "calib/gain", "calib/off", "calib/sat", "calib/plaw", "calib/res",
            "calib/w_K", "calib/w_U", "calib/w_Th", "calib/w_Rn",
            "calib/w_511", "calib/w_cosmics"]


class Sanitizer:
    def __init__(self, h5filename, directory, node=""):
        print("Initializing sanitizer")
        self.h5in = h5py.File(h5filename, 'r+')
        self.save_dir = os.path.join(directory, os.path.splitext(os.path.basename(h5filename))[0])
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
        self.node = node

    def check_calibration(self):
        keys = get_dataset_keys(self.h5in)
        if (self.node+"/listmode/calib/ts_det" not in keys) or (self.node+"/calib/gain" not in keys):
            # missing calibrated data
            print("*** Missing calibrated data")
            print("Running calibration")
            ts = self.h5in[self.node+"/listmode/raw/ts_det"][()]
            raw_val = self.h5in[self.node+"/listmode/raw/channel"][()]
            Cal = Calibrator(ts, raw_val)
            cal_val, cal_fit_params = Cal.run_calibration()
            for param in cal_fit_params:
                if self.node+param in keys: del self.h5in[self.node+param]
                self.h5in.create_dataset(self.node+param, data=cal_fit_params[param])
                time_attr = 'self' if param == "/calib/ts" else "/"+self.node+"/calib/ts" 
                unit_attr = 'unix time' if param == "/calib/ts" else "N/A"
                self.h5in[self.node+param].attrs['timeSyncLocation'] = time_attr
                self.h5in[self.node+param].attrs['unit'] = unit_attr
            if self.node+"/listmode/calib/ts_det" in keys: del self.h5in[self.node+"/listmode/calib/ts_det"]
            self.h5in.create_dataset(self.node+"/listmode/calib/ts_det",data=ts)
            self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation'] = 'self'
            self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit'] = 'unix time'
            if self.node+"/listmode/calib/energy" in keys: del self.h5in[self.node+"/listmode/calib/energy"]
            self.h5in.create_dataset(self.node+"/listmode/calib/energy",data=cal_val)
            self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation'] = "/"+self.node+"/listmode/calib/ts_det"
            self.h5in[self.node+"/listmode/calib/energy"].attrs['unit'] = 'keV'
        else:
            # check if calibration params were updated properly (i.e. no repeated sequence of values)
            gain_diff = np.diff(self.h5in[self.node+"/calib/gain"][()])
            if (gain_diff == 0).sum() > gain_diff.size // 4:
                print("*** Calibrated data was not updated properly")
                print("Running calibration")
                ts = self.h5in[self.node+"/listmode/raw/ts_det"][()]
                raw_val = self.h5in[self.node+"/listmode/raw/channel"][()]
                Cal = Calibrator(ts, raw_val)
                cal_val, cal_fit_params = Cal.run_calibration()
                for param in cal_fit_params: 
                    timesync_attr = self.h5in[self.node+param].attrs['timeSyncLocation']
                    unit_attr = self.h5in[self.node+param].attrs['unit']
                    del self.h5in[self.node+param]
                    self.h5in.create_dataset(self.node+param,data=cal_fit_params[param])
                    self.h5in[self.node+param].attrs['timeSyncLocation'] = timesync_attr
                    self.h5in[self.node+param].attrs['unit'] = unit_attr
                timesync_attr = self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation']
                unit_attr = self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit']
                del self.h5in[self.node+"/listmode/calib/ts_det"]
                self.h5in.create_dataset(self.node+"/listmode/calib/ts_det",data=ts)
                self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation'] = timesync_attr
                self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit'] = unit_attr
                timesync_attr = self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation']
                unit_attr = self.h5in[self.node+"/listmode/calib/energy"].attrs['unit']
                del self.h5in[self.node+"/listmode/calib/energy"]
                self.h5in.create_dataset(self.node+"/listmode/calib/energy",data=cal_val)
                self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation'] = timesync_attr
                self.h5in[self.node+"/listmode/calib/energy"].attrs['unit'] = unit_attr
                
    def check_rad_timestamps(self):
        print(".. checking timestamps")
        keys = get_dataset_keys(self.h5in)
        ts_keys = {"/calib/ts": ["/calib/fmin", "/calib/pval", "/calib/gain", "/calib/off",
                                 "/calib/sat", "/calib/plaw", "/calib/res",
                                 "/calib/w_K", "/calib/w_U", "/calib/w_Th", "/calib/w_Rn",
                                 "/calib/w_511", "/calib/w_cosmics"],
                   "/listmode/raw/ts_det": ["/listmode/raw/channel"],
                   "/listmode/calib/ts_det": ["/listmode/calib/energy"]}
        ts_deltas = {"/calib/ts": 65.,             # successive calib fits should be 60s apart
                     "/listmode/raw/ts_det": 1.,   # successive data should < 1s apart
                     "/listmode/calib/ts_det": 1.}
        for ts_label, val_labels in ts_keys.items():
            if self.node+ts_label not in keys:
                print("*** Missing dataset {} ***".format(ts_label))
            else:
                ts = self.h5in[self.node+ts_label][()]
                neg_diff_mask = np.where((ts[1:] - ts[:-1]) < 0)[0]
                pos_diff_mask = np.where((ts[1:] - ts[:-1]) > ts_deltas[ts_label])[0] 
                from_idx, to_idx = None, None
                if neg_diff_mask.size != 0:
                    from_idx = neg_diff_mask[0] + 1
                    if pos_diff_mask.size == 0:
                        to_idx = ts.size
                if pos_diff_mask.size != 0:
                    to_idx = pos_diff_mask[0] + 1
                    if neg_diff_mask.size == 0:
                        from_idx = 0
                if (from_idx is not None) and (to_idx is not None):
                    print(".... deleting and replacing original dataset {}".format(ts_label))
                    ts = np.delete(ts, np.s_[from_idx:to_idx])
                    timesync_attr = self.h5in[self.node+ts_label].attrs['timeSyncLocation']
                    unit_attr = self.h5in[self.node+ts_label].attrs['unit']
                    del self.h5in[self.node+ts_label]
                    self.h5in.create_dataset(self.node+ts_label,data=ts)
                    self.h5in[self.node+ts_label].attrs['timeSyncLocation'] = timesync_attr
                    self.h5in[self.node+ts_label].attrs['unit'] = unit_attr
                    for val_label in val_labels:
                        val = self.h5in[self.node+val_label][()]
                        val = np.delete(val, np.s_[from_idx:to_idx])
                        timesync_attr = self.h5in[self.node+val_label].attrs['timeSyncLocation']
                        unit_attr = self.h5in[self.node+val_label].attrs['unit']
                        del self.h5in[self.node+val_label]
                        self.h5in.create_dataset(self.node+val_label,data=val)
                        self.h5in[self.node+val_label].attrs['timeSyncLocation'] = timesync_attr
                        self.h5in[self.node+val_label].attrs['unit'] = unit_attr
                    
    def set_timerange(self):
        print(".. setting timerange")
        keys = get_dataset_keys(self.h5in)
        ts_keys = ["/env/pressure_shield/ts", "/env/pressure_enclosure/ts",
                   "/env/humidity_shield/ts", "/env/humidity_enclosure/ts",
                   "/env/temp_shield/ts", "/env/temp_enclosure/ts",
                   "/env/rain/ts", "/calib/ts",
                   "/listmode/raw/ts_det", "/listmode/calib/ts_det"]
        timestamps_f, timestamps_l = [], []
        for key in ts_keys:
            if self.node+key in keys: 
                timestamps_f.append(self.h5in[self.node+key][0])
                timestamps_l.append(self.h5in[self.node+key][-1])
        ts_f = min(timestamps_f)
        ts_l = max(timestamps_l)
        if self.node+"/gis/ts_f" in keys:
            del self.h5in[self.node+"/gis/ts_f"]
            del self.h5in[self.node+"/gis/ts_l"]
        self.h5in.create_dataset(self.node+"/gis/ts_f", data=ts_f)
        self.h5in[self.node+"/gis/ts_f"].attrs['unit'] = 'unix time'
        self.h5in.create_dataset(self.node+"/gis/ts_l", data=ts_l)
        self.h5in[self.node+"/gis/ts_l"].attrs['unit'] = 'unix time'
        print(".... tf: {}, tl: {} ({}h)".format(ts_f, ts_l, (ts_l - ts_f)/3600))
                    
    def build_timelines(self):
        print(".. building timelines")
        env_datasets = {}
        cal_datasets = {}
        env_keys = [self.node+'/'+key for key in env_keys_]
        cal_keys = [self.node+'/'+key for key in cal_keys_]
        keys = get_dataset_keys(self.h5in)
        # env data
        for key in env_keys:
            if key not in keys:
                print("*** Missing env dataset:", key, "***")
            else:
                k = key.replace(self.node+'/', '')
                env_datasets[k] = self.h5in[key][()]
        if "env/rain/ts" in env_datasets:
            t_beg = env_datasets["env/rain/ts"][0]
            t_end = env_datasets["env/rain/ts"][-1]
            beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
            end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
            rain_ts_centers = 0.5 * (env_datasets["env/rain/ts"][:-1] + env_datasets["env/rain/ts"][1:])
            rain_val = env_datasets["env/rain/val"][1:] - env_datasets["env/rain/val"][:-1]
            lam_B = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
            rain_proxy = np.zeros_like(rain_val)
            rain_proxy[0] = rain_val[0]
            for i in range(1, rain_val.size):
                rain_proxy[i] = rain_proxy[i - 1] * np.exp(-lam_B * (rain_ts_centers[i] - rain_ts_centers[i - 1])) 
                rain_proxy[i] += rain_val[i]               
            fig, ax = plt.subplots(2, sharex=True)
            ts = (rain_ts_centers - rain_ts_centers[0])/time_norm
            ax[0].plot(ts, rain_val)
            ax[0].set_ylabel("Precipitation [?]")
            ax[1].plot(ts, rain_proxy)
            ax[1].hlines(0.05, ts[0], ts[-1], color='black', linestyle='--')
            ax[1].set_xlabel("Time [day]")
            ax[1].set_ylabel("Proxy [?]")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/precipitation.png')
        if "env/temp_shield/ts" in env_datasets:
            t_beg = min(env_datasets["env/temp_shield/ts"][0],
                        env_datasets["env/pressure_shield/ts"][0],
                        env_datasets["env/humidity_shield/ts"][0])
            t_end = max(env_datasets["env/temp_shield/ts"][-1],
                        env_datasets["env/pressure_shield/ts"][-1],
                        env_datasets["env/humidity_shield/ts"][-1])
            beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
            end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
            fig, ax = plt.subplots(3, sharex=True)
            ax[0].plot((env_datasets["env/temp_shield/ts"] - t_beg)/time_norm, env_datasets["env/temp_shield/val"],
                       label="T (shield)", color='orange', alpha=0.4)
            ax[0].set_ylabel("Temperature [C]")
            ax[0].legend(loc="best")
            ax[1].plot((env_datasets["env/pressure_shield/ts"] - t_beg)/time_norm, env_datasets["env/pressure_shield/val"],
                       label="p (shield)", color='gray', alpha=0.4)
            ax[1].set_ylabel("Pressure [Pa]")
            ax[1].legend(loc="best")
            ax[2].plot((env_datasets["env/humidity_shield/ts"] - t_beg)/time_norm, env_datasets["env/humidity_shield/val"],
                       label="H (shield)", color='blue', alpha=0.4)
            ax[2].set_ylabel("Humidity [%]")
            ax[2].legend(loc="best")
            ax[2].set_xlabel("Time [day]")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/env_shield.png')
        if "env/temp_enclosure/ts" in env_datasets:
            t_beg = min(env_datasets["env/temp_enclosure/ts"][0],
                        env_datasets["env/pressure_enclosure/ts"][0],
                        env_datasets["env/humidity_enclosure/ts"][0])
            t_end = max(env_datasets["env/temp_enclosure/ts"][-1],
                        env_datasets["env/pressure_enclosure/ts"][-1],
                        env_datasets["env/humidity_enclosure/ts"][-1])
            beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
            end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
            fig, ax = plt.subplots(3, sharex=True)
            ax[0].plot((env_datasets["env/temp_enclosure/ts"] - t_beg)/time_norm, env_datasets["env/temp_enclosure/val"],
                       label="T (enclosure)", color='orange', alpha=0.4)
            ax[0].set_ylabel("Temperature [C]")
            ax[0].legend(loc="best")
            ax[1].plot((env_datasets["env/pressure_enclosure/ts"] - t_beg)/time_norm, env_datasets["env/pressure_enclosure/val"],
                       label="p (enclosure)", color='gray', alpha=0.4)
            ax[1].set_ylabel("Pressure [Pa]")
            ax[1].legend(loc="best")
            ax[2].plot((env_datasets["env/humidity_enclosure/ts"] - t_beg)/time_norm, env_datasets["env/humidity_enclosure/val"],
                       label="H (enclosure)", color='blue', alpha=0.4)
            ax[2].set_ylabel("Humidity [%]")
            ax[2].legend(loc="best")
            ax[2].set_xlabel("Time [day]")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/env_enclosure.png')
        # rad data
        if self.node+"/listmode/raw/ts_det" not in keys:
            print("*** Missing rad data ***")
        else:
            ts = self.h5in[self.node+"/listmode/raw/ts_det"][()]
            val = self.h5in[self.node+"/listmode/raw/channel"][()]
            beg = datetime.fromtimestamp(ts[0]).strftime("%m/%d/%Y, %H:%M:%S")
            end = datetime.fromtimestamp(ts[-1]).strftime("%m/%d/%Y, %H:%M:%S")
            nchannels = 1024
            bin_edges = np.arange(nchannels)
            bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
            spectrum = np.histogram(val, bins=bin_edges)[0]
            fig, ax = plt.subplots()
            ax.step(bin_centers, spectrum, where='mid', label="integrated raw spectrum")
            ax.set_xlabel("Channel")
            ax.set_ylabel("Entries")
            ax.set_yscale('log')
            ax.legend(loc="best")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/raw_spectrum.png')
        if self.node+"/listmode/calib/ts_det" not in keys:
            print("*** Missing calibrated data ***")
        else:
            ts = self.h5in[self.node+"/listmode/calib/ts_det"][()]
            val = self.h5in[self.node+"/listmode/calib/energy"][()]
            beg = datetime.fromtimestamp(ts[0]).strftime("%m/%d/%Y, %H:%M:%S")
            end = datetime.fromtimestamp(ts[-1]).strftime("%m/%d/%Y, %H:%M:%S")
            nbins = 128
            nmin, nmax = 50, 3000
            bin_edges, bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=nbins)
            bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
            spectrum = np.histogram(val, bins=bin_edges)[0]
            fig, ax = plt.subplots()
            ax.step(bin_centers, spectrum, where='mid', label="integrated cal spectrum")
            ax.set_xlabel("Energy [MeV]")
            ax.set_ylabel("Entries")
            ax.set_yscale('log')
            ax.legend(loc="best")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/calib_spectrum.png')
            # compute gross count rate
            cps_ts, cps_val = [], []
            spectrum = np.zeros(nbins)
            tf_idx = 0
            tl_idx = tf_idx + 1
            max_lt = 1.
            max_idx = ts.size
            while not tl_idx == max_idx:
                tf = ts[tf_idx]
                tl = ts[tl_idx]
                lt = tl - tf
                while lt < max_lt and tl_idx < max_idx:
                    tl = ts[tl_idx]
                    lt = tl - tf
                    tl_idx += 1
                if tl_idx == max_idx:
                    break
                cps_ts.append(tf)
                spectrum += np.histogram(val[tf_idx:tl_idx], bins=bin_edges)[0]
                cps_val.append(spectrum.sum()/max_lt)
                spectrum = np.zeros(nbins)
                tf_idx = tl_idx
            fig, ax = plt.subplots()
            cps_ts = (cps_ts - cps_ts[0])/time_norm
            ax.plot(cps_ts, cps_val)
            ax.set_xlabel("Time [day]")
            ax.set_ylabel("Gross count rate [cps]")
            fig.suptitle(beg+" - "+end)
            plt.savefig(self.save_dir+'/calib_gross_count_rate.png')
            # calib params
            if self.node+"/calib/ts" not in keys:
                print("*** Missing calibration data ***")
            else:
                for key in cal_keys:
                    k = key.replace(self.node+'/', '')
                    cal_datasets[k] = self.h5in[key][1:] # first entry is sometimes replicated
                beg = datetime.fromtimestamp(cal_datasets["calib/ts"][0]).strftime("%m/%d/%Y, %H:%M:%S")
                end = datetime.fromtimestamp(cal_datasets["calib/ts"][-1]).strftime("%m/%d/%Y, %H:%M:%S")
                cal_ts = (cal_datasets["calib/ts"] - cal_datasets["calib/ts"][0])/time_norm
                fig, ax = plt.subplots(2, sharex=True)
                ax[0].plot(cal_ts, cal_datasets["calib/fmin"])
                ax[0].set_ylabel("fmin")
                ax[1].plot(cal_ts, cal_datasets["calib/pval"])
                ax[1].set_ylabel("p-value")
                ax[1].set_xlabel("Time [day]")
                fig.suptitle(beg+" - "+end)
                plt.savefig(self.save_dir+'/calib_fit.png')
                fig, ax = plt.subplots(5, sharex=True)
                ax[0].plot(cal_ts, cal_datasets["calib/res"])
                ax[0].set_ylabel("res")
                ax[1].plot(cal_ts, cal_datasets["calib/plaw"])
                ax[1].set_ylabel("power")
                ax[2].plot(cal_ts, cal_datasets["calib/gain"])
                ax[2].set_ylabel("gain")
                ax[3].plot(cal_ts, cal_datasets["calib/sat"])
                ax[3].set_ylabel("sat")
                ax[4].plot(cal_ts, cal_datasets["calib/off"])
                ax[4].set_ylabel("off")
                ax[4].set_xlabel("Time [day]")
                fig.suptitle(beg+" - "+end)
                plt.savefig(self.save_dir+'/calib_params.png')             
                fig, ax = plt.subplots(6, sharex=True)
                ax[0].plot(cal_ts, cal_datasets["calib/w_cosmics"])
                ax[0].set_ylabel("cosmics")
                ax[1].plot(cal_ts, cal_datasets["calib/w_511"])
                ax[1].set_ylabel("511keV")
                ax[2].plot(cal_ts, cal_datasets["calib/w_K"])
                ax[2].set_ylabel("K")
                ax[3].plot(cal_ts, cal_datasets["calib/w_U"])
                ax[3].set_ylabel("U")
                ax[4].plot(cal_ts, cal_datasets["calib/w_Th"])
                ax[4].set_ylabel("Th")
                ax[5].plot(cal_ts, cal_datasets["calib/w_Rn"])
                ax[5].set_ylabel("Rn")
                ax[5].set_xlabel("Time [day]")
                fig.suptitle(beg+" - "+end)
                plt.savefig(self.save_dir+'/calib_templates.png')
