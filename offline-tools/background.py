import os
import math
import copy
import time
import h5py
from collections import defaultdict
import numpy as np
from scipy.stats import chi2
from collections import namedtuple

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) # need 1.4.0 for bad
import pandas as pd

from bad import BADLAD, BADMF
from bad.rebinning import sqrt_n_bins
from radai.algorithms import KSigma, NSCRAD
from radai.filters import EWMAFilter


def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys


TriageResult = namedtuple("TriageResult", "static rain other none")


class RainProxyFilter:
    def __init__(self):
        self.rain_proxy_init = 0
        self.rain_ts_centers = None
        
    def update(self, rain_ts, rain_val, thr=0.05, eps=1.0):
        """
        eps provides a tolerance window while checking if rain occurs at a given timestamp
        """
        if rain_ts is None: return
        rain_ts_centers = 0.5 * (rain_ts[:-1] + rain_ts[1:])
        rain_diff = rain_val[1:] - rain_val[:-1]
        lam_B = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
        rain_proxy = np.zeros_like(rain_diff)
        print("rain filter update: rain_proxy_init =", self.rain_proxy_init)
        rain_proxy[0] = self.rain_proxy_init + rain_diff[0]
        for i in range(1, rain_diff.size):
            rain_proxy[i] = rain_proxy[i - 1] * np.exp(-lam_B * (rain_ts_centers[i] - rain_ts_centers[i - 1])) # predict
            rain_proxy[i] += rain_diff[i] # update
        self.rain_proxy_init = rain_proxy[-1]
        print("rain filter update: last rain_proxy =", rain_proxy[-1])
        mask = np.where(rain_proxy > thr)
        self.rain_ts_centers = rain_ts_centers[mask]
        if self.rain_ts_centers.size == 0:
            self.rain_ts_centers = np.array([-np.inf])
        self.eps = eps
        
    def analyze(self, tf, tl):
        if self.rain_ts_centers is None: return False
        idx = np.searchsorted(self.rain_ts_centers, tf)
        if idx < self.rain_ts_centers.size:
            if self.rain_ts_centers[idx] < tl + self.eps:
                return True
        return False
    
       
class BkgDataMgr:
    def __init__(self, node):
        print("Initializing data mgr")
        self.node = node
        self.far = 0.125
        self.int_time = 1.
        self.time_norm = 24 * 3600
        self.nbins = 128
        nmin, nmax = 50, 3000
        self.bin_edges, self.bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=self.nbins)
        # filters
        self.slow_ewma_filter = None
        self.fast_ewma_filter = None
        self.scrad = None
        self.scrad_rn = None
        self.badfm_radon_filter = None
        self.triage_radon_filter = None
        # triage
        self.n_tot = 0
        self.n_static = 0
        self.n_rain = 0
        self.n_other = 0
        self.n_none = 0
        self.n_static_buff = 0
        self.n_rain_buff = 0
        self.static_spectrum = np.zeros(self.nbins)
        self.rain_spectrum = np.zeros(self.nbins)
        self.other_spectrum = np.zeros(self.nbins)
        self.none_spectrum = np.zeros(self.nbins)
        self.buffer_size = int(1/self.far) * 3600
        self.static_buffer = np.zeros((self.buffer_size, self.nbins))
        self.rain_buffer = np.zeros((self.buffer_size, self.nbins))
        # bkg models
        self.rain_comp = None
        self.static_comp = None
        self.badlad_models = {}
        self.badfm_models = {}
        # screening
        self.static_model = True
        self.model_label = "static"
        self.radon_filter_start_ts = 0
        self.radon_filter_inttime = 60
        self.badfm_radon_sum = 0
        self.badfm_radon_prev_anom_slow_up = False
        self.triage_radon_sum = 0 
        self.triage_radon_prev_anom_slow_up = False
        print("Initializing data mgr done")

    def getData(self, filename):
        print("Getting rad/env data from:", filename)
        try:
            self.h5in = h5py.File(filename, 'r')
            keys = get_dataset_keys(self.h5in)
            if self.node+"/listmode/calib/ts_det" not in keys:
                print(".. Missing rad data")
                return False
            self.ts = self.h5in[self.node+"/listmode/calib/ts_det"][()]
            self.val = self.h5in[self.node+"/listmode/calib/energy"][()]
            self.lon =  self.h5in[self.node+"/gis/lon"][()]
            self.lat =  self.h5in[self.node+"/gis/lat"][()]
            if self.node+"/env/rain/ts" not in keys:
                print(".. Missing rain data in file:", f)
                self.rain_ts, self.rain_val = None, None
            else:
                self.rain_ts = self.h5in[self.node+"/env/rain/ts"][()]
                self.rain_val = self.h5in[self.node+"/env/rain/val"][()]
            print("Getting data done")
            return True
        except:
            print(".. Exception occurred when trying to open", filename)
            return False

    def getBkgComponents(self, filename):
        print("Loading background components:", filename)
        with h5py.File(filename, 'r') as h5in:
            keys = get_dataset_keys(h5in)
            if "static_comp" in keys:
                self.static_comp = h5in["static_comp"][()]
            if "rain_comp" in keys:
                self.rain_comp = h5in["rain_comp"][()]
        print("Loading background components done")

    def getBkgModels(self, filenames):
        print("Loading background models")
        for f in filenames:
            print(".. model:", f)
            with h5py.File(f, 'r') as h5in:
                keys = get_dataset_keys(h5in)
                if "V" not in keys:
                    print(".... is not a model file")
                    continue
                model = "BADLAD" if "nlp_values" in keys else "BADFM"
                label = "static" if h5in.attrs["n_components"] == 1 else "rain"
                print("....", model, label)
            if model == "BADLAD":
                self.badlad_models[label] = BADLAD.read_model(f)
            else:
                self.badfm_models[label] = BADMF.read_model(f)
        print("Loading background models done")
        
    def initFilters(self):
        print("Initializing filters")
        self.initRainProxyFilter()
        self.initRadFilters()
        print("Initializing filters done")

    def initRainProxyFilter(self):
        print("Initializing rain filter")
        self.rain_proxy_filter = RainProxyFilter()
        print("Initializing rain filter done")

    def updateRainProxyFilter(self, rain_ts, rain_val):
        self.rain_proxy_filter.update(rain_ts, rain_val)
        
    def initRadFilters(self):        
        print("Initializing rad filters")
        if self.slow_ewma_filter is None:
            print(".. slow EWMA filter")
            tscale_slow = 2 * 60 * 60.  # static filter - 2h time scale
            fpr_slow = chi2.sf(2**2, 1) # 2 sigma
            far_slow = fpr_slow * (3600. / self.int_time)
            self.slow_ewma_filter = KSigma(
                far=self.far,
                int_time=self.int_time,
                filter= EWMAFilter(lam=self.int_time/tscale_slow),
                filter_far=far_slow
            )
        if self.fast_ewma_filter is None:
            print(".. fast EWMA filter")
            tscale_fast = 30. # rain filter - 30s time scale
            fpr_fast = chi2.sf(3**2, 1) # 3 sigma
            far_fast = fpr_fast * (3600. / self.int_time)
            self.fast_ewma_filter = KSigma(
                far=self.far,
                int_time=self.int_time,
                filter= EWMAFilter(lam=self.int_time/tscale_fast),
                filter_far=far_fast
            )
        if self.scrad is None:
            print(".. NSCRAD filter")
            tscale_scrad = 2 * 60 * 60. # 2h time scale
            self.scrad = NSCRAD(
                far=self.far,
                int_time=self.int_time,
                lam=self.int_time/tscale_scrad, # defaults to lam=0.001
                bin_edges=self.bin_edges,
                nuisance_spectra=[],
                spectral_windows=[
                    [self.bin_edges[0], 64.],
                    [195., 1530.],
                    [83., 2511.]
                ]
            )
        if self.scrad_rn is None and self.rain_comp is not None:
            print(".. NSCRAD + nuisance filter")
            self.scrad_rn = copy.deepcopy(self.scrad)
            self.scrad_rn.nuisance_spectra = self.scrad_rn._ensure_array_of_spectra([self.rain_comp])
            self.scrad_rn._set_rebinning(self.scrad_rn.spectral_windows)
            self.scrad_rn.calculate_threshold()

        # indirect radon enhancement filter
        if self.badfm_radon_filter is None:
            tscale_slow = 2 * 60 * 60.  # slow filter - 2h time scale
            fpr_slow = chi2.sf(2**2, 1) # 2 sigma
            far_slow = fpr_slow * (3600. / self.int_time)
            self.badfm_radon_filter = KSigma(
                far=self.far,
                int_time=self.int_time,
                filter= EWMAFilter(lam=self.int_time/tscale_slow),
                filter_far=far_slow
            )
        # direct (rain) radon enhancement filter
        if self.triage_radon_filter is None:
            tscale_slow = 2 * 60 * 60.  # slow filter - 2h time scale
            fpr_slow = chi2.sf(2**2, 1) # 2 sigma
            far_slow = fpr_slow * (3600. / self.int_time)
            self.triage_radon_filter = KSigma(
                far=self.far,
                int_time=self.int_time,
                filter= EWMAFilter(lam=self.int_time/tscale_slow),
                filter_far=far_slow
            )
        print("Initializing rad filters done")

    def buildModels(self, files, staticOnly=False):
        print("Building models")
        # get data range to label model files
        self.range_label = (os.path.splitext(os.path.basename(files[0]))[0]).split("_")[1]
        if len(files) > 1:
            self.range_label += '_' + (os.path.splitext(os.path.basename(files[-1]))[0]).split("_")[1]
        print(".. file range:", self.range_label)
        for f in files:
            if self.getData(f):
                self.updateRainProxyFilter(self.rain_ts, self.rain_val)
                self.fillBuffers()
        if self.createModels(staticOnly) is True:
            self.trainModels()
        print("Building models done")

    def screenForAlarms(self, files):
        print("Screening for alarms")
        for f in files:
            if self.getData(f):
                self.updateRainProxyFilter(self.rain_ts, self.rain_val)
                self.screen(f)
        print("Screening for alarms done")

    def fillBuffers(self):
        print("Filling buffers")
        tf_idx = 0
        tl_idx = tf_idx + 1
        max_lt = self.int_time
        max_idx = self.ts.size
        while tl_idx < max_idx:
            tf = self.ts[tf_idx]
            tl = self.ts[tl_idx]
            lt = tl - tf
            while lt < max_lt and tl_idx < max_idx:
                tl = self.ts[tl_idx]
                lt = tl - tf
                tl_idx += 1
            if tl_idx == max_idx:
                break
            self.n_tot += 1
            spectrum = np.histogram(self.val[tf_idx:tl_idx], bins=self.bin_edges)[0]
            res = self.triage(spectrum, tf, tl)
            if res.static:
                self.n_static += 1
                self.static_spectrum += spectrum
                if self.n_static_buff < self.buffer_size:
                    self.static_buffer[self.n_static_buff] = spectrum
                    self.n_static_buff += 1
            elif res.rain:
                self.n_rain += 1
                self.rain_spectrum += spectrum
                if self.n_rain_buff < self.buffer_size:
                    self.rain_buffer[self.n_rain_buff] = spectrum
                    self.n_rain_buff += 1
            elif res.other:
                self.n_other += 1
                self.other_spectrum += spectrum
            else:
                self.n_none += 1
                self.none_spectrum += spectrum
            tf_idx = tl_idx        
        print("Filling buffers done")
        
    #def triage(self, spectrum, tf, tl, nuisance=False):
    def triage(self, spectrum, tf, tl):
        # anomaly from rain proxy filter
        anom_proxy = self.rain_proxy_filter.analyze(tf, tl) if self.rain_proxy_filter is not None else False
        # anomaly from EWMA filters
        counts = spectrum.sum()
        lt = tl - tf
        results = self.slow_ewma_filter.analyze(spectrum, tf, lt)
        slow_mean = self.slow_ewma_filter.filter.x_k[0]
        anom_slow = results[0].is_alarm
        anom_slow_up = anom_slow & (counts >= slow_mean)        
        anom_slow_down = anom_slow & (counts < slow_mean)
        
        results = self.fast_ewma_filter.analyze(spectrum, tf, lt)
        fast_mean = self.fast_ewma_filter.filter.x_k[0]
        anom_fast = results[0].is_alarm
        anom_fast_up = anom_fast & (counts >= fast_mean)
        anom_fast_down = anom_fast & (counts < fast_mean)
       
        anom_ewma = anom_slow_up and anom_fast_up
        # anomaly from NSCRAD filters
        results = self.scrad.analyze(spectrum, tf, lt)
        anom_scrad = results[0].is_alarm
        if self.scrad_rn is not None:
            results = self.scrad_rn.analyze(spectrum, tf, lt)
            anom_scrad_rn = results[0].is_alarm
        else:
            anom_scrad_rn = False    
        anom_radon = (anom_scrad or anom_slow_up) and not anom_fast_up
        # flag static vs rain vs other categories
        anom_static = not (anom_slow_up or anom_slow_down or
                           anom_fast_up or anom_fast_down or
                           anom_scrad or anom_scrad_rn)
        anom_rain = anom_proxy or anom_radon
        anom_other = (anom_ewma or anom_scrad or anom_scrad_rn)

        static, rain, other, none = False, False, False, False
        if anom_static:
            if not (anom_rain or anom_other):
                static = True    # S
            if anom_rain:
                if not anom_other:
                    rain = True  # SR
                else:
                    other = True # SRO, does not seem to happen
            else:
                if anom_other:
                    other = True # SO, does not seem to happen
        else:
            if anom_rain:
                if not anom_other:
                    rain = True  # R
                else:
                    other = True # RO
            else:
                if anom_other:
                    other = True # O
                else:
                    none = True  # N, likely false alarm from slow/fast filter condition, we can discard those
        return TriageResult(static=static, rain=rain, other=other, none=none)
        
    def createModels(self, staticOnly):
        print("Creating models")
        print(".. STATS:")
        print(".. Total number of processed spectra:", self.n_tot)
        print(f".. Number of static spectra: {self.n_static} ({100. * self.n_static / self.n_tot:0.2f}%)")
        print(f".. Number of rain spectra: {self.n_rain} ({100. * self.n_rain / self.n_tot:0.2f}%)")
        print(f".. Number of other spectra: {self.n_other} ({100. * self.n_other / self.n_tot:0.2f}%)")
        print(f".. Number of none spectra: {self.n_none} ({100. * self.n_none / self.n_tot:0.2f}%)")
        """
        # display integrated spectra
        fig, ax = plt.subplots(4, sharex=True)
        ax[0].step(self.bin_centers, self.static_spectrum, where='mid', label="static, n="+str(self.n_static))
        ax[0].set_ylabel("Counts")
        ax[0].set_yscale('log')
        ax[0].legend(loc="best")
        ax[1].step(self.bin_centers, self.rain_spectrum, where='mid', label="rain, n="+str(self.n_rain))
        ax[1].set_ylabel("Counts")
        ax[1].set_yscale('log')
        ax[1].legend(loc="best")          
        ax[2].step(self.bin_centers, self.other_spectrum, where='mid', label="other, n="+str(self.n_other))
        ax[2].set_ylabel("Counts")
        ax[2].set_yscale('log')
        ax[2].legend(loc="best")       
        ax[3].step(self.bin_centers, self.none_spectrum, where='mid', label="none, n="+str(self.n_none))
        ax[3].set_ylabel("Counts")
        ax[3].set_yscale('log')
        ax[3].set_xlabel("Energy [keV]")
        ax[3].legend(loc="best")
        """
        print(".. Preparing components")
        if self.n_static_buff < self.buffer_size:
            print(".. Not enough static stats to train model. Need to process more files.")
            print("Creating models done")
            return False
        else:
            static_comp = self.static_spectrum / self.n_static            
        if staticOnly is False:
            if self.n_rain_buff < self.buffer_size:
                print(".. Not enough rain stats to train model. Need to process more files.")
                print("Creating models done")
                return False
            else:
                rain_comp = self.rain_spectrum / self.n_rain
                np.clip(rain_comp - static_comp, 1.E-6, None, out=rain_comp) 
                rain_comp /= rain_comp.sum()
                np.clip(rain_comp, 1.E-6, None, out=rain_comp)
        static_comp /= static_comp.sum()
        np.clip(static_comp, 1.E-6, None, out=static_comp)
        filename = "bkg_components_" + self.range_label + ".h5"
        with h5py.File(filename, 'w') as h5out:
            h5out.create_dataset("static_comp", data=static_comp)
            if staticOnly is False:
                h5out.create_dataset("rain_comp", data=rain_comp)
        print(".. Preparing components done")
        """
        # display bkg components
        fig, ax = plt.subplots()
        ax.step(self.bin_centers, static_comp, where='mid', label="static")
        if staticOnly is False:
            ax.step(self.bin_centers, rain_comp, where='mid', label="rain")
        ax.set_ylabel("pdf")
        ax.set_yscale('log')
        ax.set_xlabel("Energy [keV]")
        ax.legend(loc="best")
        plt.show()
        """
        print(".. Loading source templates")
        templates = {}
        with h5py.File("radai_source_templates.h5", 'r') as h5in:
            for src, template in list(h5in['templates'].items()):
                templates[src] = template[()]
        print(".. Loading source templates done")
        labels = ["static"] if staticOnly is True else ["static", "rain"]
        for i, label in enumerate(labels):
            n_components = i + 1
            basis = np.zeros((n_components, self.nbins))
            basis[0] = static_comp
            if i: basis[1] = rain_comp
            print(".. Creating BADLAD model", label)
            self.badlad_models[label] = BADLAD(n_components=n_components,
                                               nmf='pnmf_nnls',
                                               n_steps_iter=100,
                                               num_bins=self.nbins,
                                               bincenters=self.bin_centers,
                                               binedges=self.bin_edges,
                                               mean_inttime=self.int_time,
                                               steptime=self.int_time,
                                               V=basis)
            self.badlad_models[label].far = self.far
            self.badlad_models[label].calculate_threshold(far=self.badlad_models[label].far)
            filename = "badlad_" + label + '_' + self.range_label + "_untrained.h5"
            self.badlad_models[label].write_model(filename)
            print(".. Creating BADFM model", label)
            self.badfm_models[label] = BADMF(n_components=n_components,
                                             nmf='pnmf_nnls',
                                             templates=templates,
                                             n_steps_iter=100,
                                             num_bins=self.nbins,
                                             bincenters=self.bin_centers,
                                             binedges=self.bin_edges,
                                             mean_inttime=self.int_time,
                                             steptime=self.int_time,
                                             V=basis)
            self.badfm_models[label].far = self.far
            self.badfm_models[label].calculate_threshold_analytically(far=self.badfm_models[label].far, inttime=self.badfm_models[label].steptime)
            filename = "badfm_" + label + '_' + self.range_label + "_untrained.h5"
            self.badfm_models[label].write_model(filename)
        print("Creating models done")
        # Saving training sets
        #print("Saving training set")
        #filename = "training_set_" + self.range_label + ".h5"
        #with h5py.File(filename, 'w') as h5out:
        #    h5out.create_dataset("static_buffer", data=self.static_buffer)
        #    h5out.create_dataset("rain_buffer", data=self.rain_buffer)
        #print("Saving training set done")
        return True

    def trainModels(self):
        print("Training models")
        for label, model in self.badlad_models.items():
            # prep training buffer
            if model.n_components == 1:
                static_buffer = self.static_buffer
                buffer = static_buffer
            else:
                size = self.buffer_size // 2
                rain_buffer = np.vstack((self.rain_buffer[:size], self.static_buffer[:size]))
                buffer = rain_buffer
            print(".. Training BADLAD model:", label)
            print(".. Fitting model")
            start = time.perf_counter()
            model.fit(training=buffer, n_iter=100, eps=1.E-5)
            stop = time.perf_counter()
            print(f".. Fitting model done in {stop - start:0.2f} seconds")
            print(".. Calculating empirical threshold")
            start = time.perf_counter()
            model.calculate_threshold(training=buffer, far=model.far, set_threshold=True, empirical=True)
            stop = time.perf_counter()
            print(f".. Calculating empirical threshold done in {stop - start:0.2f} seconds")
            # make sure the trained components are clipped to lower value
            for i in range(model.n_components):
                np.clip(model.basis[i], 1.E-6, None, out=model.basis[i])
            filename = "badlad_" + label + '_' + self.range_label + "_trained.h5"
            model.write_model(filename)
            print(".. Training BADLAD model done")

        for label, model in self.badfm_models.items():
            # prep training buffer
            if model.n_components == 1:
                buffer = static_buffer
            else:
                buffer = rain_buffer
            print(".. Training BADFM model:", label)
            print(".. Fitting model")
            start = time.perf_counter()
            model.fit(training=buffer, n_iter=100, eps=1.E-5)
            stop = time.perf_counter()
            print(f".. Fitting model done in {stop - start:0.2f} seconds")
            print(".. Calculating emprical threshold")
            start = time.perf_counter()
            model.calculate_threshold(buffer, far=model.far, set_threshold=True)
            stop = time.perf_counter()
            print(f".. Calculating empirical threshold done in {stop - start:0.2f} seconds")
            # make sure the trained components are clipped to lower value
            for i in range(model.n_components):
                np.clip(model.basis[i], 1.E-6, None, out=model.basis[i])
            filename = "badfm_" + label + '_' + self.range_label + "_trained.h5"
            model.write_model(filename)
            print(".. Training BADFM model done")  
        print("Training models done")

    def screen(self, filename):
        print("Screening file:", filename)
        start = time.perf_counter()
        alarms = defaultdict(list)
        tf_idx = 0
        tl_idx = tf_idx + 1
        max_lt = self.int_time
        max_idx = self.ts.size
        while tl_idx < max_idx:
            tf = self.ts[tf_idx]
            tl = self.ts[tl_idx]
            lt = tl - tf
            while lt < max_lt and tl_idx < max_idx:
                tl = self.ts[tl_idx]
                lt = tl - tf
                tl_idx += 1
            if tl_idx == max_idx:
                break
            spectrum = np.histogram(self.val[tf_idx:tl_idx], bins=self.bin_edges)[0]
            
            # run triage
            res = self.triage(spectrum, tf, tl)
            if res.static:
                triage_flag = "static"
            elif res.rain:
                triage_flag = "rain"
            elif res.other:
                triage_flag = "other"
            else:
                triage_flag = "none"
            
            # run anomaly detection
            # BADLAD
            model = self.badlad_models[self.model_label]
            spect_info = model.analyze(spectrum, index=[0])
            index = spect_info.index[0]
            badlad_alarm = True if spect_info.alarm[index] else False
            # BADFM
            model = self.badfm_models[self.model_label]
            spect_info = model.analyze(spectrum, index=np.array([0]))
            metric_max = 0
            badfm_id = ""
            for src_idx, src in enumerate(model.templates):
                index = spect_info.index[src_idx]
                metric = spect_info.alarm_metric[index]
                if spect_info.alarm[index]:
                    if metric > metric_max:
                        metric_max = metric
                        badfm_id = src
            badfm_alarm = False if not badfm_id else True
            if badlad_alarm or badfm_alarm:
                alarms["node"].append(self.node)
                alarms["lon"].append(self.lon)
                alarms["lat"].append(self.lat)
                alarms["timestamp"].append(tf)
                alarms["spectrum"].append(spectrum)
                alarms["triage_flag"].append(triage_flag)
                alarms["badlad_alarm"].append(badlad_alarm)
                alarms["badfm_alarm"].append(badfm_alarm)
                alarms["badfm_id"].append(badfm_id)
                alarms["bkg_model"].append(self.model_label)

            # Model switch
            # Indirect Radon enhancement filter + rain filter
            if (tf - self.radon_filter_start_ts) < self.radon_filter_inttime:
                self.badfm_radon_sum += badfm_alarm and (badfm_id == "Ra-226")
                self.triage_radon_sum += res.rain      
            else:
                results = self.badfm_radon_filter.analyze(self.badfm_radon_sum, tf, lt)
                mean = self.badfm_radon_filter.filter.x_k[0]
                anom_slow = results[0].is_alarm
                badfm_radon_anom_slow_up = anom_slow & (self.badfm_radon_sum >= mean)        
                badfm_rain_trigger = badfm_radon_anom_slow_up and self.badfm_radon_prev_anom_slow_up

                results = self.triage_radon_filter.analyze(self.triage_radon_sum, tf, lt)
                mean = self.triage_radon_filter.filter.x_k[0]
                anom_slow = results[0].is_alarm
                triage_radon_anom_slow_up = anom_slow & (self.triage_radon_sum >= mean)        
                triage_rain_trigger = triage_radon_anom_slow_up and self.triage_radon_prev_anom_slow_up
                triage_static_trigger = (not triage_radon_anom_slow_up) and self.triage_radon_prev_anom_slow_up
 
                self.radon_filter_start_ts = tf
                self.badfm_radon_prev_anom_slow_up = badfm_radon_anom_slow_up
                self.triage_radon_prev_anom_slow_up = triage_radon_anom_slow_up
                self.badfm_radon_sum = 0
                self.triage_radon_sum = 0
                
                if self.model_label == "static":
                    if badfm_rain_trigger or triage_rain_trigger:
                        self.model_label = "rain"
                else:
                    if triage_static_trigger:
                        self.model_label = "static"

            tf_idx = tl_idx

        stop = time.perf_counter()
        print(f"Screening done in {stop - start:0.2f} seconds")
    
        df = pd.DataFrame(data=alarms)
        print(len(df))
        print(df)
        filename = filename.replace(".h5", "_alarms.h5")
        df.to_hdf(filename, 'alarms', mode='w')
        print("Saved alarms to file:", filename)
