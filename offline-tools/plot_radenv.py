#!/usr/bin/env python3

# Example script to produce data timelines
import argparse
import h5py
from collections import defaultdict
import numpy as np
from scipy.optimize import curve_fit
import math
import matplotlib.pyplot as plt
from datetime import datetime

def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys


def peak_and_linear_bkg(x, A, mu, sigma, a, b):
    return (A / np.sqrt(2 * math.pi)) * np.exp(-0.5 * ((x - mu) / sigma)**2) + a * x + b


def sqrt_n_bins(nmin=0, nmax=999, nbins=128):
    """ Returns quadratically-spaced energy bins."""
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins+1)
    bin_edges = bin_edges**2
    bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2
    return bin_edges, bin_centers


# binning
nbins = 128
nmin, nmax = 50, 3000
bin_edges, bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=nbins)

# nominal dataset keys
env_keys_ = ["env/pressure_shield/ts", "env/pressure_shield/val",
             "env/pressure_enclosure/ts", "env/pressure_enclosure/val",
             "env/humidity_shield/ts", "env/humidity_shield/val",
             "env/humidity_enclosure/ts", "env/humidity_enclosure/val",
             "env/temp_shield/ts", "env/temp_shield/val",
             "env/temp_enclosure/ts", "env/temp_enclosure/val",
             "env/rain/ts", "env/rain/val"]
cal_keys_ = ["calib/ts", "calib/fmin", "calib/pval",
             "calib/gain", "calib/off", "calib/sat", "calib/plaw", "calib/res",
             "calib/w_K", "calib/w_U", "calib/w_Th", "calib/w_Rn",
             "calib/w_511", "calib/w_cosmics"]


#//////////////////////////////////////////////
print('Parsing data')
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--files', help='h5 filenames', nargs='+', type=str)
args = parser.parse_args()

env_datasets = defaultdict(list)
rad_datasets = defaultdict(list)
cal_datasets = defaultdict(list)

if not args.node:
    print("Need to specify the node VSN")
else:
    node = args.node.upper()
    env_keys = [node+'/'+key for key in env_keys_]
    cal_keys = [node+'/'+key for key in cal_keys_]
        
    for f in args.files:
        print('.. parsing file:', f)
        with h5py.File(f, 'r') as h5in:
            keys = get_dataset_keys(h5in)

            # env data
            for key in env_keys:
                if key not in keys:
                    print("Missing dataset:", key)
                else:
                    k = key.replace(node+'/', '')
                    env_datasets[k].append(h5in[key][()])
        
            # check that bag contains some rad data
            # Note: this should not be necessary anymore once we update the plugin
            # to restart when digibase fails
            if node+"/listmode/raw/ts_det" not in keys:
                print("Missing rad data")
                continue
            else:
                ts = h5in[node+"/listmode/raw/ts_det"][()]
                val = h5in[node+"/listmode/raw/channel"][()]
                rad_datasets["listmode/raw/ts_det"].append(ts)
                rad_datasets["listmode/raw/channel"].append(val)

            
            if node+"/listmode/calib/ts_det" not in keys:
                print("Missing calibration")
            else:
                ts = h5in[node+"/listmode/calib/ts_det"][()]
                val = h5in[node+"/listmode/calib/energy"][()]
                rad_datasets["listmode/calib/ts_det"].append(ts)
                rad_datasets["listmode/calib/energy"].append(val)
        
            # calibration params
            for key in cal_keys:
                if key not in keys:
                    print("Missing dataset:", key)
                else:
                    k = key.replace(node+'/', '')
                    cal_datasets[k].append(h5in[key][1:]) # first entry is sometimes replicated
print('.. Parsing data done')
 

#//////////////////////////////////////////////
print('Concatenating data')
data = {}
print(".. env data")
for key in env_datasets:
    data[key] = np.concatenate(env_datasets[key])
print(".. rad data")
for key in rad_datasets:
    data[key] = np.concatenate(rad_datasets[key])
print(".. cal data")
for key in cal_datasets:
    data[key] = np.concatenate(cal_datasets[key])
print('.. Concatenating data done')


#//////////////////////////////////////////////
if "env/rain/ts" in data:
    print('Computing rain proxy')
    rain_ts_centers = 0.5 * (data["env/rain/ts"][:-1] + data["env/rain/ts"][1:])
    rain_val = data["env/rain/val"][1:] - data["env/rain/val"][:-1]
    lam_B = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
    rain_proxy = np.zeros_like(rain_val)
    rain_proxy[0] = rain_val[0]
    for i in range(1, rain_val.size):
        rain_proxy[i] = rain_proxy[i - 1] * np.exp(-lam_B * (rain_ts_centers[i] - rain_ts_centers[i - 1])) # predict
        rain_proxy[i] += rain_val[i] # update
    print('.. Computing rain proxy done')


#//////////////////////////////////////////////
# timeline of gross count rate and K40 peak
if "listmode/calib/ts_det" in data:
    print("Processing calibrated data")
    cal_ts = data["listmode/calib/ts_det"]
    cal_val = data["listmode/calib/energy"]
    #K40_ts, K40_cps, K40_mean, K40_sigma, gross_counts = [], [], [], [], []
    cps, cps_ts = [], []
    #spectrum = np.zeros(nbins)
    #sigma_init, a_init, b_init = (1,1,1)
    #norm_init = 100
    #mean_init = 1460
    #bin_min = 1250
    #bin_max = 1600
    #peak_idx = np.where(np.logical_and(bin_centers>bin_min,bin_centers<bin_max))[0]
    tf_idx = 0
    tl_idx = tf_idx + 1
    #max_lt = 300
    max_lt = 1
    max_idx = cal_ts.size
    while not tl_idx == max_idx:
        # build 1s spectrum
        tf = cal_ts[tf_idx]
        tl = cal_ts[tl_idx]
        lt = tl - tf
        while lt < max_lt and tl_idx < max_idx:
            tl = cal_ts[tl_idx]
            lt = tl - tf
            tl_idx += 1
        if tl_idx == max_idx:
            break
        energies = cal_val[tf_idx:tl_idx]
        spectrum = np.histogram(energies, bins=bin_edges)[0]
        cps.append(spectrum.sum()/lt)
        cps_ts.append(tf)
        #spectrum += np.histogram(energies, bins=bin_edges)[0]
        #x = bin_centers[peak_idx]
        #y = spectrum[peak_idx]
        #popt, pcov = curve_fit(peak_and_linear_bkg, x, y, p0=[norm_init, mean_init, sigma_init, a_init, b_init])
        #K40_ts.append(tf)
        #K40_mean.append(popt[1])
        #K40_sigma.append(popt[2])
        #K40_cps.append(y.sum()/max_lt)
        #gross_counts.append(spectrum.sum()/max_lt)
        #norm_init = popt[0]
        #mean_init = int(popt[1])
        #sigma_init = popt[2]
        #a_init = popt[3]
        #b_init = popt[4]
        #spectrum = np.zeros(nbins)
        
        tf_idx = tl_idx
    print(".. Processing calibrated data done")


#//////////////////////////////////////////////
# Plots
print("Ploting timelines")

time_norm = 24 * 3600

if "env/rain/ts" in data:
    t_beg = data["env/rain/ts"][0]
    t_end = data["env/rain/ts"][-1]
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    rain_ts_centers = 0.5 * (data["env/rain/ts"][:-1] + data["env/rain/ts"][1:])
    rain_val = data["env/rain/val"][1:] - data["env/rain/val"][:-1]
    lam_B = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
    rain_proxy = np.zeros_like(rain_val)
    rain_proxy[0] = rain_val[0]
    for i in range(1, rain_val.size):
        rain_proxy[i] = rain_proxy[i - 1] * np.exp(-lam_B * (rain_ts_centers[i] - rain_ts_centers[i - 1])) 
        rain_proxy[i] += rain_val[i]               
    fig, ax = plt.subplots(2, sharex=True)
    ts = (rain_ts_centers - rain_ts_centers[0])/time_norm
    ax[0].plot(ts, rain_val)
    ax[0].set_ylabel("Precipitation [?]")
    ax[1].plot(ts, rain_proxy)
    ax[1].hlines(0.05, ts[0], ts[-1], color='black', linestyle='--')
    ax[1].set_xlabel("Time [day]")
    ax[1].set_ylabel("Proxy [?]")
    fig.suptitle(node+": "+beg+" - "+end)
    #plt.savefig(subdir+'/precipitation.png')
if "env/temp_shield/ts" in data:
    t_beg = min(data["env/temp_shield/ts"][0],
                data["env/pressure_shield/ts"][0],
                data["env/humidity_shield/ts"][0])
    t_end = max(data["env/temp_shield/ts"][-1],
                data["env/pressure_shield/ts"][-1],
                data["env/humidity_shield/ts"][-1])
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot((data["env/temp_shield/ts"] - t_beg)/time_norm, data["env/temp_shield/val"],
               label="T (shield)", color='orange', alpha=0.4)
    ax[0].set_ylabel("Temperature [C]")
    ax[0].legend(loc="best")
    ax[1].plot((data["env/pressure_shield/ts"] - t_beg)/time_norm, data["env/pressure_shield/val"],
               label="p (shield)", color='gray', alpha=0.4)
    ax[1].set_ylabel("Pressure [Pa]")
    ax[1].legend(loc="best")
    ax[2].plot((data["env/humidity_shield/ts"] - t_beg)/time_norm, data["env/humidity_shield/val"],
               label="H (shield)", color='blue', alpha=0.4)
    ax[2].set_ylabel("Humidity [%]")
    ax[2].legend(loc="best")
    ax[2].set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)
    #plt.savefig(subdir+'/env_shield.png')
if "env/temp_enclosure/ts" in data:
    t_beg = min(data["env/temp_enclosure/ts"][0],
                data["env/pressure_enclosure/ts"][0],
                data["env/humidity_enclosure/ts"][0])
    t_end = max(data["env/temp_enclosure/ts"][-1],
                data["env/pressure_enclosure/ts"][-1],
                data["env/humidity_enclosure/ts"][-1])
    beg = datetime.fromtimestamp(t_beg).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(t_end).strftime("%m/%d/%Y, %H:%M:%S")
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot((data["env/temp_enclosure/ts"] - t_beg)/time_norm, data["env/temp_enclosure/val"],
               label="T (enclosure)", color='orange', alpha=0.4)
    ax[0].set_ylabel("Temperature [C]")
    ax[0].legend(loc="best")
    ax[1].plot((data["env/pressure_enclosure/ts"] - t_beg)/time_norm, data["env/pressure_enclosure/val"],
               label="p (enclosure)", color='gray', alpha=0.4)
    ax[1].set_ylabel("Pressure [Pa]")
    ax[1].legend(loc="best")
    ax[2].plot((data["env/humidity_enclosure/ts"] - t_beg)/time_norm, data["env/humidity_enclosure/val"],
               label="H (enclosure)", color='blue', alpha=0.4)
    ax[2].set_ylabel("Humidity [%]")
    ax[2].legend(loc="best")
    ax[2].set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)
    #plt.savefig(subdir+'/env_enclosure.png')

if "listmode/raw/ts_det" in data:
    beg = datetime.fromtimestamp(data["listmode/raw/ts_det"][0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(data["listmode/raw/ts_det"][-1]).strftime("%m/%d/%Y, %H:%M:%S")
    nchannels = 1024
    bin_edges = np.arange(nchannels)
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    spectrum = np.histogram(data["listmode/raw/channel"], bins=bin_edges)[0]
    fig, ax = plt.subplots()
    ax.step(bin_centers, spectrum, where='mid', label="integrated raw spectrum")
    ax.set_xlabel("Channel")
    ax.set_ylabel("Entries")
    ax.set_yscale('log')
    ax.legend(loc="best")
    fig.suptitle(node+": "+beg+" - "+end)
if "listmode/calib/ts_det" in data:
    beg = datetime.fromtimestamp(data["listmode/calib/ts_det"][0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(data["listmode/calib/ts_det"][-1]).strftime("%m/%d/%Y, %H:%M:%S")
    nbins = 128
    nmin, nmax = 50, 3000
    bin_edges, bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=nbins)
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    spectrum = np.histogram(data["listmode/calib/energy"], bins=bin_edges)[0]
    fig, ax = plt.subplots()
    ax.step(bin_centers, spectrum, where='mid', label="integrated cal spectrum")
    ax.set_xlabel("Energy [MeV]")
    ax.set_ylabel("Entries")
    ax.set_yscale('log')
    ax.legend(loc="best")
    fig.suptitle(node+": "+beg+" - "+end)
    
    #fig, ax = plt.subplots(4, sharex=True)
    #K40_ts = (K40_ts - K40_ts[0])/time_norm
    #ax[0].plot(K40_ts, K40_mean)
    #ax[0].set_ylabel("Mean K40")
    #ax[1].plot(K40_ts, K40_sigma)
    #ax[1].set_ylabel("Std dev K40")
    #ax[2].plot(K40_ts, K40_cps)
    #ax[2].set_ylabel("Peak counts [cps]")
    #ax[3].plot(K40_ts, gross_counts)
    #ax[3].set_ylabel("Gross counts [cps]")
    fig, ax = plt.subplots()
    beg = datetime.fromtimestamp(cps_ts[0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(cps_ts[-1]).strftime("%m/%d/%Y, %H:%M:%S")
    cps_ts = (cps_ts - cps_ts[0])/time_norm
    ax.plot(cps_ts, cps)
    ax.set_ylabel("cps")
    ax.set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)

if "calib/ts" in data:
    beg = datetime.fromtimestamp(data["calib/ts"][0]).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(data["calib/ts"][-1]).strftime("%m/%d/%Y, %H:%M:%S")
    cal_ts = (data["calib/ts"] - data["calib/ts"][0])/time_norm
    fig, ax = plt.subplots(2, sharex=True)
    ax[0].plot(cal_ts, data["calib/fmin"])
    ax[0].set_ylabel("fmin")
    ax[1].plot(cal_ts, data["calib/pval"])
    ax[1].set_ylabel("p-value")
    ax[1].set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)
    fig, ax = plt.subplots(5, sharex=True)
    ax[0].plot(cal_ts, data["calib/res"])
    ax[0].set_ylabel("res")
    ax[1].plot(cal_ts, data["calib/plaw"])
    ax[1].set_ylabel("power")
    ax[2].plot(cal_ts, data["calib/gain"])
    ax[2].set_ylabel("gain")
    ax[3].plot(cal_ts, data["calib/sat"])
    ax[3].set_ylabel("sat")
    ax[4].plot(cal_ts, data["calib/off"])
    ax[4].set_ylabel("off")
    ax[4].set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)
    fig, ax = plt.subplots(6, sharex=True)
    ax[0].plot(cal_ts, data["calib/w_cosmics"])
    ax[0].set_ylabel("cosmics")
    ax[1].plot(cal_ts, data["calib/w_511"])
    ax[1].set_ylabel("511keV")
    ax[2].plot(cal_ts, data["calib/w_K"])
    ax[2].set_ylabel("K")
    ax[3].plot(cal_ts, data["calib/w_U"])
    ax[3].set_ylabel("U")
    ax[4].plot(cal_ts, data["calib/w_Th"])
    ax[4].set_ylabel("Th")
    ax[5].plot(cal_ts, data["calib/w_Rn"])
    ax[5].set_ylabel("Rn")
    ax[5].set_xlabel("Time [day]")
    fig.suptitle(node+": "+beg+" - "+end)
    
print(".. Ploting timelines done")


plt.show()
