#---------------------------------------
#
# Curation script:
# - dowloads 1 month worth of rad/env data
# - converts bags to BDC hdf5 format + sanity checks
# - performs BDC ingestion in collections of 80 files
#
# Usage:
# --node, node VSN
# --year, year to process
# --month, month to process
#
#---------------------------------------
import logging
import argparse
import os
from bdc import bdc_ingest

# seems needed to make sure the code exits, otherwise rospy will somehow consume resources and not allow a normal exit
import importlib
importlib.reload(logging)

# constants
BDC_DIR = "/clusterfs/minos/bdc/register_data/PANDA/Chicago_deployment"

# parse args in
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--year', type=str)
parser.add_argument('--month', type=str)
args = parser.parse_args()
node = args.node.upper()
year = args.year
month = args.month

#---------------------------------------
# setup logging
coll = year + '_' + month
logdir = os.path.join(os.getcwd(), "Log/" + node)
logdir = os.path.join(BDC_DIR, node + "/BDC/Log")
if not os.path.exists(logdir): os.makedirs(logdir)
logfile = os.path.join(logdir, coll + "_ingest.log")
LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s[%(levelname)s] %(funcName)s: %(message)s')
filelog_handler = logging.FileHandler(logfile, 'w')
filelog_handler.setLevel(logging.INFO)
filelog_handler.setFormatter(formatter)
LOG.addHandler(filelog_handler)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)
LOG.addHandler(console)
LOG.info("Logging file: {}".format(logfile))
LOG.info("Node {}, dataset: {}".format(node, year+'-'+month))

#---------------------------------------
# BDC ingestion
LOG.info("Ingesting collections")
bdc_ingest(node, coll)





