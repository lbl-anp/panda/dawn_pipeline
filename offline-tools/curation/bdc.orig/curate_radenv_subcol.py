#---------------------------------------
#
# Curation script:
# - dowloads 1 month worth of rad/env data
# - converts bags to BDC hdf5 format + sanity checks
# - performs BDC ingestion in collections of 80 files
#
# Usage:
# --node, node VSN
# --year, year to process
# --month, month to process
#
#---------------------------------------
import logging
import argparse
import subprocess
import os
import pathlib
import math
import numpy as np
from warnings import filterwarnings
filterwarnings(action='ignore', category=DeprecationWarning, message='`np.bool` is a deprecated alias for the builtin `bool`.')
import sage_data_client
from bag2bdch5 import Converter
from sanity import Sanitizer

# seems needed to make sure the code exits, otherwise rospy will somehow consume resources and not allow a normal exit
import importlib
importlib.reload(logging)

# constants
BDC_DIR = "/clusterfs/minos/bdc/register_data/PANDA/Chicago_deployment"

# parse args in
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--year', type=str)
parser.add_argument('--month', type=str)
parser.add_argument('--subcol', type=str)
parser.add_argument('--h5', action='store_true')
args = parser.parse_args()
node = args.node.upper()
year = args.year
month = args.month
subcol = args.subcol
useh5files = args.h5

#---------------------------------------
coll = year + '_' + month
subcoll = "PANDAWN_" + node + '_' + coll + '_' + subcol
subcoll_dir = os.path.join(BDC_DIR, node + '/BDC/' + subcoll)
os.chmod(subcoll_dir, 0o775)
os.chown(subcoll_dir, uid=-1, gid=800)
print("Converting files to BDC hdf5 format...")
converter = Converter(node=node)
if not useh5files:
    h5files = []
    for filepath in pathlib.Path(subcoll_dir).glob('**/*.bag'):
        f = converter.to_bdch5(str(filepath.absolute()))
        if f is not None: h5files.append(f)
else:
    h5files = pathlib.Path(subcoll_dir).glob('**/*.h5')
print("Running sanity checks...")
for f in h5files:
    print("running on file:", f)
    sanitizer = Sanitizer(f, node=node)
    #sanitizer.check_calibration()
    sanitizer.check_rad_timestamps()
    sanitizer.check_calibration()
    sanitizer.set_timerange()
    sanitizer.close()
print("Processed collection: {}".format(subcoll_dir))
