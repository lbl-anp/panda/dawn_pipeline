#
# Sanity checks on rad data
# Known issues:
# - rad data may be missing
# - calibration may be missing
# - calibration may be stuck at same param values over time
# - first msg of calib params and calib listmode msg are replicated
#   from the first bag in the current series of bags
#
# Checks
# - bag contains dbaserh listmode data --> if not skip
# - bag contains calibration data --> if not, calibrate
# - calib fit is not stuck on same param values --> if not, re-calibrate
# - no replicated calibration data --> if not, remove replicated data
#
# Output
# - if all OK with rad data, simply rename file with extension _check.bag
# - if filtering of calibrated rad data required, filter to _check.bag file and remove original bag
# - if calibration is required, calibrate, copy/write to _check.bag file and remove original bag
# - if no dbaserh data or calibration fails repeatedly, skip file
#

# ROS
from rosbag import Bag
# generic
import argparse
import time
import numpy as np
from collections import namedtuple
import os
# PANDA
from utils import bag_info, sqrt_n_bins
from utils import RadCalibMsgReader, RadMsgReader, RadDataBinModeAggregator, RadDataBinModeRollingAggregator
from panda_cal.calibrator import TemplatesCalibrator, CalibModel, CalibProcessor
from panda_cal.templates import Templates
from raddata_types import RadData
# PANDA ROS
from ros_msgs._to_RadData import to_RadData_ROS_msg
from to_raddata_types import to_RadData

CalibFitParams = namedtuple("CalibFitParams", "thr_lower thr_upper fmin pval \
                                               res plaw gain sat off \
                                               w_cosmics w_511 w_K w_U w_Th w_Rn alarm")
class Calibration:
    def __init__(self, bin_edges=None, initialize=True):
        # initialize with fixed params
        self.calibImpl = TemplatesCalibrator(
            Templates.from_csv_path("panda_cal/templates/new_air_2000mm_alu_1mm", 
                                    np.arange(0, 3600, 4),
                                    exclude=["P", "B"]))
        self.calibProc = CalibProcessor(self.calibImpl, cutoff=[380, 2800], opt_type="lightyield")
        self.calibFcn = CalibModel(model="lightyield")
        self.fmin_thr = {'lower': 0.6, 'upper': 1.4}
        self.bin_edges = bin_edges
        self.initialize = initialize
        self.prev_failed = False
        self.count_failed, self.max_count_failed = 0, 10
        self.calibFitParams = None

    def fit(self, spectrum, initialize=True):
        try:
            if initialize:
                self.calibProc.find_initial_guess(spectrum, self.bin_edges, verbose=True,
                                                  fix_offset=1.8, mlem_iter=2000)
            self.calibProc.weights = None
            params, weights, fmin, pval = self.calibProc.calibrate_spectrum(spectrum, self.bin_edges,
                                                                            mlem_iter=500, return_pvalue=True)
        except:
            print("*** exception raised in calibrator")
            if self.prev_failed is True:
                self.count_failed += 1
            else:
                self.prev_failed = True
                self.count_failed = 1
            return False

        alarm = fmin < self.fmin_thr['lower'] or fmin > self.fmin_thr['upper']
        if alarm:
            print("*** calibration failed (|fmin|>thr)")
            if self.prev_failed is True:
                self.count_failed += 1
            else:
                self.prev_failed = True
                self.count_failed = 1
            return False
        else:
            self.prev_failed = False
            self.count_failed = 0
            self.calibFitParams = CalibFitParams(thr_lower=self.fmin_thr['lower'], thr_upper=self.fmin_thr['upper'],
                                                 fmin=fmin, pval=pval,
                                                 res=params[0], plaw=params[2],
                                                 gain=params[3], sat=params[4], off=params[5],
                                                 w_cosmics=weights[0], w_511=weights[1],
                                                 w_K=weights[2], w_U=weights[5], w_Th=weights[4],
                                                 w_Rn=weights[3], alarm=alarm)
            return True
        
    def has_failed_too_many_times(self):
        return self.count_failed > self.max_count_failed

    def fit_params(self):
        return self.calibFitParams
   
    def apply(self):
        pass    
        
parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()

for file in args.files:
    start = time.time()
    with Bag(file, 'r') as bag:
        print(f"opening file: {file}")
        info = bag_info(bag)
        print(f"begin: {info.beg}, end: {info.end}, dt: {info.duration}h")
        #
        # check dbaserh data
        #
        topics = bag.get_type_and_topic_info()[1].keys()
        if "/dbaserh/listmode" not in topics:
            print(f"WARNING: *** no dbaserh data, skipping file {file}")
            continue
        #
        # check if re-calibration or filtering of calibration data is required
        #
        calibration_required, calibration_failure = False, False
        filtering_required = False
        if "/calib/params" not in topics:
            calibration_required = True
            print("WARNING: calibration required, missing calibration")
        else:
            # check if calibration repeats
            # limit to max of 10 consecutive repetitions, corresponding to 10min
            reader = RadCalibMsgReader()
            ts, gain = [], []
            prev_repeat = False
            count_repeat, max_repeat = 0, 10
            msgs = bag.read_messages(topics="/calib/params")
            msg = next(msgs)
            fields = reader.read(msg)
            ts.append(fields.ts)
            gain.append(fields.gain)
            for msg in msgs:
                fields = reader.read(msg)
                if fields.gain == gain[-1]:
                    if prev_repeat: count_repeat += 1
                    else:
                        count_repeat = 1
                        prev_repeat = True
                else: prev_repeat = False
                ts.append(fields.ts)
                gain.append(fields.gain)
            if count_repeat > max_repeat:
                calibration_required = True
                print(f"WARNING: calibration required, {count_repeat} repetitions > max repetitions")
            else:
                # check if first timestamp of calibration is repeated from first bag in series
                # in which case the time difference with the next msg will be larger than the expected 60s interval
                if ts[1] - ts[0] > 65.:
                    filtering_required = True
                    print("WARNING: first calibration entry is repeated")
                    # find repeated msg in calib listmode data
                    reader = RadMsgReader()
                    tl = []
                    replicated_idx = None
                    msgs = bag.read_messages(topics="/calib/listmode")
                    prev_msg = next(msgs)
                    fields = reader.read(prev_msg)
                    tl.append(fields.tl)
                    for i, msg in enumerate(msgs):
                        fields = reader.read(msg)
                        if (fields.tf - tl[-1]) > 1.:
                            print(f"WARNING: repeated msg in calib listmode")
                            replicated_idx = i
                            break
                        tl.append(fields.tl)
        #
        # re-calibrate if required
        #
        if calibration_required:
            print("running calibration")           
            inttime, steptime = 240, 60
            nbins = 1024
            bin_edges = np.linspace(0.0, nbins, nbins+1)
            radDataAgg = RadDataBinModeRollingAggregator(int_time=inttime, step_time=steptime,
                                                         nbins=nbins, bin_edges=bin_edges, calibrated=False)
            calibration = Calibration(bin_edges=bin_edges, initialize=True)
            calib_params = []
            radMsgReader = RadMsgReader(calibrated=False)
            msgs = bag.read_messages(topics="/dbaserh/listmode")
            for msg in msgs:
                fields = radMsgReader.read(msg)
                if fields is None: continue
                if radDataAgg.aggregate(fields.tf, fields.tl, fields.channels):
                    data = radDataAgg.data()
                    if calibration.fit(data.spectrum, initialize=calibration.initialize):
                        calibration.initialize = False
                        params = calibration.fit_params()
                        calib_params.append(RadData())
                        calib_params[-1].add_scalars(ts=data.ts, tf=data.ts, lt=data.lt,
                                                     thr_lower=params.thr_lower, thr_upper=params.thr_upper,
                                                     fmin=params.fmin, pval=params.pval,
                                                     res=params.res, plaw=params.plaw,
                                                     gain=params.gain, sat=params.sat, off=params.off,
                                                     w_cosmics=params.w_cosmics,
                                                     w_511=params.w_511,
                                                     w_K=params.w_K,
                                                     w_U=params.w_U,
                                                     w_Th=params.w_Th,
                                                     w_Rn=params.w_Rn,
                                                     alarm=params.alarm)
                    else:
                        calibration.initialize = True
                        if calibration.has_failed_too_many_times():
                            calibration_failure = True
                            break
        if calibration_failure is True:
            print(f"ERROR: too many failures, rad data cannot be calibrated, skipping file {file}")

        #
        # move/write to new bag
        #
        new_file = file.replace(".bag", "_check.bag")
        if (not calibration_required and not filtering_required):
            print("rad data passed all checks, moving file to _check.bag extension")
            os.rename(file, new_file)
        else:
            print(f"creating new bag {new_file}")
            rad_topics = ["/calib/params", "/calib/listmode", "/dbaserh/listmode"]
            topics = [topic for topic in topics if topic not in rad_topics]               
            with Bag(new_file, 'w') as outbag:
                # copy original messages from all but radiation topics
                print("copying original non-radiation data to new bag")
                for topic in topics:
                    msgs = bag.read_messages(topics=topic)
                    for msg in msgs:
                        outbag.write(msg.topic, msg.message, msg.message.header.stamp)
                if filtering_required:
                    print("filtering out replicated calibration data")
                    # copy dbaserh listmode topic
                    msgs = bag.read_messages(topics="/dbaserh/listmode")
                    for msg in msgs:
                        outbag.write(msg.topic, msg.message, msg.message.header.stamp)
                    # filter calib params and calib listmode topics
                    msgs = bag.read_messages(topics="/calib/params")
                    next(msgs)
                    for msg in msgs:
                        outbag.write(msg.topic, msg.message, msg.message.header.stamp)
                    msgs = bag.read_messages(topics="/calib/listmode")
                    for i, msg in enumerate(msgs):
                        if i == replicated_idx: continue
                        else:
                            outbag.write(msg.topic, msg.message, msg.message.header.stamp)
                if calibration_required:
                    print("applying new calibration")
                    # read dbaserh listmode topic, apply calibration, and write radiation topics to new bag
                    # calibration is applied up to the end of following integration range
                    calib_idx = 0
                    max_calib_ts = calib_params[calib_idx].scalars["tf"] + inttime + steptime
                    msgs = bag.read_messages(topics="/dbaserh/listmode")
                    msg = next(msgs, None)
                    while msg is not None:
                        dbaserh_lm = to_RadData(msg.message)
                        if not dbaserh_lm.arrays.data_length:
                            msg = next(msgs, None)
                            continue
                        tf = dbaserh_lm.arrays["timestamp_det"][0]
                        if tf > max_calib_ts:
                            # write calib params msg
                            calib_params_msg = to_RadData_ROS_msg(calib_params[calib_idx])
                            outbag.write("/calib/params", calib_params_msg, calib_params_msg.header.stamp)
                            # update calibration
                            # if we reach the last calibration set max_calib_ts to infinity to make sure
                            # we apply the calibration to all msg passed the following integration range
                            calib_idx += 1
                            if calib_idx == (len(calib_params) - 1):
                                max_calib_ts = np.inf
                            else: max_calib_ts = calib_params[calib_idx].scalars["tf"] + inttime + steptime
                        # apply calibration
                        channels = np.asarray(dbaserh_lm.arrays["channel"])
                        rand_channels = np.random.rand(len(channels)) + channels
                        calib_channels = calibration.calibFcn(rand_channels,
                                                              gain=calib_params[calib_idx].scalars["gain"],
                                                              saturation=calib_params[calib_idx].scalars["sat"],
                                                              offset=calib_params[calib_idx].scalars["off"])
                        # write calib listmode msg to bag
                        calib_lm = RadData()
                        calib_lm.add_arrays(det_id=dbaserh_lm.arrays["det_id"],
                                            timestamp_det=dbaserh_lm.arrays["timestamp_det"],
                                            channel=np.array(calib_channels).astype(np.float32))
                        calib_lm_msg = to_RadData_ROS_msg(calib_lm)
                        outbag.write("/calib/listmode", calib_lm_msg, calib_lm_msg.header.stamp)
                        # write dbaserh listmode msg to bag
                        outbag.write(msg.topic, msg.message, msg.message.header.stamp)
                        msg = next(msgs, None)

            # remove original bag
            print("replacing original bag with _check.bag")
            os.remove(file)
    stop = time.time()
    print(f"file processed in {(stop - start)}s")
