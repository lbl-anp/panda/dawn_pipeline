#---------------------------------------
#
# Curation script:
# - dowloads 1 month worth of rad/env data
# - converts bags to BDC hdf5 format + sanity checks
# - performs BDC ingestion in collections of 80 files
#
# Usage:
# --node, node VSN
# --year, year to process
# --month, month to process
#
#---------------------------------------
import logging
import argparse
import subprocess
import os
import pathlib
import math
import numpy as np
from warnings import filterwarnings
filterwarnings(action='ignore', category=DeprecationWarning, message='`np.bool` is a deprecated alias for the builtin `bool`.')
import sage_data_client
from bag2bdch5 import Converter
from sanity import Sanitizer

# seems needed to make sure the code exits, otherwise rospy will somehow consume resources and not allow a normal exit
import importlib
importlib.reload(logging)

# constants
N_FILES_PER_COLL = 80
BDC_DIR = "/clusterfs/minos/bdc/register_data/PANDA/Chicago_deployment"

# parse args in
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--year', type=str)
parser.add_argument('--month', type=str)
args = parser.parse_args()
node = args.node.upper()
year = args.year
month = args.month

#---------------------------------------
# setup logging
coll = year + '_' + month
logdir = os.path.join(os.getcwd(), "Log/" + node)
logdir = os.path.join(BDC_DIR, node + "/BDC/Log")
if not os.path.exists(logdir): os.makedirs(logdir)
logfile = os.path.join(logdir, coll + "_curate.log")
#logging.basicConfig(format='%(asctime)s[%(levelname)s] %(funcName)s: %(message)s',
#                    datefmt='%d/%m/%Y %I:%M:%S %p',
#                    level=logging.INFO)
#LOG = logging.getLogger('feature_engineering')
LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s[%(levelname)s] %(funcName)s: %(message)s')
filelog_handler = logging.FileHandler(logfile, 'w')
filelog_handler.setLevel(logging.INFO)
filelog_handler.setFormatter(formatter)
LOG.addHandler(filelog_handler)
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
console.setFormatter(formatter)
LOG.addHandler(console)
LOG.info("Logging file: {}".format(logfile))
LOG.info("Node {}, dataset: {}".format(node, year+'-'+month))

#---------------------------------------
# query URLs
start = year + '-' + month + "-01"
if month[0] == '0':
    if month[1] == '9': month = "10"
    else: month = '0' + str(int(month[1])+1)
else:
    if month[1] == '2': 
        month = "01"
        year = str(int(year)+1)
    else:
        month = '1' + str(int(month[1])+1)
end = year + '-' + month + "-01"
LOG.info("Querying data range: {} - {}".format(start, end))
task = "panda-rosbag-radenv"
if year == "2022" or (year == "2023" and month == "01"):
    task = "wes-panda-radenv-rosbag"
df = sage_data_client.query(
    start=start+" 00:00:00.000000000+00:0",
    end=end+" 00:00:00.000000000+00:0",
    filter={
        "name": "upload",
        "vsn": node,
        "task": task,
    },
)
LOG.info("Query returned {} urls".format(df.value.size))

#---------------------------------------
# loop over subcollections
ncoll = math.ceil(df.value.size / N_FILES_PER_COLL)
LOG.info("Splitting query in {} collections".format(ncoll))
ranges = list(range(0, df.value.size, N_FILES_PER_COLL))
ranges.append(df.value.size)
for i in range(ncoll):
    # create coll dir 
    subcoll = "PANDAWN_" + node + '_' + coll + '_' + str(i+1)
    colldir = os.path.join(BDC_DIR, node + '/BDC/' + subcoll)
    if not os.path.exists(colldir): 
        os.mkdir(colldir)
        os.chmod(colldir, 0o775)
        os.chown(colldir, uid=-1, gid=800)
    LOG.info("Processing collection: {}".format(colldir))
    range_f = ranges[i]
    range_l = ranges[i+1]
    LOG.info("From file: {}".format(df.value[range_f]))
    LOG.info("To file: {}".format(df.value[range_l-1]))
    # create csv file for coll urls
    df.value[range_f:range_l].to_csv("urls_"+str(i+1)+".txt", index=False, header=False)
    # download files to coll dir
    LOG.info("Downloading files...")
    subprocess.check_call(["wget",
                           "--user=***", "--password=***",
                           "--directory-prefix="+colldir, "-nd",
                           "-r", "-N", "-i",
                           "urls_"+str(i+1)+".txt"])
    # convert files to BDC hdf5 format
    LOG.info("Converting files to BDC hdf5 format...")
    converter = Converter(node=node)
    h5files = []
    for filepath in pathlib.Path(colldir).glob('**/*.bag'):
        f = converter.to_bdch5(str(filepath.absolute()))
        if f is not None: h5files.append(f)
    # perform sanity checks
    LOG.info("Running sanity checks...")
    for f in h5files:
        sanitizer = Sanitizer(f, node=node)
        #sanitizer.check_calibration()
        sanitizer.check_rad_timestamps()
        sanitizer.check_calibration()
        sanitizer.set_timerange()
        sanitizer.close()
    LOG.info("Processed collection: {}".format(colldir))

