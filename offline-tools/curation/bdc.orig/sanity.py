import math
import numpy as np
import h5py
import os
from datetime import datetime
import logging
from calib import Calibrator


def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys

# nominal rad/env dataset keys
env_keys_ = ["env/pressure_shield/ts", "env/pressure_shield/val",
            "env/pressure_enclosure/ts", "env/pressure_enclosure/val",
            "env/humidity_shield/ts", "env/humidity_shield/val",
            "env/humidity_enclosure/ts", "env/humidity_enclosure/val",
            "env/temp_shield/ts", "env/temp_shield/val",
            "env/temp_enclosure/ts", "env/temp_enclosure/val",
            "env/rain/ts", "env/rain/val"]
cal_keys_ = ["calib/ts", "calib/fmin", "calib/pval",
            "calib/gain", "calib/off", "calib/sat", "calib/plaw", "calib/res",
            "calib/w_K", "calib/w_U", "calib/w_Th", "calib/w_Rn",
            "calib/w_511", "calib/w_cosmics"]

class Sanitizer:
    def __init__(self, h5filename, node=""):
        self.h5in = h5py.File(h5filename, 'r+')
        self.node = node
        logging.info("Checking file {}".format(h5filename))

    def check_calibration(self):
        keys = get_dataset_keys(self.h5in)
        if (self.node+"/listmode/calib/ts_det" not in keys) or (self.node+"/calib/gain" not in keys):
            # missing calibrated data
            logging.warning("*** Missing calibrated data")
            logging.info("Running calibration")
            ts = self.h5in[self.node+"/listmode/raw/ts_det"][()]
            raw_val = self.h5in[self.node+"/listmode/raw/channel"][()]
            Cal = Calibrator(ts, raw_val)
            cal_val, cal_fit_params = Cal.run_calibration()
            for param in cal_fit_params:
                if self.node+param in keys: del self.h5in[self.node+param]
                self.h5in.create_dataset(self.node+param, data=cal_fit_params[param])
                time_attr = 'self' if param == "/calib/ts" else "/"+self.node+"/calib/ts" 
                unit_attr = 'unix time' if param == "/calib/ts" else "N/A"
                self.h5in[self.node+param].attrs['timeSyncLocation'] = time_attr
                self.h5in[self.node+param].attrs['unit'] = unit_attr
            if self.node+"/listmode/calib/ts_det" in keys: del self.h5in[self.node+"/listmode/calib/ts_det"]
            self.h5in.create_dataset(self.node+"/listmode/calib/ts_det",data=ts)
            self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation'] = 'self'
            self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit'] = 'unix time'
            if self.node+"/listmode/calib/energy" in keys: del self.h5in[self.node+"/listmode/calib/energy"]
            self.h5in.create_dataset(self.node+"/listmode/calib/energy",data=cal_val)
            self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation'] = "/"+self.node+"/listmode/calib/ts_det"
            self.h5in[self.node+"/listmode/calib/energy"].attrs['unit'] = 'keV'
        else:
            # check if calibration params were updated properly (i.e. no repeated sequence of values)
            gain_diff = np.diff(self.h5in[self.node+"/calib/gain"][()])
            if (gain_diff == 0).sum() > gain_diff.size // 4:
                logging.warning("*** Calibrated data was not updated properly")
                logging.info("Running calibration")
                ts = self.h5in[self.node+"/listmode/raw/ts_det"][()]
                raw_val = self.h5in[self.node+"/listmode/raw/channel"][()]
                Cal = Calibrator(ts, raw_val)
                cal_val, cal_fit_params = Cal.run_calibration()
                if cal_val is None:
                    logging.warning("*** Calibration failed over N trials ***")
                    return
                for param in cal_fit_params: 
                    timesync_attr = self.h5in[self.node+param].attrs['timeSyncLocation']
                    unit_attr = self.h5in[self.node+param].attrs['unit']
                    del self.h5in[self.node+param]
                    self.h5in.create_dataset(self.node+param,data=cal_fit_params[param])
                    self.h5in[self.node+param].attrs['timeSyncLocation'] = timesync_attr
                    self.h5in[self.node+param].attrs['unit'] = unit_attr
                timesync_attr = self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation']
                unit_attr = self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit']
                del self.h5in[self.node+"/listmode/calib/ts_det"]
                self.h5in.create_dataset(self.node+"/listmode/calib/ts_det",data=ts)
                self.h5in[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation'] = timesync_attr
                self.h5in[self.node+"/listmode/calib/ts_det"].attrs['unit'] = unit_attr
                timesync_attr = self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation']
                unit_attr = self.h5in[self.node+"/listmode/calib/energy"].attrs['unit']
                del self.h5in[self.node+"/listmode/calib/energy"]
                self.h5in.create_dataset(self.node+"/listmode/calib/energy",data=cal_val)
                self.h5in[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation'] = timesync_attr
                self.h5in[self.node+"/listmode/calib/energy"].attrs['unit'] = unit_attr

    def check_rad_timestamps(self):
        keys = get_dataset_keys(self.h5in)
        ts_keys = {"/calib/ts": ["/calib/fmin", "/calib/pval", "/calib/gain", "/calib/off",
                                 "/calib/sat", "/calib/plaw", "/calib/res",
                                 "/calib/w_K", "/calib/w_U", "/calib/w_Th", "/calib/w_Rn",
                                 "/calib/w_511", "/calib/w_cosmics"],
                   "/listmode/raw/ts_det": ["/listmode/raw/channel"],
                   "/listmode/calib/ts_det": ["/listmode/calib/energy"]}
        ts_deltas = {"/calib/ts": 65.,             # successive calib fits should be 60s apart
                     "/listmode/raw/ts_det": 1.,   # successive data should < 1s apart
                     "/listmode/calib/ts_det": 1.}
        for ts_label, val_labels in ts_keys.items():
            if self.node+ts_label not in keys:
                logging.warning("*** Missing dataset {} ***".format(ts_label))
            else:
                ts = self.h5in[self.node+ts_label][()]
                neg_diff_mask = np.where((ts[1:] - ts[:-1]) < 0)[0]
                pos_diff_mask = np.where((ts[1:] - ts[:-1]) > ts_deltas[ts_label])[0] 
                from_idx, to_idx = None, None
                if neg_diff_mask.size != 0:
                    from_idx = neg_diff_mask[0] + 1
                    if pos_diff_mask.size == 0:
                        to_idx = ts.size
                if pos_diff_mask.size != 0:
                    to_idx = pos_diff_mask[0] + 1
                    if neg_diff_mask.size == 0:
                        from_idx = 0
                if (from_idx is not None) and (to_idx is not None):
                    logging.info("Replacing original dataset {}".format(ts_label))
                    ts = np.delete(ts, np.s_[from_idx:to_idx])
                    timesync_attr = self.h5in[self.node+ts_label].attrs['timeSyncLocation']
                    unit_attr = self.h5in[self.node+ts_label].attrs['unit']
                    del self.h5in[self.node+ts_label]
                    self.h5in.create_dataset(self.node+ts_label,data=ts)
                    self.h5in[self.node+ts_label].attrs['timeSyncLocation'] = timesync_attr
                    self.h5in[self.node+ts_label].attrs['unit'] = unit_attr
                    for val_label in val_labels:
                        val = self.h5in[self.node+val_label][()]
                        val = np.delete(val, np.s_[from_idx:to_idx])
                        timesync_attr = self.h5in[self.node+val_label].attrs['timeSyncLocation']
                        unit_attr = self.h5in[self.node+val_label].attrs['unit']
                        del self.h5in[self.node+val_label]
                        self.h5in.create_dataset(self.node+val_label,data=val)
                        self.h5in[self.node+val_label].attrs['timeSyncLocation'] = timesync_attr
                        self.h5in[self.node+val_label].attrs['unit'] = unit_attr
                    
    def set_timerange(self):
        keys = get_dataset_keys(self.h5in)
        ts_keys = ["/env/pressure_shield/ts", "/env/pressure_enclosure/ts",
                   "/env/humidity_shield/ts", "/env/humidity_enclosure/ts",
                   "/env/temp_shield/ts", "/env/temp_enclosure/ts",
                   "/env/rain/ts", "/calib/ts",
                   "/listmode/raw/ts_det", "/listmode/calib/ts_det"]
        timestamps_f, timestamps_l = [], []
        for key in ts_keys:
            if self.node+key in keys: 
                timestamps_f.append(self.h5in[self.node+key][0])
                timestamps_l.append(self.h5in[self.node+key][-1])
        ts_f = min(timestamps_f)
        ts_l = max(timestamps_l)
        if self.node+"/gis/ts_f" in keys:
            del self.h5in[self.node+"/gis/ts_f"]
            del self.h5in[self.node+"/gis/ts_l"]
        self.h5in.create_dataset(self.node+"/gis/ts_f", data=ts_f)
        self.h5in[self.node+"/gis/ts_f"].attrs['unit'] = 'unix time'
        self.h5in.create_dataset(self.node+"/gis/ts_l", data=ts_l)
        self.h5in[self.node+"/gis/ts_l"].attrs['unit'] = 'unix time'
        logging.info("Setting timestamps tf: {}, tl: {} ({}h)".format(ts_f, ts_l, (ts_l - ts_f)/3600))

    def close(self):
        self.h5in.close()                    
