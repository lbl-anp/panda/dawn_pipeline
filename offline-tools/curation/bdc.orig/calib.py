import h5py
import numpy as np
from collections import defaultdict, namedtuple
from panda_cal.calibrator import (
    TemplatesCalibrator,
    CalibModel,
    CalibProcessor,
)
from panda_cal.templates import Templates
import logging

def get_dataset_keys(f):
    keys = []
    f.visit(lambda key : keys.append(key) if isinstance(f[key], h5py.Dataset) else None)
    return keys

CalibParams = namedtuple("CalibParams", "gain sat off")

class Calibrator:
    def __init__(self, ts, raw_val):
        logging.info("Initializing calibrator")
        self.inttime = 240
        self.overlap = 180
        self.cutoff_init = [380, 2800] 
        self.calibratorImpl = TemplatesCalibrator(
            Templates.from_csv_path("panda_cal/templates/new_air_2000mm_alu_1mm", 
                                    np.arange(0, 3600, 4),
                                    exclude=["P", "B"])
        )
        self.processor = CalibProcessor(self.calibratorImpl, cutoff=self.cutoff_init, opt_type="lightyield")
        self.calibFcn = CalibModel(model="lightyield")
        self.fmin_range = [0.6, 1.4]
        self.calibParams = None
        self.init_calib = True
        self.ts = ts
        self.raw_val = raw_val
        self.cal_val = np.zeros_like(self.raw_val)
        self.cal_fit_params = defaultdict(list)

    def calibrate(self, tmin_idx, tmax_idx, init=False):
        channels = self.raw_val[tmin_idx:tmax_idx]
        rand_channels = np.random.rand(len(channels)) + channels
        hist_values, hist_edges = np.histogram(rand_channels, np.linspace(0.0, 1024.0, 1025))
        if init is True:
            self.processor.find_initial_guess(
                hist_values, hist_edges, verbose=True,
                fix_offset=1.8, mlem_iter=2000)
        self.processor.weights = None
        params, weights, fmin, pval = self.processor.calibrate_spectrum(
            hist_values, hist_edges,
            mlem_iter=500,  
            return_pvalue=True)

        res = params[0]
        plaw = params[2]
        gain = params[3]
        sat = params[4]
        off = params[5]
        w_cosmics = weights[0] 
        w_511 = weights[1] 
        w_K = weights[2]
        w_Rn = weights[3]
        w_Th = weights[4]
        w_U = weights[5]
        del params
        del weights
        self.cal_fit_params["/calib/ts"].append(self.ts[tmin_idx])
        self.cal_fit_params["/calib/fmin"].append(fmin)
        self.cal_fit_params["/calib/pval"].append(pval)
        self.cal_fit_params["/calib/res"].append(res)
        self.cal_fit_params["/calib/plaw"].append(plaw)
        self.cal_fit_params["/calib/gain"].append(gain)
        self.cal_fit_params["/calib/sat"].append(sat)
        self.cal_fit_params["/calib/off"].append(off)
        self.cal_fit_params["/calib/w_cosmics"].append(w_cosmics)
        self.cal_fit_params["/calib/w_511"].append(w_511)
        self.cal_fit_params["/calib/w_K"].append(w_K)
        self.cal_fit_params["/calib/w_U"].append(w_U)
        self.cal_fit_params["/calib/w_Th"].append(w_Th)
        self.cal_fit_params["/calib/w_Rn"].append(w_Rn)
        failed = fmin < self.fmin_range[0] or fmin > self.fmin_range[1]
        if failed:
            logging.warning("*** Calibration failed ***")
            return None
        else:
            return CalibParams(gain, sat, off)
        
    def apply_calibration(self, tmin_idx, tmax_idx):
        channels = self.raw_val[tmin_idx:tmax_idx]
        rand_channels = np.random.rand(len(channels)) + channels
        self.cal_val[tmin_idx:tmax_idx] = self.calibFcn(rand_channels,
                                                        gain=self.calibParams.gain,
                                                        saturation=self.calibParams.sat,
                                                        offset=self.calibParams.off)

    def run_calibration(self):
        # calibrate the first inttime + skip coverage together
        # then each inttime coverage calibrates the successive skip coverage
        # |--------- 240s --------|
        # ........................|-- 60s --|
        # ... 60s ...|-------- 240s --------|
        # ..................................|-- 60s --|
        skip = self.inttime - self.overlap
        tmin = self.ts[0]
        tmin_idx = 0
        n_trials = 0
        while self.calibParams is None:
            tmax = self.ts[tmin_idx] + self.inttime
            tmax_idx = np.searchsorted(self.ts, tmax)
            tmax = self.ts[min(self.ts.size-1,tmax_idx)]
            try:
                self.calibParams = self.calibrate(tmin_idx, tmax_idx, init=True)
            except:
                logging.warning("Exception was raised in calibrator")
                self.calibParams = None
            if self.calibParams is None:
                logging.warning("*** Initial calibration failed ***")
                n_trials += 1
                if n_trials == 5:
                    logging.warning("*** Failed to calibrate over N trials ***")
                    return None, None
            tmin += skip
            tmin_idx = np.searchsorted(self.ts, tmin)
        tmax = self.ts[tmin_idx] + self.inttime
        tmax_idx = np.searchsorted(self.ts, tmax)
        tmin_cal_idx, tmax_cal_idx = 0, tmax_idx
        self.apply_calibration(tmin_cal_idx, tmax_cal_idx)
        while tmax_idx != self.ts.size:
            tmax = self.ts[tmax_idx]
            try:            
                params = self.calibrate(tmin_idx, tmax_idx)
            except:
                logging.warning("Exception was raised in calibrator")
            self.calibParams = params if params is not None else self.calibParams
            tmin_cal_idx = tmax_idx
            tmin += skip
            tmin_idx = np.searchsorted(self.ts, tmin)
            tmax = self.ts[tmin_idx] + self.inttime
            tmax_idx = np.searchsorted(self.ts, tmax)
            tmax_cal_idx = tmax_idx
            self.apply_calibration(tmin_cal_idx, min(self.ts.size,tmax_cal_idx))
        return self.cal_val, self.cal_fit_params

