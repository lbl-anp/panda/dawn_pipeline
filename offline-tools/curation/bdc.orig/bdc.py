from copy import deepcopy
from datetime import datetime, timezone
import json
import h5py
import numpy as np
import logging
import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "minos.settings")
from django.core.management import execute_from_command_line
import fnmatch
import re
import subprocess

# constants
PROJECTS = {"PANDA / 8"}
DATA_TYPE = "Collection"
PRIMARY_DOMAIN = "Radiation"
DATA_FORMAT = "ROS bag, HDF5"
PROJECT = "PANDA / 8"
BDC_DIR = "/clusterfs/bdc_redux/register_data/PANDA/Chicago_deployment"

PANDAWN_LATLON = {'W022': (0, 0),
                  'W01C': (41.88452644522827, -87.62460057836556),
                  'W059': (41.880798782501344, -87.62787947725498),
                  'W05A': (41.88918080948867, -87.63241563638988),
                  'W05B': (41.88676494472552, -87.63095765266233),
                  'W05C': (41.88312765231458, -87.62932260591538),
                  'W05D': (41.88455003362158, -87.62797295963482),
                  'W05E': (41.87429749692395, -87.66644603994878),
                  'W072': (41.86673872385933, -87.6665645143102),
                  'W073': (41.88315300556273, -87.62765984093555),
                  'W074': (41.88929390665504, -87.63122288140154),
                  'W075': (41.88317977577514, -87.63373999393055),
                  'W076': (0, 0),
                  'W077': (41.88675294995436, -87.63235008038846),
                  }

def __removeprefix(text: str, prefix: str):
    # can use str.removeprefix in py3.9
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def get_time_extents(filepath, node=""):
    with h5py.File(filepath, 'r') as f:
        ts_f = f[node+"/gis/ts_f"][()] # timestamp first
        ts_l = f[node+"/gis/ts_l"][()] # timestamp last
    return ts_f, ts_l

# missing: CollectionInfo.ID, TimeInfo.TimeExtents, FileInfo.FileList
catalog_template = {
    "SchemaVersion": "1.4bdc",
    "ProjectInfo": {"ID": "PANDA / 8"},
    "CollectionInfo": {"ID": ""},
    "SubmitterInfo": {"ID": "nabgrall"},
    "AuthorsList": [],
    "TimeInfo": {
        "TimeExtents": []
     },
    "LocationInfo": {
        "LongitudeList": [],
        "LatitudeList": [],
        "GeodeticDatum": "",
        "Description": ""
    },
    "DataInfo": {
        "POCInfo": {"Name": "Nico Abgrall", "Lab":"LBNL"}, 
        "DataType": DATA_TYPE,
        "DataLevel": "",
        "DomainList": {
            "Primary": [PRIMARY_DOMAIN],
            "Secondary": []
        },
        "Instrument": "",
        "Description": "",
        "DataFormat": DATA_FORMAT,
        "ParserCodeFile":"Null"
    },
    "Comment": "",
    "FileInfo": {"FileList": []}
}

def _make_timestamp(t: datetime):
    # return datetime.fromtimestamp(t).replace(tzinfo=timezone.utc)\
    return datetime.utcfromtimestamp(t)\
        .isoformat(timespec="microseconds")

def to_datetime(t_unix):
    datetime.utcfromtimestamp(t_unix)

def make_catalog(
    collection_id: str, t_i: float, t_f: float, file_list: list,
    project: str, long: float, lat: float
):
    assert project in PROJECTS
    catalog = deepcopy(catalog_template)
    catalog["CollectionInfo"]["ID"] = collection_id
    catalog["TimeInfo"]["TimeExtents"] = [t_i, t_f]
    catalog["FileInfo"]["FileList"] = file_list
    catalog["ProjectInfo"]["ID"] = project
    catalog["LocationInfo"]["LongitudeList"] = [long]
    catalog["LocationInfo"]["LatitudeList"] = [lat]
    return catalog

def write_catalog(
    filepath: str, collection_id: str, t_i: float, t_f: float,
    file_list: list, project: str, long: float, lat: float
):
    t_i = _make_timestamp(t_i)
    t_f = _make_timestamp(t_f)
    catalog = make_catalog(collection_id, t_i, t_f, file_list, project, long, lat)
    with open(filepath, 'w') as json_file:
        json.dump(catalog, json_file, indent=2)

def bdc_ingest(node, coll, subcol=''):
    DIR = BDC_DIR + '/' + node + "/BDC"
    LAT = PANDAWN_LATLON[node][0]
    LONG = PANDAWN_LATLON[node][1]
    collections_ignored = []
    collections = []
    if subcol:
        collection_dirs = fnmatch.filter(os.listdir(DIR), "*_"+coll+"_"+subcol+'*')
    else:
        collection_dirs = fnmatch.filter(os.listdir(DIR), "*_"+coll+"_*")
    for collection_dir in collection_dirs:
        collection_id = collection_dir
        logging.info("collection ID: {}".format(collection_id))
        collection_fullpath = os.path.join(DIR, collection_dir)
        catalog_filepath = os.path.join(collection_fullpath, collection_id + ".json")
        # make file list
        filelist_fullpath = []
        for dir_tuple in os.walk(collection_fullpath):
            filelist_fullpath.extend([os.path.join(dir_tuple[0], f) for f in dir_tuple[2] if f != "Icon\r"])
        # check to see if there's HDF5 files in filelist
        h5_filelist = [f for f in filelist_fullpath if f.endswith(".h5") or f.endswith(".hdf5")]
        # Only make JSONs for the collections with HDF5s:
        if h5_filelist:
            collections.append(collection_dir)
            ts_i, ts_f = zip(*[get_time_extents(os.path.join(catalog_filepath, f), node=node) for f in h5_filelist])
            t_i = np.min(ts_i)
            t_f = np.max(ts_f)
            filelist = [__removeprefix(f, f"{collection_fullpath}/") for f in filelist_fullpath]
            logging.info("creating collection catalog: {}".format(catalog_filepath))
            write_catalog(catalog_filepath, collection_id, t_i, t_f, filelist, PROJECT, LONG, LAT)
            logging.info("ingesting collection")
            coll_DIR = collection_fullpath
            cmd = "python3 /var/www/bdc_redux/minos/minos/manage.py ingest_catalog --catalog " + catalog_filepath
            output = subprocess.check_output(cmd, shell=True).decode()
            print(output)
            search = re.search(r"(?<=collection ).*?(?= created)", output)
            if search is not None:
                coll_NAME = search.group(0)
                logging.info(".. found collection NAME: {}".format(coll_NAME))
                logging.info("setting collection permissions")
                cmd = "echo 'db.getSiblingDB(\"minos_mongo\").datacollections.findOne({\"name\": \"" \
                   + coll_NAME \
                   + "\"})' > get_coll_ID.js"
                os.system(cmd)
                output = subprocess.check_output('mongo < get_coll_ID.js', shell=True).decode()
                search = re.search(r"(?<=ObjectId\(\").*?(?=\")", output)
                if search is not None:
                    coll_ID = search.group(0)
                    print(".. found collection ID:", coll_ID)
                    cmd = "echo 'db.getSiblingDB(\"minos_mongo\").datacollections.update({\"_id\" : ObjectId(\"" \
                        + coll_ID \
                        + "\")},{\"$set\":{\"access_controls\": " \
                        + "{\"nnsa_na22\":{\"read\":true,\"write\":false}," \
                        + "\"ornl-crafti\":{\"read\":true,\"write\":false}," \
                        + "\"lbnl-panda\":{\"read\":true,\"write\":true}}}})\n" \
                        + "db.file_access_control.update({\"filepath\": {\"$regex\": \""\
                        + coll_DIR \
                        + "/\"}},{\"$set\":{\"access_controls\": " \
                        + "{\"nnsa_na22\":{\"read\":true,\"write\":false}," \
                        + "\"ornl-crafti\":{\"read\":true,\"write\":false}," \
                        + "\"lbnl-panda\":{\"read\":true,\"write\":true}}}},{\"multi\":true})'" \
                        + " > set_coll_permissions.js"
                    os.system(cmd)
                    output = subprocess.check_output('mongo < set_coll_permissions.js', shell=True).decode()
                    logging.info("permissions set to:")
                    logging.info(output)
                else:
                    logging.info("Could not find collection ID")
            else:
                logging.info("Could not find collection NAME")
        else:
            collections_ignored.append(collection_dir)
            logging.info(f"{collection_dir} ignored.")
