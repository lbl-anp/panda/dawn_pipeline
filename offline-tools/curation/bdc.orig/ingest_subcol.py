#---------------------------------------
#
# Curation script:
# - dowloads 1 month worth of rad/env data
# - converts bags to BDC hdf5 format + sanity checks
# - performs BDC ingestion in collections of 80 files
#
# Usage:
# --node, node VSN
# --year, year to process
# --month, month to process
#
#---------------------------------------
import logging
import argparse
import os
from bdc import bdc_ingest

# seems needed to make sure the code exits, otherwise rospy will somehow consume resources and not allow a normal exit
import importlib
importlib.reload(logging)

# constants
BDC_DIR = "/clusterfs/minos/bdc/register_data/PANDA/Chicago_deployment"

# parse args in
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--year', type=str)
parser.add_argument('--month', type=str)
parser.add_argument('--subcol', type=str)
args = parser.parse_args()
node = args.node.upper()
year = args.year
month = args.month
subcol = args.subcol
coll = year + '_' + month

#---------------------------------------
# BDC ingestion
print("Ingesting collections")
bdc_ingest(node, coll, subcol=subcol)





