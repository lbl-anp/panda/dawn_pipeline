#
# Script launched from monthly crontab to ingest BDC collections.
# Scheduled on fifth of the month to ingest collection of previous month.
# This should give us some margin to make sure the uploader to beehive 
# completed for the last day of the month, and data has been processed.
#
# Does NOT require conda panda_curation env to be activated.
#

# generic
import logging
from datetime import datetime
from dateutil import relativedelta
import pathlib
import subprocess
import os
import re
import time
import json
# fix issue with rospy logging interference
import importlib
importlib.reload(logging)

# setup logging
LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
LOG_FORMATTER = logging.Formatter('%(asctime)s[%(levelname)s] %(funcName)s: %(message)s')

BDC_DIR = "/clusterfs/bdc_redux/register_data/PANDA/Chicago_operation"
# Deployed nodes
NODES = ["W01A", "W01C",
         "W059", "W05A", "W05B", "W05C", "W05D",
         "W072", "W073", "W074", "W075", "W076", "W077"]
NODES_LATLON = {'W022': (0., 0.),
                'W01A': (37.87568296320878, -122.25030483738881),
                'W01C': (41.88452644522827, -87.62460057836556),
                'W059': (41.880798782501344, -87.62787947725498),
                'W05A': (41.88918080948867, -87.63241563638988),
                'W05B': (41.88676494472552, -87.63095765266233),
                'W05C': (41.88312765231458, -87.62932260591538),
                'W05D': (41.88455003362158, -87.62797295963482),
                'W05E': (0., 0.),
                'W072': (41.86673872385933, -87.6665645143102),
                'W073': (41.88315300556273, -87.62765984093555),
                'W074': (41.88929390665504, -87.63122288140154),
                'W075': (41.88317977577514, -87.63373999393055),
                'W076': (41.88461142499264, -87.62937943517525),
                'W077': (41.88675294995436, -87.63235008038846)}

# get current time range
date = datetime.today()
coll_date = date - relativedelta.relativedelta(months=1, day=1)

# ingest collections
for node in NODES:
    start = time.time()
    coll_name = "PANDAWN_" + node + '_' + coll_date.strftime("%Y_%m")
    coll_dir = os.path.join(BDC_DIR, node, coll_name)
    if not os.path.exists(coll_dir):
        print(f"Missing collection dir: {coll_dir}")
        continue
    #
    # append to collection log file
    #
    logfile = os.path.join(coll_dir, "Log", coll_name+".log")
    handler = logging.FileHandler(logfile)
    handler.setLevel(logging.INFO)
    handler.setFormatter(LOG_FORMATTER)
    LOG.addHandler(handler)
    LOG.info(f">>> Logging to collection {coll_name}, on {date.strftime('%Y-%m-%d')}")
    #
    # create file list
    #
    filelist = []
    for filepath in pathlib.Path(coll_dir).glob('**/*radenv-stream*_check.bag'):
        filelist.append(filepath.name)
    for filepath in pathlib.Path(coll_dir).glob('**/*radenv-stream*_check*.h5'):
        filelist.append(filepath.name)
    for filepath in pathlib.Path(coll_dir).glob('**/*cam-stream*.bag'):
        filelist.append(filepath.name)
    for filepath in pathlib.Path(coll_dir).glob('**/*lidar-stream*.bag'):
        filelist.append(filepath.name)
    #
    # update catalog with file list
    #
    LOG.info("updating catalog's file list for ingestion")
    jsonfile = coll_dir + '/' + coll_name + ".json"
    with open(jsonfile, 'r') as infile:
        catalog = json.load(infile)
        catalog["FileInfo"]["FileList"] = filelist
    with open(jsonfile, "w") as outfile:
        json.dump(catalog, outfile, indent=2)
    #
    # ingest catalog
    #
    LOG.info("ingest catalog")
    cmd = "python3 /var/www/bdc_redux/minos/minos/manage.py ingest_catalog --catalog " + jsonfile
    output = subprocess.check_output(cmd, shell=True).decode()
    search = re.search(r"(?<=collection ).*?(?= created)", output)
    if search is not None:
        NAME = search.group(0)
        cmd = "echo 'db.getSiblingDB(\"minos_mongo\").datacollections.findOne({\"name\": \"" \
            + NAME \
            + "\"})' > get_coll_ID.js"
        os.system(cmd)
        output = subprocess.check_output('mongo < get_coll_ID.js', shell=True).decode()
        search = re.search(r"(?<=ObjectId\(\").*?(?=\")", output)
        if search is not None:
            ID = search.group(0)
            cmd = "echo 'db.getSiblingDB(\"minos_mongo\").datacollections.update({\"_id\" : ObjectId(\"" \
                + ID \
                + "\")},{\"$set\":{\"access_controls\": " \
                + "{\"nnsa_na22\":{\"read\":true,\"write\":false}," \
                + "\"ornl-crafti\":{\"read\":true,\"write\":false}," \
                + "\"lbnl-panda\":{\"read\":true,\"write\":true}}}})\n" \
                + "db.file_access_control.update({\"filepath\": {\"$regex\": \""\
                + coll_dir \
                + "/\"}},{\"$set\":{\"access_controls\": " \
                + "{\"nnsa_na22\":{\"read\":true,\"write\":false}," \
                + "\"ornl-crafti\":{\"read\":true,\"write\":false}," \
                + "\"lbnl-panda\":{\"read\":true,\"write\":true}}}},{\"multi\":true})'" \
                + " > set_coll_permissions.js"
            os.system(cmd)
            output = subprocess.check_output('mongo < set_coll_permissions.js', shell=True).decode()
        else:
            LOG.warning("Ingestion Error: Could not find collection ID")
    else:
        LOG.warning("Ingestion Error: Could not create collection")
    stop = time.time()
    LOG.info(f"monthly ingestion done in {(stop - start)}s")
    LOG.removeHandler(handler)
