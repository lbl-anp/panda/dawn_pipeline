import argparse
import h5py
import matplotlib.pyplot as plt
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()


for file in args.files:
    with h5py.File(file, 'r') as h5in:
        fig, ax = plt.subplots()
        ax.set_xlabel("Energy [keV]")
        ax.set_ylabel("pdfs")
        ax.set_yscale('log')
        n_comp = h5in.attrs["n_components"]
        bin_centers = h5in["bin_centers"][()]
        for i in range(n_comp):
            comp = h5in['V'][i]
            #comp[comp < 1e-5] = 1e-5
            ax.step(bin_centers, comp, where="mid", label="comp-"+str(i))
        ax.set_title(file)
        ax.legend(loc="best")          

plt.show()
