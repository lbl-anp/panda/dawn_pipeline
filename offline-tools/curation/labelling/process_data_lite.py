import warnings
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"The frame.append method is deprecated*")
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"arrays to stack*")
# ROS
from rosbag import Bag
# generic
import time
import numpy as np
import h5py
# PANDA
from utils import bag_info, sqrt_n_bins
from utils import ListToListAccumulator, ListToHistAccumulator
from utils import SeriesListDesc, SeriesListPlot, SeriesHistDesc, SeriesHistPlot, MarkerDesc
from utils import RaingaugeMsgReader, TemperatureMsgReader
from utils import RadCalibMsgReader, RadMsgReader, RadDataBinModeAggregator
from background import SpectralTriage
from background import BackgroundModel, ModelBootstrap, ModelTraining
from background import AnomalyDetection, ModelSelection, ModelMonitoring
# args
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
parser.add_argument('--modelfiles', nargs='+', type=str)
args = parser.parse_args()

#
# BARD (Background Adaptive Radiation Detection)
#
# background model
if args.modelfiles:
    model = BackgroundModel(files=args.modelfiles)
    INT_TIME = model.impl["static"]["badlad"].mean_inttime
    NBINS = model.impl["static"]["badlad"].num_bins
    BIN_EDGES = model.impl["static"]["badlad"].binedges
    BIN_CENTERS = model.impl["static"]["badlad"].bincenters
    FAR = model.impl["static"]["badlad"].far
else:
    INT_TIME = 2.
    NBINS = 128
    BIN_EDGES, BIN_CENTERS = sqrt_n_bins(nmin=50, nmax=3000, nbins=NBINS)
    FAR = 1./8
    model = BackgroundModel(int_time=INT_TIME, far=FAR, bin_edges=BIN_EDGES)
# spectral triage
spectralTriage = SpectralTriage(int_time=INT_TIME, far=FAR,
                                tscale_slow=2*3600, fpr_slow=1,
                                tscale_fast=30, fpr_fast=2,
                                tscale_nscrad=2*3600, bin_edges=BIN_EDGES,
                                radonProxy=True)
# bootstrapping
bootstrap = ModelBootstrap(minutes=30, model=model)
# anomaly detection
anomalyDetection = AnomalyDetection(model=model, encounter_split=10)
# training
training = ModelTraining(model=model)
# model selection
modelSelection = ModelSelection(model=model, int_time=30., tscale=60., thr=0.1)
# model monitoring
modelMonitoring = ModelMonitoring(model=model, int_time=1., tscale=1800.)
# TO-DO: source template generation from anomaly clustering

#
# readers/aggregators
#
temperatureMsgReader = TemperatureMsgReader()
raingaugeMsgReader = RaingaugeMsgReader()
radCalibMsgReader = RadCalibMsgReader()
radMsgReader = RadMsgReader()
radDataAgg = RadDataBinModeAggregator(int_time=INT_TIME, nbins=NBINS, bin_edges=BIN_EDGES)

#
# loop over data files
# Note: msgs in _check bags are not timestamp interleaved anymore
# Will need to adapt radon proxy filter for triage of rad data msgs
#
msgTopics = ["/sensors/raingauge",
             "/calib/listmode"]
for file in args.files:
    start = time.time()
    with Bag(file, 'r') as bag:
        print(f"opening file: {file}")
        info = bag_info(bag)
        print(f"begin: {info.beg}, end: {info.end}, dt: {info.duration}h")
        msgs = bag.read_messages(topics=msgTopics)
        for msg in msgs:
            # read raingauge data + update radon proxy
            if msg.topic == "/sensors/raingauge":
                fields = raingaugeMsgReader.read(msg)
                if spectralTriage.has_RadonProxyFilter():
                    spectralTriage.updateRadonProxyFilter(fields.ts, fields.total_acc)
            # read rad data + perform BARD
            if msg.topic == "/calib/listmode":
                fields = radMsgReader.read(msg)
                if radDataAgg.aggregate(fields.tf, fields.tl, fields.channels):
                    data = radDataAgg.data()
                    metrics, triage = spectralTriage.analyze(data)

                    # block data flow till bootstrap of static flavor is done
                    if not bootstrap.static_is_done():
                        if not (triage.anomaly or triage.none):
                            bootstrap.append(data, triage)
                        continue
                    if not (triage.anomaly or triage.none):
                        # start appending to training sets as soon as static model is bootstrapped  
                        training.append(data, triage)
                        # continue bootstrapping radon flavor till it gets trained
                        if not training.radon_is_done():
                            bootstrap.append(data, triage)
                            bootstrap.update_radon()
                    
                    # start running anomaly detection with bootstrapped flavors
                    detection = anomalyDetection.detect(data)

                    # start model selection as soon as all flavors are bootstrapped
                    if bootstrap.is_done():
                        if not (triage.anomaly or triage.none):
                            if modelSelection.aggregate(data, triage, detection):
                                selection = modelSelection.selection()
                                model.flavor = selection.flavor
                                
                    # block data flow to monitoring till first flavor is trained
                    # note: using bitwise OR to avoid short-circuiting evaluation
                    if not (training.static_is_done() | training.radon_is_done()):
                        continue

                    # monitor trained flavors
                    #if model.order[model.flavor] and not (triage.anomaly or triage.none):
                    #    monitoring = modelMonitoring.aggregate(data)
                    
        stop = time.time()
        print(f"file processed in {(stop - start)}s")



