# generic
import numpy as np
import h5py
from collections import namedtuple, defaultdict, Counter
import time
import sys
from datetime import datetime
from dateutil import relativedelta
# PANDA
from bad import BADLAD, BADMF
from filters import EWMADerivativeFilter, KSigmaFilter, NSCRADFilter, RadonProxyFilter

TriageMetrics = namedtuple("TriageMetrics", "radon_proxy mean_slow metric_slow mean_fast metric_fast metric_nscrad")
TriageResult = namedtuple("TriageResult", "static radon anomaly none label")
class SpectralTriage:
    #
    # Spectra are classified into 4 buckets: "static", "radon", "anomaly" (and "none")
    # Triage is based on the combination of count rate filters, NSCRAD filter and radon proxy filter
    #
    def __init__(self, int_time=1., far=0.125,
                 tscale_slow=2*3600, fpr_slow=2,
                 tscale_fast=30, fpr_fast=3,
                 tscale_nscrad=2*3600, bin_edges=None,
                 radonProxy=False):
        self.slowCountRateFilter = KSigmaFilter(int_time=int_time, tscale=tscale_slow, far=far, fpr=fpr_slow)
        self.fastCountRateFilter = KSigmaFilter(int_time=int_time, tscale=tscale_fast, far=far, fpr=fpr_fast)
        self.nscradFilter = NSCRADFilter(int_time=int_time, tscale=tscale_nscrad, far=far, bin_edges=bin_edges)
        self.radonProxyFilter = RadonProxyFilter() if radonProxy is True else None
        self.tot, self.static, self.radon, self.anomaly, self.none = 0., 0., 0., 0., 0.
        self.radon_proxy_ts, self.anomaly_weak_ts,  self.anomaly_strong_ts = [], [], []

    def has_RadonProxyFilter(self):
        return self.radonProxyFilter is not None

    def updateRadonProxyFilter(self, ts, acc):
        return self.radonProxyFilter.update(ts, acc)
        
    def analyze(self, data):
        spectrum = data.spectrum
        ts = data.ts
        lt = data.lt
        radon_proxy = None
        alarm_radon_proxy = False
        if self.has_RadonProxyFilter:
            radon_proxy = self.radonProxyFilter.get_proxy()
            alarm_radon_proxy = self.radonProxyFilter.analyze(ts)
        mean_slow, metric_slow, alarm_slow_up, alarm_slow_down = self.slowCountRateFilter.analyze(spectrum, ts, lt)
        mean_fast, metric_fast, alarm_fast_up, alarm_fast_down = self.fastCountRateFilter.analyze(spectrum, ts, lt)
        metric_nscrad, alarm_nscrad = self.nscradFilter.analyze(spectrum, ts, lt)
        """
        # original
        alarm_cps = alarm_slow_up and alarm_fast_up
        alarm_radon_cps = (alarm_scrad or alarm_slow_up) and not alarm_fast_up
        static = not (alarm_slow_up or alarm_slow_down or
                      alarm_fast_up or alarm_fast_down or
                      alarm_scrad or alarm_radon_proxy)
        radon = alarm_radon_proxy or alarm_radon_cps
        anomaly = (alarm_cps or alarm_scrad)
        if radon and anomaly:
            radon = False
        """
        # new 
        static = not (alarm_slow_up or alarm_fast_up or alarm_nscrad or alarm_radon_proxy)
        radon = alarm_radon_proxy
        anomaly_weak = alarm_slow_up or alarm_fast_up or alarm_nscrad
        anomaly_strong = alarm_slow_up and (alarm_nscrad or alarm_fast_up)
        anomaly = anomaly_weak or anomaly_strong
        if radon and anomaly_strong: radon = False
        if radon and anomaly_weak: anomaly = False
        if radon: self.radon_proxy_ts.append(data.ts)
        if anomaly:
            if anomaly_weak: self.anomaly_weak_ts.append(data.ts)
            if anomaly_strong: self.anomaly_strong_ts.append(data.ts)
        
        self.tot += 1
        if static:
            self.static += 1
            label = "static"
        if radon:
            self.radon += 1
            label = "radon"
        if anomaly:
            self.anomaly += 1
            label = "anomaly"
        none = False
        if not (static or radon or anomaly):
            none = True
            self.none += 1
            label = "none"
        #if none:
        #    print(f"slow_up {alarm_slow_up}, fast_up {alarm_fast_up}, nscrad {alarm_nscrad}, proxy {alarm_radon_proxy}")
        return TriageMetrics(radon_proxy=radon_proxy,
                             mean_slow=mean_slow, metric_slow=metric_slow,
                             mean_fast=mean_fast, metric_fast=metric_fast,
                             metric_nscrad=metric_nscrad), TriageResult(static=static, radon=radon, anomaly=anomaly, none=none, label=label)

ModelInfo = namedtuple("ModelInfo", "ID fileID flavor impl iter order ts")
def model_info(filename):
    with h5py.File(filename, 'r') as h5in:
        return ModelInfo(ID=h5in.attrs["ID"], fileID=h5in.attrs["fileID"], flavor=h5in.attrs["flavor"],
                         impl=h5in.attrs["impl"], iter=h5in.attrs["iter"],
                         order=h5in.attrs["order"], ts=h5in.attrs["timestamp"])

class BackgroundModel:
    #
    # Background model comes in two flavors, i.e. static vs radon
    # Each flavor has a BADLAD and a BADFM implemention
    # - BADLAD impl for monitoring
    # - BADFM impl for anomaly detection
    # Model iteration refers to the training count
    # Model order is defined as:
    # - 0: bootstrapped model
    # - 1: first order model, i.e. 1 static comp. or 1 static + 1 radon comp.
    # - 2: second order model, same as first order with one additional static comp.
    # Model ID is the same for all flavors/implementations 
    # Model file ID refers to flavor/implementation specific model files
    #
    def __init__(self, int_time=1., far=0.125, bin_edges=np.linspace(0., 3000, 301), files=None):
        self.ID = None
        self.fileID = defaultdict(dict)
        self.impl = defaultdict(dict)
        self.iter = {}
        self.order = {}
        self.flavors = []
        self.flavor = None
        if files is not None:
            flavors = []
            for file in files:
                info = model_info(file)
                print(f"Loading model ID {info.ID}, file ID {info.fileID}, flavor {info.flavor}, impl {info.impl}, order {info.order}, iter {info.iter}, ts {info.ts}")
                self.ID = info.ID
                self.fileID[info.flavor][info.impl] = info.fileID
                self.impl[info.flavor][info.impl] = BADLAD.read_model(file) if info.impl == "badlad" else BADMF.read_model(file)
                self.iter[info.flavor] = info.iter
                self.order[info.flavor] = info.order
                flavors.append(info.flavor)
            flavors = list(np.unique(np.asarray(flavors)))
            if "static" not in flavors:
                print("A static model needs to be specified")
                sys.exit(1)
            self.int_time = self.impl["static"]["badlad"].mean_inttime
            self.far = self.impl["static"]["badlad"].far
            self.bin_edges = self.impl["static"]["badlad"].binedges
            self.bin_centers = self.impl["static"]["badlad"].bincenters
            self.nbins = self.impl["static"]["badlad"].num_bins
            self.flavors = flavors
            self.flavor = "static"
        else:
            self.int_time = int_time
            self.far = far
            self.bin_edges = bin_edges
            self.bin_centers = 0.5 * (self.bin_edges[1:] + self.bin_edges[:-1])
            self.nbins = len(self.bin_centers)
                
class ModelBootstrap:
    #
    # Builds the initial model that allows us to start anomaly detection as soon as
    # possible with an untrained model
    # Marks flavors as already bootstrapped if corresponding model files were passed in
    #
    def __init__(self, minutes=30, model=None):
        self.model = model
        nmax = int(minutes * 60. / self.model.int_time)
        self.spectrum, self.count, self.nmax = {}, {}, {}
        self.ready = {}
        self.ready_ts = defaultdict(list)
        self.in_process, self.done = {}, {}
        self.flavors = ["static", "radon"]
        for flavor in self.flavors:
            self.spectrum[flavor] = np.zeros(self.model.nbins)
            self.count[flavor] = 0
            self.nmax[flavor] = (1 + 1 * (flavor == "radon")) * nmax
            self.ready[flavor] = False 
            self.in_process[flavor] = False
            self.done[flavor] = False
        if self.model.flavors:
            self.done["static"] = True

    def append(self, data, triage):
        flavor = self.flavors[triage.radon]
        if flavor == "static":
            if self.ready[flavor]: return
            else:
                self.spectrum[flavor] += data.spectrum
                self.count[flavor] += 1
                if self.count[flavor] == self.nmax[flavor]:
                    self.ready_ts[flavor].append(data.ts)
                    self.ready[flavor] = True
        else:
            self.spectrum[flavor] += data.spectrum
            self.count[flavor] += 1
            if not (self.count[flavor] % self.nmax[flavor]):
                self.ready_ts[flavor].append(data.ts)
                self.ready[flavor] = True

    def static_is_done(self):
        if self.done["static"]: return True
        if not self.ready["static"]: return False
        if not (self.done["static"] or self.in_process["static"]):
            # in_process keeps from repeated bootstrapping when
            # the later is run in async/offloaded mode
            self.in_process["static"] = True
            self.bootstrap_model("static")
        else: return False

    def update_radon(self):
        if (self.ready["radon"] and not self.in_process["radon"]):
            self.ready_ts["radon"].append(max(self.ready_ts["static"]+self.ready_ts["radon"]))
            self.in_process["radon"] = True
            self.ready["radon"] = False
            self.bootstrap_model("radon")
        else: return False

    def is_done(self):
        return self.done["static"] and self.done["radon"]
        
    def bootstrap_model(self, flavor):
        print(f"bootstrapping {flavor} flavor")
        self.model.impl[flavor] = {}
        if flavor == "static":
            static_comp = self.spectrum["static"] / self.count["static"]
            static_comp /= static_comp.sum()
            np.clip(static_comp, 1.E-6, None, out=static_comp)
            ncomp = 1
            basis = np.asarray([static_comp])
            self.model.flavors.append("static")
            self.model.iter["static"] = 0
            self.model.order["static"] = 0
            self.model.flavor = "static"
        else:
           static_comp = self.spectrum["static"] / self.count["static"]
           radon_comp = self.spectrum["radon"] / self.count["radon"]
           np.clip(radon_comp - static_comp, 1.E-6, None, out=radon_comp) 
           radon_comp /= radon_comp.sum()
           np.clip(radon_comp, 1.E-6, None, out=radon_comp)
           static_comp /= static_comp.sum()
           np.clip(static_comp, 1.E-6, None, out=static_comp)
           ncomp = 2
           basis = np.asarray([static_comp, radon_comp])
           self.model.flavors.append("radon")
           self.model.iter["radon"] = 0
           self.model.order["radon"] = 0
           self.model.flavor = "radon"
        now = datetime.utcnow()
        timestamp = time.mktime(now.timetuple())
        fileID = now.strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badlad"] = fileID
        ID = fileID if self.model.ID is None else self.model.ID
        # BADLAD
        print(f"badlad implementation of {flavor} flavor")
        self.model.impl[flavor]["badlad"] = BADLAD(n_components=ncomp,
                                                   nmf='pnmf_nnls',
                                                   n_steps_iter=100,
                                                   num_bins=self.model.nbins,
                                                   bincenters=self.model.bin_centers,
                                                   binedges=self.model.bin_edges,
                                                   mean_inttime=self.model.int_time,
                                                   steptime=self.model.int_time,
                                                   V=basis)
        self.model.impl[flavor]["badlad"].far = self.model.far
        self.model.impl[flavor]["badlad"].calculate_threshold(far=self.model.far)
        filename = fileID + ".h5"
        self.model.impl[flavor]["badlad"].write_model(filename)
        with h5py.File(filename, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["fileID"] = fileID
            h5in.attrs["ID"] = ID
            h5in.attrs["impl"] = "badlad"
            h5in.attrs["iter"] = 0
            h5in.attrs["order"] = 0
            h5in.attrs["timestamp"] = timestamp
        # BADFM model
        print(f"badfm implementation of {flavor} flavor")
        templates = {}
        with h5py.File("templates/radai_source_templates.h5", 'r') as h5in:
            for src, template in list(h5in['templates'].items()):
                templates[src] = np.reshape(template, (1,self.model.nbins))
        self.model.impl[flavor]["badfm"] = BADMF(n_components=ncomp,
                                                 nmf='pnmf_nnls',
                                                 templates=templates,
                                                 n_steps_iter=100,
                                                 num_bins=self.model.nbins,
                                                 bincenters=self.model.bin_centers,
                                                 binedges=self.model.bin_edges,
                                                 mean_inttime=self.model.int_time,
                                                 steptime=self.model.int_time,
                                                 V=basis)
        self.model.impl[flavor]["badfm"].far = self.model.far
        self.model.impl[flavor]["badfm"].calculate_threshold_analytically(far=self.model.far, inttime=self.model.int_time)
        fileID = (now + relativedelta.relativedelta(seconds=1)).strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badfm"] = fileID
        filename = fileID + ".h5"
        self.model.impl[flavor]["badfm"].write_model(filename)
        with h5py.File(filename, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["ID"] = ID
            h5in.attrs["fileID"] = fileID
            h5in.attrs["impl"] = "badfm"
            h5in.attrs["iter"] = 0
            h5in.attrs["order"] = 0
            h5in.attrs["timestamp"] = timestamp
        self.model.ID = ID
        self.done[flavor] = True
        self.in_process[flavor] = False
    
class ModelTraining:
    #
    # Takes care of training the bootstrap model, as well as performing any training request
    # Marks boostrapped model as trained if corresponding model files were passed
    #
    def __init__(self, model=None):
        self.model = model
        self.flavors = ["static", "radon"]
        self.nmax, self.halfnmax = {}, {}
        self.training_set, self.ts, self.index = {}, {}, {}
        self.half, self.once = {}, {}
        self.ready, self.ready_ts = {}, {}
        self.in_process, self.done = {}, {}
        self.nmax["static"] = int(3600. / self.model.far / self.model.int_time)
        self.nmax["radon"] = self.nmax["static"] // 2
        for flavor in self.flavors:
            self.halfnmax[flavor] = self.nmax[flavor] // 2
            self.training_set[flavor] = np.zeros((self.nmax[flavor], self.model.nbins))
            self.ts[flavor] = np.zeros(self.nmax[flavor])
            self.index[flavor] = 0
            self.half[flavor] = False
            self.once[flavor] = False
            self.ready[flavor] = False
            self.in_process[flavor] = False
            self.done[flavor] = False
        for flavor in self.model.flavors:
            if self.model.order[flavor]: self.done[flavor] = True

    def append(self, data, triage):
        flavor = self.flavors[triage.radon]
        if not self.half[flavor]:
            if self.index[flavor] == (self.halfnmax[flavor] - 1): self.half[flavor] = True
        if not self.once[flavor]:
            if self.index[flavor] == (self.nmax[flavor] - 1): self.once[flavor] = True
        if not self.ready[flavor]:
            if flavor == "static" and self.once[flavor]:
                self.ready[flavor] = True
                self.ready_ts[flavor] = data.ts
            if flavor == "radon" and self.once[flavor] and self.half["static"]:
                    self.ready[flavor] = True
                    self.ready_ts[flavor] = data.ts
        if self.index[flavor] == self.nmax[flavor]: self.index[flavor] = 0
        self.training_set[flavor][self.index[flavor]] = data.spectrum
        self.ts[flavor][self.index[flavor]] = data.ts
        self.index[flavor] += 1

    def static_is_done(self):
        if self.done["static"]: return True
        else:
            if (self.ready["static"] and not(self.done["static"] or self.in_process["static"])): 
                self.in_process["static"] = True
                self.train_model("static")
        return False

    def radon_is_done(self):
        if self.done["radon"]: return True
        else:
            if (self.ready["radon"] and not (self.done["radon"] or self.in_process["radon"])):
                self.in_process["radon"] = True
                self.train_model("radon")
        return False

    def train_model(self, flavor):
        # TO-DO
        # account for model order properly 
        print(f"training {flavor} model")
        if flavor == "static":
            training = self.training_set["static"]
        else:
            training = np.vstack((self.training_set["radon"],
                                  self.training_set["static"][:self.nmax["radon"]]))
        now = datetime.utcnow()
        timestamp = time.mktime(now.timetuple())
        # BADLAD training
        start = time.time()
        #self.model.impl[flavor]["badlad"].fit(training=training, n_iter=100, eps=1.E-5)
        self.model.impl[flavor]["badlad"].fit(training=training, n_iter=50, eps=1.E-5)
        self.model.impl[flavor]["badlad"].calculate_threshold(training=training, far=self.model.far, set_threshold=True, empirical=True)
        for i in range(self.model.impl[flavor]["badlad"].n_components):
            np.clip(self.model.impl[flavor]["badlad"].basis[i], 1.E-6, None, out=self.model.impl[flavor]["badlad"].basis[i])
        fileID = now.strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badlad"] = fileID
        filename = fileID + ".h5"
        self.model.impl[flavor]["badlad"].write_model(filename)
        with h5py.File(filename, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["ID"] = self.model.ID
            h5in.attrs["fileID"] = self.model.ID
            h5in.attrs["impl"] = "badlad"
            h5in.attrs["iter"] = self.model.iter[flavor]
            h5in.attrs["order"] = 1
            h5in.attrs["timestamp"] = timestamp
        stop = time.time()
        print(f"BADLAD implementation trained in {(stop - start):.1f}s")
        # BADFM training
        start = time.time()
        #self.model.impl[flavor]["badfm"].fit(training=training, n_iter=100, eps=1.E-5)
        self.model.impl[flavor]["badfm"].fit(training=training, n_iter=50, eps=1.E-5)
        self.model.impl[flavor]["badfm"].calculate_threshold(training, far=self.model.far, set_threshold=True)
        for i in range(self.model.impl[flavor]["badfm"].n_components):
            np.clip(self.model.impl[flavor]["badfm"].basis[i], 1.E-6, None, out=self.model.impl[flavor]["badfm"].basis[i])
        fileID = (now + relativedelta.relativedelta(seconds=1)).strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badfm"] = fileID
        filename = fileID + ".h5"
        self.model.impl[flavor]["badfm"].write_model(filename)
        with h5py.File(filename, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["ID"] = self.model.ID
            h5in.attrs["ID"] = fileID
            h5in.attrs["impl"] = "badfm"
            h5in.attrs["iter"] = self.model.iter[flavor]
            h5in.attrs["order"] = 1
            h5in.attrs["timestamp"] = timestamp
        stop = time.time()
        print(f"BADFM implementation trained in {(stop - start):.1f}s")
        # increment model iteration
        self.model.iter[flavor] += 1
        self.done[flavor] = True
        self.in_process[flavor] = False

AnomalyDetectionResult = namedtuple("AnomalyDetectionResult", "alarm ids metrics weights_bkg weights_src")
class AnomalyDetection:
    #
    # Uses the BADFM implementation of the models for anomaly detection
    # If an anomaly is detected, will return the 3 isotope IDs with highest metric
    # as well as their template weights
    # Holds a 10s buffer for pre-alarm/post-alarm spectra
    # Fills up a buffer for encounter spectra, encouters being defined as a time series
    # of alarm spectra within alarm_split seconds of each other
    #
    def __init__(self, model=None, encounter_split=10):
        self.model = model
        self.encounter_split = encounter_split
        self.in_encounter = False
        self.prev_alarm = False
        self.split = 1
        # encounter meta data
        self.tf, self.tl, self.lt = 0, 0, 0
        self.n_spectra, self.n_alarms = 0, 0
        self.model_id = ""
        self.model_fileid = ""
        self.model_flavor = ""
        self.model_ncomp = 0
        self.model_iter = 0
        self.model_order = 0
        self.global_id = ""
        self.global_id_prob = 0
        # encounter spectral data
        self.ts = []
        self.ncomp = []
        self.spectra = []
        self.alarms = []
        self.ids = []
        self.metrics = []
        self.weights_bkg = []
        self.weights_src = []
        # pre-alarm buffer
        self.buf_idx = 0
        self.buf_size = self.encounter_split
        self.buf_ts = np.zeros(self.buf_size)
        self.buf_ncomp = np.zeros(self.buf_size)
        self.buf_spectra = np.zeros((self.buf_size, self.model.nbins))
        self.buf_ids = np.zeros((self.buf_size, 3), dtype=np.int8)
        self.buf_metrics = np.zeros((self.buf_size, 3))
        self.buf_weights_bkg = np.zeros((self.buf_size, 9))
        self.buf_weights_src = np.zeros((self.buf_size, 3))
        keys = ["Am-241", "Ba-133", "Co-57", "Co-60", "Cs-137", "Cu-67",
                "DepletedU", "F-18", "FGPu", "HEU", "I-131", "Ir-192",
                "K-40", "LEU", "Lu-177", "NatU", "Ra-226", "RefinedU",
                "Sr-90", "Tc-99m", "Th-232", "Tl-201", "WGPu", "Xe-133"]
        self.index = dict(zip(keys, np.arange(len(keys))))
        
        # debugging
        self.encounter_tf, self.encounter_tl = [], []
        self.h5out = h5py.File("encounters.h5", 'w')
        self.count = 0
        
    def detect(self, data):
        impl = self.model.impl[self.model.flavor]["badfm"]
        spect_info = impl.analyze(data.spectrum, index=[0]).loc[0, :]
        alarm = any(spect_info['alarm'].values)
        spect_info.sort_values(by=['alarm_metric'], ascending=False, inplace=True)
        ids = spect_info.iloc[:3].index.values.tolist()
        ids = np.asarray([self.index[id] for id in ids])
        metrics = np.asarray(spect_info.iloc[:3]['alarm_metric'].tolist())
        weights_bkg = np.zeros(9)
        for i in range(impl.n_components):
            weights = spect_info.iloc[:3]['b'+str(i)].tolist()
            for j, w in enumerate(weights):
                weights_bkg[i + j*3] = w
        weights_src = np.asarray(spect_info.iloc[:3]["s00"].tolist())
        if self.in_encounter:
            self.ts.append(data.ts)
            self.ncomp.append(impl.n_components)
            self.spectra.append(data.spectrum)
            self.alarms.append(alarm)
            self.ids.append(ids)
            self.metrics.append(metrics)
            self.weights_bkg.append(weights_bkg)
            self.weights_src.append(weights_src)
            if (not alarm) and (not self.prev_alarm):
                self.split += 1
            else:
                self.split = 1
            if self.split == self.encounter_split:
                # stop encounter
                self.in_encounter = False
                # fill in encouter meta data
                self.tf = self.ts[0]
                self.tl = self.ts[-1]
                self.lt = self.ts[-1] - self.tf

                self.encounter_tf.append(self.ts[0])
                self.encounter_tl.append(self.ts[-1])
                
                self.n_spectra = len(self.ts)
                self.n_alarms = sum(self.alarms)
                self.model_id = self.model.ID
                self.model_fileid = self.model.fileID[self.model.flavor]["badfm"]
                self.model_flavor = self.model.flavor
                self.model_ncomp = impl.n_components
                self.model_iter = self.model.iter[self.model.flavor]
                self.model_order = self.model.order[self.model.flavor]
                ids = [id[0] for id in self.ids]
                counter = Counter(ids)
                self.global_id = max(counter, key= lambda x: counter[x])
                self.global_id_prob = float(counter[self.global_id]) / sum(counter.values())
                # copy post buffer to pre-buffer
                idx = len(self.ts) - self.encounter_split
                self.buf_ts = np.asarray(self.ts[idx:])
                self.buf_ncomp = np.asarray(self.ncomp[idx:])
                self.buf_spectra = np.asarray(self.spectra[idx:])
                self.buf_ids = np.asarray(self.ids[idx:])
                self.buf_metrics = np.asarray(self.metrics[idx:])
                self.buf_weights_bkg = np.asarray(self.weights_bkg[idx:])
                self.buf_weights_src = np.asarray(self.weights_src[idx:])

                # store to h5 files
                root = "/encounter-"+str(self.count)
                self.h5out.create_dataset(root+"/ts", data=self.ts)
                self.h5out.create_dataset(root+"/ncomp", data=self.ncomp)
                self.h5out.create_dataset(root+"/spectra", data=self.spectra)
                self.h5out.create_dataset(root+"/alarms", data=self.alarms)
                self.h5out.create_dataset(root+"/ids", data=self.ids)
                self.h5out.create_dataset(root+"/metrics", data=self.metrics)
                self.h5out.create_dataset(root+"/weights_bkg", data=self.weights_bkg)
                self.h5out.create_dataset(root+"/weights_src", data=self.weights_src)
                self.h5out[root].attrs["tf"] = self.tf
                self.h5out[root].attrs["tl"] = self.tl
                self.h5out[root].attrs["lt"] = self.lt
                self.h5out[root].attrs["n_spectra"] = self.n_spectra
                self.h5out[root].attrs["n_alarms"] = self.n_alarms
                self.h5out[root].attrs["model_id"] = self.model_id
                self.h5out[root].attrs["model_fileid"] = self.model_fileid
                self.h5out[root].attrs["model_flavor"] = self.model_flavor
                self.h5out[root].attrs["model_ncomp"] = self.model_ncomp
                self.h5out[root].attrs["model_iter"] = self.model_iter
                self.h5out[root].attrs["model_order"] = self.model_order
                self.h5out[root].attrs["global_id"] = self.global_id
                self.h5out[root].attrs["global_id_prob"] = self.global_id_prob
                self.h5out[root].attrs["model_inttime"] = self.model.int_time
                self.count += 1
                
                # reset encounter
                self.ts = []
                self.ncomp = []
                self.spectra = []
                self.alarms = []
                self.ids = []
                self.metrics = []
                self.weights_bkg = []
                self.weights_src = []
                self.split = 1
                global_id = list(self.index.keys())[list(self.index.values()).index(self.global_id)]
                print(f"==> encounter: tf {self.tf}, lt {self.lt}, n_spectra {self.n_spectra}, n_alarms {self.n_alarms}," \
                      f" model {self.model_id}, ncomp {self.model_ncomp}, global ID {global_id}, prob {self.global_id_prob}")
            self.prev_alarm = alarm
        else:
            # fill pre-alarm buffer
            if not alarm:
                self.buf_ts[self.buf_idx] = data.ts
                self.buf_ncomp[self.buf_idx] = impl.n_components
                self.buf_spectra[self.buf_idx] = data.spectrum
                self.buf_ids[self.buf_idx] = ids
                self.buf_metrics[self.buf_idx] = metrics
                self.buf_weights_bkg[self.buf_idx] = weights_bkg
                self.buf_weights_src[self.buf_idx] = weights_src
                self.buf_idx = (self.buf_idx + 1) if self.buf_idx < (self.buf_size - 1) else 0
            else:
                # start encounter
                self.in_encounter = True
                self.prev_alarm = True
                # copy pre-alarm buffer
                if self.buf_idx:
                    roll = self.buf_size - self.buf_idx
                    self.buf_ts = np.roll(self.buf_ts, roll, axis=0)
                    self.buf_ncomp = np.roll(self.buf_ncomp, roll, axis=0)
                    self.buf_spectra = np.roll(self.buf_spectra, roll, axis=0)
                    self.buf_ids = np.roll(self.buf_ids, roll, axis=0)
                    self.buf_metrics = np.roll(self.buf_metrics, roll, axis=0)
                    self.buf_weights_bkg = np.roll(self.buf_weights_bkg, roll, axis=0)
                    self.buf_weights_src = np.roll(self.buf_weights_src, roll, axis=0)
                    self.buf_idx = 0
                for i in range(self.buf_size):
                    self.ts.append(self.buf_ts[i])
                    self.ncomp.append(self.buf_ncomp[i])
                    self.spectra.append(self.buf_spectra[i])
                    self.alarms.append(0)
                    self.ids.append(self.buf_ids[i])
                    self.metrics.append(self.buf_metrics[i])
                    self.weights_bkg.append(self.buf_weights_bkg[i])
                    self.weights_src.append(self.buf_weights_src[i])
                # append first alarm of encounter
                self.ts.append(data.ts)
                self.ncomp.append(impl.n_components)
                self.spectra.append(data.spectrum)
                self.alarms.append(alarm)
                self.ids.append(ids)
                self.metrics.append(metrics)
                self.weights_bkg.append(weights_bkg)
                self.weights_src.append(weights_src)
        if alarm:
            return AnomalyDetectionResult(alarm=True, ids=ids, metrics=metrics,
                                          weights_bkg=weights_bkg, weights_src=weights_src)
        else:
            return AnomalyDetectionResult(alarm=False, ids=[], metrics=[], weights_bkg=[], weights_src=[])
            
SelectionResult = namedtuple("SelectionResult", "flavor ts n_radon mean_radon metric_radon")
class ModelSelection:
    #
    # Based on detection of direct and indirect radon enhancement in the data
    #
    def __init__(self, model=None, int_time=1., tscale=3600., thr=0.1):
        self.model = model
        self.int_time = int_time
        self.radonFilter = EWMADerivativeFilter(int_time=int_time, tscale=tscale, thr=thr)
        self.thr = thr #TO-DO: analytic thr calculation?
        self.live_time = 0.
        self.n_direct_radon, self.n_indirect_radon = 0., 0.
        self.prev_alarm = False
        self.modelSelection = None
        # debug
        self.static = np.zeros(self.model.nbins)
        self.direct_radon = np.zeros(self.model.nbins)
        self.indirect_radon = np.zeros(self.model.nbins)
        self.n_tot = 0.
        self.n_static_tot = 0.
        self.n_direct_radon_tot, self.n_indirect_radon_tot = 0., 0.
        self.radon_ts = []
        self.static_ts = []

        keys = ["Am-241", "Ba-133", "Co-57", "Co-60", "Cs-137", "Cu-67",
                "DepletedU", "F-18", "FGPu", "HEU", "I-131", "Ir-192",
                "K-40", "LEU", "Lu-177", "NatU", "Ra-226", "RefinedU",
                "Sr-90", "Tc-99m", "Th-232", "Tl-201", "WGPu", "Xe-133"]
        self.index = dict(zip(keys, np.arange(len(keys))))
        
    def aggregate(self, data, triage, detection):
        # debug
        self.n_tot += 1
        if triage.static:
            self.static += data.spectrum
            self.n_static_tot += 1
        if triage.radon:
            self.direct_radon += data.spectrum
            self.n_direct_radon_tot += 1
        if detection.alarm and (self.index["Ra-226"] in detection.ids):
            self.indirect_radon += data.spectrum
            self.n_indirect_radon_tot += 1
        #        
        self.n_direct_radon += triage.radon
        self.n_indirect_radon += detection.alarm and (self.index["Ra-226"] in detection.ids)
        self.live_time += data.lt
        if self.live_time >= self.int_time:
            n_radon = self.n_direct_radon + self.n_indirect_radon
            mean, metric, alarm_up, alarm_down = self.radonFilter.analyze([n_radon], data.ts, 1.)
            #radon_trigger = self.prev_alarm and alarm_up
            #static_trigger = self.prev_alarm and alarm_down
            radon_trigger = alarm_up
            static_trigger = alarm_down
            self.prev_alarm = alarm_up or alarm_down
            flavor = self.model.flavor
            if radon_trigger:
                if self.model.flavor == "static":
                    print("switch to radon model")
                    flavor = "radon"
                    self.radon_ts.append(data.ts)
                else:
                    print("radon trigger while using radon model")
            if static_trigger:
                if self.model.flavor == "radon":
                    print("switch to static model")
                    flavor = "static"
                    self.static_ts.append(data.ts)
                else:
                    print("static trigger while using static model")
            
            self.modelSelection = SelectionResult(flavor=flavor, ts=data.ts,
                                                  n_radon=n_radon, mean_radon=mean, metric_radon=metric)
            # reset aggregator
            self.live_time = 0.
            self.n_direct_radon, self.n_indirect_radon = 0., 0.
            return True
        else:
            return False

    def selection(self):
        return self.modelSelection

MonitoringResults = namedtuple("MonitoringAResults", "label metric_bad weights alarm_aic metric_aic aic mean_aic")
class ModelMonitoring:
    #
    # Based on BADLAD implementation of model to compute AIC score
    # on spectra not classified as "anomaly"
    #
    # TO-DO:
    # We need field data to see the actual behavior of that metric in the field
    # and design alarm and improvement estimation criteria
    #
    def __init__(self, model=None, int_time=1., tscale=3600., mean=0., thr=0.1):
        self.model = model
        self.int_time = int_time
        self.AICScoreFilter = EWMADerivativeFilter(int_time=int_time, tscale=tscale, mean=mean, thr=thr)
        self.thr = thr
        self.live_time, self.aic = 0., 0.
        self.flavor = None
        self.prev_alarm = False
        self.monitoringResults = None, None
        # debug
        self.alarm_ts = []

    def aggregate(self, data):
        impl = self.model.impl[self.model.flavor]["badlad"]
        label = "monitoring_" + self.model.flavor + '_' + str(impl.n_components)
        spect_info = impl.analyze(data.spectrum, index=[0]).loc[0, :]
        alarm_bad = spect_info["alarm"]
        metric_bad = spect_info["alarm_metric"]
        weights = [spect_info['b'+str(i)] for i in range(impl.n_components)]

        """
        # make sure we aggregate continuously for the same flavor,
        # model selection could switch to a different model
        if (self.flavor is None) or (self.model.flavor == self.flavor):
            self.live_time += data.lt
            self.aic += 2 * impl.n_components + spect_info["deviance"]
            self.flavor = self.model.flavor
        else:
            self.live_time = 0.
            self.aic = 0.
            self.prev_alarm = False
        if self.live_time >= self.int_time:
            mean_aic, metric_aic, alarm_up, _ = self.AICScoreFilter.analyze([self.aic], data.ts, 1.)
            aic_trigger = alarm_up and self.prev_alarm
            self.prev_alarm = alarm_up
            self.monitoringResults = MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
                                                       alarm_aic=aic_trigger, metric_aic=metric_aic, aic=self.aic, mean_aic=mean_aic)
            if aic_trigger: self.alarm_ts.append(data.ts)
            # reset aggregator
            self.live_time = 0.
            self.aic = 0.
            return True
        else:
            self.monitoringResults = MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
                                                       alarm_aic=False, metric_aic=0., aic=0., mean_aic=0.)
            return False
        """
        self.aic = 2 * impl.n_components + spect_info["deviance"]
        mean_aic, metric_aic, alarm_up, _ = self.AICScoreFilter.analyze([self.aic], data.ts, 1.)
        #self.monitoringResults = MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
        #                                           alarm_aic=False, metric_aic=metric_aic, aic=self.aic, mean_aic=mean_aic)
        #return True
        return MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
                                 alarm_aic=False, metric_aic=metric_aic, aic=self.aic, mean_aic=mean_aic)
        
    def monitoring(self):
        return self.monitoringResults

    def on_alarm(self):
        #
        # TO-DO
        # Consecutive trainings:
        # - first re-training: as is
        # - if second re-training required: add one static comp if not already done
        # - if third training required
        #
        # NOTE: this strategy will likely change based on the actual behavior of field data
        pass
