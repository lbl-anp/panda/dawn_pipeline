# generic
import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
# panda
from utils import sqrt_n_bins

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--file', type=str)
parser.add_argument('--num', type=str)
parser.add_argument('--templates', type=str)
args = parser.parse_args()

# binning
NBINS = 128
BIN_EDGES, BIN_CENTERS = sqrt_n_bins(nmin=50, nmax=3000, nbins=NBINS)

# templates
srcs = ["Am-241", "Ba-133", "Co-57", "Co-60", "Cs-137", "Cu-67",
        "DepletedU", "F-18", "FGPu", "HEU", "I-131", "Ir-192",
        "K-40", "LEU", "Lu-177", "NatU", "Ra-226", "RefinedU",
        "Sr-90", "Tc-99m", "Th-232", "Tl-201", "WGPu", "Xe-133"]
idx_to_src = dict(zip(np.arange(len(srcs)), srcs))
templates = {}
with h5py.File(args.templates, 'r') as h5in:
    for src, template in list(h5in['templates'].items()):
        templates[src] = np.reshape(template, (1,NBINS))



model_files = []
basis = {}
colors = ["green", "orange", "blue"]
    
with h5py.File(args.file, 'r') as h5in:
    encounter = h5in["encounter-"+args.num]
    print("encounter info:")
    for k, v in encounter.attrs.items():
        print(k, v)

    model_file = encounter.attrs["model_fileid"]+".h5"
    if model_file not in model_files:
        model_files.append(model_file)
        with h5py.File("models/"+model_file, 'r') as h5in:
            basis[model_file] = h5in["V"][()]


    dir = os.path.join(os.getcwd(), "encounters/fig")
    if not os.path.exists(dir):
        os.makedirs(dir)
            
    count = 0
            
    n_spectra = encounter.attrs["n_spectra"]
    for idx in range(n_spectra):
        spectrum = encounter["spectra"][idx]
        ts = encounter["ts"][idx]
        alarm = encounter["alarms"][idx]
        srcs = [idx_to_src[id] for id in encounter["ids"][idx]]
        metrics = encounter["metrics"][idx]
        weights_src = encounter["weights_src"][idx]
        ncomp = int(encounter["ncomp"][idx])
        #weights_bkg = []
        #for j in range(3):
        #    weights_bkg.append(encounter["weights_bkg"][idx][j*3:(j*3)+ncomp])
        # just use weights for 1st isotope
        weight_bkg = encounter["weights_bkg"][idx][:ncomp]


        px = 1/plt.rcParams['figure.dpi']  # pixel in inches
        fig, ax = plt.subplots(figsize=(640*px, 480*px))
        #fig, ax = plt.subplots(figsize=(7, 6))
        #fig, ax = plt.subplots(figsize=(7, 6))
        ax.step(BIN_CENTERS, spectrum, where="mid", color="gray", label="data")
        for i in range(ncomp):
            ax.step(BIN_CENTERS, weight_bkg[i] * basis[model_file][i], where="mid", color="black", label=f"bkg comp {i}")
        for src, metric, weight_src, color in zip(srcs, metrics, weights_src, colors):
            ax.step(BIN_CENTERS, templates[src][0] * weight_src, where="mid", color=color, label=f"{src}, {metric:.2f}")
            
        ax.set_yscale("log")
        ax.set_ylabel("counts")
        ax.set_xlabel("energy [keV]")
        ax.legend(loc="upper right")
        ax.set_title(f"spectrum {idx+1}/{n_spectra}, ts {ts:.2f}, top ID {srcs[0]}, alarm {alarm}")
        fig.suptitle(f"encounter {args.num}, lt {encounter.attrs['lt']:.2f}s, global ID {idx_to_src[encounter.attrs['global_id']]}")
        plt.savefig(dir+'/'+f"ecounter-{args.num}-{idx}.png")
        plt.close()
        #plt.show()

        #count += 1
        #if count == 15:
        #    break
