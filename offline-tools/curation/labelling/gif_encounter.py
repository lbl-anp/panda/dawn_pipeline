import os
import cv2
import numpy as np
from pathlib import Path

gifdir = "/Users/nico/Desktop/PANDA/BARD/encounters/gif"
raddir = "/Users/nico/Desktop/PANDA/BARD/encounters/fig"
odtdir = "/Users/nico/Desktop/PANDA/OD-PANDA-dev/ODT/yolov7/runs/detect/encounter-206"
radfigs = sorted(Path(raddir).iterdir(), key=os.path.getmtime)
odtfigs = sorted(Path(odtdir).iterdir(), key=os.path.getmtime)

# rad rate is 0.5Hz
# cam rate ~23.5Hz, so 47 frames per 2s
mul = 47
n = int(len(odtfigs) / mul)
count = 0
for i in range(n):
    radimg = cv2.imread(radfigs[i].as_posix())
    odtimgs = [cv2.imread(img.as_posix()) for img in odtfigs[i*mul:(i+1)*mul]]
    for img in odtimgs:
        full_img = np.hstack((radimg, img))
        cv2.imwrite(f"{gifdir}/img_{count}.png", full_img)
        count += 1
# add the last bit
radimg = cv2.imread(radfigs[n].as_posix())
odtimgs = [cv2.imread(img.as_posix()) for img in odtfigs[n*mul:]]
for img in odtimgs:
    full_img = np.hstack((radimg, img))
    cv2.imwrite(f"{gifdir}/img_{count}.png", full_img)
    count += 1

    

