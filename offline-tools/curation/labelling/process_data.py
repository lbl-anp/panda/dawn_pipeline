import warnings
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"The frame.append method is deprecated*")
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"arrays to stack*")
# ROS
from rosbag import Bag
# OpenCV
#import cv2
#from cv_bridge import CvBridge
# generic
import time
import numpy as np
import h5py
# PANDA
from utils import bag_info, sqrt_n_bins
from utils import ListToListAccumulator, ListToHistAccumulator
from utils import SeriesListDesc, SeriesListPlot, SeriesHistDesc, SeriesHistPlot, MarkerDesc
from utils import RaingaugeMsgReader, TemperatureMsgReader
from utils import RadCalibMsgReader, RadMsgReader, RadDataBinModeAggregator
from background import SpectralTriage
from background import BackgroundModel, ModelBootstrap, ModelTraining
from background import AnomalyDetection, ModelSelection, ModelMonitoring
# args
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
parser.add_argument('--modelfiles', nargs='+', type=str)
args = parser.parse_args()




#from datetime import datetime
#from dateutil import relativedelta
#from bad import BADLAD, BADMF





#
# BARD (Background Adaptive Radiation Detection)
#
# background model
if args.modelfiles:
    model = BackgroundModel(files=args.modelfiles)
    INT_TIME = model.impl["static"]["badlad"].mean_inttime
    NBINS = model.impl["static"]["badlad"].num_bins
    BIN_EDGES = model.impl["static"]["badlad"].binedges
    BIN_CENTERS = model.impl["static"]["badlad"].bincenters
    FAR = model.impl["static"]["badlad"].far
else:
    INT_TIME = 2.
    NBINS = 128
    BIN_EDGES, BIN_CENTERS = sqrt_n_bins(nmin=50, nmax=3000, nbins=NBINS)
    FAR = 1./8
    model = BackgroundModel(int_time=INT_TIME, far=FAR, bin_edges=BIN_EDGES)
# spectral triage
spectralTriage = SpectralTriage(int_time=INT_TIME, far=FAR,
                                tscale_slow=2*3600, fpr_slow=1,
                                tscale_fast=30, fpr_fast=2,
                                tscale_nscrad=2*3600, bin_edges=BIN_EDGES,
                                radonProxy=True)
# bootstrapping
bootstrap = ModelBootstrap(minutes=30, model=model)
# anomaly detection
anomalyDetection = AnomalyDetection(model=model, encounter_split=10)
# training
training = ModelTraining(model=model)
# model selection
modelSelection = ModelSelection(model=model, int_time=30., tscale=60., thr=0.1)
# model monitoring
#modelMonitoring = ModelMonitoring(model=model, int_time=30., tscale=180., mean=4000., thr=5.)
modelMonitoring = ModelMonitoring(model=model, int_time=1., tscale=1800.)
# TO-DO: source template generation from anomaly clustering

#
# readers/aggregators
#
temperatureMsgReader = TemperatureMsgReader()
raingaugeMsgReader = RaingaugeMsgReader()
radCalibMsgReader = RadCalibMsgReader()
radMsgReader = RadMsgReader(calibrated=True)
radDataAgg = RadDataBinModeAggregator(int_time=INT_TIME, nbins=NBINS, bin_edges=BIN_EDGES)
# accumulators for plots
series = {}
topicFields = {"temp": ["ts", "temp"],
               "raingauge": ["ts", "precipitation", "proxy"],
               "calib": ["ts", "gain"],
               "triage": ["ts", "rate", "radon_proxy",
                          "mean_slow", "metric_slow",
                          "mean_fast", "metric_fast",
                          "metric_scrad"],
               "selection": ["ts", "n_radon", "mean_radon", "metric_radon"],
               "monitoring_static_1": ["ts", "metric_bad", "b0", "aic", "mean_aic"],
               "monitoring_radon_2": ["ts", "metric_bad", "b0", "b1", "aic", "mean_aic"]
               }
for topic, fields in topicFields.items():
    series[topic] = ListToListAccumulator(topic, fields)
histograms = {}
topicFields = {"static": ["channels"],
               "radon": ["channels"],
               "anomaly": ["channels"],
               "none": ["channels"]}
topicBins = {"static": [BIN_EDGES],
             "radon": [BIN_EDGES],
             "anomaly": [BIN_EDGES],
             "none": [BIN_EDGES]}
for topic, fields in topicFields.items():
    histograms[topic] = ListToHistAccumulator(topic, fields, topicBins[topic])

#
# loop over data files
# Note: msgs in _check bags are not timestamp interleaved anymore
# Will need to adapt radon proxy filter for triage of rad data msgs
#
msgTopics = ["/sensors/env/temperature",
             "/sensors/raingauge",
             "/calib/params",
             "/calib/listmode"]
for file in args.files:
    start = time.time()
    for acc in series.values(): acc.new_series()
    for acc in histograms.values(): acc.new_series()
    with Bag(file, 'r') as bag:
        print(f"opening file: {file}")
        info = bag_info(bag)
        print(f"begin: {info.beg}, end: {info.end}, dt: {info.duration}h")
        msgs = bag.read_messages(topics=msgTopics)
        for msg in msgs:
            # read temperature data
            if (msg.topic == "/sensors/env/temperature") and (msg.message.zone[0] == 'e'):
            #if (msg.topic == "/sensors/env/temperature"):
                fields = temperatureMsgReader.read(msg)
                series["temp"].append([fields.ts, fields.temperature])
            # read raingauge data + update radon proxy
            if msg.topic == "/sensors/raingauge":
                fields = raingaugeMsgReader.read(msg)
                if spectralTriage.has_RadonProxyFilter():
                    ts, precipitation, proxy = spectralTriage.updateRadonProxyFilter(fields.ts, fields.total_acc)
                    series["raingauge"].append([ts, precipitation, proxy])
            # read calibration data
            if msg.topic == "/calib/params":
                fields = radCalibMsgReader.read(msg)
                series["calib"].append([fields.ts, fields.gain])
            # read rad data + perform BARD
            if msg.topic == "/calib/listmode":
                fields = radMsgReader.read(msg)
                if radDataAgg.aggregate(fields.tf, fields.tl, fields.channels):
                    data = radDataAgg.data()
                    metrics, triage = spectralTriage.analyze(data)
                    series["triage"].append([data.ts, data.spectrum.sum()/data.lt,
                                             metrics.radon_proxy, metrics.mean_slow, metrics.metric_slow,
                                             metrics.mean_fast, metrics.metric_fast,
                                             metrics.metric_nscrad])
                    histograms[triage.label].append([data.ts, data.spectrum])
                    
                    # block data flow till bootstrap of static flavor is done
                    if not bootstrap.static_is_done():
                        if not (triage.anomaly or triage.none):
                            bootstrap.append(data, triage)
                        continue
                    if not (triage.anomaly or triage.none):
                        # start appending to training sets as soon as static model is bootstrapped  
                        training.append(data, triage)
                        # continue bootstrapping radon flavor till it gets trained
                        if not training.radon_is_done():
                            bootstrap.append(data, triage)
                            bootstrap.update_radon()
                    
                    # start running anomaly detection with bootstrapped flavors
                    detection = anomalyDetection.detect(data)

                    # start model selection as soon as all flavors are bootstrapped
                    if bootstrap.is_done():
                        if not (triage.anomaly or triage.none):
                            if modelSelection.aggregate(data, triage, detection):
                                selection = modelSelection.selection()
                                model.flavor = selection.flavor
                                series["selection"].append([selection.ts, selection.n_radon, selection.mean_radon, selection.metric_radon])

                    # block data flow to monitoring till first flavor is trained
                    # note: using bitwise OR to avoid short-circuiting evaluation
                    if not (training.static_is_done() | training.radon_is_done()):
                        continue

                    # monitor trained flavors
                    if model.order[model.flavor] and not (triage.anomaly or triage.none):
                        monitoring = modelMonitoring.aggregate(data)
                        series[monitoring.label].append([data.ts, monitoring.metric_bad, *monitoring.weights,
                                                         monitoring.aic, monitoring.mean_aic])
                    
        stop = time.time()
        print(f"file processed in {(stop - start)}s")

print("triage stats")
print(f"total: {spectralTriage.tot}")
print(f"static: {spectralTriage.static} ({100.*spectralTriage.static/spectralTriage.tot:.2f})")
print(f"radon: {spectralTriage.radon} ({100.*spectralTriage.radon/spectralTriage.tot:.2f})")
print(f"anomaly: {spectralTriage.anomaly} ({100.*spectralTriage.anomaly/spectralTriage.tot:.2f})")
print(f"none: {spectralTriage.none} ({100.*spectralTriage.none/spectralTriage.tot:.2f})")
print(f"sum: {spectralTriage.static+spectralTriage.radon+spectralTriage.anomaly+spectralTriage.none}")
print("model selection stats")
print(f"total: {modelSelection.n_tot}")
print(f"static: {modelSelection.n_static_tot}")
print(f"direct radon: {modelSelection.n_direct_radon_tot}")
print(f"indirect radon: {modelSelection.n_indirect_radon_tot}")
print(f"sum: {modelSelection.n_static_tot+modelSelection.n_direct_radon_tot+modelSelection.n_indirect_radon_tot}")

#
# plots
#
if not series["temp"].empty():
    plot = SeriesListPlot(series)
    plot.versus(field="ts", label="time [h]", norm=3600)
    plot.series([SeriesListDesc(topic="temp", field="temp", thr=None,
                                label="T [Celsius]", legend="", color="gray", subplot=0),
                 SeriesListDesc(topic="raingauge", field="precipitation", thr=None,
                                label="precipitation [AU]", legend="", color="gray", subplot=1),
                 SeriesListDesc(topic="raingauge", field="proxy", thr=[spectralTriage.radonProxyFilter.thr],
                                label="radon proxy [AU]", legend="", color="gray", subplot=2),
                 SeriesListDesc(topic="calib", field="gain", thr=None,
                                label="gain [AU]", legend="", color="gray", subplot=3),
                 SeriesListDesc(topic="triage", field="rate", thr=None,
                                label="count rate [cnts/s]", legend="", color="gray", subplot=4)])
    plot.show()

plot = SeriesListPlot(series)
plot.versus(field="ts", label="time [h]", norm=3600)
plot.series([SeriesListDesc(topic="triage", field="radon_proxy", thr=[spectralTriage.radonProxyFilter.thr],
                            label="radon proxy [AU]",legend="", color="gray", subplot=0),
             SeriesListDesc(topic="triage", field="rate", thr=None,
                            label="count rate [cnts/s]", legend="data", color="gray", subplot=1),
             SeriesListDesc(topic="triage", field="mean_slow", thr=None,
                            label="count rate [cnts/s]", legend="slow mean", color="orange", subplot=1),
             SeriesListDesc(topic="triage", field="metric_slow", thr=[spectralTriage.slowCountRateFilter.thr],
                            label="metric slow", legend="", color="gray", subplot=2),
             SeriesListDesc(topic="triage", field="rate", thr=None,
                           label="count rate [cnts/s]", legend="data", color="gray", subplot=3),
             SeriesListDesc(topic="triage", field="mean_fast", thr=None,
                            label="count rate [cnts/s]", legend="fast mean", color="orange", subplot=3),
             SeriesListDesc(topic="triage", field="metric_fast", thr=[spectralTriage.fastCountRateFilter.thr],
                            label="metric fast", legend="", color="gray", subplot=4),
             SeriesListDesc(topic="triage", field="metric_scrad", thr=[spectralTriage.nscradFilter.thr],
                            label="metric NSCRAD", legend="", color="gray", subplot=5)])
color = {"static": "gray", "radon": "blue"}
for flavor in bootstrap.ready_ts:
    plot.add_markers([MarkerDesc(markers=bootstrap.ready_ts[flavor], ymin=0., ymax=0.15, subplots=[0],
                                 legend=f"bootstrap {flavor}", color=color[flavor], ls="solid")])
for flavor in training.ready_ts:
    plot.add_markers([MarkerDesc(markers=[training.ready_ts[flavor]], ymin=0., ymax=0.15, subplots=[0],
                                 legend=f"training {flavor}", color=color[flavor], ls="dashdot")])
plot.add_markers([MarkerDesc(markers=modelSelection.radon_ts, ymin=0., ymax=0.15, subplots=[0],
                             legend="selected radon", color="blue", ls="dashed"),
                  MarkerDesc(markers=modelSelection.static_ts, ymin=0., ymax=0.15, subplots=[0],
                             legend="selected static", color="gray", ls="dashed")])

plot.add_markers([MarkerDesc(markers=spectralTriage.radon_proxy_ts, ymin=0.85, ymax=1., subplots=[0],
                             legend="triage radon proxy", color="blue", ls="solid"),
                  MarkerDesc(markers=spectralTriage.anomaly_strong_ts, ymin=0.7, ymax=0.85, subplots=[0],
                             legend="triage anomaly strong", color="red", ls="solid"),
                  MarkerDesc(markers=spectralTriage.anomaly_weak_ts, ymin=0.55, ymax=0.7, subplots=[0],
                             legend="triage anomaly week", color="green", ls="solid")])

plot.add_markers([MarkerDesc(markers=anomalyDetection.encounter_tf, ymin=0., ymax=1., subplots=list(range(6)),
                             legend="", color="blue", ls="solid"),
                  MarkerDesc(markers=anomalyDetection.encounter_tl, ymin=0., ymax=1., subplots=list(range(6)),
                             legend="", color="red", ls="solid")])



plot.show()


n_static, n_radon, n_anom, n_none = 0., 0., 0., 0.
static, radon = np.zeros(NBINS), np.zeros(NBINS)
anom, none = np.zeros(NBINS), np.zeros(NBINS)
for idx in range(histograms["static"].n_series()):
    n_static += histograms["static"].count(idx)
    static += histograms["static"].get("channels", idx)
for idx in range(histograms["radon"].n_series()):
    n_radon += histograms["radon"].count(idx)
    radon += histograms["radon"].get("channels", idx)
for idx in range(histograms["anomaly"].n_series()):
    n_anom += histograms["anomaly"].count(idx)
    anom += histograms["anomaly"].get("channels", idx)
for idx in range(histograms["none"].n_series()):
    n_none += histograms["none"].count(idx)
    none += histograms["none"].get("channels", idx)
static_mean = static / n_static
static_comp = static_mean / static_mean.sum()
np.clip(static_comp, 1.E-6, None, out=static_comp)
radon_mean = radon / n_radon
radon_comp = (radon_mean - static_mean)
radon_comp /= radon_comp.sum()
np.clip(radon_comp, 1.E-6, None, out=radon_comp)


"""
# bootstrap radon model
ncomp = 2
basis = np.asarray([static_comp, radon_comp])
now = datetime.utcnow()
timestamp = time.mktime(now.timetuple())
ID = now.strftime("%Y%m%d-%H%M%S")
# BADLAD
print(f"badlad implementation of radon model")
badlad_impl = BADLAD(n_components=ncomp,
                     nmf='pnmf_nnls',
                     n_steps_iter=100,
                     num_bins=NBINS,
                     bincenters=BIN_CENTERS,
                     binedges=BIN_EDGES,
                     mean_inttime=INT_TIME,
                     steptime=INT_TIME,
                     V=basis)
badlad_impl.far = FAR
badlad_impl.calculate_threshold(far=FAR)
filename = now.strftime("%Y%m%d-%H%M%S") + ".h5"
badlad_impl.write_model(filename)
with h5py.File(filename, "r+") as h5in:
    h5in.attrs["flavor"] = "radon"
    h5in.attrs["ID"] = ID
    h5in.attrs["impl"] = "badlad"
    h5in.attrs["iter"] = 0
    h5in.attrs["order"] = 0
    h5in.attrs["timestamp"] = timestamp
# BADFM model
print(f"badfm implementation of radon model")
templates = {}
with h5py.File("templates/radai_source_templates.h5", 'r') as h5in:
    for src, template in list(h5in['templates'].items()):
        templates[src] = np.reshape(template, (1,NBINS))
badfm_impl = BADMF(n_components=ncomp,
                   nmf='pnmf_nnls',
                   templates=templates,
                   n_steps_iter=100,
                   num_bins=NBINS,
                   bincenters=BIN_CENTERS,
                   binedges=BIN_EDGES,
                   mean_inttime=INT_TIME,
                   steptime=INT_TIME,
                   V=basis)
badfm_impl.far = FAR
badfm_impl.calculate_threshold_analytically(far=FAR, inttime=INT_TIME)
filename = (now + relativedelta.relativedelta(seconds=1)).strftime("%Y%m%d-%H%M%S") + ".h5"
badfm_impl.write_model(filename)
with h5py.File(filename, "r+") as h5in:
    h5in.attrs["flavor"] = "radon"
    h5in.attrs["ID"] = ID
    h5in.attrs["impl"] = "badfm"
    h5in.attrs["iter"] = 0
    h5in.attrs["order"] = 0
    h5in.attrs["timestamp"] = timestamp
"""


anom *= (sum(static[BIN_CENTERS>=1500]) / sum(anom[BIN_CENTERS>=1500]))
anom /= anom.sum()
none *= (sum(static[BIN_CENTERS>=1500]) / sum(none[BIN_CENTERS>=1500]))
none /= none.sum()
import matplotlib.pyplot as plt
fig, ax = plt.subplots(4, sharex=True)
ax[0].step(BIN_CENTERS, static_comp, color="gray", label=f"static, n={n_static}")
ax[0].set_ylabel("pdf")
ax[0].set_yscale("log")
ax[0].legend(loc="best")
ax[1].step(BIN_CENTERS, radon_comp, color="gray", label=f"radon, n={n_radon}")
ax[1].set_ylabel("pdf")
ax[1].set_yscale("log")
ax[1].legend(loc="best")
ax[2].step(BIN_CENTERS, anom, color="gray", label=f"anom, n={n_anom}")
ax[2].set_ylabel("pdf")
ax[2].set_yscale("log")
ax[2].legend(loc="best")
ax[3].step(BIN_CENTERS, none, color="gray", label=f"none, n={n_none}")
ax[3].set_ylabel("pdf")
ax[3].set_yscale("log")
ax[3].legend(loc="best")
ax[3].set_xlabel("Energy [keV]")
plt.show()

"""
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
direct = modelSelection.direct_radon / modelSelection.n_direct_radon_tot
indirect = modelSelection.indirect_radon / modelSelection.n_indirect_radon_tot
np.clip(direct - static_mean, 1.E-6, None, out=direct) 
direct /= direct.sum()
np.clip(direct, 1.E-6, None, out=direct)
np.clip(indirect - static_mean, 1.E-6, None, out=indirect) 
indirect /= indirect.sum()
np.clip(indirect, 1.E-6, None, out=indirect)
ax.step(BIN_CENTERS, static_comp, color="gray", label="static")
ax.step(BIN_CENTERS, direct, color="orange", label="direct radon")
ax.step(BIN_CENTERS, indirect, color="red", label="indirect radon")
ax.set_yscale("log")
ax.legend(loc="best")
plt.show()
"""
if not series["selection"].empty():
    plot = SeriesListPlot(series)
    plot.versus(field="ts", label="time [h]", norm=3600)
    plot.series([SeriesListDesc(topic="selection", field="n_radon", thr=None,
                                label="N radon", legend="data", color="gray", subplot=0),
                 SeriesListDesc(topic="selection", field="mean_radon", thr=None,
                                label="N radon", legend="mean", color="orange", subplot=0),
                 SeriesListDesc(topic="selection", field="metric_radon", thr=[-modelSelection.radonFilter.thr, modelSelection.radonFilter.thr],
                                label="metric", legend="", color="gray", subplot=1)])
    plot.add_markers([MarkerDesc(markers=modelSelection.radon_ts, ymin=0., ymax=1., subplots=list(range(2)),
                                 legend="selected radon", color="blue", ls="dashed"),
                      MarkerDesc(markers=modelSelection.static_ts, ymin=0., ymax=1., subplots=list(range(2)),
                                 legend="selected static", color="gray", ls="dashed")])
    plt.show()


if not series["monitoring_static_1"].empty():
    plot = SeriesListPlot(series)
    plot.versus(field="ts", label="time [h]", norm=3600)
    plot.series([SeriesListDesc(topic="monitoring_static_1", field="metric_bad", thr=[model.impl["static"]["badlad"].threshold],
                                label="metric BAD", legend="", color="gray", subplot=0),
                 SeriesListDesc(topic="monitoring_static_1", field="b0", thr=None,
                                label="weight", legend="static-0", color="gray", subplot=1),
                 SeriesListDesc(topic="monitoring_static_1", field="aic", thr=None,
                                label="AIC", legend="data", color="gray", subplot=2),
                 SeriesListDesc(topic="monitoring_static_1", field="mean_aic", thr=None,
                                label="AIC", legend="mean", color="orange", subplot=2)])
                 #SeriesListDesc(topic="monitoring_static_1_aic", field="metric_aic", thr=[-modelMonitoring.AICScoreFilter.thr, modelMonitoring.AICScoreFilter.thr],
                 #               label="metric", legend="", color="gray", subplot=3)])
    if not series["monitoring_radon_2"].empty():
        plot.add_series([SeriesListDesc(topic="monitoring_radon_2", field="metric_bad", thr=[model.impl["radon"]["badlad"].threshold],
                                        label="metric BAD", legend="", color="blue", subplot=0),
                         SeriesListDesc(topic="monitoring_radon_2", field="b0", thr=None,
                                        label="weight", legend="radon-0", color="gray", subplot=1),
                         SeriesListDesc(topic="monitoring_radon_2", field="b1", thr=None,
                                        label="weight", legend="radon-1", color="blue", subplot=1),
                         SeriesListDesc(topic="monitoring_radon_2", field="aic", thr=None,
                                        label="AIC", legend="data", color="blue", subplot=2),
                         SeriesListDesc(topic="monitoring_radon_2", field="mean_aic", thr=None,
                                        label="AIC", legend="mean", color="red", subplot=2)])
                         #SeriesListDesc(topic="monitoring_radon_2_aic", field="metric_aic", thr=[-modelMonitoring.AICScoreFilter.thr, modelMonitoring.AICScoreFilter.thr],
                         #               label="metric AIC", legend="", color="blue", subplot=3)])
    plot.add_markers([MarkerDesc(markers=modelMonitoring.alarm_ts, ymin=0.85, ymax=1., subplots=list(range(3)),
                                 legend="alarms", color="red", ls="dashed")])
    color = {"static": "gray", "radon": "blue"}
    for flavor in training.ready_ts:
        plot.add_markers([MarkerDesc(markers=[training.ready_ts[flavor]], ymin=0.85, ymax=1., subplots=list(range(3)),
                                     legend=f"training {flavor}", color=color[flavor], ls="dashdot")])
        plot.add_markers([MarkerDesc(markers=modelSelection.radon_ts, ymin=0.85, ymax=1., subplots=list(range(3)),
                                     legend="selected radon", color="blue", ls="dashed"),
                          MarkerDesc(markers=modelSelection.static_ts, ymin=0.85, ymax=1., subplots=list(range(3)),
                                     legend="selected static", color="gray", ls="dashed")])
    plot.show()

    
