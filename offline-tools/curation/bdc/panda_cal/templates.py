import numpy as np
import os
import math
import numba
from .cal_utils import rebin_fast


__frac_sqrt_2pi = 1.0 / math.sqrt(2 * math.pi)


@numba.njit  # (parallel=True)
def _variable_moving_gauss_filter_impl(edges, centers, widths, inp, sigmas, precision):
    klen, ilen = inp.shape
    out = np.zeros((klen, ilen))
    for i in range(ilen):
        start = centers[i] - sigmas[i] * precision
        stop = centers[i] + sigmas[i] * precision
        j1, j2 = i - 1, i + 1
        nom = -0.5 / (sigmas[i] * sigmas[i])
        fo = widths[i] * __frac_sqrt_2pi / sigmas[i]
        for k in range(klen):
            out[k, i] += inp[k, i] * fo
        while j1 >= 0 and edges[j1 + 1] > start:
            fo = (
                math.exp((centers[j1] - centers[i]) ** 2 * nom)
                * widths[j1]
                * __frac_sqrt_2pi
                / sigmas[i]
            )
            for k in range(klen):
                out[k, j1] += inp[k, i] * fo
            j1 -= 1
        while j2 < ilen and edges[j2] < stop:
            fo = (
                math.exp((centers[j2] - centers[i]) ** 2 * nom)
                * widths[j2]
                * __frac_sqrt_2pi
                / sigmas[i]
            )
            for k in range(klen):
                out[k, j2] += inp[k, i] * fo
            j2 += 1
    return out


class Templates:
    def __init__(
        self,
        templates,
        edges,
        labels=None,
        fast=True,
        precision=3,
        res_func=None,
        exclude=[],
    ):

        self.templates = np.array(templates)[[lab not in exclude for lab in labels]]
        for i in range(len(self.templates)):
            self.templates[i][:] /= np.sum(self.templates[i])
        self.edges = np.asarray(edges)
        self.centers = (self.edges[:-1] + self.edges[1:]) * 0.5
        assert np.all(self.centers > 0.0), "Templates requires positive centers."
        self.widths = self.edges[1:] - self.edges[:-1]
        self.labels = [lab for lab in labels if lab not in exclude]
        self.fast = fast
        self.precision = precision
        self._res_func = res_func

    def resolution(self, r):
        if self._res_func is None:
            if isinstance(r, float):
                return r * 662.0 * np.sqrt(self.centers / 662.0) / 2.355
            elif len(r) == 1:
                return r[0] * 662.0 * np.sqrt(self.centers / 662.0) / 2.355
            elif len(r) == 2:
                return (r[0] * 662.0 * np.sqrt(self.centers / 662.0) + r[1] * self.centers) /  2.355
        return self._res_func(r, self.centers)

    @classmethod
    def from_files(cls, file_names, edges, labels=None, **kwargs):
        templates = []
        for fname in file_names:
            tmp_data = np.loadtxt(fname, delimiter=",")
            tmp_data[0, 1] = 0.0
            tmp_delta = np.mean(np.diff(tmp_data[:, 0])) * 0.5
            window = int(np.mean(np.diff(edges)) / (tmp_delta * 2.0) + 0.5)
            tmp_edges = np.asarray(
                [*(tmp_data[:, 0] - tmp_delta), tmp_data[-1, 0] + tmp_delta]
            )
            tmp_data[:, 1] = (
                np.convolve(tmp_data[:, 1], np.ones(window), "same") / window
            )
            tmp_counts = rebin_fast(
                np.asarray(tmp_data[:, 1], dtype=np.float64),
                tmp_edges,
                np.asarray([-np.inf, *edges, np.inf], dtype=np.float64),
            )[1:-1]
            templates.append(tmp_counts / np.sum(tmp_counts))
        return cls(templates, edges, labels=labels, **kwargs)

    @classmethod
    def from_csv_path(cls, path, edges, **kwargs):
        # loading templates and defining some necessary parameters
        files = [os.path.join(path, f) for f in sorted(os.listdir(path)) if "csv" in f]
        labels = [((f.split("."))[-2].split("_"))[-1] for f in files]
        return cls.from_files(files, edges, labels=labels, **kwargs)

    def get_templates(self, resolution, cosmics=[], new_edges=None, as_dict=False):
        """Calculate the templates for a given resolution and cosmics parameters.
        Cosmics is assumed to be an array. If new_edges is defined, it will rebin
        on the fly. If will remove over and underflow."""
        cosmic_components = len(cosmics)
        ttt = np.zeros(
            (self.templates.shape[0] + cosmic_components, self.templates.shape[1]),
            dtype=np.float64,
        )
        ttt[:cosmic_components, :] = (
            (self.centers[:, None]) ** (-np.atleast_2d(cosmics))
        ).transpose()
        # ttt[:cosmic_components, :] /= np.sum(ttt[:cosmic_components, :], axis=1, keepdims=True)
        if self.fast is True:
            ttt[cosmic_components:] = self.variable_moving_gauss_filter(resolution)
        else:
            matrix = self.calc_fwhm_matrix(resolution)
            ttt[cosmic_components:] = (matrix @ self.templates.transpose()).transpose()
        if new_edges is not None:
            ttt = rebin_fast(
                ttt,
                self.edges,
                np.concatenate((np.array([-np.inf]), new_edges, np.array([np.inf]))),
            )[..., 1:-1]
        # ttt /= np.sum(ttt, axis=1, keepdims=True)
        if as_dict:
            cos_lab = [f"C{i}" for i in range(len(cosmics))]
            return {k: t for k, t in zip([*cos_lab, *self.labels], ttt)}
        return ttt

    def variable_moving_gauss_filter(self, resolution):
        """Calculate the convolusion matrix to fold a simulated spectrum with."""
        return _variable_moving_gauss_filter_impl(
            self.edges,
            self.centers,
            self.widths,
            self.templates,
            self.resolution(resolution),
            self.precision,
        )

    def calc_fwhm_matrix(self, resolution):
        """Calculate the convolusion matrix to fold a simulated spectrum with."""
        kernel = np.exp(
            -0.5
            * (self.centers[:, None] - self.centers[None, :]) ** 2
            / self.resolution(resolution)**2
        )
        kernel /= np.sum(kernel, axis=0)
        return kernel

    def remove_templates(self, labels):
        for l in labels:
            if l in self.labels:
                idx = self.labels.index(l)
                del self.labels[idx]
                self.templates = np.delete(self.templates, idx, axis=0)
