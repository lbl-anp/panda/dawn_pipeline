import numpy as np
import h5py
import os
import becquerel as bq

def rebin_fast(in_spectra, in_edges, out_edges):
    return bq.core.rebin._rebin_interpolation(
        in_spectra,
        in_edges,
        out_edges[:-1],
        np.zeros_like(in_spectra, dtype=np.float64),
    )


def string_as_int_or_float(arg):
    try:
        return int(arg)
    except ValueError:
        return float(arg)


DATA_NAME_MAP = {
    "rad": [
        ("/dbaserh/listmode/ts_det", "/dbaserh/listmode/value"),
        ("/listmode/raw/ts_bag", "/listmode/raw/channel"),
        ("/listmode/raw/ts_det", "/listmode/raw/channel"),
    ],
    "temperature": [
        ("/bme280/temperature/header_stamp", "/bme280/temperature/temperature"),
        ("/env/ts_bag", "/env/temperature"),
        ("/env/temp_ts_enclos", "/env/temp_val_enclos")
    ],
    "humidity": [
        ("/bme280/humidity/header_stamp", "/bme280/humidity/relative_humidity"),
        ("/env/ts_bag", "/env/humidity")
    ],
    "pressure": [
        ("/bme280/pressure/header_stamp", "/bme280/pressure/fluid_pressure"),
        ("/env/ts_bag", "/env/pressure")
    ],
    "rain": [
        ("/rain/ts_bag", "/rain/accumulated"),
        ("/env/rain_ts", "/env/rain_val")
    ]
}


def load_data_from_path(
    path,
    channel_edges,
    time_granularity,
    first=None,
    last=None,
    calib=None,
    tedges=None,
):
    files = os.listdir(path)
    files = sorted(files)
    _files = []
    for fn in files:
        if ".h5" in fn:
            _files.append(os.path.join(path, fn))
    files = _files[first:last]
    assert files, f"There are not enough h5 files in {path}"
    print(f"Reading {files}")

    if tedges is None:
        tmin, tmax = np.inf, 0
        for f in files:
            with h5py.File(f, "r") as h5f:
                for name in DATA_NAME_MAP["rad"]:
                    try:
                        tmin = min(tmin, h5f[name[0]][0])
                        tmax = max(tmax, h5f[name[0]][-1])
                        break
                    except KeyError:
                        continue
        print("Create time dimension with", tmin, tmax, time_granularity)
        tedges = np.arange(tmin, tmax, time_granularity)

    rad, _, _ = np.histogram2d([], [], bins=[tedges, channel_edges])
    temp = [
        np.histogram([], bins=tedges, weights=[])[0],
        np.histogram([], bins=tedges)[0],
    ]
    humi = [
        np.histogram([], bins=tedges, weights=[])[0],
        np.histogram([], bins=tedges)[0],
    ]
    pres = [
        np.histogram([], bins=tedges, weights=[])[0],
        np.histogram([], bins=tedges)[0],
    ]
    rain = [
        np.histogram([], bins=tedges, weights=[])[0],
        np.histogram([], bins=tedges)[0],
    ]
    for f in files:
        print(f)
        with h5py.File(f, "r") as h5f:

            ts_det, values = None, None
            for name in DATA_NAME_MAP["rad"]:
                try:
                    ts_det = h5f[name[0]][:]
                    values = (
                        np.random.rand(len(h5f[name[1]]))
                        + h5f[name[1]][:]
                    )
                    break
                except KeyError:
                    continue
            assert ts_det is not None and values is not None, "Could not find any rad data in h5"

            for data, key in zip([temp, humi, pres, rain], ["temperature", "humidity", "pressure", "rain"]):
                for name in DATA_NAME_MAP[key]:
                    try:
                        data[0] += np.histogram(
                            h5f[name[0]][:],
                            weights=h5f[name[1]][:],
                            bins=tedges,
                        )[0]
                        data[1] += np.histogram(
                            h5f[name[0]][:], bins=tedges
                        )[0]
                        break
                    except KeyError:
                        continue

            if calib is not None:
                values = calib(values, ts_det)
            rad += np.histogram2d(ts_det, values, bins=[tedges, channel_edges])[0]

    temp[1][temp[1] < 1.0] = 1.0
    humi[1][humi[1] < 1.0] = 1.0
    pres[1][pres[1] < 1.0] = 1.0
    rain[1][rain[1] < 1.0] = 1.0
    return {
        "temperature": temp[0] / temp[1],
        "humidity": humi[0] / humi[1],
        "pressure": pres[0] / pres[1],
        "rain": rain[0] / rain[1],
        "radiation": rad,
        "time": (tedges[:-1] + tedges[1:]) * 0.5,
        "channels": (channel_edges[:-1] + channel_edges[1:]) * 0.5,
        "tedges": tedges,
        "cedges": channel_edges,
        "files": files,
    }
