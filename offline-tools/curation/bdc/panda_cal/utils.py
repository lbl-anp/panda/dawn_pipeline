import numpy as np


def sqrt_n_bins(nmin, nmax, nbins, **kwargs):
    """Returns quadratically-spaced energy bins (edges).

    Parameters
    ----------

    Outputs
    ----------
    """
    # check arguments
    assert nmin >= 0
    assert nmax > nmin
    assert nbins >= 1
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins + 1, **kwargs)
    bin_edges = bin_edges ** 2
    return bin_edges, (bin_edges[1:] + bin_edges[:-1]) * 0.5
