#
# Script launched from daily crontab to pull data from beehive,
# run sanity checks, convert rad+env ROS bags to HDF5 files,
# and create daily collection catalog.
# Data is always pulled for the previous day.
#
# Requires special conda panda_curation env to be activated.
#

# generic
import logging
from datetime import datetime
from dateutil import relativedelta
import os
from copy import deepcopy
import json
import sage_data_client
import subprocess
import pathlib
import time
# PANDA
from sanity import Sanitizer
from bag2bdch5 import Converter
# fix issue with rospy logging interference
import importlib
importlib.reload(logging)
# args
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--date', type=str)
parser.add_argument('--nodes', nargs='+', type=str)
args = parser.parse_args()

# setup logging
LOG = logging.getLogger()
LOG.setLevel(logging.DEBUG)
LOG_FORMATTER = logging.Formatter('%(asctime)s[%(levelname)s] %(funcName)s: %(message)s')

BDC_DIR = "/clusterfs/bdc_redux/register_data/PANDA/Chicago_operation"
# Deployed nodes
if args.nodes:
    NODES = args.nodes
else:
    NODES = ["W01A", "W01C",
             "W059", "W05A", "W05B", "W05C", "W05D", "W05E",
             "W072", "W073", "W074", "W075", "W076", "W077"]
NODES_LATLON = {'W022': (0., 0.),
                'W01A': (37.87568296320878, -122.25030483738881),
                'W01C': (41.88452644522827, -87.62460057836556),
                'W059': (41.880798782501344, -87.62787947725498),
                'W05A': (41.88918080948867, -87.63241563638988),
                'W05B': (41.88676494472552, -87.63095765266233),
                'W05C': (41.88312765231458, -87.62932260591538),
                'W05D': (41.88455003362158, -87.62797295963482),
                'W05E': (41.71769425053989, -87.98290531739545),
                'W072': (41.86673872385933, -87.6665645143102),
                'W073': (41.88315300556273, -87.62765984093555),
                'W074': (41.88929390665504, -87.63122288140154),
                'W075': (41.88317977577514, -87.63373999393055),
                'W076': (41.88461142499264, -87.62937943517525),
                'W077': (41.88675294995436, -87.63235008038846)}
CATALOG_TEMPLATE = {
    "SchemaVersion": "1.4bdc",
    "ProjectInfo": {"ID": "PANDA / 8"},
    "CollectionInfo": {"ID": ""},
    "SubmitterInfo": {"ID": "nabgrall"},
    "AuthorsList": [],
    "TimeInfo": {
        "TimeExtents": []
    },
    "LocationInfo": {
        "LongitudeList": [],
        "LatitudeList": [],
        "GeodeticDatum": "",
        "Description": ""
    },
    "DataInfo": {
        "POCInfo": {"Name": "Nico Abgrall", "Lab":"LBNL"},
        "DataType": "Collection",
        "DataLevel": "",
        "DomainList": {
            "Primary": ["Radiation"],
            "Secondary": ["Video", "Lidar"]
        },
        "Instrument": "",
        "Description": "",
        "DataFormat": "ROS bag, HDF5",
        "ParserCodeFile":"Null"
    },
    "Comment": "",
    "FileInfo": {"FileList": []}
}
TASKS = ["panda-rosbag-radenv",
         "panda-camera-sched", "panda-camera-trigg",
         "panda-lidar-sched", "panda-lidar-trigg"]

# set dates for collection and pull range
if args.date:
    date = datetime.strptime(args.date, "%Y-%m-%d")
else:
    date = datetime.today()
pull_from_date = date - relativedelta.relativedelta(days=1)
pull_to_date = date
coll_from_date = pull_from_date
coll_to_date = coll_from_date + relativedelta.relativedelta(months=1)

# pull down data from beehive and process radenv data
for node in NODES:
    #
    # prep collection directory
    #
    start = time.time()
    coll_name = "PANDAWN_" + node + '_' + coll_from_date.strftime("%Y_%m")
    coll_dir = os.path.join(BDC_DIR, node, coll_name)
    if not os.path.exists(coll_dir):
        # set permissions/owner for ingestion
        os.makedirs(coll_dir)
        os.chmod(coll_dir, 0o775)
        os.chown(coll_dir, uid=-1, gid=800)
        # set up log dir for collection
        LOG_DIR = os.path.join(coll_dir, "Log")
        os.makedirs(LOG_DIR)
    #
    # append to collection log file
    #
    logfile = os.path.join(coll_dir, "Log", coll_name+".log")
    handler = logging.FileHandler(logfile)
    handler.setLevel(logging.INFO)
    handler.setFormatter(LOG_FORMATTER)
    LOG.addHandler(handler)
    LOG.info(f">>> Logging to collection {coll_name}, on {date.strftime('%Y-%m-%d')}")
    #
    # create daily catalog
    #
    catalog = deepcopy(CATALOG_TEMPLATE)
    catalog["CollectionInfo"]["ID"] = coll_name
    catalog["TimeInfo"]["TimeExtents"] = [coll_from_date.strftime("%Y-%m")+"-01T00:00:00.000000",
                                          coll_to_date.strftime("%Y-%m")+"-01T00:00:00.000000"]
    catalog["LocationInfo"]["LongitudeList"] = [NODES_LATLON[node][1]]
    catalog["LocationInfo"]["LatitudeList"] = [NODES_LATLON[node][0]]
    #
    # run over tasks
    #
    filelist = []
    for task in TASKS:
        # downlaod task URLs
        LOG.info(f"Pulling files for task: {task}")
        # query
        df = sage_data_client.query(
            start=pull_from_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
            end=pull_to_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
            filter={
                "name": "upload",
                "vsn": node,
                "task": task
            })
        if not df.empty:
            urls_file = os.path.join(coll_dir, "Log", "urls-"+task+".txt")
            df.value.to_csv(urls_file, index=False, header=False)
            subprocess.check_call(["wget", "--user=***", "--password=***",
                                   "--directory-prefix="+coll_dir,
                                   "-nd", "-r", "-N", "-c",  "-i", urls_file])
            if task == "panda-rosbag-radenv":
                radenv_bags = []
                for url in  open(urls_file, 'r'):
                    radenv_bags.append(coll_dir + '/' + os.path.basename(url.strip()))
            else:
                for url in  open(urls_file, 'r'):
                    filelist.append(os.path.basename(url.strip()))     
        else:
            LOG.warning(f"No files available for task: {task}")
            continue
        # sanity checks and conversion to hdf5 for radenv data
        if task == "panda-rosbag-radenv":
            LOG.info(f"Processing radenv data")
            sanitizer = Sanitizer()
            converter = Converter()
            for bag in radenv_bags:
                # sanity checks
                checked_bag = sanitizer.check(bag)
                # conversion
                if checked_bag is not None:
                    filelist.append(os.path.basename(checked_bag))
                    h5file = converter.to_bdch5(checked_bag, node=node)
                    if h5file is not None:
                        filelist.append(os.path.basename(h5file))
    #
    # append file list to catalog
    #
    catalog["FileInfo"]["FileList"] = filelist
    jsonfile = os.path.join(coll_dir, coll_name + '_' + pull_from_date.strftime('%d') +".json")
    LOG.info(f"writing daily catalog {jsonfile}")
    with open(jsonfile, "w") as outfile:
        json.dump(catalog, outfile, indent=2)
    stop = time.time()
    LOG.info(f"daily pull done in {(stop - start)}s")
    LOG.removeHandler(handler)

