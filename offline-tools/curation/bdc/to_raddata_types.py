import numpy as np
from ros_msgs._StructuredArrayField import *
from ros_msgs._DTypes import *
from raddata_types import Scalars, Arrays, RadData

#
# Conversion from ROS msg to raddata_types
#
DTYPE_INT_TO_NP = {
    DTypes.BOOL: np.dtype('bool'),
    DTypes.INT8: np.dtype('int8'),
    DTypes.INT16: np.dtype('int16'),
    DTypes.INT32: np.dtype('int32'),
    DTypes.INT64: np.dtype('int64'),
    DTypes.UINT8: np.dtype('uint8'),
    DTypes.UINT16: np.dtype('uint16'),
    DTypes.UINT32: np.dtype('uint32'),
    DTypes.UINT64: np.dtype('uint64'),
    DTypes.FLOAT32: np.dtype('float32'),
    DTypes.FLOAT64: np.dtype('float64'),
}

def to_numpy_dtype(arg):
    return np.dtype(
        [
            (saf.name, DTYPE_INT_TO_NP[saf.dtype]) +
            (() if saf.count == 1 else (saf.count, )) for saf in arg
        ]
    )

def to_RadData(msg):
    scalars, arrays = None, None
    if msg.scalars.length:
        dtype = to_numpy_dtype(msg.scalars.fields)
        if len(dtype) != 0:
            scalars = Scalars.from_array(np.frombuffer(msg.scalars.data, dtype=dtype))
    if msg.arrays.length:
        dtype = to_numpy_dtype(msg.arrays.fields)
        if len(dtype) != 0:
            arrays = Arrays.from_array(np.frombuffer(msg.arrays.data, dtype=dtype))
    return RadData(scalars=scalars, arrays=arrays)


