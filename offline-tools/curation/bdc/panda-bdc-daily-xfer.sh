#!/bin/bash

conda run -n panda_curation python3 /home/nabgrall/PANDA/Curation/panda-bdc-daily-pull.py
# Note: need to assure that following script is called at the
# same date the previous one was
python3 /home/nabgrall/PANDA/Curation/panda-bdc-daily-ingest.py
