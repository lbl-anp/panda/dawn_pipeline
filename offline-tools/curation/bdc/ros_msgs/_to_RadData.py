from ._RadData import *
from ._to_StructuredArray import *

#
# Conversion from raddata_types to ROS msg
#
def to_RadData_ROS_msg(arg):
    scalars = NPArray_to_StructuredArray(arg.scalars.to_array())
    arrays = NPArray_to_StructuredArray(arg.arrays.to_array())
    return RadData(scalars=scalars, arrays=arrays)
                                
