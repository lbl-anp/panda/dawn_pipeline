import numpy as np
from ._DTypes import *
from ._StructuredArray import *
from ._StructuredArrayField import *

#
# Conversion to ROS msg
#
DTYPE_INT_TO_NP = {
    DTypes.BOOL: np.dtype('bool'),
    DTypes.INT8: np.dtype('int8'),
    DTypes.INT16: np.dtype('int16'),
    DTypes.INT32: np.dtype('int32'),
    DTypes.INT64: np.dtype('int64'),
    DTypes.UINT8: np.dtype('uint8'),
    DTypes.UINT16: np.dtype('uint16'),
    DTypes.UINT32: np.dtype('uint32'),
    DTypes.UINT64: np.dtype('uint64'),
    DTypes.FLOAT32: np.dtype('float32'),
    DTypes.FLOAT64: np.dtype('float64'),
}

DTYPE_NP_TO_INT = {v: k for k, v in DTYPE_INT_TO_NP.items()}   

def NPArray_to_StructuredArray(arg):
    fields = []
    for _name in arg.dtype.names:
        field = arg.dtype.fields[_name][0]
        if field.subdtype is None:
            # scalar
            dtype = DTYPE_NP_TO_INT[field]
            count = 1
        else:
            # tuple
            dtype = DTYPE_NP_TO_INT[field.subdtype]
            assert len(field.shape) == 1
            count = field.shape[0]
        fields.append(
            StructuredArrayField(
                name=_name, dtype=dtype, count=count
            )
        )
    return StructuredArray(
        fields=fields,
        data=arg.tobytes(),
        length=arg.size)
