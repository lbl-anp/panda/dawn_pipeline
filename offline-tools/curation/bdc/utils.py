# ROS
from rosbag import Bag
import yaml
# generic
import numpy as np
from collections import namedtuple
from datetime import datetime

#
# ROS bags
#
BagInfo = namedtuple("BagInfo", "beg end duration")
def bag_info(bag):
    info_dict = yaml.safe_load(bag._get_yaml_info())
    beg_ts = info_dict['start']
    end_ts = info_dict['end']
    beg = datetime.fromtimestamp(beg_ts).strftime("%m/%d/%Y, %H:%M:%S")
    end = datetime.fromtimestamp(end_ts).strftime("%m/%d/%Y, %H:%M:%S")
    duration = (end_ts - beg_ts) / 3600
    return BagInfo(beg=beg, end=end, duration=duration)

#
# binning
#
def sqrt_n_bins(nmin=0, nmax=3000, nbins=128):
    """ Returns quadratically-spaced energy bins."""
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins+1)
    bin_edges = bin_edges**2
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    return bin_edges, bin_centers

#
# helper classes
#
RadMsgFields = namedtuple("RadMsgFields", "tf tl channels")
class RadMsgReader:
    def __init__(self, calibrated=True):
        if calibrated:
            self.dtype = [("det_id", np.dtype("uint8")),
                          ("timestamp_det", np.dtype("float64")),
                          ("channel", np.dtype("float32"))]
        else:
            self.dtype = [("det_id", np.dtype("uint8")),
                          ("timestamp_det", np.dtype("float64")),
                          ("channel", np.dtype("uint32"))]
        
    def read(self, msg):
        data = np.frombuffer(msg.message.arrays.data, self.dtype)
        if data.size == 0: return None
        return RadMsgFields(tf=data[0]["timestamp_det"],
                            tl=data[-1]["timestamp_det"],
                            channels=data[:]["channel"])
    
RadCalibMsgFields = namedtuple("RadCalibMsgFields", "ts gain")
class RadCalibMsgReader:
    def __init__(self):
        self.dtype = [("ts", np.dtype("float64")),
                      ("tf", np.dtype("float64")),
                      ("lt", np.dtype("float64")),
                      ("thr_lower", np.dtype("float64")),
                      ("thr_upper", np.dtype("float64")),
                      ("fmin", np.dtype("float64")),
                      ("pval", np.dtype("float64")),
                      ("res", np.dtype("float64")),
                      ("plaw", np.dtype("float64")),
                      ("gain", np.dtype("float64")),
                      ("sat", np.dtype("float64")),
                      ("off", np.dtype("float64")),
                      ("w_cosmics", np.dtype("float64")),
                      ("w_511", np.dtype("float64")),
                      ("w_K", np.dtype("float64")),
                      ("w_U", np.dtype("float64")),
                      ("w_Th", np.dtype("float64")),
                      ("w_Rn", np.dtype("float64")),
                      ("alarm", np.dtype("bool"))]

    def read(self, msg):
        data = np.frombuffer(msg.message.scalars.data, self.dtype)
        return RadCalibMsgFields(ts=data["ts"], gain=data["gain"])

RadDataBinMode = namedtuple("RadDataBinMode", "ts lt spectrum")
class RadDataBinModeAggregator:
    def __init__(self, int_time=1., nbins=128, bin_edges=None, calibrated=True):
        self.int_time = int_time
        self.nbins = nbins
        self.bin_edges = bin_edges
        self.calibrated = calibrated
        self.live_time = 0.
        self.spectrum = np.zeros(nbins)
        self.ts = 0.
        self.ts_agg = 0.
        self.radData = None
        
    def aggregate(self, tf, tl, channels):
        if tf < self.ts:
            print(f"error in rad series timestamps: current {tf}, prev {self.ts}")
            return False
        # This is a temp. fix related to the dbaserh connection check we do every minute.
        # Affects aggregation for inttime close to 1.
        # Removed the check from deployed plugin from 01/19/24 on
        #
        #if (tl - tf) > 1.:
        #    return False
        #
        self.ts = tf
        if self.live_time == 0.:
            self.ts_agg = tf
        self.live_time += tl - tf
        if not self.calibrated:
            # randomize ADC channels
            channels = np.random.rand(len(channels)) + channels
        self.spectrum += np.histogram(channels, bins=self.bin_edges)[0]
        if self.live_time >= self.int_time:
            self.radData = RadDataBinMode(ts=self.ts_agg, lt=self.live_time, spectrum=self.spectrum)
            # reset aggregator
            self.live_time = 0.
            self.spectrum = np.zeros(self.nbins)
            return True
        else:
            return False

    def data(self):
        return self.radData

class RadDataBinModeRollingAggregator:
    def __init__(self, int_time=1., step_time=1., nbins=128, bin_edges=None, calibrated=True):
        self.int_time = int_time
        self.step_time = step_time
        assert (int_time % step_time) == 0, f"The int_time {int_time} must be a multiple of step_time {step_time}"
        self.nbins = nbins
        self.bin_edges = bin_edges
        self.calibrated = calibrated
        self.n_buffers = int(int_time / step_time)
        self.buffer_idx = []
        self.live_time = np.zeros(self.n_buffers)
        self.spectrum = np.zeros((self.n_buffers, nbins))
        self.ts_agg = np.zeros(self.n_buffers)
        self.ts = 0.
        self.radData = None
        
    def aggregate(self, tf, tl, channels):
        if tf < self.ts:
            print(f"error in rad series timestamps: current {tf}, prev {self.ts}")
            return False
        self.ts = tf
        if len(self.buffer_idx) < self.n_buffers:
            self.buffer_idx = [i for i in range(int(self.live_time[0] / self.step_time) + 1)]
        for idx in self.buffer_idx:
            if self.live_time[idx] == 0.:
                self.ts_agg[idx] = tf
            self.live_time[idx] += tl - tf
            if not self.calibrated:
                # randomize ADC channels
                channels = np.random.rand(len(channels)) + channels
            self.spectrum[idx] += np.histogram(channels, bins=self.bin_edges)[0]
        if self.live_time[0] >= self.int_time:
            self.radData = RadDataBinMode(ts=self.ts_agg[0], lt=self.live_time[0], spectrum=self.spectrum[0])
            # roll aggregator
            self.ts_agg = np.roll(self.ts_agg, self.n_buffers - 1, axis=0)
            self.live_time = np.roll(self.live_time, self.n_buffers - 1, axis=0)
            self.spectrum = np.roll(self.spectrum, self.n_buffers - 1, axis=0)
            # reset aggregator
            self.live_time[-1] = 0.
            self.spectrum[-1] = np.zeros(self.nbins)
            return True
        else:
            return False

    def data(self):
        return self.radData
