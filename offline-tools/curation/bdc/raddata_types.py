import abc
from abc import ABC
from collections.abc import MutableMapping
import numpy as np

scalar_like = (
    bool, str, bytes, bytearray, int, float, np.bool_, np.integer, np.floating
)
array_like = tuple, list, np.ndarray

#
# base types
#
class DataDict(ABC, MutableMapping):
    @staticmethod
    def _transform_key(key):
        return str(key)

    def __init__(self, *args, **kwargs):
        self._data = dict()
        self._dtype = dict()
        self.update(dict(*args, **kwargs))

    def __getitem__(self, key):
        return self._data[self._transform_key(key)]

    def __setitem__(self, key, value):
        if isinstance(value, np.ndarray):
            dtype = value.dtype
        else:
            dtype = np.dtype(type(value))
        if dtype.kind in {'U', 'S', 'O'}:
            raise NotImplementedError(
                'Unsupported dtype in DataDict: ({}) {}'.format(dtype, value)
            )
        self._dtype[self._transform_key(key)] = dtype
        self._data[self._transform_key(key)] = value

        setattr(self, key, self[key])

    def __delitem__(self, key):
        del self._data[self._transform_key(key)]
        del self._dtype[self._transform_key(key)]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __eq__(self, other):
        if isinstance(other, (DataDict, dict)):
            for k in self:
                if k not in other:
                    return False
                if not is_close(self[k], other[k]):
                    return False
            return True
        else:
            raise TypeError(
                'Cannot compare {} to {}'.format(type(self), type(other))
            )

    def keys(self):
        keys = super().keys()
        return tuple(keys)

    def __str__(self):
        return '{}:'.format(self.__class__.__name__)

    # TODO remove once merged with stark
    def __repr__(self):
        """Alias for __str__."""
        return self.__str__()

    @property
    def data(self):
        return self._data

    @property
    def dtype(self):
        return self._dtype
        
    def add_data(self, **entries):
        self.update(entries)

    def rename(self, current, new):
        if new in self:
            raise KeyError(
                f'Cannot rename {current} to {new}, {new} already exists.'
            )
        self[new] = self[current]
        del self[current]
        return self

    def copy(self):
        return self.__class__(**deepcopy(self._data))


    @classmethod
    @abc.abstractmethod
    def from_array(cls, arr):
        assert isinstance(arr, np.ndarray)
        assert arr.dtype.names is not None

    def to_array(self):
        arr = np.empty(
            (self.data_length, ),
            dtype=np.dtype([(k, self.dtype[k]) for k in self]),
        )
        for k in self:
            arr[k][:] = self[k]
        return arr

class Scalars(DataDict):
    def __setitem__(self, key, value):
        assert isinstance(value, scalar_like)
        super().__setitem__(key, value)

    @classmethod
    def from_array(cls, arr):
        super().from_array(arr)
        assert len(arr) == 1
        return cls(**{k: arr[k][0] for k in arr.dtype.names})

    @property
    def data_length(self):
        return 1

    @property
    def nbytes(self):
        return self.to_array().nbytes

class Arrays(DataDict):
    def __setitem__(self, key, value):
        assert isinstance(value, array_like)
        value = np.asarray(value).flatten()
        if self.num_fields > 0:
            assert len(value) == len(self)
        super().__setitem__(key, value)

    @classmethod
    def from_array(cls, arr):
        super().from_array(arr)
        return cls(**{k: arr[k] for k in arr.dtype.names})

    def __len__(self):
        return self.data_length

    @property
    def data_length(self):
        if len(self._data) == 0:
            return 0
        else:
            return len(next(iter(self._data.values())))

    @property
    def num_fields(self):
        return len(self.keys())

    @property
    def nbytes(self):
        return sum([arr.nbytes for arr in self.values()])

class BaseMode(ABC):
    def __init__(self, scalars=None):
        if isinstance(scalars, Scalars):
            self._scalars = scalars
        elif scalars is None or isinstance(scalars, dict):
            self._scalars = Scalars()
            if isinstance(scalars, dict):
                self.add_scalars(**scalars)
        else:
            raise TypeError('Cannot set scalars as {}'.format(scalars))

    @property
    def scalars(self):
        return self._scalars
    
    def add_scalars(self, **kwargs):
        scalars = dict(**kwargs)
        self._scalars.add_data(**{k: v for k, v in scalars.items() if v is not None})
        
class RadData(BaseMode):
    def __init__(self, scalars=None, arrays=None):
        super().__init__(scalars=scalars)
        if isinstance(arrays, Arrays):
            self._arrays = arrays
        elif arrays is None or isinstance(arrays, dict):
            self._arrays = Arrays()
            if isinstance(arrays, dict):
                self.add_arrays(**arrays)
        else:
            raise TypeError('Cannot set arrays as {}'.format(arrays))
        
    @property
    def arrays(self):
        return self._arrays

    def add_arrays(self, **kwargs):
        arrays = dict(**kwargs)
        self._arrays.add_data(**{k: v for k, v in arrays.items() if v is not None})

