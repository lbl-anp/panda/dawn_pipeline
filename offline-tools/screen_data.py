import os
import argparse
from background import BkgDataMgr

"""
Usage:
 Building background models
 - python3 screen_data.py --node --build_models --files <path-to-h5files>/*.h5
 Screening for alarms
 - python3 screen_data.py --node --bkg_components <path-to-h5file>/bkg_component.h5
                          --bkg_models <path-to-h5files>/*.h5
                          --files <path-to-h5files>/*.h5
"""

print('Processing data')
parser = argparse.ArgumentParser()
parser.add_argument('--node', type=str)
parser.add_argument('--build_models', action='store_true')
parser.add_argument('--bkg_components', type=str)
parser.add_argument('--bkg_models', nargs='+', type=str)
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()

if not args.node:
    print("Need to specify the node VSN")
else:
    mgr = BkgDataMgr(args.node.upper())
    if args.build_models is True:
        mgr.initFilters()
        mgr.buildModels(args.files)
    else:
        mgr.getBkgComponents(args.bkg_components)
        mgr.getBkgModels(args.bkg_models)
        mgr.initFilters()
        mgr.screenForAlarms(args.files)
    print('Processing data done')    

