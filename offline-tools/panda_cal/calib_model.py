import numpy as np
from scipy.optimize import root_scalar
from scipy.interpolate import interp1d
import inspect

# 4x4 inch curve (based on https://gitlab.com/lbl-anp/GRPP/-/blob/master/grpp/core/models.py
lightyield_energies = np.array(
                [
                    22.390,
                    35.917,
                    59.992,
                    88.146,
                    133.327,
                    192.335,
                    277.460,
                    362.690,
                    509.310,
                    657.003,
                    928.561,
                    1320.840,
                    2615.0,
                ]
            )
lightyield_response = (
    np.array(
        [
            1.605,
            1.532,
            1.580,
            1.578,
            1.507,
            1.477,
            1.447,
            1.434,
            1.424,
            1.416,
            1.407,
            1.400,
            1.400,
        ]
    )
    / 1.400
)


def _numpify(values):
    values = np.asarray(values)
    if values.ndim == 0:
        return True, values[np.newaxis]
    return False, values


class CalibModel():
    def __init__(self, model=None, func=None, inv=None, x=None, y=None):
        """A class that defines a calibration function (and it's inverse).

        Parameters
        ----------
        model : str, optional
            One of the default models ("gain", "gain_saturation",
            "gain_saturation_offset", "lightyield", "power3"). Doesn't
            need any of the other parameters if used,
            by default None
        func : functional, optional
            The forward function, if custom, by default None
        inv : functional, optional
            The inverse function, by default None
        x : np.array(1d), optional
            An array with energy values (used for lightyield model),
            by default None
        y : np.array(1d), optional
            An array with response values (used for lightyield model),
            by default None
        """
        if model is not None:
            if model == "gain":
                assert func is None
                def func(a, gain):
                    return a / gain
                def inv(a, gain):
                    return a * gain
            elif model == "gain_saturation":
                assert func is None
                def func(a, gain, saturation):
                    return a / (gain - saturation * a)
                def inv(a, gain, saturation):
                    return (gain * a) / (1.0 + saturation * a)
            elif model == "gain_saturation_offset":
                assert func is None
                def func(a, gain, saturation, offset):
                    return (a - offset) / (gain - saturation * (a - offset))
                def inv(a, gain, saturation, offset):
                    return (gain * a) / (1.0 + saturation * a) + offset
            elif model == "lightyield":
                if x is None:
                    x = lightyield_energies
                if y is None:
                    y = lightyield_response
                self._e2l = interp1d(x=x, y=x * y, fill_value="extrapolate")
                self._l2e = interp1d(x=x * y, y=x, fill_value="extrapolate")
                def func(a, gain, saturation, offset):
                    return self._l2e((a - offset)[()] / (gain - saturation * (a - offset)))
                def inv(a, gain, saturation, offset):
                    z = self._e2l(a)[()]
                    return (gain * z) / (1.0 + saturation * z) + offset
            elif model == "power3":
                assert func is None
                def func(a, p0, p1, p2, p3):
                    return  p0 + p1 * a + p2 * a ** 2 + p3 * a ** 3
            else:
                raise ValueError(f"'{model}' is not a valid model")
        assert func is not None, "func needs to be defined at this point"
        self._func = func
        self._inv = inv
        if inv is not None:
            inv_len = len(inspect.signature(self._inv).parameters) - 1
            inv_keys = list(inspect.signature(self._func).parameters.keys())[1:]
            assert inv_len == self.__len__()
            assert inv_keys == self.keys()

    def __call__(self, values, *args, **kwargs):
        """Calls the calibration. both values and the calibration parameters
        (either provided as args or kwargs) can be np.arrays."""
        is_scalar, values = _numpify(values)
        ret_val = self._func(values, *args, **kwargs)
        if is_scalar:
            return ret_val[0]
        return ret_val

    def __len__(self):
        """Returns the number of parameters of the calibration function."""
        return len(inspect.signature(self._func).parameters) - 1

    def keys(self):
        """Returns the names of the parameters of the calibration function."""
        return list(inspect.signature(self._func).parameters.keys())[1:]

    def inv(self, values, *args, bracket=[0, 1e4], **kwargs):
        """Inverse of the calibration. By default slow and it's use
        should be prevented. Can be overwritten with a functional form
        to be faster.
        """
        is_scalar, values = _numpify(values)
        if self._inv is not None:
            ret_val = self._inv(values, *args, **kwargs)
        else:
            ret_val = np.array(
                [
                    root_scalar(
                        lambda v, k:  self(v, **k) - v,
                        args=(v, kwargs),
                        bracket=bracket,
                    ).root
                    for v in values
                ]
            )
        if is_scalar:
            return ret_val[0]
        return ret_val

    @staticmethod
    def nearest_index(to_be_searched, values, sorted=True):
        """Helper method to extract matching indices for a set of values.
        This is useful to match listmode timestamps to calibration time bin centers.
        """
        to_be_searched = np.atleast_1d(to_be_searched)
        is_scalar, values = _numpify(values)
        if sorted is False:
            argsort = np.argsort(to_be_searched)
        else:
            argsort = None
        indices = np.searchsorted(to_be_searched[1:-1], values, side="left", sorter=argsort) + 1
        indices[
            np.abs(to_be_searched[indices] - values) > np.abs(to_be_searched[indices - 1] - values)
        ] -= 1
        if is_scalar:
            return indices[0]
        return indices