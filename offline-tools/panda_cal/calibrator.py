import numpy as np
import becquerel as bq
import scipy
import nlopt
from .templates import Templates
from .calib_model import CalibModel

from scipy.optimize import nnls

try:
    import poisson_gof as pg
except:
    pg = None
    pass
try:
    import numdifftools as nd
except:
    nd = None
    pass


def rebin_fast(in_spectra, in_edges, out_edges):
    return bq.core.rebin._rebin_interpolation(
        in_spectra,
        in_edges,
        out_edges[:-1],
        np.zeros_like(in_spectra, dtype=np.float64),
    )


# def mlem(weights, response, signal, iterations=100, history=None):
#     sensitivity = np.sum(response, axis=1)
#     for i in range(iterations):
#         weights *= response @ (signal / (weights @ response)) / sensitivity
#         if history is not None:
#             history.append(np.array(weights))
#     return weights


# this is considerably faster than the update rule and very accurate
# the precision with i < 4 is less than 1e-8 in deviance
def mlem(weights, response, signal, iterations=100, history=None):
    i = 0
    while i < 4:
        err = np.sqrt(np.maximum(weights @ response, 1))
        weights = nnls((response / err).T, signal / err)[0]
        i += 1
    return weights


class TemplatesCalibrator:
    def __init__(
        self, templates, opt=None, calib_func=None, mlem_iter=500
    ):
        self.templates = templates
        self.weight_guess = None
        self.correct_templates = True
        self.min_norm = 1
        self._min_norms = {
            1: lambda x: np.sum(x),
            2: lambda x: np.sqrt(np.sum(x ** 2)),
            3: lambda x: np.power(np.sum(x ** 3), 1.0 / 3.0),
        }
        self._counter = 0
        self._cutoff = [1, len(self.templates.edges) - 1]
        self._ttt = None
        self._weights = None
        self._last_val = None
        self._data = None
        self._channel_edges = None
        self._min_func = (
            lambda x, grad: self.deviance(
                *self.evaluate(x, self._data, self._channel_edges),
                use_events=self.use_events,
                use_overflow=self.use_overflow,
                use_dof=self.use_dof,
            )
            + self.reg_func(x)
        )
        self._dof = 1
        self._num_free_params = 0

        # permanent parameters (not affected by init function)
        self.reg_func = lambda x: 0.0
        self.mlem_iter = mlem_iter
        self.fast = True
        self.cosmic_comp = 1
        self.res_comp = 1

        # parameters changed by init functions
        self.opt = opt
        self.lower_bounds = None
        self.upper_bounds = None
        self.calib_func = calib_func
        self.use_overflow = False
        self.use_events = True
        self.use_dof = False

    @property
    def cutoff(self):
        return self.templates.edges[self._cutoff[0]], self.templates.edges[self._cutoff[1]]

    @cutoff.setter
    def cutoff(self, cutoff):
        self._cutoff = [1, len(self.templates.edges) - 1]
        assert len(cutoff) == 2
        if cutoff[0] is not None:
            self._cutoff[0] = np.searchsorted(self.templates.edges[1:], cutoff[0], side="right")
        if cutoff[1] is not None:
            self._cutoff[1] = (
                np.searchsorted(self.templates.edges[1:], cutoff[1], side="right") + 1
            )

    @property
    def fit_centers(self):
        centers = self.templates.centers
        centers = centers[self._cutoff[0] : self._cutoff[1]]
        return centers

    @property
    def fit_edges(self):
        edges = self.templates.edges
        edges = edges[self._cutoff[0] : self._cutoff[1] + 1]
        return edges

    def cut_data(self, params, data_bin, channel_edges, cutoff=[None, None]):
        assert self.opt.get_dimension() == len(params), "params is wrong dimension"
        assert len(cutoff) == 2
        cind = [0, len(channel_edges) - 1]
        corrected_edges = self.calib_func(
            channel_edges, *params[self.res_comp + self.cosmic_comp :]
        )
        if cutoff[0] is not None:
            cind[0] = np.searchsorted(corrected_edges[1:], cutoff[0], side="right")
        if cutoff[1] is not None:
            cind[1] = np.searchsorted(corrected_edges[1:], cutoff[1], side="right") + 1
        return (
            data_bin[cind[0] : cind[1]].copy(),
            channel_edges[cind[0] : cind[1] + 1].copy(),
        )

    def evaluate(
        self, params, data_bins, channel_edges, weights=None, mlem_history=None
    ):
        self._counter += 1
        corrected_edges = self.calib_func(
            channel_edges, *params[self.res_comp + self.cosmic_comp :]
        )
        if np.any(np.diff(corrected_edges) < 0) or corrected_edges.size == 0:
            print(
                f"WARNING: Calibration parameters {params[self.res_comp + self.cosmic_comp:]} "
                f"leads to a non monotonic solution, will be skipped."
            )
            return [1], [0], 0
        if self.correct_templates:
            c1, c2, c3, f1, f2, o1, o2 = None, None, None, 1, 1, 0, 0
            if self.cutoff[0] > corrected_edges[0]:
                c1 = np.searchsorted(corrected_edges[1:-1], self.cutoff[0], "right")
                f1 = 1.0 - (self.cutoff[0] - corrected_edges[c1]) / (
                    corrected_edges[c1 + 1] - corrected_edges[c1]
                )
                o1 = np.sum(data_bins[:c1])
            if self.cutoff[1] < corrected_edges[-1]:
                c2 = np.searchsorted(corrected_edges[1:-1], self.cutoff[1], "left")
                f2 = 1.0 - (corrected_edges[c2 + 1] - self.cutoff[1]) / (
                    corrected_edges[c2 + 1] - corrected_edges[c2]
                )
                c3 = c2 + 1
                o2 = np.sum(data_bins[c2:])
            cbins = data_bins[c1:c2].copy()
            if cbins.size == 0:
                print(
                    f"WARNING: Calibration parameters {params[self.res_comp + self.cosmic_comp:]} "
                    f"leads to empty range, will be skipped."
                )
                return [1], [0], 0
            cbins[0] *= f1
            cbins[-1] *= f2
            self._uncorrected_edges = channel_edges[c1:c3]
            self._corrected_edges = corrected_edges[c1:c3]
            self._overflow = (o1, o2)
            self._ttt = self.templates.get_templates(
                params[0 : self.res_comp],
                params[self.res_comp : self.res_comp + self.cosmic_comp],
                new_edges=self._corrected_edges
            )
        else:
            cbins = rebin_fast(
                np.asarray(data_bins, dtype=np.float64),
                corrected_edges,
                np.concatenate((np.array([-np.inf]), self.templates.edges, np.array([np.inf]))).astype(np.float64),
            )
            self._overflow = (cbins[0], cbins[-1])
            cbins = cbins[1:-1]
            cbins = cbins[self._cutoff[0] : self._cutoff[1]]
            self._ttt = self.templates.get_templates(
                params[0 : self.res_comp],
                params[self.res_comp : self.res_comp + self.cosmic_comp],
            )
            self._ttt = self._ttt[:, self._cutoff[0] : self._cutoff[1]]

        evts = np.sum(cbins)
        if weights is None:
            naive_guess = evts / np.array([len(self._ttt)] * len(self._ttt)) / np.sum(self._ttt, axis=1)
            if self.weight_guess is None:
                weight_guess = naive_guess
            else:
                weight_guess = np.maximum(np.array(self.weight_guess), naive_guess * 0.1)
            self._weights = mlem(
                weight_guess,
                self._ttt,
                cbins,
                iterations=self.mlem_iter,
                history=mlem_history,
            )
        else:
            self._weights = np.array(weights)
        sbins = np.sum(self._ttt.transpose() * self._weights, axis=1)
        self._dof = len(sbins) - self._num_free_params
        return cbins, sbins, evts

    def deviance(
        self, cbins, sbins, evts, use_events=True, use_overflow=False, use_dof=False
    ):
        """This is only the deviance if use_events=False, use_overflow=False
        and self.min_norm=1"""
        d_arr = (
            scipy.special.xlogy(cbins, cbins)
            - scipy.special.xlogy(cbins, sbins)
            + sbins
            - cbins
        )
        if use_overflow is True:
            d_arr = np.hstack(
                (
                    d_arr,
                    [
                        scipy.special.xlogy(self._overflow[1], self._overflow[1])
                        - self._overflow[1]
                    ],
                )
            )
        d = self._min_norms[self.min_norm](d_arr)
        if use_events is True:
            d /= evts
        if use_dof is True:
            d /= self._dof
        return d * 2.0

    def get_weights(self, params, data_bins, channel_edges):
        self.evaluate(params, data_bins, channel_edges)
        return self._weights

    def calc_residuals(
        self,
        cbins,
        sbins,
        evts,
        use_overflow=False,
        use_events=False,
        params=None,
        use_dof=False,
    ):
        d_arr = (
            scipy.special.xlogy(cbins, cbins)
            - scipy.special.xlogy(cbins, sbins)
            + sbins
            - cbins
        )
        if use_overflow is True:
            d_arr = np.hstack(
                (
                    d_arr,
                    [
                        scipy.special.xlogy(self._overflow[1], self._overflow[1])
                        - self._overflow[1]
                    ],
                )
            )
        if use_events is True:
            d_arr /= evts
        if use_dof is True:
            d_arr /= self._dof
        d_arr *= 2
        if params is not None:
            d_arr += self.reg_func(params)
        return d_arr


    def init_res_func(self, params=1):
        if params > 2:
            raise NotImplementedError()
        old_res_comp = self.res_comp
        self.res_comp = params

        if self.opt is not None:
            ftol_rel = self.opt.get_ftol_rel()
            maxeval = self.opt.get_maxeval()
            pop = self.opt.get_population()
            npar = self.opt.get_dimension() - self.cosmic_comp - old_res_comp
            self.opt = nlopt.opt(
                self.opt.get_algorithm(), npar + self.cosmic_comp + self.res_comp
            )
            self.opt.set_maxeval(maxeval)
            self.opt.set_ftol_rel(ftol_rel)
            self.opt.set_population(pop)

    def _defaults(self, opt_alg, maxeval, ftol, local, use_events, cal_par, use_dof):
        if local is True:
            opt_alg = nlopt.LN_SBPLX if opt_alg is None else opt_alg
            # maxeval = 0
            self.use_overflow = False
        else:
            opt_alg = nlopt.GN_CRS2_LM if opt_alg is None else opt_alg
            ftol = 0
            self.use_overflow = True
        self.opt = nlopt.opt(opt_alg, cal_par + self.cosmic_comp + self.res_comp)
        self.opt.set_ftol_rel(ftol)
        self.opt.set_maxeval(int(maxeval))
        self.opt.set_population(0)
        self.use_events = use_events
        self.lower_bounds = None
        self.upper_bounds = None
        self.use_dof = use_dof

    def init_gain_optimizer(
        self,
        local=False,
        maxeval=1000,
        ftol=1e-6,
        use_events=True,
        opt_alg=None,
        use_dof=True,
    ):
        self._defaults(opt_alg, maxeval, ftol, local, use_events, 1, use_dof)
        self.opt.set_population(int(maxeval / 10))
        self.calib_func = CalibModel(model="gain")

    def init_full_optimizer(
        self,
        local=True,
        maxeval=10000,
        ftol=1e-6,
        use_events=False,
        opt_alg=None,
        use_dof=True,
    ):
        self._defaults(opt_alg, maxeval, ftol, local, use_events, 3, use_dof)
        self.calib_func = CalibModel(model="gain_saturation_offset")

    def init_lightyield_optimizer(
        self,
        local=True,
        maxeval=10000,
        ftol=1e-6,
        use_events=False,
        response_energies=None,
        response_values=None,
        opt_alg=None,
        use_dof=True,
    ):
        self._defaults(opt_alg, maxeval, ftol, local, use_events, 3, use_dof)
        self.calib_func =  CalibModel(
            model="lightyield",
            x=response_energies,
            y=response_values
        )

    def init_power_optimizer(
        self,
        local=True,
        maxeval=10000,
        ftol=1e-6,
        use_events=False,
        opt_alg=None,
        use_dof=True,
    ):
        self._defaults(opt_alg, maxeval, ftol, local, use_events, 4, use_dof)
        self.calib_func = CalibModel(model="power3")

    def init_no_offset_optimizer(
        self,
        local=True,
        maxeval=10000,
        ftol=1e-6,
        use_events=False,
        opt_alg=None,
        use_dof=True,
    ):
        self._defaults(opt_alg, maxeval, ftol, local, use_events, 2, use_dof)
        self.calib_func = CalibModel(model="gain_saturation")

    def optimize(
        self,
        init_params,
        data_bins,
        channel_edges,
        lower_bounds=None,
        upper_bounds=None,
        init_step=None,
    ):
        data_bins = np.asarray(data_bins, dtype=np.float64)
        channel_edges = np.asarray(channel_edges, dtype=np.float64)
        if lower_bounds is not None:
            self.lower_bounds = lower_bounds
        if upper_bounds is not None:
            self.upper_bounds = upper_bounds
        assert self.opt is not None
        assert self.calib_func is not None
        opt_dim = self.opt.get_dimension()
        if len(init_params) < opt_dim:
            _init_params = np.zeros(opt_dim)
            _init_params[: len(init_params)] = init_params
            init_params = _init_params
        assert self.lower_bounds is not None
        assert self.upper_bounds is not None
        self.opt.set_lower_bounds(self.lower_bounds)
        self.opt.set_upper_bounds(self.upper_bounds)
        self.opt.set_min_objective(self._min_func)
        if init_step is not None:
            self.opt.set_initial_step(init_step)
        self._counter = 0
        self._data = data_bins
        self._channel_edges = channel_edges
        res = self.opt.optimize(init_params)
        self._num_free_params = opt_dim + len(self.templates.templates)
        for i in range(opt_dim):
            if np.isclose(self.lower_bounds[i], self.upper_bounds[i]):
                self._num_free_params -= 1
        self.evaluate(res, data_bins, channel_edges)
        return res, self._weights

    def simulate_spectrum(
        self, params, weights, channel_edges, generator=None, rollover=None
    ):
        if generator is None:
            generator = np.random.default_rng()
        data_bins = np.zeros(len(channel_edges) - 1)
        sbins = self.evaluate(params, data_bins, channel_edges, weights=weights)[1]
        if rollover is not None:
            centers = (self._corrected_edges[1:] + self._corrected_edges[:-1]) * 0.5
            xr = scipy.special.erf((centers - rollover[0]) / (np.sqrt(2) * rollover[1]))
            xr[xr < 0] = 0
            sbins *= xr
        return generator.poisson(sbins), self._uncorrected_edges

    def covariance_matrix(
        self, params, data_bins, channel_edges, init_step=None, weights=None
    ):
        if nd is None:
            raise ImportError("Please install numdifftools.")

        def min_func(params):
            cbins, sbins, evts = self.evaluate(
                params, data_bins, channel_edges, weights=weights
            )
            return np.sum(scipy.special.xlogy(cbins, sbins) - sbins)

        hess = nd.Hessian(min_func, step=init_step)(params)
        return np.linalg.inv(-hess)  # fisher information is minus the hessian

    def p_value(
        self, params, data_bins, channel_edges, weights=None, num_free_params=None
    ):
        """num_free_params should be set, otherwise it is assumed all params are free."""
        if pg is None:
            raise ImportError("Please install poisson_gof.")
        cbins, sbins, _ = self.evaluate(
            params, data_bins, channel_edges, weights=weights
        )
        dev = pg.deviance(cbins, sbins)
        if num_free_params is None:
            num_free_params = len(params) + len(weights)
        return pg.deviance.pvalue(dev, sbins, num_free_params)


class CalibProcessor:
    """A class to simplify processing of a large amount of time sorted spectra."""

    def __init__(
        self,
        calibrator,
        params=[0.1, 0.0, 2.5, None, 0.0, 0.0],
        lower_bounds=[0.04, 0.0, 1.0, None, -1e-4, -20],
        upper_bounds=[0.30, 0.0, 4.0, None, 1e-4, 20],
        cutoff=[350, 2850],
        opt_type="lightyield",
        cosmic_comp=1,
    ):
        """
        Initialize the object. Only a TemplateCalibrator is required for initialization.

        Optional args:
            params: Parameters used to start search, if gain is set to None it will be interfered from spectrum
            lower_bounds: Limits to confine search, if gain is set to None it will be interfered from spectrum
            upper_bounds: Limits to confine search, if gain is set to None it will be interfered from spectrum
            cutoff: lower and upper bound of range where fit is performed
            opt_type: "full" or "lightyield". Last optimizer to be used.
            cosmic_comp: Number of cosmic components
        """
        self.cc = int(cosmic_comp)
        if self.cc == 0:
            if len(params) == 6:
                del params[2]
            if len(lower_bounds) == 6:
                del lower_bounds[2]
            if len(upper_bounds) == 6:
                del upper_bounds[2]

        self.calibrator = calibrator
        self.params = params
        self.weights = None
        self.lower_bounds = lower_bounds
        self.upper_bounds = upper_bounds
        self.cutoff = cutoff
        self.opt_type = opt_type
        self.spect = None
        self.edges = None

    def find_initial_guess(
        self,
        spectrum,
        edges,
        mlem_iter=500,
        fix_offset=False,
        verbose=False,
        gain_opt_exclude_labels=["A", "R", "P", "B"],
        gain_opt_mlem_iter=100,
        no_offset_opt_mlem_iter=100,
    ):
        """
        Performs the initial calibration parameter estimation. It performs a three stage optimization,
        first it searches resolution, cosmics and gain only in a global search, then adds saturation
        and if requested, finally will use an offset, all the later stages with a local search algorithm.

        Note: If lower and upper bounds are set equally the parameter will be fixed. The second resolution parameter
              is forced to zero by default and its bounds need to be adjusted if you would like to run with a non-zero value.

        Args:
          spectrum: observed bin counts
          edges: edges of histogram
          mlem_iter: Number of mlem_iterations to perform
          fix_offset: fix the offset to value * gain in last step
          verbose: If true, print additional information

        Returns:
            optimization parameters, weights
        """
        lower_bounds = list(self.lower_bounds)
        upper_bounds = list(self.upper_bounds)
        cutoff = self.cutoff
        initial_guess = list(self.params)

        res_b = initial_guess.pop(1)
        low_res_b = lower_bounds.pop(1)
        upp_res_b = upper_bounds.pop(1)

        if lower_bounds[self.cc + 1] is None:
            lower_bounds[self.cc + 1] = (
                (edges[np.min(np.nonzero(spectrum))] + 1) / self.cutoff[1] * 2.0
            )
        if upper_bounds[self.cc + 1] is None:
            upper_bounds[self.cc + 1] = (
                edges[np.max(np.nonzero(spectrum))] / self.cutoff[0] * 0.5
            )
        if initial_guess[self.cc + 1] is None:
            initial_guess[self.cc + 1] = np.sqrt(
                lower_bounds[self.cc + 1] * upper_bounds[self.cc + 1]
            )  # geometric mean

        if verbose:
            print(
                f"Gain range: [{lower_bounds[self.cc + 1]}, {upper_bounds[self.cc + 1]}], initial guess: {initial_guess[self.cc + 1]}"
            )

        # first we perform only the gain optimization step, we use a special calibrator here
        gain_mask = [l not in gain_opt_exclude_labels for l in self.calibrator.templates.labels]
        self._gain_calibrator = TemplatesCalibrator(
            Templates(
                self.calibrator.templates.templates[gain_mask],
                self.calibrator.templates.edges,
                labels=[l for l, m in zip(self.calibrator.templates.labels, gain_mask) if m],
            )
        )
        self._gain_calibrator.cutoff = cutoff
        self._gain_calibrator.mlem_iter = gain_opt_mlem_iter
        self._gain_calibrator.cosmic_comp = self.cc
        self._gain_calibrator.init_gain_optimizer(local=False, maxeval=2000)
        self._gain_calibrator.init_res_func(1)
        res, weights = self._gain_calibrator.optimize(
            initial_guess[: self.cc + 2],
            spectrum,
            edges,
            lower_bounds=lower_bounds[: self.cc + 2],
            upper_bounds=upper_bounds[: self.cc + 2],
        )
        if verbose:
            print(f"Gain optimizer result: {res}, weights: {weights}")

        self._initial_gain_params = res.copy()
        self._initial_gain_weights = weights.copy()

        # second we add the saturation
        self._no_offset_calibrator = TemplatesCalibrator(
            Templates(
                self.calibrator.templates.templates,
                self.calibrator.templates.edges,
                labels=self.calibrator.templates.labels,
            )
        )
        self._no_offset_calibrator.cutoff = cutoff
        self._no_offset_calibrator.mlem_iter = no_offset_opt_mlem_iter
        self._no_offset_calibrator.cosmic_comp = self.cc
        self._no_offset_calibrator.init_no_offset_optimizer(local=True)
        self._no_offset_calibrator.init_res_func(1)

        cosm = np.array(res[1 : (self.cc + 1)])
        lbs = [
            res[0] - 0.05,
            *(cosm - 0.5),
            0.7 * res[self.cc + 1],
            lower_bounds[self.cc + 2],
        ]
        ubs = [
            res[0] + 0.05,
            *(cosm + 0.5),
            1.3 * res[self.cc + 1],
            upper_bounds[self.cc + 2],
        ]
        res = [res[0], *cosm, res[self.cc + 1], initial_guess[self.cc + 2]]

        res, weights = self._no_offset_calibrator.optimize(
            res, spectrum, edges, lower_bounds=lbs, upper_bounds=ubs
        )
        if verbose:
            print(f"No offset optimizer result: {res}, weights: {weights}")

        self._initial_no_offset_params = res.copy()
        self._initial_no_offset_weights = weights.copy()

        # third optimization, full optimizer (or light_yield) with data cut
        dd, cc = self._no_offset_calibrator.cut_data(
            res, spectrum, edges, cutoff=cutoff
        )
        self.calibrator.cosmic_comp = self.cc
        self.calibrator.init_res_func(2)
        self.calibrator.cutoff = [None, None]
        self.calibrator.mlem_iter = mlem_iter
        if self.opt_type.lower() == "full":
            self.calibrator.init_full_optimizer(local=True)
        elif self.opt_type.lower() == "lightyield":
            self.calibrator.init_lightyield_optimizer(local=True)
        else:
            raise ValueError(f"Unknown final_calib {self.opt_type.lower()}")

        if fix_offset is not False and fix_offset is not None:
            initial_guess[self.cc + 3] = lower_bounds[self.cc + 3] = upper_bounds[
                self.cc + 3
            ] = (fix_offset * res[self.cc + 1])

        # cosm = np.array(res[1 : (self.cc + 1)])
        lbs = [
            res[0] - 0.05,
            low_res_b,
            *lower_bounds[1 : (self.cc + 1)],
            0.7 * res[self.cc + 1],
            lower_bounds[self.cc + 2],
            lower_bounds[self.cc + 3],
        ]
        ubs = [
            res[0] + 0.05,
            upp_res_b,
            *upper_bounds[1 : (self.cc + 1)],
            1.3 * res[self.cc + 1],
            upper_bounds[self.cc + 2],
            upper_bounds[self.cc + 3],
        ]
        res = [
            res[0],
            res_b,
            *cosm,
            res[self.cc + 1],
            res[self.cc + 2],
            initial_guess[self.cc + 3],
        ]

        res, weights = self.calibrator.optimize(
            res, dd, cc, lower_bounds=lbs, upper_bounds=ubs
        )
        if verbose:
            print(
                f"Result: {res}, weights: {weights}, optimum: {self.calibrator.opt.last_optimum_value()}"
            )

        cosm = np.array(res[2 : (self.cc + 2)])
        self.lower_bounds = [
            res[0] - 0.05,
            low_res_b,
            *(cosm - 0.5),
            0.8 * res[self.cc + 2],
            lower_bounds[self.cc + 2],
            lower_bounds[self.cc + 3],
        ]
        self.upper_bounds = [
            res[0] + 0.05,
            upp_res_b,
            *(cosm + 0.5),
            1.2 * res[self.cc + 2],
            upper_bounds[self.cc + 2],
            upper_bounds[self.cc + 3],
        ]
        self.params = res.copy()
        self.weights = weights.copy()
        self.spect = dd
        self.edges = cc

        return res, weights

    def calibrate_spectrum(self, spectrum, edges, mlem_iter=500, return_pvalue=False):
        """
        Performs the calibration for a spectrum. It performs a single optimization. The parameters are continously updated
        and the calibration of the last spectrum is used for an intial guess of the next.

        Note: If lower and upper bounds are set equally the parameter will be fixed. The second resolution parameter
              is forced to zero by default and its bounds need to be adjusted if you would like to run with a non-zero value.

        Args:
            spectra: list of spectrum (observed bin counts)
            edges: edges of histogram
            mlem_iter: Number of mlem_iterations to perform
            return_pvalue: Return the pvalue as well
        Yields:
            optimization parameters, weights, function minimum
        """

        self.calibrator.mlem_iter = mlem_iter

        res = self.params.copy()
        dd, cc = self.calibrator.cut_data(res, spectrum, edges, cutoff=self.cutoff)

        self.calibrator.weight_guess = self.weights
        res, weights = self.calibrator.optimize(
            res, dd, cc, lower_bounds=self.lower_bounds, upper_bounds=self.upper_bounds
        )
        self.params = res
        self.weights = weights

        # cosm = np.array(res[2 : (self.cc + 2)])
        self.lower_bounds = [
            res[0] - 0.05,
            self.lower_bounds[1],
            *self.lower_bounds[2 : (self.cc + 2)],
            0.8 * res[self.cc + 2],
            self.lower_bounds[self.cc + 3],
            self.lower_bounds[self.cc + 4],
        ]
        self.upper_bounds = [
            res[0] + 0.05,
            self.upper_bounds[1],
            *self.upper_bounds[2 : (self.cc + 2)],
            1.2 * res[self.cc + 2],
            self.upper_bounds[self.cc + 3],
            self.upper_bounds[self.cc + 4],
        ]
        self.spect = dd
        self.edges = cc
        ov = self.calibrator.opt.last_optimum_value()
        if return_pvalue:
            num_free_params = len(res) + len(weights)
            for i in [1, self.cc + 3, self.cc + 4]:
                if np.isclose(self.lower_bounds[i], self.upper_bounds[i]):
                    num_free_params -= 1
            pv = self.calibrator.p_value(
                res, dd, cc, weights=weights, num_free_params=num_free_params
            )
            return res.copy(), weights.copy(), ov, pv
        else:
            return res.copy(), weights.copy(), ov

