from datetime import datetime
import rospy
from rosbag import Bag
import yaml
import pandas as pd
from collections import OrderedDict
import numpy as np
import h5py
import os
import logging


DTYPE_INT_TO_NP = {
    0: np.dtype("bool"),
    1: np.dtype("int8"),
    2: np.dtype("int16"),
    3: np.dtype("int32"),
    4: np.dtype("int64"),
    5: np.dtype("uint8"),
    6: np.dtype("uint16"),
    7: np.dtype("uint32"),
    8: np.dtype("uint64"),
    9: np.dtype("float32"),
    10: np.dtype("float64"),
}

def to_numpy_array(arg):
    dtype = np.dtype(
        [
            (saf.name, DTYPE_INT_TO_NP[saf.dtype])
            + (() if saf.count == 1 else (saf.count,))
            for saf in arg.fields
        ]
    )
    return np.frombuffer(arg.data, dtype=dtype)

def flatten_listmode(topic, msg):
    data = OrderedDict()
    data_arr = to_numpy_array(msg.arrays)
    data_size = data_arr.size
    if data_size:
        label = ["channel", "energy"]
        data.setdefault(label[topic=="/listmode/calib"], []).extend(data_arr['channel'])
        data.setdefault('ts_det', []).extend(data_arr['timestamp_det'])
    else: data = None
    return data

node_latlon = {'W022': (0, 0),
               'W01C': (41.88452644522827, -87.62460057836556),
               'W059': (41.880798782501344, -87.62787947725498),
               'W05A': (41.88918080948867, -87.63241563638988),
               'W05B': (41.88676494472552, -87.63095765266233),
               'W05C': (41.88312765231458, -87.62932260591538),
               'W05D': (41.88455003362158, -87.62797295963482),
               'W05E': (41.87429749692395, -87.66644603994878),
               'W072': (41.86673872385933, -87.6665645143102),
               'W073': (41.88315300556273, -87.62765984093555),
               'W074': (41.88929390665504, -87.63122288140154),
               'W075': (41.88317977577514, -87.63373999393055),
               'W076': (0, 0),
               'W077': (41.88675294995436, -87.63235008038846),
               }

class Converter:
    def __init__(self, node=""):
        self.node = node

    def to_bdch5(self, bagfilename): 
        with Bag(bagfilename, 'r') as bag:
            info_dict = yaml.safe_load(bag._get_yaml_info())
            beg_ts = info_dict['start']
            end_ts = info_dict['end']
            beg_dt = datetime.fromtimestamp(beg_ts).strftime("%m/%d/%Y, %H:%M:%S")
            end_dt = datetime.fromtimestamp(end_ts).strftime("%m/%d/%Y, %H:%M:%S")
            dt = (end_ts - beg_ts)/3600
            print("bag: {}".format(bagfilename))
            print("begin: {}, end: {}, dt: {}".format(beg_dt, end_dt, dt))
            if '/dbaserh/listmode' not in bag.get_type_and_topic_info()[1].keys():
                print("*** no digibase data")
                return None
            
            # env data
            pressure_ts = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}  # for bkw compatibility
            pressure_val = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}
            humidity_ts = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}
            humidity_val = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}
            temp_ts = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}
            temp_val = {"shield": [], "enclosure": [], "iio-rpi": [], "iio-enclosure": []}
            rain_ts, rain_val = [], []
            # calibration params
            cal_ts, cal_fmin, cal_pval = [], [], []
            cal_res, cal_plaw, cal_gain, cal_sat, cal_off = [], [], [], [], []
            cal_cosmics, cal_511, cal_K, cal_U, cal_Th, cal_Rn = [], [], [], [], [], []
            # rad data
            data = {}
            for topic, msg, bag_ts in bag:
                if topic == '/sensors/env/temperature':
                    sensor = msg.zone if "zone" in dir(msg) else msg.host  # for bwd compatibility
                    if sensor != "wes-iio-bme680":                         # for bwd compatibility
                        ts = msg.header.stamp.secs
                        val = msg.value
                        temp_ts[sensor].append(ts)
                        temp_val[sensor].append(val)
                elif topic == '/sensors/env/pressure':
                    sensor = msg.zone if "zone" in dir(msg) else msg.host
                    if sensor != "wes-iio-bme680": 
                        ts = msg.header.stamp.secs
                        val = msg.value
                        pressure_ts[sensor].append(ts)
                        pressure_val[sensor].append(val)
                elif topic == '/sensors/env/humidity':
                    sensor = msg.zone if "zone" in dir(msg) else msg.host
                    if sensor != "wes-iio-bme680": 
                        ts = msg.header.stamp.secs
                        val = msg.value
                        humidity_ts[sensor].append(ts)
                        humidity_val[sensor].append(val)
                elif topic == '/sensors/raingauge':
                    rain_ts.append(msg.header.stamp.secs)
                    rain_val.append(msg.total_accumulated)
                elif topic == '/calib/params':
                    params = to_numpy_array(msg.scalars)
                    cal_ts.append(params[0][1])
                    cal_fmin.append(params[0][5])
                    cal_pval.append(params[0][6])
                    cal_res.append(params[0][7])
                    cal_plaw.append(params[0][8])
                    cal_gain.append(params[0][9])
                    cal_sat.append(params[0][10])
                    cal_off.append(params[0][11])
                    cal_cosmics.append(params[0][12])
                    cal_511.append(params[0][13])
                    cal_K.append(params[0][14])
                    cal_U.append(params[0][15])
                    cal_Th.append(params[0][16])
                    cal_Rn.append(params[0][17])
                elif topic == '/calib/listmode':
                    data_ = flatten_listmode('/listmode/calib', msg)
                    if data_ is not None: 
                        data.setdefault(self.node+'/listmode/calib', []).append(data_)
                elif topic == '/dbaserh/listmode':
                    data_ = flatten_listmode('/listmode/raw', msg)
                    if data_  is not None:
                        data.setdefault(self.node+'/listmode/raw', []).append(data_)
                    
            # write to h5 file
            h5filename = bagfilename.replace(".bag", ".h5")
            print("BDC hdf5 file: {}".format(h5filename))
            with h5py.File(h5filename, 'w') as h5out:
                # geolocation
                h5out.create_dataset(self.node+"/gis/lat", data=node_latlon[self.node][0])
                h5out[self.node+"/gis/lat"].attrs['unit'] = 'degrees'
                h5out.create_dataset(self.node+"/gis/lon", data=node_latlon[self.node][1])
                h5out[self.node+"/gis/lon"].attrs['unit'] = 'degrees'
                if len(pressure_ts["iio-rpi"]):
                    h5out.create_dataset(self.node+"/env/pressure_shield/ts", data=pressure_ts["iio-rpi"])
                    h5out.create_dataset(self.node+"/env/pressure_shield/val", data=pressure_val["iio-rpi"])
                    h5out[self.node+"/env/pressure_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/pressure_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/pressure_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/pressure_shield/ts" 
                    h5out[self.node+"/env/pressure_shield/val"].attrs['unit'] = 'Pa' 
                elif len(pressure_ts["shield"]):
                    h5out.create_dataset(self.node+"/env/pressure_shield/ts", data=pressure_ts["shield"])
                    h5out.create_dataset(self.node+"/env/pressure_shield/val", data=pressure_val["shield"])
                    h5out[self.node+"/env/pressure_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/pressure_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/pressure_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/pressure_shield/ts" 
                    h5out[self.node+"/env/pressure_shield/val"].attrs['unit'] = 'Pa'
                else:
                    print("*** no pressure data")
                if len(pressure_ts["iio-enclosure"]):
                    h5out.create_dataset(self.node+"/env/pressure_enclosure/ts", data=pressure_ts["iio-enclosure"])
                    h5out.create_dataset(self.node+"/env/pressure_enclosure/val", data=pressure_val["iio-enclosure"])
                    h5out[self.node+"/env/pressure_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/pressure_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/pressure_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/pressure_enclosure/ts" 
                    h5out[self.node+"/env/pressure_enclosure/val"].attrs['unit'] = 'Pa' 
                elif len(pressure_ts["enclosure"]):
                    h5out.create_dataset(self.node+"/env/pressure_enclosure/ts", data=pressure_ts["enclosure"])
                    h5out.create_dataset(self.node+"/env/pressure_enclosure/val", data=pressure_val["enclosure"])
                    h5out[self.node+"/env/pressure_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/pressure_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/pressure_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/pressure_enclosure/ts" 
                    h5out[self.node+"/env/pressure_enclosure/val"].attrs['unit'] = 'Pa'
                else:
                    print("*** no pressure data")
                if len(humidity_ts["iio-rpi"]):
                    h5out.create_dataset(self.node+"/env/humidity_shield/ts", data=humidity_ts["iio-rpi"])
                    h5out.create_dataset(self.node+"/env/humidity_shield/val", data=humidity_val["iio-rpi"])
                    h5out[self.node+"/env/humidity_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/humidity_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/humidity_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/humidity_shield/ts" 
                    h5out[self.node+"/env/humidity_shield/val"].attrs['unit'] = '%' 
                elif len(humidity_ts["shield"]):
                    h5out.create_dataset(self.node+"/env/humidity_shield/ts", data=humidity_ts["shield"])
                    h5out.create_dataset(self.node+"/env/humidity_shield/val", data=humidity_val["shield"])
                    h5out[self.node+"/env/humidity_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/humidity_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/humidity_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/humidity_shield/ts" 
                    h5out[self.node+"/env/humidity_shield/val"].attrs['unit'] = '%'
                else:
                    print("*** no humidity data")
                if len(humidity_ts["iio-enclosure"]):
                    h5out.create_dataset(self.node+"/env/humidity_enclosure/ts", data=humidity_ts["iio-enclosure"])
                    h5out.create_dataset(self.node+"/env/humidity_enclosure/val", data=humidity_val["iio-enclosure"])
                    h5out[self.node+"/env/humidity_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/humidity_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/humidity_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/humidity_enclosure/ts" 
                    h5out[self.node+"/env/humidity_enclosure/val"].attrs['unit'] = '%' 
                elif len(humidity_ts["enclosure"]):
                    h5out.create_dataset(self.node+"/env/humidity_enclosure/ts", data=humidity_ts["enclosure"])
                    h5out.create_dataset(self.node+"/env/humidity_enclosure/val", data=humidity_val["enclosure"])
                    h5out[self.node+"/env/humidity_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/humidity_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/humidity_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/humidity_enclosure/ts" 
                    h5out[self.node+"/env/humidity_enclosure/val"].attrs['unit'] = '%'
                else:
                    print("*** no humidity data")
                if len(temp_ts["iio-rpi"]):   
                    h5out.create_dataset(self.node+"/env/temp_shield/ts", data=temp_ts["iio-rpi"])
                    h5out.create_dataset(self.node+"/env/temp_shield/val", data=temp_val["iio-rpi"])
                    h5out[self.node+"/env/temp_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/temp_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/temp_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/temp_shield/ts" 
                    h5out[self.node+"/env/temp_shield/val"].attrs['unit'] = 'C' 
                elif len(temp_ts["shield"]):   
                    h5out.create_dataset(self.node+"/env/temp_shield/ts", data=temp_ts["shield"])
                    h5out.create_dataset(self.node+"/env/temp_shield/val", data=temp_val["shield"])
                    h5out[self.node+"/env/temp_shield/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/temp_shield/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/temp_shield/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/temp_shield/ts" 
                    h5out[self.node+"/env/temp_shield/val"].attrs['unit'] = 'C'
                else:
                    print("*** no temperature data")
                if len(temp_ts["iio-enclosure"]):
                    h5out.create_dataset(self.node+"/env/temp_enclosure/ts", data=temp_ts["iio-enclosure"])
                    h5out.create_dataset(self.node+"/env/temp_enclosure/val", data=temp_val["iio-enclosure"])
                    h5out[self.node+"/env/temp_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/temp_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/temp_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/temp_enclosure/ts" 
                    h5out[self.node+"/env/temp_enclosure/val"].attrs['unit'] = 'C' 
                elif len(temp_ts["enclosure"]):
                    h5out.create_dataset(self.node+"/env/temp_enclosure/ts", data=temp_ts["enclosure"])
                    h5out.create_dataset(self.node+"/env/temp_enclosure/val", data=temp_val["enclosure"])
                    h5out[self.node+"/env/temp_enclosure/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/temp_enclosure/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/temp_enclosure/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/temp_enclosure/ts" 
                    h5out[self.node+"/env/temp_enclosure/val"].attrs['unit'] = 'C'
                else:
                    print("*** no temperature data")
                if len(rain_ts):
                    h5out.create_dataset(self.node+"/env/rain/ts", data=rain_ts)
                    h5out.create_dataset(self.node+"/env/rain/val", data=rain_val)
                    h5out[self.node+"/env/rain/ts"].attrs['timeSyncLocation'] = 'self' 
                    h5out[self.node+"/env/rain/ts"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/env/rain/val"].attrs['timeSyncLocation'] = "/"+self.node+"/env/rain/ts" 
                    h5out[self.node+"/env/rain/val"].attrs['unit'] = 'precipitation unit'
                else:
                    print("*** no rain data")
                if self.node+'/listmode/raw' in data:
                    for topic in data:
                        df = pd.concat(pd.DataFrame(d) for d in data[topic])
                        for col in df:
                            h5path = os.path.join(topic, col)
                            this_data = df[col].values
                            h5out.create_dataset(h5path, data=this_data,
                                                 dtype=this_data.dtype,
                                                 compression="gzip")
                    h5out[self.node+"/listmode/raw/ts_det"].attrs['timeSyncLocation'] = 'self'
                    h5out[self.node+"/listmode/raw/ts_det"].attrs['unit'] = 'unix time'
                    h5out[self.node+"/listmode/raw/channel"].attrs['timeSyncLocation'] = "/"+self.node+"/listmode/raw/ts_det"
                    h5out[self.node+"/listmode/raw/channel"].attrs['unit'] = 'ADC channel'
                    if self.node + "/listmode/calib" in data:
                        h5out[self.node+"/listmode/calib/ts_det"].attrs['timeSyncLocation'] = 'self'
                        h5out[self.node+"/listmode/calib/ts_det"].attrs['unit'] = 'unix time'
                        h5out[self.node+"/listmode/calib/energy"].attrs['timeSyncLocation'] = "/"+self.node+"/listmode/calib/ts_det"
                        h5out[self.node+"/listmode/calib/energy"].attrs['unit'] = 'keV'
                        if len(cal_ts):
                            h5out.create_dataset(self.node+"/calib/ts", data=cal_ts)
                            h5out.create_dataset(self.node+"/calib/fmin", data=cal_fmin)
                            h5out.create_dataset(self.node+"/calib/pval", data=cal_pval)
                            h5out.create_dataset(self.node+"/calib/res", data=cal_res)
                            h5out.create_dataset(self.node+"/calib/plaw", data=cal_plaw)
                            h5out.create_dataset(self.node+"/calib/gain", data=cal_gain)
                            h5out.create_dataset(self.node+"/calib/sat", data=cal_sat)
                            h5out.create_dataset(self.node+"/calib/off", data=cal_off)
                            h5out.create_dataset(self.node+"/calib/w_cosmics", data=cal_cosmics)
                            h5out.create_dataset(self.node+"/calib/w_511", data=cal_511)
                            h5out.create_dataset(self.node+"/calib/w_K", data=cal_K)
                            h5out.create_dataset(self.node+"/calib/w_U", data=cal_U)
                            h5out.create_dataset(self.node+"/calib/w_Th", data=cal_Th)
                            h5out.create_dataset(self.node+"/calib/w_Rn", data=cal_Rn)
                            h5out[self.node+"/calib/ts"].attrs['timeSyncLocation'] = 'self' 
                            h5out[self.node+"/calib/ts"].attrs['unit'] = 'unix time'
                            h5out[self.node+"/calib/fmin"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/fmin"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/pval"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/pval"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/res"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/res"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/plaw"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/plaw"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/gain"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/gain"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/sat"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/sat"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/off"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/off"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_cosmics"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/w_cosmics"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_511"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/w_511"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_K"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/w_K"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_U"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/w_U"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_Th"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts" 
                            h5out[self.node+"/calib/w_Th"].attrs['unit'] = 'N/A'
                            h5out[self.node+"/calib/w_Rn"].attrs['timeSyncLocation'] = "/"+self.node+"/calib/ts"
                            h5out[self.node+"/calib/w_Rn"].attrs['unit'] = 'N/A'
                        else:
                            print("*** no calibration data")
                    else:
                        print("*** no calibrated data")
        return h5filename

