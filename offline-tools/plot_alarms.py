import argparse
import pandas as pd
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt

# binning
from bad.rebinning import sqrt_n_bins
nmin, nmax = 50, 3000
nbins = 128
bin_edges, bin_centers = sqrt_n_bins(nmin=nmin, nmax=nmax, nbins=nbins)

#labels = ["Am-241", "Ba-133", "Co-57", "Co-60", "Cs-137", "Cu-67",
#          "DepletedU", "F-18", "FGPu", "HEU", "I-131", "Ir-192",
#          "K-40", "LEU", "Lu-177", "NatU", "Ra-226", "RefinedU",
#          "Sr-90", "Tc-99m", "Th-232", "Tl-201", "WGPu", "Xe-133"]

spectra = defaultdict(lambda: np.zeros(nbins))
stats = defaultdict(int)

n_spec_rain, n_spec_rain_rain, n_spec_rain_static = 0, 0, 0
spec_rain = np.zeros(nbins)
spec_rain_rain = np.zeros(nbins)
spec_rain_static = np.zeros(nbins)
n_spec_static, n_spec_static_rain, n_spec_static_static = 0, 0, 0
spec_static = np.zeros(nbins)
spec_static_rain = np.zeros(nbins)
spec_static_static = np.zeros(nbins)

parser = argparse.ArgumentParser()
parser.add_argument('--files', nargs='+', type=str)
args = parser.parse_args()
for f in args.files:
    print("file:", f)
    df = pd.read_hdf(f)

    print(df)
    
    if not len(df):
        print(".. no alarm data")
    for label in np.unique(df.loc[:, "badfm_id"].values):
        if not label: continue
        sel = df[df["badfm_id"] == label]

        print(f, label, len(sel))
        
        #if len(sel):
        spectra[label] += sel.loc[:, "spectrum"].values.sum()
        stats[label] += len(sel)

        
        if label == "Ra-226":
            subsel = sel[sel["triage_flag"] == "rain"]
            if len(subsel):
                n_spec_rain += len(subsel)
                spec_rain += subsel.loc[:, "spectrum"].values.sum()

                subsubsel = subsel[subsel["bkg_model"] == "rain"]
                if len(subsubsel):
                    n_spec_rain_rain += len(subsubsel)
                    spec_rain_rain += subsubsel.loc[:, "spectrum"].values.sum()
                subsubsel = subsel[subsel["bkg_model"] == "static"]
                if len(subsubsel):
                    n_spec_rain_static += len(subsubsel)
                    spec_rain_static += subsubsel.loc[:, "spectrum"].values.sum()
                
            subsel = sel[sel["triage_flag"] == "static"]
            if len(subsel):
                n_spec_static += len(subsel)
                spec_static += subsel.loc[:, "spectrum"].values.sum()

                subsubsel = subsel[subsel["bkg_model"] == "rain"]
                if len(subsubsel):
                    n_spec_static_rain += len(subsubsel)
                    spec_static_rain += subsubsel.loc[:, "spectrum"].values.sum()
                subsubsel = subsel[subsel["bkg_model"] == "static"]
                if len(subsubsel):
                    n_spec_static_static += len(subsubsel)
                    spec_static_static += subsubsel.loc[:, "spectrum"].values.sum()
        
"""
for label, spectrum in spectra.items():
    fig, ax = plt.subplots()
    ax.step(bin_centers, spectrum, where='mid', color="gray", label=label+", n = "+str(stats[label]))
    ax.set_xlabel("Energy [keV]")
    ax.set_ylabel("Entries")
    ax.set_yscale('log')
    ax.legend(loc="best")
"""

fig, ax = plt.subplots()
ax.step(bin_centers, spectra["Ra-226"], where='mid', color="gray", label="Ra-226, n = "+str(stats["Ra-226"]))
ax.step(bin_centers, spec_rain, where='mid', color="blue", label="triage rain, n = "+str(n_spec_rain))
ax.step(bin_centers, spec_rain_rain, where='mid', color="blue", alpha=0.8, label="triage rain, model rain, n = "+str(n_spec_rain_rain))
ax.step(bin_centers, spec_rain_static, where='mid', color="blue", alpha=0.6, label="triage rain, model static, n = "+str(n_spec_rain_static))
ax.step(bin_centers, spec_static, where='mid', color="red", label="triage static, n = "+str(n_spec_static))
ax.step(bin_centers, spec_static_rain, where='mid', color="red", alpha=0.8, label="triage static, model rain, n = "+str(n_spec_static_rain))
ax.step(bin_centers, spec_static_static, where='mid', color="red", alpha=0.6, label="triage static, model static, n = "+str(n_spec_static_static))
ax.set_xlabel("Energy [keV]")
ax.set_ylabel("Entries")
ax.set_yscale('log')
ax.legend(loc="best")


sorted_stats = dict(sorted(stats.items(), key=lambda x:x[1]))
fig, ax = plt.subplots()
ax.barh(list(sorted_stats.keys()), list(sorted_stats.values()), color="gray")
ax.set_xlabel("# alarms")
ax.grid(b = True, color ="gray", linestyle='-.', linewidth=0.5, alpha=0.2)

plt.show()

