
### Scope:

This application is part of the PANDA project, collaboration between the ANP Group at LBNL and ANL.  
The PANDA project leverages the DAWN nodes to deploy radiation sensors in the city of Chicago, IL.  
The goal of the depoyment of these so-called PANDAWN nodes is to demonstrate the capability to track  
radioactive sources through an urban enviroment using a sparse array of detectors, with additional  
contextual information, and the possibility to use the array in a network configuration.

### Plugin:

The panda-raingauge plugin receives precipitation data from the raingauge sensor of the PANDAWN nodes.  
The data is saved in the ROS bag format and uploaded to the beehive. The plugin republishes the data  
for downstream plugins related to radiation background learning and monitoring, where the precipitation  
information is used to flag the presence of increased concentrations of Ra-226 proginy. 
