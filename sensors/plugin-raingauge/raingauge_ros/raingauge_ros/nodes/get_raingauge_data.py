#!/usr/bin/env python
import rospy
from raingauge_ros_msgs.msg import RainGauge
import waggle.plugin as plg 
import logging

class RaingaugeNode(object):

    def __init__(self):
        # init the node
        rospy.init_node('raingauge', anonymous=False)
        
        # subbscribe to sensor data stream msgs
        self.plugin = plg.Plugin()
        #self.plugin.subscribe("env.raingauge.acc") # NOT PUBLISHED
        self.plugin.subscribe("env.raingauge.event_acc")
        self.plugin.subscribe("env.raingauge.total_acc")

        self.values = {}

        # create publisher
        self.pub = rospy.Publisher('/sensors/raingauge', RainGauge, queue_size=10)
    
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()
    
    def close(self):
        pass
    
    def make_message(self, values):
        msg = RainGauge()
        msg.header.frame_id = "/sensors/raingauge"
        #msg.header.stamp = rospy.Time()
        tss = str(values["ts"])
        msg.header.stamp.secs = int(tss[:10])
        msg.header.stamp.nsecs = int(tss[10:])
        #msg.accumulated = values["env.raingauge.acc"]
        msg.event_accumulated = values["env.raingauge.event_acc"]
        msg.total_accumulated = values["env.raingauge.total_acc"]
        
        #rospy.loginfo("secs: %s, event acc: %s, total acc: %s", msg.header.stamp.secs, msg.event_accumulated, msg.total_accumulated)
        logging.info("secs: %s, event acc: %s, total acc: %s", msg.header.stamp.secs, msg.event_accumulated, msg.total_accumulated)
        return msg

    def run(self):
        while not rospy.is_shutdown(): 
            msg = self.plugin.get()
            if len(self.values) == 0: 
                if msg.name != "env.raingauge.event_acc":
                    logging.info("Missed raingauge msg")
                    continue
                else: self.values["ts"] = msg.timestamp
            self.values[msg.name] = msg.value
            if len(self.values) == 3:
                msg = self.make_message(self.values)
                self.pub.publish(msg)
                self.values.clear()

if __name__ == '__main__':
    with RaingaugeNode() as node:
        node.run()  
