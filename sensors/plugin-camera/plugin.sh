#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

source /home/panda/.bashrc
source /home/panda/panda_ws/devel/setup.bash

export CALIBDIR="/home/panda/panda_ws/src/camera_ros/calib"
export BAGDIR="/home/panda/mount/bags"

roslaunch camera_ros ${CAM_MODE}.launch
