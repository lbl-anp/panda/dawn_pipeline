#!/usr/bin/env python
import os
import cv2
from cv_bridge import CvBridge
import rospy
import numpy as np
from sensor_msgs.msg import CameraInfo, CompressedImage, Image
from waggle.data.vision import resolve_device
from waggle.data.timestamp import get_timestamp
import time
import threading

class CameraNode(object):
    """
    Node to republish camera samples from the POE device's rtsp stream
    to ROS msgs
    """
    def __init__(self):
        rospy.init_node('camera', anonymous=False)
    
        self.CALIB_DIR = os.getenv('CALIBDIR')

        # get node params
        self.params = self.get_parameters()
        self.bag_rate = self.params["rate"]
        self.rectify = self.params["rectify"]
        # access camera rtsp stream from device's IP
        # profile9: h265 encoding, half resolution, 15Hz
        # profile6: h265 encoding, half resolution, 30Hz
        # profile3: h265 encoding, full resolution, 30Hz
        profile = self.params["profile"]
        if profile == 9:
            self.scale = 1.0  # calibration is done on half resolution, scale accordingly
            self.width = 1280
            self.height = 960
            self.stream = "rtsp://" + self.params["IP"] + ":554/0/profile" + str(profile) + "/media.smp"
        elif profile == 6:
            self.scale = 1.0
            self.width = 1280 
            self.height = 960
            self.stream = "rtsp://" + self.params["IP"] + ":554/0/profile" + str(profile) + "/media.smp"
        elif profile == 3:
            self.scale = 2.0;
            self.width = 2560
            self.height = 1920
            self.stream = "rtsp://" + self.params["IP"] + "/profile" + str(profile) + "/media.smp"
        print("Camera node params:")
        print(".. profile:", profile)
        print(".. rectification:", self.rectify)
        print(".. bagging rate:", self.bag_rate)

        # set up camera capture with background thread to read out cv buffer as fast as possible
        self.vcap = cv2.VideoCapture(resolve_device(self.stream))
        if self.vcap.isOpened() is False:
            print("[Exiting]: Error accessing camera rstp stream.")
            exit(0)
        grabbed, data = self.vcap.read() # initialize with a single read
        if not grabbed:
            print("[Exiting]: Error grabbing frame at initialization.")
            exit(0)
        self.t = threading.Thread(target=self.update, args=())
        self.t.daemon = True  # daemon threads run in background
        self.lock = threading.Lock()
        self.stopped = False
        self.data, self.timestamp = None, None

        # CvBridge for conversion to ROS msgs
        self.cvbr = CvBridge()
        
        # camera calibration params
        self.cam_mtx = np.zeros((3, 3))
        self.dist = np.zeros((1,5))
        self.proj_mtx = np.zeros((3, 4))
        self.camInfoMsg = CameraInfo()
        self.init_calibration()

        # publishers
        if self.rectify is True:
            topic = "image_rect_color"
        else:
            topic = "image_color"
        print(".. image topic:", topic)
        # bagging - publish compressed images
        self.pub_img_bag = rospy.Publisher("/bag/sensors/camera/"+topic, CompressedImage, queue_size=60)
        self.pub_caminfo_bag = rospy.Publisher('/bag/sensors/camera/camera_info', CameraInfo, queue_size=60)
        
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def get_parameters(self):
        params = {}
        params["IP"] = rospy.get_param("~IP", default="")
        params["calib_file"] = rospy.get_param("~calib_file", default="")
        params["profile"] = rospy.get_param("~profile", default=9)
        params["rate"] = rospy.get_param("~rate", default=15)
        params["rectify"] = rospy.get_param("~rectify", default=False)
        return params

    def start(self):
        self.t.start()

    def update(self):
        # here we should do only the fast grab step and assign the timestamp
        sleep = 0.01 # sleeping 10ms between grabs
        while True:
            if self.stopped: break
            self.lock.acquire()
            ok = self.vcap.grab()
            if not ok:
                print("[Exiting]: Error grabbing frame.")
                exit(0) 
            self.timestamp = get_timestamp()
            ok, self.data = self.vcap.retrieve()
            if not ok:
                print("[Exiting]: Error retrieving frame.")
                exit(0)
            self.lock.release() 
            time.sleep(sleep)
        self.vcap.release()
    
    def read(self):
        # here we should do the longer retrieve step only when needed
        #timestamp = self.timestamp
        #ok, data = self.vcap.retrieve()
        #if not ok:
        #    print("[Exiting]: Error retrieving last frame.")
        #    exit(0)
        #return data, timestamp
        self.lock.acquire()
        data = self.data
        timestamp = self.timestamp
        self.lock.release()
        return data, timestamp

    def stop(self):
        self.stopped = True

    def close(self):
        pass
 
    def init_calibration(self):
        """
        reads in calibration file to init camera matrix/distortion vector
        and prepares CameraInfo msg
        """
        filename = self.CALIB_DIR + '/' + self.params["calib_file"]
        f = open(filename, 'r')
        for i, line in enumerate(f):
            if i < 3:
                self.cam_mtx[i] = np.asarray(list(map(float, line.split())))
            elif i == 3:
                self.dist[0] = np.asarray(list(map(float, line.split())))
            else:
                self.proj_mtx[i-4] = np.asarray(list(map(float, line.split())))

        # rescale intrinsic matrix based on stream resolution
        # lower diagonal coeff should be set to 1
        self.cam_mtx *= self.scale
        self.cam_mtx[2, 2] = 1.
        self.proj_mtx *= self.scale
        self.proj_mtx[2, 2] = 1.

        # CameraInfo
        # http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html
        # Header
        # .. timestamp, set at publication
        self.camInfoMsg.header.frame_id = "/sensors/camera"
        # camera full pixel resolution
        self.camInfoMsg.height = self.height
        self.camInfoMsg.width = self.width
        # distortion model
        self.camInfoMsg.distortion_model = "plumb_bob"
        # distortion vector
        self.camInfoMsg.D = [self.dist[0][0], self.dist[0][1], self.dist[0][2], self.dist[0][3], self.dist[0][4]]
        # camera intrinsic matrix (row-major)
        self.camInfoMsg.K = [self.cam_mtx[0][0], self.cam_mtx[0][1], self.cam_mtx[0][2],
                             self.cam_mtx[1][0], self.cam_mtx[1][1], self.cam_mtx[1][2],
                             self.cam_mtx[2][0], self.cam_mtx[2][1], self.cam_mtx[2][2]]
        # rectification matrix for stereos camera only, set to identity for monocular cameras
        self.camInfoMsg.R = [1., 0., 0., 0., 1., 0., 0., 0., 1.]
        # projection matrix
        self.camInfoMsg.P = [self.proj_mtx[0][0], self.proj_mtx[0][1], self.proj_mtx[0][2], self.proj_mtx[0][3],
                             self.proj_mtx[1][0], self.proj_mtx[1][1], self.proj_mtx[1][2], self.proj_mtx[1][3],
                             self.proj_mtx[2][0], self.proj_mtx[2][1], self.proj_mtx[2][2], self.proj_mtx[2][3]]

        rospy.loginfo("camera matrix:")
        rospy.loginfo("%f %f %f", self.camInfoMsg.K[0], self.camInfoMsg.K[1], self.camInfoMsg.K[2])
        rospy.loginfo("%f %f %f", self.camInfoMsg.K[3], self.camInfoMsg.K[4], self.camInfoMsg.K[5])
        rospy.loginfo("%f %f %f", self.camInfoMsg.K[6], self.camInfoMsg.K[7], self.camInfoMsg.K[8])
        rospy.loginfo("distortion vector:")
        rospy.loginfo("%f %f %f %f %f", self.camInfoMsg.D[0], self.camInfoMsg.D[1], self.camInfoMsg.D[2], self.camInfoMsg.D[3], self.camInfoMsg.D[4])
        rospy.loginfo("projection matrix:")
        rospy.loginfo("%f %f %f %f", self.camInfoMsg.P[0], self.camInfoMsg.P[1], self.camInfoMsg.P[2], self.camInfoMsg.P[3])
        rospy.loginfo("%f %f %f %f", self.camInfoMsg.P[4], self.camInfoMsg.P[5], self.camInfoMsg.P[6], self.camInfoMsg.P[7])
        rospy.loginfo("%f %f %f %f", self.camInfoMsg.P[8], self.camInfoMsg.P[9], self.camInfoMsg.P[10], self.camInfoMsg.P[11])

    def make_comp_img_message(self, ts, data): 
        msg = self.cvbr.cv2_to_compressed_imgmsg(data) 
        tss = str(ts)
        msg.header.stamp.secs = int(tss[:10])
        msg.header.stamp.nsecs = int(tss[10:])
        msg.header.frame_id = "/sensors/camera"
        return msg

    def make_img_message(self, ts, data):
        msg = self.cvbr.cv2_to_imgmsg(data)
        tss = str(ts)
        msg.header.stamp.secs = int(tss[:10])
        msg.header.stamp.nsecs = int(tss[10:])
        msg.header.frame_id = "/sensors/camera"
        msg.encoding = "rgb8"
        return msg

    def make_caminfo_message(self, ts):
        tss = str(ts)
        self.camInfoMsg.header.stamp.secs = int(tss[:10])
        self.camInfoMsg.header.stamp.nsecs = int(tss[10:])
        return self.camInfoMsg

    def run(self):
        dt = (1. / self.bag_rate) * 1.E9
        prev_ts = 0
        self.start()
        time.sleep(0.1)
        while not rospy.is_shutdown():
            data, timestamp = self.read()
            data = cv2.cvtColor(data, cv2.COLOR_RGB2BGR) 
            if self.rectify is True:
                data = cv2.undistort(data, self.cam_mtx, self.dist, None, self.proj_mtx)
            
            # publish images for bagging at given rate
            if (timestamp - prev_ts) >= dt:
                img_msg = self.make_comp_img_message(timestamp, data)
                self.pub_img_bag.publish(img_msg)
                cam_msg = self.make_caminfo_message(timestamp)
                self.pub_caminfo_bag.publish(cam_msg)
                prev_ts = timestamp
        self.stop()

if __name__ == '__main__':
    with CameraNode() as node:
        node.run()  
