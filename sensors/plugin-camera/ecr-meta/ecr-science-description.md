
### Scope:

This application is part of the PANDA project, collaboration between the ANP Group at LBNL and ANL.  
The PANDA project leverages the DAWN nodes to deploy radiation sensors in the city of Chicago, IL.  
The goal of the depoyment of these so-called PANDAWN nodes is to demonstrate the capability to track  
radioactive sources through an urban enviroment using a sparse array of detectors, with additional  
contextual information, and the possibility to use the array in a network configuration.

### Plugin:

The panda-camera plugin subscribes to the camera stream (profile 9, half resolution). The data is  
saved in the ROS bag format and uploaded to the beehive. The plugin republishes the data for  
downstream plugins performing object detection, depth extraction and tracking.
