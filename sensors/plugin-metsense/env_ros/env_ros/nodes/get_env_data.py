#!/usr/bin/env python
import rospy
from env_ros_msgs.msg import Env
import logging
import waggle.plugin as plg

class EnvNode(object):

    def __init__(self):
        # init the node
        rospy.init_node('env', anonymous=False)
        
        # subbscribe to env data stream msgs
        #plugin.init()
        self.plugin = plg.Plugin()
        self.plugin.subscribe("env.temperature")
        self.plugin.subscribe("env.pressure")
        self.plugin.subscribe("env.relative_humidity")

        # create publisher
        #self.pub = rospy.Publisher('/sensors/env', Env, queue_size=10)
        self.temperature_pub = rospy.Publisher('/sensors/env/temperature', Env, queue_size=10)
        self.pressure_pub = rospy.Publisher('/sensors/env/pressure', Env, queue_size=10)
        self.humidity_pub = rospy.Publisher('/sensors/env/humidity', Env, queue_size=10)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()
    
    def close(self):
        pass
    
    def make_message(self, name, zone, timestamp, value):
        msg = Env()
        msg.header.frame_id = "/sensors/env"
        tss = str(timestamp)
        msg.header.stamp.secs = int(tss[:10])
        msg.header.stamp.nsecs = int(tss[10:])
        msg.zone = zone
        msg.value = value
        #rospy.loginfo("name: %s, secs: %s, host %s, value: %s", name, msg.header.stamp.secs, msg.host, msg.value)
        #logging.info("name: %s, secs: %s, host %s, value: %s", name, msg.header.stamp.secs, msg.host, msg.value)
        return msg

    def run(self):
        while not rospy.is_shutdown(): 
            msg = self.plugin.get()
            sensor = msg.meta.get("sensor")
            # discard bme280 on NX
            if sensor == "bme280": continue
            name = msg.name
            timestamp = msg.timestamp
            value = msg.value
            zone = msg.meta.get("zone")
            msg = self.make_message(name, zone, timestamp, value)
            if name == "env.temperature":
                self.temperature_pub.publish(msg)
            elif name == "env.pressure":
                self.pressure_pub.publish(msg)
            elif name == "env.relative_humidity":
                self.humidity_pub.publish(msg)

if __name__ == '__main__':
    with EnvNode() as node:
        node.run()  
