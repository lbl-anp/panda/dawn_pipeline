#!/bin/bash

version=$1
sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:${version}
#sudo docker build . -t lblanp/panda-plugin-metsense:${version}
