#!/bin/bash

rm -rf becquerel
rm -rf curie
rm -rf curie_ros
rm -rf panda_cal
rm -rf panda_cal_ros
rm -rf poisson-gof
rm -rf stark
rm -rf stark_ros

cp -r ../../dependencies/becquerel .
cp -r ../../dependencies/curie .
cp -r ../../dependencies/curie_ros . # dev_panda branch
cp -r ../../dependencies/panda_cal .
cp -r ../../dependencies/panda_cal_ros .
cp -r ../../dependencies/poisson-gof .
cp -r ../../dependencies/stark .
cp -r ../../dependencies/stark_ros . # dev_panda branch

sudo docker build . --no-cache -t lblanp/panda:plugin-calib
