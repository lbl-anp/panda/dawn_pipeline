#!/usr/bin/env python3
import argparse
#import subprocess
import time
import waggle.plugin as plugin
import logging
import os
import glob

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--rate", default=300, type=float, help="interval in seconds between upload checks")
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(message)s',
        datefmt='%Y/%m/%d %H:%M:%S')

    plugin.init()

    BAGDIR = os.getenv('BAGDIR')
    wildcard = BAGDIR + "/*.bag"
    lastbag = None

    logging.info("uploader started")
    while True:
        time.sleep(args.rate)
        logging.info("checking for upload")

        bags = [bag for bag in glob.glob(wildcard)]
        if len(bags) == 0: 
            print("no new files to upload")
            continue
        bags = sorted(bags, key=os.path.getmtime)
        print("current list:", bags)

        try:
            start = bags.index(lastbag) + 1
        except ValueError:
            start = 0

        if lastbag == bags[-1]:
            print("no new files to upload")
        else:
            lastbag = bags[-1]
            print("uploading list:", bags[start:])
            for bag in bags[start:]:
                plugin.upload_file(bag)

if __name__ == "__main__":
    main()
