#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

source /home/panda/.bashrc
source /home/panda/panda_ws/devel/setup.bash

export ROS_LOG_DIR="/home/panda/mount"

roscore
