#!/bin/bash

rm -rf curie_ros
rm -rf stark_ros

cp -r ../../dependencies/curie_ros . # dev_panda branch
cp -r ../../dependencies/stark_ros . # dev_panda branch

version=$1
sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:${version}
