#!/usr/bin/env python3
import argparse
#import subprocess
import time
import waggle.plugin as plg
import logging
import os
import glob

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--rate", default=300, type=float, help="interval in seconds between upload checks")
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(message)s',
        datefmt='%Y/%m/%d %H:%M:%S')

    #plugin.init()

    BAGDIR = os.getenv('BAGDIR')
    wildcard = BAGDIR + "/*.bag"
    lastbag = None

    logging.info("uploader started for dir:")
    logging.info(BAGDIR)
    with plg.Plugin() as plugin:
        while True:
            time.sleep(args.rate)
            logging.info("checking for upload")

            bags = [bag for bag in glob.glob(wildcard)]
            if len(bags) == 0: 
                logging.info("no new files to upload")
                continue
            bags = sorted(bags, key=os.path.getmtime)
            logging.info("current list:")
            for bag in bags:
                logging.info(bag)

            try:
                start = bags.index(lastbag) + 1
            except ValueError:
                start = 0

            if lastbag == bags[-1]:
                logging.info("no new files to upload")
            else:
                lastbag = bags[-1]
                logging.info("uploading list:")
                for bag in bags[start:]:
                    logging.info(bag)
                    plugin.upload_file(bag)

if __name__ == "__main__":
    main()
