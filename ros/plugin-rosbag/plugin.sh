#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    #rosnode kill /dbaserh_bagger
    #At this point the child process has been killed already, so the node won't be found anymore
    #However we need to exit the parent process properly for the bags to not be marked as active.
    echo '... Sleeping for 3s'
    for i in `seq 1 3`; do
	sleep 1
	echo -en '.'
    done
    echo -en '\n'
    exit $?
}
trap control_c SIGINT

source /home/panda/.bashrc
source /home/panda/panda_ws/devel/setup.bash

export ROS_LOG_DIR="/home/panda/mount"

echo -en "ROSBAG MODE: "${MODE}'\n'
cd ${BAGDIR}
echo -en "ROSBAG_PRFX: "${PRFX}'\n'
echo -en "ROSBAG_TOPICS: "${TOPICS}'\n'

if [[ "${MODE}" == "sched" || "${MODE}" == "rand" ]]; then
    if [ ${SIZE} ]; then
        echo -en "ROSBAG_SIZE: "${SIZE}'\n'    
        #rosbag record --bz2 --size=${SIZE} -o ${PRFX} ${TOPICS}
        rosbag record --size=${SIZE} -o ${PRFX} ${TOPICS}
    elif [ ${DURATION} ]; then
        echo -en "ROSBAG_DURATION: "${DURATION}'\n'
        #rosbag record --bz2 --duration=${DURATION} -o ${PRFX} ${TOPICS}
        rosbag record --duration=${DURATION} -o ${PRFX} ${TOPICS}
    fi
    # introduce a sleep before exit to make sure the upload background process
    # moves the bag on permanent storage before
    sleep 60
else
    if [ ${SIZE} ]; then
        echo -en "ROSBAG_SIZE: "${SIZE}'\n'    
        #rosbag record --bz2 --split --size=${SIZE} -o ${PRFX} ${TOPICS}
	rosbag record --split --size=${SIZE} -o ${PRFX} ${TOPICS}
    elif [ ${DURATION} ]; then
        echo -en "ROSBAG_DURATION: "${DURATION}'\n'
        #rosbag record --bz2 --split --duration=${DURATION} -o ${PRFX} ${TOPICS}
	rosbag record --split --duration=${DURATION} -o ${PRFX} ${TOPICS}
    fi
fi

