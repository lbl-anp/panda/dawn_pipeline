#!/bin/bash

rm -rf curie_ros
rm -rf stark_ros
rm -rf rosboard
#rm -rf ros_numpy
rm -rf curie
rm -rf stark
cp -r ../../dependencies/curie_ros . # dev_panda branch
cp -r ../../dependencies/stark_ros . # dev_panda branch
cp -r ../../dependencies/rosboard .  # dev_panda branch
#cp -r ../../dependencies/ros_numpy . # detached branch
cp -r ../../dependencies/curie .     # detached branch
cp -r ../../dependencies/stark .     # detached branch

rm -rf raingauge_ros_msgs
rm -rf env_ros_msgs
rm -rf bard_ros_msgs
cp -r ../../sensors/plugin-raingauge/raingauge_ros/raingauge_ros_msgs .
cp -r ../../sensors/plugin-metsense/env_ros/env_ros_msgs .
cp -r ../../rad/plugin-bard/bard_ros/bard_ros_msgs .

version=$1
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:${version}
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:${version}
