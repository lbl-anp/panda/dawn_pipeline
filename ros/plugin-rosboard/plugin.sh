#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    #rosnode kill /dbaserh_bagger
    #At this point the child process has been killed already, so the node won't be found anymore
    #However we need to exit the parent process properly for the bags to not be marked as active.
    echo '... Sleeping for 3s'
    for i in `seq 1 3`; do
	sleep 1
	echo -en '.'
    done
    echo -en '\n'
    exit $?
}
trap control_c SIGINT

source /home/panda/.bashrc
source /home/panda/panda_ws/devel/setup.bash

export ROS_LOG_DIR="/home/panda/mount"

roslaunch rosboard rosboard.launch
