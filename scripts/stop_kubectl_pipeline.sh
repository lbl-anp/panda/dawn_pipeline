#!/bin/bash

# remove all plugins currently running
while read job _; do                                  # ignore following words on line with _
	if [[ "${job}" == *"panda"* ]]; then
		sudo kubectl delete job ${job}
	fi
done < <(command sudo kubectl get job | tail -n +2) # skip first line of output
