#!/bin/bash
# Attach containers with IO so we can intercept the SIGTERM signal for cleanup.
# For containers with bagging, this pauses the parent process long enough for bags 
# to be closed appropriately and not be flagged as active after the process is terminated.
sudo docker attach wes-panda-radenv-rosbag
sudo docker rm -f wes-panda-radenv-rosbag
sudo docker attach wes-panda-cam-rosbag
sudo docker rm -f wes-panda-cam-rosbag
sudo docker attach wes-panda-lid-rosbag
sudo docker rm -f wes-panda-lid-rosbag
sudo docker attach wes-panda-obj-rosbag
sudo docker rm -f wes-panda-obj-rosbag
sudo docker attach wes-panda-rosbag-sched
sudo docker rm -f wes-panda-rosbag-sched
sudo docker attach wes-panda-rosbag-rand
sudo docker rm -f wes-panda-rosbag-rand

# object detection
sudo docker rm -f wes-panda-obj-detection
sudo docker rm -f wes-panda-obj-depth
sudo docker rm -f wes-panda-obj-tracker

# bkg learning plugins
sudo docker rm -f wes-panda-bkg-monitoring

# calibration plugins
sudo docker rm -f wes-panda-calib-agg
sudo docker rm -f wes-panda-calib

# dbaserh plugins
sudo docker rm -f wes-panda-dbaserh-agg
sudo docker rm -f wes-panda-dbaserh

# sensor plugins
sudo docker rm -f wes-panda-raingauge
sudo docker rm -f wes-panda-metsense
sudo docker rm -f wes-panda-camera
sudo docker rm -f wes-panda-lidar
sudo docker rm -f wes-panda-audio

# roscore services
sudo docker rm -f wes-panda-rosboard
sudo docker rm -f wes-panda-rosplay
sudo docker rm -f wes-panda-roscore
