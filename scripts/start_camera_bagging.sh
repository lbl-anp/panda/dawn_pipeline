#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#
# config settings
#
# Node ID
VSN=$( cat /etc/waggle/vsn )

# Network
NETWORK_CONF="--net host --env ROS_HOSTNAME=localhost --env ROS_MASTER_URI=http://localhost:11311"

# Logging
LOGGING_CONF="--log-opt mode=non-blocking --log-opt max-buffer-size=4m"

# Shared volumes
#MOUNT_CONF="--mount src=/media/plugin-data/sensors,target=/workspace/mount,type=bind"
MOUNT_CONF="--mount src=/media/plugin-data/sensors/bags,target=/home/panda/mount/bags,type=bind"
#DATA_CONF="--env BAGDIR=/workspace/mount"

# Access kubernetes data streams for metsense and raingauge
ip=$(sudo kubectl describe service wes-rabbitmq | grep IP: | tr -d ' ' | cut -d ':' -f 2)
WAGGLE_USR="--env WAGGLE_PLUGIN_USERNAME=plugin"
WAGGLE_PWD="--env WAGGLE_PLUGIN_PASSWORD=plugin"
WAGGLE_HOST="--env WAGGLE_PLUGIN_HOST=${ip}"
WAGGLE_CONF="$WAGGLE_USR $WAGGLE_PWD $WAGGLE_HOST"

# NVIDIA
NVIDIA_CONF="--runtime=nvidia"

#
# roscore plugin
#
echo 'Launching roscore plugin'
sudo docker run -dit --name panda-roscore \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
	    ${MOUNT_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-roscore:v1.0.1
sleep 3
echo '... Sleeping for 3s to make sure all containers see roscore up and running'
for i in `seq 1 3`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'

#
# Camera plugin (scheduled bagging)
#
# W01A
#IP="--env IP=10.31.81.15"
#CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
# W022
IP="--env IP=10.31.81.14"
CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
PROFILE="--env PROFILE=6"
RATE="--env RATE=60"
RECTIFY="--env RECTIFY=False"
MODE="--env CAM_MODE=camera_readout_bag_host"
DURATION="--env DURATION=0.5"
CAM_CONF="$IP $CALIB_FILE $PROFILE $RATE $RECTIFY $MODE $DURATION"
sudo docker run -dit --name panda-camera \
        ${NETWORK_CONF} \
        ${LOGGING_CONF} \
        ${MOUNT_CONF} \
        ${NVIDIA_CONF} \
        ${CAM_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-camera:v2.3.2
echo '... done'

