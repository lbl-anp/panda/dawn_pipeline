#!/bin/bash

sudo docker rm -f panda-roscore

sudo docker rm -f panda-metsense
sudo docker rm -f panda-raingauge

sudo docker rm -f panda-dbaserh
sudo docker rm -f panda-dbaserh-agg
sudo docker rm -f panda-calib

sudo docker rm -f panda-bard

sudo docker rm -f panda-odt-soa

sudo docker rm -f panda-rosboard

sudo docker rm -f panda-rosplay
