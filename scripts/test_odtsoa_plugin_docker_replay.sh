#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#
# config settings
#
# Node ID
VSN=$( cat /etc/waggle/vsn )

# Network
NETWORK_CONF="--net host --env ROS_HOSTNAME=localhost --env ROS_MASTER_URI=http://localhost:11311"

# Logging
LOGGING_CONF="--log-opt mode=non-blocking --log-opt max-buffer-size=4m"

# Shared volumes
MOUNT_CONF="--mount src=/media/plugin-data/sensors,target=/workspace/mount,type=bind"

# Access kubernetes data streams for metsense and raingauge
ip=$(sudo kubectl describe service wes-rabbitmq | grep IP: | tr -d ' ' | cut -d ':' -f 2)
WAGGLE_USR="--env WAGGLE_PLUGIN_USERNAME=plugin"
WAGGLE_PWD="--env WAGGLE_PLUGIN_PASSWORD=plugin"
WAGGLE_HOST="--env WAGGLE_PLUGIN_HOST=${ip}"
WAGGLE_CONF="$WAGGLE_USR $WAGGLE_PWD $WAGGLE_HOST"

# NVIDIA
NVIDIA_CONF="--runtime=nvidia"

#
# roscore plugin
#
echo 'Launching roscore plugin'
sudo docker run -dit --name panda-roscore \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
	    ${MOUNT_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-roscore:v1.0.1
echo '... Sleeping for 3s to make sure all containers see roscore up and running'
for i in `seq 1 3`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'


#
# BARD
#
echo 'Launching bard plugin'
FAR="--env FAR=0.125"
#INTTIME="--env INTTIME=1"
INTTIME="--env INTTIME=0.5"
CLOUD_VSN="--env CLOUD_VSN=V037"
PANDA_VSN="--env PANDA_VSN=W01A"
PANDA_USR="--env PANDA_USR=nabgrall"
PANDA_PWD="--env PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
#PANDA_BKGFILES=""
# W01A
PANDA_BKGFILES="--env PANDA_BKGFILES=W01A_Passby_static_BADFM_trained.h5,W01A_Passby_static_BADLAD_trained.h5"
PANDA_BKGDIR="--env PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby"
BARD_CONF="$FAR $INTTIME $CLOUD_VSN $PANDA_VSN $PANDA_USR $PANDA_PWD $PANDA_BKGDIR $PANDA_BKGFILES"
sudo docker run -dit --name panda-bard \
	${NETWORK_CONF} \
	${LOGGING_CONF} \
	${MOUNT_CONF} \
	${BARD_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:latest
echo '... done'


#: <<'END'
#
# ODT-SOA plugin
#
echo 'Launching ODT-SOA plugin'
# W01A
CAM_IP="--env IP=10.31.81.15"
CAM_CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
# W022
#CAM_IP="--env IP=10.31.81.14"
#CAM_CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
OD_MODEL_FILE="--env MODEL_FILE=\"yolov7-tiny_fp16.trt\""
OD_LABEL_FILE="--env LABEL_FILE=\"yolo_coco_labels.txt\""
OD_CONF_THR="--env CONF_THR=0.25"
OD_NMS_THR="--env NMS_THR=0.45"
ODT_SOA_CONF="$CAM_IP $CAM_CALIB_FILE $OD_MODEL_FILE $OD_LABEL_FILE $OD_CONF_THR $OD_NMS_THR"
sudo docker run -dit --name panda-odt-soa \
        ${NETWORK_CONF} \
        ${LOGGING_CONF} \
        ${NVIDIA_CONF} \
        ${ODT_SOA_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa-replay:latest
echo '... done'
#END

#
# ROSBoard
#
echo 'Launching rosboard plugin'
sudo docker run -dit --name panda-rosboard \
        ${NETWORK_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:latest
echo '... done'



#: <<'END'
echo '... Sleeping for 60s before replay'
for i in `seq 1 60`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'
ROSPLAY_TOPICS="TOPICS=/sensors/raingauge /calib/listmode /bag/sensors/camera/image_color"
ROSPLAY_BAGS="BAGS="
#for file in "/media/plugin-data/sensors/bags/W01A-Drive-by/Co60"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Ba133"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Co60_1"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Co60_2"/*
for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Cs137"/*
do
  ROSPLAY_BAGS+="${file##*/} "
done
echo $ROSPLAY_TOPICS
echo $ROSPLAY_BAGS
#REPLAY_DIR="--env REPLAYDIR=/workspace/mount/bags/W01A-Drive-by/Co60"
#REPLAY_DIR="--env REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Ba133"
#REPLAY_DIR="--env REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Co60_1"
#REPLAY_DIR="--env REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Co60_2"
REPLAY_DIR="--env REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Cs137"
ROSPLAY_CONF="$REPLAY_DIR"
echo 'Launching rosbag play'
sudo docker run -dit --name panda-rosplay \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${ROSPLAY_CONF} \
            --env "$ROSPLAY_BAGS" \
            --env "$ROSPLAY_TOPICS" \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosplay:v1.0.1
echo '... done'
#END
