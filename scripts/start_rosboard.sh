#!/bin/bash

echo 'Starting rosboard service'
sudo kubectl apply -f /home/waggle/dawn_pipeline/ros/plugin-rosboard/svc/wes-rosboard.yaml

SVC_IP=$( sudo kubectl get svc | grep rosboard | awk '{print $3;}' )
echo 'Service IP ' ${SVC_IP}


