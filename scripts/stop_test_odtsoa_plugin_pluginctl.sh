#!/bin/bash

sudo pluginctl rm panda-metsense
sudo pluginctl rm panda-raingauge

sudo pluginctl rm panda-dbaserh
sudo pluginctl rm panda-dbaserh-agg
sudo pluginctl rm panda-calib

sudo pluginctl rm panda-bard

sudo pluginctl rm panda-odt-soa

sudo pluginctl rm panda-rosboard

sudo pluginctl rm panda-rosplay
