#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#///////////////////////////////////////////////////////////
#
# Node settings
#
#///////////////////////////////////////////////////////////
# Node ID
VSN=$( cat /etc/waggle/vsn )
echo 'Node VSN ' ${VSN}

# NVIDIA
NVIDIA_VIS_DEVICES="-e NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="-e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"

#///////////////////////////////////////////////////////////
#
# Plugin config
#
#///////////////////////////////////////////////////////////
# Turn plugins ON/OFF
TRAINING=true

# TRAINING
PANDA_USR="-e PANDA_USR=nabgrall"
PANDA_PWD="-e PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
TRAINING_CONF="$PANDA_USR $PANDA_PWD"


#///////////////////////////////////////////////////////////
#
# TRAINING plugin
#
#///////////////////////////////////////////////////////////
if [ "$TRAINING" = true ]; then
echo '... Launching BARD plugin'
sudo pluginctl deploy --name panda-training \
	--develop \
	${TRAINING_CONF} \
	--resource request.cpu=200m,request.memory=100Mi,limit.cpu=2000m,limit.memory=1Gi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-training:latest
echo '... done'
fi	
