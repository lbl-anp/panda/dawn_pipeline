#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#
# config settings
#
# Node ID
VSN=$( cat /etc/waggle/vsn )

# Network
NETWORK_CONF="--net host --env ROS_HOSTNAME=localhost --env ROS_MASTER_URI=http://localhost:11311"

# Logging
LOGGING_CONF="--log-opt mode=non-blocking --log-opt max-buffer-size=4m"

# Shared volumes
MOUNT_CONF="--mount src=/media/plugin-data/sensors,target=/workspace/mount,type=bind"

# Access kubernetes data streams for metsense and raingauge
ip=$(sudo kubectl describe service wes-rabbitmq | grep IP: | tr -d ' ' | cut -d ':' -f 2)
WAGGLE_USR="--env WAGGLE_PLUGIN_USERNAME=plugin"
WAGGLE_PWD="--env WAGGLE_PLUGIN_PASSWORD=plugin"
WAGGLE_HOST="--env WAGGLE_PLUGIN_HOST=${ip}"
WAGGLE_CONF="$WAGGLE_USR $WAGGLE_PWD $WAGGLE_HOST"

# NVIDIA
NVIDIA_CONF="--runtime=nvidia"

#
# roscore plugin
#
echo 'Launching roscore plugin'
sudo docker run -dit --name panda-roscore \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
	    ${MOUNT_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-roscore:v1.0.1
echo '... Sleeping for 3s to make sure all containers see roscore up and running'
for i in `seq 1 3`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'

#
# All rad+env plugins 
#
#ALL=$1
ALL=1
if [ $ALL = 1 ]; then
	echo 'Launching ENV plugins'
	# metsense
	echo '.. metsense'
	sudo docker run -dit --name panda-metsense \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:v1.0.3
	# raingauge
	echo '.. Raingauge'
	sudo docker run -dit --name panda-raingauge \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.2
	echo '... done'
	echo 'Launching RAD plugins'
	# dbaserh + aggregator
	echo '.. DBaseRH'
	# W01A
	CRYSTAL_SER="--env CRYSTAL_SER=65008-01912"
        DBASERH_SER="--env DBASERH_SER=19172912"
        DBASERH_HV="--env DBASERH_HV=1000"
        DBASERH_FGN="--env DBASERH_FGN=0.95"
        DBASERH_LLD="--env DBASERH_LLD=12"
        # W022 DEMO
	#CRYSTAL_SER="--env CRYSTAL_SER=65008-00951"
	#DBASERH_SER="--env DBASERH_SER=0"
	#DBASERH_HV="--env DBASERH_HV=900"
	#DBASERH_FGN="--env DBASERH_FGN=0.75"
	#DBASERH_LLD="--env DBASERH_LLD=12"
	# back up bar for W022 DEMO
	#CRYSTAL_SER="--env CRYSTAL_SER=65008-01003"
	#DBASERH_SER="--env DBASERH_SER=0"
	#DBASERH_HV="--env DBASERH_HV=950"
	#DBASERH_FGN="--env DBASERH_FGN=0.85"
	#DBASERH_LLD="--env DBASERH_LLD=12"
	DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
        sudo docker run -dit --name panda-dbaserh \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${DBASERH_CONF} \
            --device /dev/bus/usb \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.12
        echo '.. DBsaeRH aggregator'
	DBASERH_AGG_CONF="--env MODE=daq_listmode_aggregator"
	sudo docker run -dit --name panda-dbaserh-agg \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${DBASERH_AGG_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v2.0.0
	# calibration
	echo '.. Calibration'
	INTTIME="--env INTTIME=240"
	OVERLAP="--env OVERLAP=180"
	FAR="--env FAR=0.125"
	CALIB_CONF="$INTTIME $OVERLAP $FAR"
	sudo docker run -dit --name panda-calib \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${CALIB_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v2.0.0
	echo '... done'
fi

#
# BARD
#
echo 'Launching bard plugin'
FAR="--env FAR=0.125"
INTTIME="--env INTTIME=1"
CLOUD_VSN="--env CLOUD_VSN=V037"
PANDA_VSN="--env PANDA_VSN=W01A"
PANDA_USR="--env PANDA_USR=nabgrall"
PANDA_PWD="--env PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
#PANDA_BKGFILES=""
# W01A
PANDA_BKGFILES="--env PANDA_BKGFILES=W01A_Passby_static_BADFM_trained.h5,W01A_Passby_static_BADLAD_trained.h5"
PANDA_BKGDIR="--env PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby"
# W022
#PANDA_BKGFILES="--env PANDA_BKGFILES=W022_ANL_00951_static_BADFM_trained.h5,W022_ANL_00951_static_BADLAD_trained.h5"
#PANDA_BKGDIR="--env PANDA_BKGDIR=/workspace/mount/bkg_models/W022-ANL"
BARD_CONF="$FAR $INTTIME $CLOUD_VSN $PANDA_VSN $PANDA_USR $PANDA_PWD $PANDA_BKGDIR $PANDA_BKGFILES"
sudo docker run -dit --name panda-bard \
	${NETWORK_CONF} \
	${LOGGING_CONF} \
	${MOUNT_CONF} \
	${BARD_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:latest
echo '... done'


#: <<'END'
#
# ODT-SOA plugin
#
echo 'Launching ODT-SOA plugin'
# W01A
CAM_IP="--env IP=10.31.81.15"
CAM_CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
# W022
#CAM_IP="--env IP=10.31.81.14"
#CAM_CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib_W01A.dat\""
OD_MODEL_FILE="--env MODEL_FILE=\"yolov7-tiny_fp16.trt\""
OD_LABEL_FILE="--env LABEL_FILE=\"yolo_coco_labels.txt\""
OD_CONF_THR="--env CONF_THR=0.25"
OD_NMS_THR="--env NMS_THR=0.45"
ODT_SOA_CONF="$CAM_IP $CAM_CALIB_FILE $OD_MODEL_FILE $OD_LABEL_FILE $OD_CONF_THR $OD_NMS_THR"
sudo docker run -dit --name panda-odt-soa \
        ${NETWORK_CONF} \
        ${LOGGING_CONF} \
        ${NVIDIA_CONF} \
        ${ODT_SOA_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa:latest
echo '... done'
#END

#
# ROSBoard
#
echo 'Launching rosboard plugin'
sudo docker run -dit --name panda-rosboard \
        ${NETWORK_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:latest
echo '... done'
