#!/bin/bash

# remove all plugins currently running
while read plugin _; do                                       # ignore following words on line with _
	sudo pluginctl rm ${plugin}
done < <(command sudo pluginctl ps | head -n -1 | tail -n +2) # skip first and last (empty) lines of output
