#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#///////////////////////////////////////////////////////////
#
# Node settings
#
#///////////////////////////////////////////////////////////
# Node ID
VSN=$( cat /etc/waggle/vsn )
NXCORE=$( sudo kubectl get node | grep nxcore | awk '{print $1;}' )
NXAGENT=$( sudo kubectl get node | grep nxagent | awk '{print $1;}' )
echo 'Node VSN ' ${VSN}
echo 'NX core node ' ${NXCORE}
echo 'NX agent node ' ${NXAGENT}

# NVIDIA
NVIDIA_VIS_DEVICES="-e NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="-e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"


#///////////////////////////////////////////////////////////
#
# Plugin config
#
#///////////////////////////////////////////////////////////
# Turn plugins ON/OFF
RAINGAUGE=false
METSENSE=false
CAMERA=false
LIDAR=false
AUDIO=false

DBASERH=false
CALIB=false

BKGMON=false

OBJDET=true
OBJDEPTH=false
OBJTRK=false

ROSBAG_RADENV=false
ROSBAG_CAM=false
ROSBAG_LID=false
ROSBAG_OBJ=false

ROSBOARD=false

# metsense 
METSENSE_TOPICS="/sensors/env/temperature /sensors/env/pressure /sensors/env/humidity"

# rain gauge
RAINGAUGE_TOPICS="/sensors/raingauge"

# camera
CAM_MODE="-e CAM_MODE=get_camera_data"
if [ "$VSN" = "W01A" ]; then
	CAM_IP="-e IP=10.31.81.15"
fi
if [ "$VSN" = "W01B" ]; then
        CAM_IP="-e IP=10.31.81.14"
fi
if [ "$VSN" = "W01C" ]; then
        CAM_IP="-e IP=10.31.81.12"
fi
if [ "$VSN" = "W022" ]; then
        CAM_IP="-e IP=10.31.81.14"
fi
CAM_CALIB_FILE="-e CALIB_FILE=cam_panda_calib.dat"
CAM_PROFILE="-e PROFILE=6"
CAM_PUB_RATE="-e RATE=0.5"
CAM_RECTIFY="-e RECTIFY=False"
CAM_CONF="$CAM_MODE $CAM_IP $CAM_CALIB_FILE $CAM_PROFILE $CAM_PUB_RATE $CAM_RECTIFY"
if [ "$CAM_RECTIFY" = "-e RECTIFY=False" ]; then
	RECTIFY=false
	CAMERA_TOPICS="/bag/sensors/camera/image_color /bag/sensors/camera/camera_info"
else
	RECTIFY=true
	CAMERA_TOPICS="/bag/sensors/camera/image_rect_color /bag/sensors/camera/camera_info"
fi	

# lidar
if [ "$VSN" = "W01A" ]; then
	SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.24"
fi
if [ "$VSN" = "W01B" ]; then
        SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.20"
fi
if [ "$VSN" = "W01C" ]; then
        SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.24"
fi
if [ "$VSN" = "W022" ]; then
        SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.22"
fi
if [ "$VSN" = "W072" ]; then
        SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.23"
fi
if [ "$VSN" = "W077" ]; then
        SENSOR_HOSTNAME="-e SENSOR_HOSTNAME=10.31.81.22"
fi
LIDAR_PORT="-e LIDAR_PORT=7502"
IMU_PORT="-e IMU_PORT=7503"
LIDAR_MODE="-e LIDAR_MODE=512x10"
LIDAR_PUB_RATE="-e RATE=0.1"
LIDAR_CONF="$SENSOR_HOSTNAME $LIDAR_PORT $IMU_PORT $LIDAR_MODE $LIDAR_PUB_RATE"
LIDAR_TOPICS="/os_cloud_node/points"

# dbaserh
if [ "$VSN" = "W01A" ]; then
	CRYSTAL_SER="-e CRYSTAL_SER=65008-01912"
	DBASERH_SER="-e DBASERH_SER=19172912"
	DBASERH_HV="-e DBASERH_HV=1000"
	DBASERH_FGN="-e DBASERH_FGN=0.95"
	DBASERH_LLD="-e DBASERH_LLD=12"
	DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W01B" ]; then
        CRYSTAL_SER="-e CRYSTAL_SER=65008-00962"
        DBASERH_SER="-e DBASERH_SER=19172912"
        DBASERH_HV="-e DBASERH_HV=950"
        DBASERH_FGN="-e DBASERH_FGN=0.72"
        DBASERH_LLD="-e DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W01C" ]; then
        CRYSTAL_SER="-e CRYSTAL_SER=65008-02504"
        DBASERH_SER="-e DBASERH_SER=21223133"
        DBASERH_HV="-e DBASERH_HV=1000"
        DBASERH_FGN="-e DBASERH_FGN=0.8"
        DBASERH_LLD="-e DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W022" ]; then
        CRYSTAL_SER="-e CRYSTAL_SER=65008-01003"
        DBASERH_SER="-e DBASERH_SER=20189980"
        DBASERH_HV="-e DBASERH_HV=950"
        DBASERH_FGN="-e DBASERH_FGN=0.85"
        DBASERH_LLD="-e DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
DBASERH_AGG_CONF="-e MODE=daq_listmode_aggregator"
DBASERH_TOPICS="/dbaserh/listmode"

# calibration
INTTIME="-e INTTIME=240"
OVERLAP="-e OVERLAP=180"
FAR="-e FAR=0.125"
CALIB_CONF="$INTTIME $OVERLAP $FAR"
CALIB_AGG_CONF="-e MODE=calib_listmode_aggregator"
CALIB_TOPICS="/calib/listmode /calib/params"

# background monitoring
BKGMON_TOPICS="/bkg_monitoring/triage_output /bkg_monitoring/training_request /bkg_monitoring/training_return /bkg_monitoring/aic_request /bkg_monitoring/aic_return /bkg_monitoring/anomaly_detection_models /bkg_monitoring/anomaly_detection_metric"

# object detection
if [ "$RECTIFY" = false ]; then
	OBJDET_IMG_TOPIC="-e IMG_TOPIC=/sensors/camera/image_color"
else
	OBJDET_IMG_TOPIC="-e IMG_TOPIC=/sensors/camera/image_rect_color"
fi
OBJDET_HZ="-e OD_MIN_PERIOD=1" #1 Hz inference
OBJDET_ENGINE="-e OD_ENGINE=/workspace/yolov7/yolov7-models/yolov7-tiny.trt"
OBJDET_LIB="-e OD_LIBRARY=/workspace/yolov7/yolov7-libs/lib_yolov7_nms.py"
OBJDET_LABELS="-e OD_LABELS=/workspace/yolov7/yolov7-labels/yolo_coco_labels.txt"
OBJDET_CONF_THR="-e OD_CONF_THR=0.35"
OBJDET_NMS_THR="-e OD_NMS_THR=0.65"
OBJDET_CONF="$OBJDET_IMG_TOPIC $OBJDET_HZ $OBJDET_ENGINE $OBJDET_LIB $OBJDET_LABELS $OBJDET_CONF_THR $OBJDET_NMS_THR"
OBJDET_TOPICS="/object_detection/detections"

# depth extraction
if [ "$RECTIFY" = false ]; then
        OBJDEPTH_IMG_RECT="-e IMG_RECT=False"
else
        OBJDEPTH_IMG_RECT="-e IMG_RECT=True"
fi
MAP_FILE="-e MAP_FILE=xyz_map.h5"
CAM_INFO_TOPIC="-e CAM_INFO_TOPIC=/sensors/camera/camera_info"
OBJDEPTH_CONF="$OBJDEPTH_IMG_RECT $MAP_FILE $CAM_INFO_TOPIC"
OBJDEPTH_TOPICS="/depth_extraction/detections"

# tracking
OBJTRK_MATCH_THR="-e MATCH_THR=0.8"
OBJTRK_FILTER_THR="-e FILTER_THR=0.2"
OBJTRK_CONF="$OBJTRK_MATCH_THR $OBJTRK_FILTER_THR"
OBJTRK_TOPICS="/object_tracker/tracks"


# rosbag
BAG_SIZE="-e SIZE=256"
BAG_DIR="-e BAGDIR=/home/panda/mount/bags"
if [ "$ROSBAG_RADENV" = true ]; then
RADENV_BAG_PRFX="-e PRFX=panda-radenv-stream"
RADENV_PLUGINS=( "METSENSE" "RAINGAUGE" "DBASERH" "CALIB" "BKGMON" )
RADENV_BAG_TOPICS="TOPICS="
for plugin in "${RADENV_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    RADENV_BAG_TOPICS+="${!topics} "
done
echo ".. radenv bagged topics: $RADENV_BAG_TOPICS"
RADENV_BAG_CONF="$BAG_SIZE $BAG_DIR $RADENV_BAG_PRFX"
fi
if [ "$ROSBAG_CAM" = true ]; then
CAM_BAG_PRFX="-e PRFX=panda-cam-stream"
CAM_PLUGINS=( "CAMERA" )
CAM_BAG_TOPICS="TOPICS="
for plugin in "${CAM_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    CAM_BAG_TOPICS+="${!topics} "
done
echo ".. camera bagged topics: $CAM_BAG_TOPICS"
CAM_BAG_CONF="$BAG_SIZE $BAG_DIR $CAM_BAG_PRFX"
fi
if [ "$ROSBAG_LID" = true ]; then
LID_BAG_PRFX="-e PRFX=panda-lid-stream"
LID_PLUGINS=( "LIDAR"  )
LID_BAG_TOPICS="TOPICS="
for plugin in "${LID_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    LID_BAG_TOPICS+="${!topics} "
done
echo ".. lidar bagged topics: $LID_BAG_TOPICS"
LID_BAG_CONF="$BAG_SIZE $BAG_DIR $LID_BAG_PRFX"
fi
if [ "$ROSBAG_OBJ" = true ]; then
OBJ_BAG_PRFX="-e PRFX=panda-obj-stream"
OBJ_PLUGINS=( "OBJDET" "OBJDEPTH" "OBJTRK" )
OBJ_BAG_TOPICS="TOPICS="
for plugin in "${OBJ_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    OBJ_BAG_TOPICS+="${!topics} "
done
echo ".. obj bagged topics: $OBJ_BAG_TOPICS"
OBJ_BAG_CONF="$BAG_SIZE $BAG_DIR $OBJ_BAG_PRFX"
fi


#///////////////////////////////////////////////////////////
#
# sensor plugins
#
#///////////////////////////////////////////////////////////
if [ "$RAINGAUGE" = true ]; then
echo 'Launching raingauge plugin'
sudo pluginctl deploy --name wes-panda-raingauge \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.1
echo '... done'
fi
if [ "$METSENSE" = true ]; then
echo 'Launching env plugin'
sudo pluginctl deploy --name wes-panda-metsense \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:v1.0.2
echo '... done'
fi
if [ "$CAMERA" = true ]; then
echo 'Launching camera plugin'
sudo pluginctl deploy --name wes-panda-camera \
	${CAM_CONF} \
	--privileged --node ${NXCORE} \
	--resource limit.memory=1000Mi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-camera:v1.0.18
echo '... done'
fi
if [ "$LIDAR" = true ]; then
echo 'Launching lidar plugin'
sudo pluginctl deploy --name wes-panda-lidar \
	${LIDAR_CONF} \
	--privileged --node ${NXCORE} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-lidar:v1.0.2
echo '... done'
fi
if [ "$AUDIO" = true ]; then
echo 'Launching audio plugin'
sudo pluginctl deploy --name wes-panda-audio \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-audio:v1.0.1 \
	--rate 30
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# dbaserh plugin
#
#///////////////////////////////////////////////////////////
if [ "$DBASERH" = true ]; then
echo '... Launching dbaserh plugin'
sudo pluginctl deploy --name wes-panda-dbaserh \
	--privileged --node ${NXCORE} \
	${DBASERH_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.9
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# calibration plugin
#
#///////////////////////////////////////////////////////////
if [ "$CALIB" = true ]; then
echo '... Launching listmode-agg plugin'
sudo pluginctl deploy --name wes-panda-dbaserh-agg \
	${DBASERH_AGG_CONF} \
	--privileged --node ${NXCORE} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v1.0.4
echo '... done'
echo '... Launching calibration plugin'
sudo pluginctl deploy --name wes-panda-calib \
	${CALIB_CONF} \
	--privileged --node ${NXCORE} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v1.0.8
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# bkg monitoring plugin
#
#///////////////////////////////////////////////////////////
if [ "$BKGMON" = true ]; then
echo '... Launching listmode-agg plugin'
sudo pluginctl deploy --name wes-panda-calib-agg \
	${CALIB_AGG_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v1.0.4
echo '... done'
echo '... Launching bkg monitoring plugin'
sudo pluginctl deploy --name wes-panda-bkg-monitoring \
	${BKGMON_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bkg-monitoring:v1.0.1
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# object detection/depth extraction/tracking plugins
#
#///////////////////////////////////////////////////////////
if [ "$OBJDET" = true ]; then
echo '... Launching obj detection plugin'
sudo pluginctl deploy --name wes-panda-obj-detection \
	--privileged --node ${NXAGENT} \
	--resource limit.memory=1500Mi \
	${OBJDET_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-detection:v1.0.8
echo '... done'
fi
if [ "$OBJDEPTH" = true ]; then
echo '... Launching obj-depth plugin'
sudo pluginctl deploy --name wes-panda-obj-depth \
	--privileged --node ${NXAGENT} \
	${OBJDEPTH_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-depth:v1.0.7
echo '... done'
fi
if [ "$OBJTRK" = true ]; then
echo '... Launching obj-tracking plugin'
sudo pluginctl deploy --name wes-panda-obj-tracker \
	--privileged --node ${NXAGENT} \
	${OBJTRK_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-tracker:v1.0.3
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# rosbag plugin
#
#///////////////////////////////////////////////////////////
if [ "$ROSBAG_RADENV" = true ]; then
echo 'Launching rosbag plugin'
sudo pluginctl deploy --name wes-panda-radenv-rosbag \
	${RADENV_BAG_CONF} \
	-e "$RADENV_BAG_TOPICS" \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_CAM" = true ]; then
echo 'Launching rosbag plugin'
sudo pluginctl deploy --name wes-panda-cam-rosbag \
	${CAM_BAG_CONF} \
	-e "$CAM_BAG_TOPICS" \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_LID" = true ]; then
echo 'Launching rosbag plugin'
sudo pluginctl deploy --name wes-panda-lid-rosbag \
	${LID_BAG_CONF} \
	-e "$LID_BAG_TOPICS" \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_OBJ" = true ]; then
echo 'Launching rosbag plugin'
sudo pluginctl deploy --name wes-panda-obj-rosbag \
	${OBJ_BAG_CONF} \
	-e "$OBJ_BAG_TOPICS" \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag:v1.0.1
echo '... done'
fi

