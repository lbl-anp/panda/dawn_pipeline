#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#///////////////////////////////////////////////////////////
#
# Node settings
#
#///////////////////////////////////////////////////////////
# Node ID
VSN=$( cat /etc/waggle/vsn )

# Network
NETWORK_CONF="--net host --env ROS_HOSTNAME=localhost --env ROS_MASTER_URI=http://localhost:11311"

# Logging
LOGGING_CONF="--log-opt mode=non-blocking --log-opt max-buffer-size=4m"

# Shared volumes
MOUNT_CONF="--mount src=/media/plugin-data/sensors,target=/home/panda/mount,type=bind"

# Access kubernetes data streams for metsense and raingauge
ip=$(sudo kubectl describe service wes-rabbitmq | grep IP: | tr -d ' ' | cut -d ':' -f 2)
WAGGLE_USR="--env WAGGLE_PLUGIN_USERNAME=plugin"
WAGGLE_PWD="--env WAGGLE_PLUGIN_PASSWORD=plugin"
WAGGLE_HOST="--env WAGGLE_PLUGIN_HOST=${ip}"
WAGGLE_CONF="$WAGGLE_USR $WAGGLE_PWD $WAGGLE_HOST"

# NVIDIA
NVIDIA_VIS_DEVICES="--env NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="--env NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"


#///////////////////////////////////////////////////////////
#
# Plugin config
#
#///////////////////////////////////////////////////////////
# Turn plugins ON/OFF
RAINGAUGE=false
METSENSE=false
CAMERA=false
LIDAR=false
AUDIO=false

DBASERH=false
CALIB=true
BKGMON=false

OBJDET=false
OBJDEPTH=false
OBJTRK=false

ROSBAG_RADENV=false
ROSBAG_CAM=false
ROSBAG_LID=false
ROSBAG_OBJ=false
ROSPLAY=false
ROSBOARD=false


# metsense 
METSENSE_TOPICS="/sensors/env/temperature /sensors/env/pressure /sensors/env/humidity"

# rain gauge
RAINGAUGE_TOPICS="/sensors/raingauge"

# camera
CAM_MODE="--env CAM_MODE=get_camera_data"
if [ "$VSN" = "W01A" ]; then
	CAM_IP="--env IP=10.31.81.15"
fi
if [ "$VSN" = "W01B" ]; then
        CAM_IP="--env IP=10.31.81.14"
fi
if [ "$VSN" = "W01C" ]; then
        CAM_IP="--env IP=10.31.81.12"
fi
if [ "$VSN" = "W022" ]; then
        CAM_IP="--env IP=10.31.81.14"
fi
CAM_CALIB_FILE="--env CALIB_FILE=\"cam_panda_calib.dat\""
CAM_PROFILE="--env PROFILE=6"
CAM_PUB_RATE="--env RATE=60"
CAM_RECTIFY="--env RECTIFY=False"
CAM_CONF="$CAM_MODE $CAM_IP $CAM_CALIB_FILE $CAM_PROFILE $CAM_PUB_RATE $CAM_RECTIFY"
if [ "$CAM_RECTIFY" = "--env RECTIFY=False" ]; then
	RECTIFY=false
	CAMERA_TOPICS="/bag/sensors/camera/image_color /bag/sensors/camera/camera_info"
else
	RECTIFY=true
	CAMERA_TOPICS="/bag/sensors/camera/image_rect_color /bag/sensors/camera/camera_info"
fi	

# lidar
if [ "$VSN" = "W01A" ]; then
	SENSOR_HOSTNAME="--env SENSOR_HOSTNAME=\"10.31.81.24\""
fi
if [ "$VSN" = "W01B" ]; then
        SENSOR_HOSTNAME="--env SENSOR_HOSTNAME=\"10.31.81.20\""
fi
if [ "$VSN" = "W01C" ]; then
        SENSOR_HOSTNAME="--env SENSOR_HOSTNAME=\"10.31.81.24\""
fi
if [ "$VSN" = "W022" ]; then
        SENSOR_HOSTNAME="--env SENSOR_HOSTNAME=\"10.31.81.22\""
fi
if [ "$VSN" = "W076" ]; then
        SENSOR_HOSTNAME="--env SENSOR_HOSTNAME=\"10.31.81.21\""
fi
LIDAR_PORT="--env LIDAR_PORT=7502"
IMU_PORT="--env IMU_PORT=7503"
LIDAR_MODE="--env LIDAR_MODE=\"512x10\""
LIDAR_PUB_RATE="--env RATE=0.1"
LIDAR_CONF="$SENSOR_HOSTNAME $LIDAR_PORT $IMU_PORT $LIDAR_MODE $LIDAR_PUB_RATE"
LIDAR_TOPICS="/os_cloud_node/points"

# audio
PULSE_SERVER="--env PULSE_SERVER=$(sudo kubectl get service | grep wes-audio-server | awk '{print $3}')"
AUDIO_CONF="$PULSE_SERVER"

# dbaserh
if [ "$VSN" = "W01A" ]; then
	CRYSTAL_SER="--env CRYSTAL_SER=65008-01912"
	DBASERH_SER="--env DBASERH_SER=19172912"
	DBASERH_HV="--env DBASERH_HV=1000"
	DBASERH_FGN="--env DBASERH_FGN=0.95"
	DBASERH_LLD="--env DBASERH_LLD=12"
	DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W01B" ]; then
        CRYSTAL_SER="--env CRYSTAL_SER=65008-00962"
        DBASERH_SER="--env DBASERH_SER=19172912"
        DBASERH_HV="--env DBASERH_HV=950"
        DBASERH_FGN="--env DBASERH_FGN=0.72"
        DBASERH_LLD="--env DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W01C" ]; then
        CRYSTAL_SER="--env CRYSTAL_SER=65008-02504"
        DBASERH_SER="--env DBASERH_SER=21223133"
        DBASERH_HV="--env DBASERH_HV=1000"
        DBASERH_FGN="--env DBASERH_FGN=0.8"
        DBASERH_LLD="--env DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W022" ]; then
        CRYSTAL_SER="--env CRYSTAL_SER=65008-01003"
        DBASERH_SER="--env DBASERH_SER=20189980"
        DBASERH_HV="--env DBASERH_HV=950"
        DBASERH_FGN="--env DBASERH_FGN=0.85"
        DBASERH_LLD="--env DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
if [ "$VSN" = "W05E" ]; then
        CRYSTAL_SER="--env CRYSTAL_SER=65008-02089"
        DBASERH_SER="--env DBASERH_SER=20189980"
        DBASERH_HV="--env DBASERH_HV=1100"
        DBASERH_FGN="--env DBASERH_FGN=0.51"
        DBASERH_LLD="--env DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
DBASERH_AGG_CONF="--env MODE=daq_listmode_aggregator"
DBASERH_TOPICS="/dbaserh/listmode"

# calibration
INTTIME="--env INTTIME=240"
OVERLAP="--env OVERLAP=180"
FAR="--env FAR=0.125"
CALIB_CONF="$INTTIME $OVERLAP $FAR"
CALIB_AGG_CONF="--env MODE=calib_listmode_aggregator"
CALIB_TOPICS="/calib/listmode /calib/params"

# background monitoring
BKGMON_TOPICS="/bkg_monitoring/triage_output /bkg_monitoring/training_request /bkg_monitoring/training_return /bkg_monitoring/aic_request /bkg_monitoring/aic_return /bkg_monitoring/anomaly_detection_models /bkg_monitoring/anomaly_detection_metric"

# object detection
if [ "$RECTIFY" = false ]; then
	if [ "$ROSPLAY" = true ]; then
		OBJDET_IMG_TOPIC="--env IMG_TOPIC=/bag/sensors/camera/image_color"
	else
		OBJDET_IMG_TOPIC="--env IMG_TOPIC=/sensors/camera/image_color"
	fi
else
	if [ "$ROSPLAY" = true ]; then
		OBJDET_IMG_TOPIC="--env IMG_TOPIC=/bag/sensors/camera/image_rect_color"
	else
		OBJDET_IMG_TOPIC="--env IMG_TOPIC=/sensors/camera/image_rect_color"
	fi
fi
OBJDET_HZ="--env OD_MIN_PERIOD=0" #1 Hz inference
OBJDET_ENGINE="--env OD_ENGINE=/workspace/yolov7/yolov7-models/yolov7-tiny.trt"
OBJDET_LIB="--env OD_LIBRARY=/workspace/yolov7/yolov7-libs/lib_yolov7_nms.py"
OBJDET_LABELS="--env OD_LABELS=/workspace/yolov7/yolov7-labels/yolo_coco_labels.txt"
OBJDET_CONF_THR="--env OD_CONF_THR=0.1"
OBJDET_NMS_THR="--env OD_NMS_THR=0.45"
OBJDET_CONF="$OBJDET_IMG_TOPIC $OBJDET_HZ $OBJDET_ENGINE $OBJDET_LIB $OBJDET_LABELS $OBJDET_CONF_THR $OBJDET_NMS_THR"
OBJDET_TOPICS="/object_detection/detections"

# depth extraction
if [ "$RECTIFY" = false ]; then
        OBJDEPTH_IMG_RECT="--env IMG_RECT=False"
else
        OBJDEPTH_IMG_RECT="--env IMG_RECT=True"
fi
if [ "$ROSPLAY" = true ]; then
	CAM_INFO_TOPIC="--env CAM_INFO_TOPIC=/bag/sensors/camera/camera_info"
else
	CAM_INFO_TOPIC="--env CAM_INFO_TOPIC=/sensors/camera/camera_info"
fi
MAP_FILE="--env MAP_FILE=xyz_map.h5"
OBJDEPTH_CONF="$OBJDEPTH_IMG_RECT $MAP_FILE $CAM_INFO_TOPIC"
OBJDEPTH_TOPICS="/depth_extraction/detections"

# tracking
OBJTRK_MATCH_THR="--env MATCH_THR=0.8"
OBJTRK_FILTER_THR="--env FILTER_THR=0.2"
OBJTRK_CONF="$OBJTRK_MATCH_THR $OBJTRK_FILTER_THR"
OBJTRK_TOPICS="/object_tracker/tracks"

# rosbag
BAG_SIZE="--env SIZE=5"
BAG_DIR="--env BAGDIR=/home/panda/mount/bags"
if [ "$ROSBAG_RADENV" = true ]; then
RADENV_BAG_PRFX="--env PRFX=panda-radenv-stream"
RADENV_PLUGINS=( "METSENSE" "RAINGAUGE" "DBASERH" "CALIB" "BKGMON" )
RADENV_BAG_TOPICS="TOPICS="
for plugin in "${RADENV_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    RADENV_BAG_TOPICS+="${!topics} "
done
echo ".. radenv bagged topics: $RADENV_BAG_TOPICS"
RADENV_BAG_CONF="$BAG_SIZE $BAG_DIR $RADENV_BAG_PRFX"
fi
if [ "$ROSBAG_CAM" = true ]; then
CAM_BAG_PRFX="--env PRFX=panda-cam-stream"
CAM_PLUGINS=( "CAMERA" )
CAM_BAG_TOPICS="TOPICS="
for plugin in "${CAM_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    CAM_BAG_TOPICS+="${!topics} "
done
echo ".. camera bagged topics: $CAM_BAG_TOPICS"
CAM_BAG_CONF="$BAG_SIZE $BAG_DIR $CAM_BAG_PRFX"
fi
if [ "$ROSBAG_LID" = true ]; then
LID_BAG_PRFX="--env PRFX=panda-lid-stream"
LID_PLUGINS=( "LIDAR"  )
LID_BAG_TOPICS="TOPICS="
for plugin in "${LID_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    LID_BAG_TOPICS+="${!topics} "
done
echo ".. lidar bagged topics: $LID_BAG_TOPICS"
LID_BAG_CONF="$BAG_SIZE $BAG_DIR $LID_BAG_PRFX"
fi
if [ "$ROSBAG_OBJ" = true ]; then
OBJ_BAG_PRFX="--env PRFX=panda-obj-stream"
OBJ_PLUGINS=( "OBJDET" "OBJDEPTH" "OBJTRK" )
OBJ_BAG_TOPICS="TOPICS="
for plugin in "${OBJ_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    OBJ_BAG_TOPICS+="${!topics} "
done
echo ".. obj bagged topics: $OBJ_BAG_TOPICS"
OBJ_BAG_CONF="$BAG_SIZE $BAG_DIR $OBJ_BAG_PRFX"
fi

# rosplay
if [ "$ROSPLAY" = true ]; then
#ROSPLAY_PLUGINS=( "CAMERA" "METSENSE" "RAINGAUGE" "DBASERH" )
ROSPLAY_PLUGINS=( "CAMERA" )
ROSPLAY_TOPICS="TOPICS="
for plugin in "${ROSPLAY_PLUGINS[@]}"; do
    #echo ".. $plugin replay is ON" 
    topics=$plugin"_TOPICS"
    ROSPLAY_TOPICS+="${!topics} "
done
echo ".. replayed topics: $ROSPLAY_TOPICS"
ROSPLAY_BAGS="BAGS="
for file in "$1"/*
do
  ROSPLAY_BAGS+="${file##*/} "
done
echo ".. replaying from bags in $1:"
echo $ROSPLAY_BAGS
REPLAY_DIR="--env REPLAYDIR=/home/panda/mount/bags/replay/test_OD"
ROSPLAY_CONF="$REPLAY_DIR"
fi


#///////////////////////////////////////////////////////////
#
# roscore plugin
#
#///////////////////////////////////////////////////////////
echo 'Launching roscore plugin'
sudo docker run -dit --name wes-panda-roscore \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
	    ${MOUNT_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-roscore:v1.0.1
sleep 3
echo '... Sleeping for 3s to make sure all containers will see roscore up and running'
for i in `seq 1 3`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'


#///////////////////////////////////////////////////////////
#
# sensor plugins
#
#///////////////////////////////////////////////////////////
if [ "$RAINGAUGE" = true ]; then
echo 'Launching raingauge plugin'
sudo docker run -dit --name wes-panda-raingauge \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.1
echo '... done'
fi
if [ "$METSENSE" = true ]; then
echo 'Launching env plugin'
sudo docker run -dit --name wes-panda-metsense \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:v1.0.2
echo '... done'
fi
if [ "$CAMERA" = true ]; then
echo 'Launching camera plugin'
sudo docker run -dit --name wes-panda-camera \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            ${CAM_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-camera:v1.0.18
echo '... done'
fi
if [ "$LIDAR" = true ]; then
echo 'Launching lidar plugin'
sudo docker run -dit --name wes-panda-lidar \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            ${LIDAR_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-lidar:v1.0.2
echo '... done'
fi
if [ "$AUDIO" = true ]; then
echo 'Launching audio plugin'
sudo docker run -dit --name wes-panda-audio \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
	    ${AUDIO_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-audio:v1.0.1 \
	    --rate 30
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# dbaserh plugin
#
#///////////////////////////////////////////////////////////
if [ "$DBASERH" = true ]; then
echo '... Launching dbaserh plugin'
sudo docker run -dit --name wes-panda-dbaserh \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
	    ${DBASERH_CONF} \
	    --device /dev/bus/usb \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.10
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# calibration plugin
#
#///////////////////////////////////////////////////////////
if [ "$CALIB" = true ]; then
#echo '... Launching listmode-agg plugin'
#sudo docker run -dit --name wes-panda-dbaserh-agg \
#            ${NETWORK_CONF} \
#            ${LOGGING_CONF} \
#            ${MOUNT_CONF} \
#            ${DBASERH_AGG_CONF} \
#            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v1.0.4
#echo '... done'
echo '... Launching calibration plugin'
sudo docker run -dit --name wes-panda-calib \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
	    ${CALIB_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:test
#            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v1.0.8
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# bkg monitoring plugin
#
#///////////////////////////////////////////////////////////
if [ "$BKGMON" = true ]; then
echo '... Launching listmode-agg plugin'
sudo docker run -dit --name wes-panda-calib-agg \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${CALIB_AGG_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v1.0.4
echo '... done'
echo '... Launching bkg monitoring plugin'
sudo docker run -dit --name wes-panda-bkg-monitoring \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bkg-monitoring:v1.0.1
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# object detection/depth extraction/tracking plugins
#
#///////////////////////////////////////////////////////////
if [ "$OBJDET" = true ]; then
echo '... Launching obj detection plugin'
sudo docker run -dit --name wes-panda-obj-detection \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
	    ${NVIDIA_CONF} \
	    ${OBJDET_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-detection:v1.0.8
echo '... done'
fi
if [ "$OBJDEPTH" = true ]; then
echo '... Launching obj-depth plugin'
sudo docker run -dit --name wes-panda-obj-depth \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            ${NVIDIA_CONF} \
	    ${OBJDEPTH_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-depth:v1.0.7
echo '... done'
fi
if [ "$OBJTRK" = true ]; then
echo '... Launching obj-tracking plugin'
sudo docker run -dit --name wes-panda-obj-tracker \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            ${NVIDIA_CONF}\
	    ${OBJTRK_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-tracker:v1.0.3
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# rosbag plugin
#
#///////////////////////////////////////////////////////////
if [ "$ROSBAG_RADENV" = true ]; then
echo 'Launching rosbag plugin'
sudo docker run -dit --name wes-panda-radenv-rosbag \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${RADENV_BAG_CONF} \
	    --env "$RADENV_BAG_TOPICS" \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_CAM" = true ]; then
echo 'Launching rosbag plugin'
sudo docker run -dit --name wes-panda-cam-rosbag \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${CAM_BAG_CONF} \
	    --env "$CAM_BAG_TOPICS" \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_LID" = true ]; then
echo 'Launching rosbag plugin'
sudo docker run -dit --name wes-panda-lid-rosbag \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${LID_BAG_CONF} \
	    --env "$LID_BAG_TOPICS" \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:v1.0.1
echo '... done'
fi
if [ "$ROSBAG_OBJ" = true ]; then
echo 'Launching rosbag plugin'
sudo docker run -dit --name wes-panda-obj-rosbag \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${OBJ_BAG_CONF} \
	    --env "$OBJ_BAG_TOPICS" \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:v1.0.1
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# rosplay plugin 
#
#///////////////////////////////////////////////////////////
if [ "$ROSPLAY" = true ]; then
echo 'Launching rosbag replay'
sudo docker run -dit --name wes-panda-rosplay \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${DATA_CONF} \
            ${ROSPLAY_CONF} \
	    --env "$ROSPLAY_BAGS" \
	    --env "$ROSPLAY_TOPICS" \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosplay:v1.0.1
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# rosboard plugin
#
#///////////////////////////////////////////////////////////
if [ "$ROSBOARD" = true ]; then
echo 'Launching rosboard plugin'
sudo docker run -dit --name wes-panda-rosboard \
            ${NETWORK_CONF} \
            -p 4200:8888 \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:v1.0.1
echo '... done'
fi

