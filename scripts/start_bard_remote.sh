#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#///////////////////////////////////////////////////////////
#
# Node settings
#
#///////////////////////////////////////////////////////////
# Node ID
VSN=$( cat /etc/waggle/vsn )
NXCORE=$( sudo kubectl get node | grep nxcore | awk '{print $1;}' )
NXAGENT=$( sudo kubectl get node | grep nxagent | awk '{print $1;}' )
echo 'Node VSN ' ${VSN}
echo 'NX core node ' ${NXCORE}
echo 'NX agent node ' ${NXAGENT}

# NVIDIA
NVIDIA_VIS_DEVICES="-e NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="-e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"

#///////////////////////////////////////////////////////////
#
# Plugin config
#
#///////////////////////////////////////////////////////////
# Turn plugins ON/OFF
RAINGAUGE=false
DBASERH=false
CALIB=false
BARD=true

# dbaserh
if [ "$VSN" = "W01A" ]; then
        CRYSTAL_SER="-e CRYSTAL_SER=65008-01912"
        DBASERH_SER="-e DBASERH_SER=19172912"
        DBASERH_HV="-e DBASERH_HV=1000"
        DBASERH_FGN="-e DBASERH_FGN=0.95"
        DBASERH_LLD="-e DBASERH_LLD=12"
        DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
fi
DBASERH_AGG_CONF="-e MODE=daq_listmode_aggregator"

# calibration
INTTIME="-e INTTIME=240"
OVERLAP="-e OVERLAP=180"
FAR="-e FAR=0.125"
CALIB_CONF="$INTTIME $OVERLAP $FAR"

# BARD
PANDA_USR="-e PANDA_USR=nabgrall"
PANDA_PWD="-e PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
PANDA_VSN="-e PANDA_VSN=${VSN}"
CLOUD_VSN="-e CLOUD_VSN=V037"
BARD_CONF="$PANDA_USR $PANDA_PWD $PANDA_VSN $CLOUD_VSN"


#///////////////////////////////////////////////////////////
#
# sensor plugins
#
#///////////////////////////////////////////////////////////
if [ "$RAINGAUGE" = true ]; then
echo 'Launching raingauge plugin'
sudo pluginctl deploy --name panda-raingauge \
	--privileged --node ${NXCORE} \
	--resource request.cpu=10m,request.memory=50Mi,limit.cpu=500m,limit.memory=500Mi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.1
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# dbaserh plugin
#
#///////////////////////////////////////////////////////////
if [ "$DBASERH" = true ]; then
echo '... Launching dbaserh plugin'
sudo pluginctl deploy --name panda-dbaserh \
	--privileged --node ${NXCORE} \
	--resource request.cpu=100m,request.memory=100Mi,limit.cpu=500m,limit.memory=500Mi \
	${DBASERH_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.12
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# calibration plugin
#
#///////////////////////////////////////////////////////////
if [ "$CALIB" = true ]; then
echo '... Launching listmode-agg plugin'
sudo pluginctl deploy --name panda-dbaserh-agg \
	${DBASERH_AGG_CONF} \
	--privileged --node ${NXCORE} \
	--resource request.cpu=100m,request.memory=100Mi,limit.cpu=500m,limit.memory=500Mi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v1.0.4
echo '... done'
echo '... Launching calibration plugin'
sudo pluginctl deploy --name panda-calib \
	${CALIB_CONF} \
	--privileged --node ${NXCORE} \
	--resource request.cpu=200m,request.memory=100Mi,limit.cpu=2000m,limit.memory=1Gi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v1.0.8
echo '... done'
fi


#///////////////////////////////////////////////////////////
#
# BARD plugin
#
#///////////////////////////////////////////////////////////
if [ "$BARD" = true ]; then
echo '... Launching BARD plugin'
sudo pluginctl deploy --name panda-bard \
	--develop \
	${BARD_CONF} \
	--privileged --node ${NXCORE} \
	--resource request.cpu=200m,request.memory=100Mi,limit.cpu=2000m,limit.memory=1Gi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:latest
echo '... done'
fi

