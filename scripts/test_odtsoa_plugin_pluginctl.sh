#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#
# config settings
#
# Node ID
VSN=$( cat /etc/waggle/vsn )
NXCORE=$( sudo kubectl get node | grep nxcore | awk '{print $1;}' )
NXAGENT=$( sudo kubectl get node | grep nxagent | awk '{print $1;}' )
echo 'Node VSN ' ${VSN}
echo 'NX core node ' ${NXCORE}
echo 'NX agent node ' ${NXAGENT}

# NVIDIA
NVIDIA_VIS_DEVICES="-e NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="-e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"


#
# Shared volumes with pluginctl
# This is a temp convenience feature of the pluginctl-dev binary
# that we set up to easily load bkg model files in the BARD plugin 
# at the PANDAWN ORS DEMO in DC
#
MOUNT_CONF="--volume /media/plugin-data/sensors:/workspace/mount"

#: <<'END'
#
# All rad+env plugins 
#
echo 'Launching ENV plugins'
# metsense
echo '.. metsense'
# W01A, enclosure RPi is down
#--selector zone=shield \
# W022
#--selector zone=enclosure \
sudo pluginctl-dev deploy --name panda-metsense \
	--resource request.cpu=10m,limit.cpu=500m,request.memory=50Mi,limit.memory=500Mi \
	--selector zone=shield \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:v1.0.3
# raingauge
echo '.. Raingauge'
sudo pluginctl-dev deploy --name panda-raingauge \
	--resource request.cpu=10m,limit.cpu=500m,request.memory=50Mi,limit.memory=500Mi \
	--selector zone=shield \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.2
echo '... done'
echo 'Launching RAD plugins'
# dbaserh + aggregator
echo '.. DBaseRH'
# W01A
CRYSTAL_SER="-e CRYSTAL_SER=65008-01912"
DBASERH_SER="-e DBASERH_SER=19172912"
DBASERH_HV="-e DBASERH_HV=1000"
DBASERH_FGN="-e DBASERH_FGN=0.95"
DBASERH_LLD="-e DBASERH_LLD=12"
# W022 DEMO
#CRYSTAL_SER="-e CRYSTAL_SER=65008-00951"
#DBASERH_SER="-e DBASERH_SER=0"
#DBASERH_HV="-e DBASERH_HV=900"
#DBASERH_FGN="-e DBASERH_FGN=0.75"
#DBASERH_LLD="-e DBASERH_LLD=12"
# back up bar for W022 DEMO
#CRYSTAL_SER="--env CRYSTAL_SER=65008-01003"
#DBASERH_SER="--env DBASERH_SER=0"
#DBASERH_HV="--env DBASERH_HV=950"
#DBASERH_FGN="--env DBASERH_FGN=0.85"
#DBASERH_LLD="--env DBASERH_LLD=12"
DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
sudo pluginctl-dev deploy --name panda-dbaserh \
	--privileged \
	--selector zone=core \
	--resource request.cpu=100m,limit.cpu=500m,request.memory=100Mi,limit.memory=500Mi \
	${DBASERH_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.12
echo '.. DBsaeRH aggregator'
DBASERH_AGG_CONF="-e MODE=daq_listmode_aggregator"
sudo pluginctl-dev deploy --name panda-dbaserh-agg \
	--resource request.cpu=100m,limit.cpu=500m,request.memory=100Mi,limit.memory=500Mi \
	--selector zone=core \
	${DBASERH_AGG_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v2.0.0
# calibration
echo '.. Calibration'
INTTIME="-e INTTIME=240"
OVERLAP="-e OVERLAP=180"
FAR="-e FAR=0.125"
CALIB_CONF="$INTTIME $OVERLAP $FAR"
sudo pluginctl-dev deploy --name panda-calib \
        --resource request.cpu=200m,limit.cpu=2000m,request.memory=100Mi,limit.memory=1Gi \
        --selector zone=core \
	${CALIB_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v2.0.0
echo '... done'
#END

#: <<'END'
#
# BARD
#
echo 'Launching BARD plugin'
FAR="-e FAR=0.125"
INTTIME="-e INTTIME=1"
CLOUD_VSN="-e CLOUD_VSN=V037"
PANDA_VSN="-e PANDA_VSN=W01A"
PANDA_USR="-e PANDA_USR=nabgrall"
PANDA_PWD="-e PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
#-e PANDA_BKGFILES=""
# W01A
#-e PANDA_BKGFILES="W01A_Passby_static_BADFM_trained.h5 W01A_Passby_static_BADLAD_trained.h5" \
#-e PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby \
# W022
#-e PANDA_BKGFILES="W022_ANL_00951_static_BADFM_trained.h5 W022_ANL_00951_static_BADLAD_trained.h5" \
#-e PANDA_BKGFILES="W022_ANL_01003_static_BADFM_trained.h5 W022_ANL_01003_static_BADLAD_trained.h5" \
#-e PANDA_BKGDIR=/workspace/mount/bkg_models/W022-ANL \
BARD_CONF="$FAR $INTTIME $CLOUD_VSN $PANDA_VSN $PANDA_USR $PANDA_PWD"
sudo pluginctl-dev deploy --name panda-bard \
	--resource request.cpu=200m,limit.cpu=2000m,request.memory=100Mi,limit.memory=1Gi \
	--selector zone=core \
	--volume /media/plugin-data/sensors:/workspace/mount \
	-e PANDA_BKGFILES="W01A_Passby_static_BADFM_trained.h5 W01A_Passby_static_BADLAD_trained.h5" \
	-e PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby \
	${BARD_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:latest
echo '... done'
#END

#: <<'END'
#
# ODT-SOA plugin
#
echo 'Launching ODT-SOA plugin'
# W01A
#CAM_IP="-e IP=10.31.81.15"
#CAM_CALIB_FILE="-e CALIB_FILE=\"cam_panda_calib_W01A.dat\""
# W022
CAM_IP="-e IP=10.31.81.14"
CAM_CALIB_FILE="-e CALIB_FILE=cam_panda_calib_W01A.dat"
OD_MODEL_FILE="-e MODEL_FILE=yolov7-tiny_fp16.trt"
OD_LABEL_FILE="-e LABEL_FILE=yolo_coco_labels.txt"
OD_CONF_THR="-e CONF_THR=0.25"
OD_NMS_THR="-e NMS_THR=0.45"
ODT_SOA_CONF="$CAM_IP $CAM_CALIB_FILE $OD_MODEL_FILE $OD_LABEL_FILE $OD_CONF_THR $OD_NMS_THR"
sudo pluginctl-dev deploy --name panda-odt-soa \
        --selector zone=agent \
	--develop \
	--resource request.cpu=1000m,limit.cpu=5000m,request.memory=100Mi,limit.memory=5Gi \
	${ODT_SOA_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa:latest
echo '... done'
#END

#: <<'END'
#
# ROSBoard
#
echo 'Launching rosboard plugin'
sudo pluginctl-dev deploy --develop --name panda-rosboard \
	--resource request.cpu=100m,limit.cpu=1000m,request.memory=100Mi,limit.memory=500Mi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:latest
echo '... done'
#END
#for i in `seq 1 20`; do
#    sleep 1
#    echo -n '.'
#done
#sudo kubectl port-forward --address 0.0.0.0 pod/panda-rosboard 8888:8888
