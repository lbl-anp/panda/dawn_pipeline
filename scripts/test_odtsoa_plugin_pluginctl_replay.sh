#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#
# config settings
#
# Node ID
VSN=$( cat /etc/waggle/vsn )
NXCORE=$( sudo kubectl get node | grep nxcore | awk '{print $1;}' )
NXAGENT=$( sudo kubectl get node | grep nxagent | awk '{print $1;}' )
echo 'Node VSN ' ${VSN}
echo 'NX core node ' ${NXCORE}
echo 'NX agent node ' ${NXAGENT}

# NVIDIA
NVIDIA_VIS_DEVICES="-e NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="-e NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"


#
# Shared volumes with pluginctl
# This is a temp convenience feature of the pluginctl-dev binary
# that we set up to easily load bkg model files in the BARD plugin 
# at the PANDAWN ORS DEMO in DC
#
MOUNT_CONF="--volume /media/plugin-data/sensors:/workspace/mount"


#: <<'END'
#
# BARD
#
echo 'Launching BARD plugin'
FAR="-e FAR=0.125"
INTTIME="-e INTTIME=1"
#INTTIME="-e INTTIME=0.5"
CLOUD_VSN="-e CLOUD_VSN=V037"
PANDA_VSN="-e PANDA_VSN=W01A"
PANDA_USR="-e PANDA_USR=nabgrall"
PANDA_PWD="-e PANDA_PWD=0524b37ff0019a12c45f7b5ce255616069e5da49"
#-e PANDA_BKGFILES=""
# W01A
#-e PANDA_BKGFILES="W01A_Passby_static_BADFM_trained.h5 W01A_Passby_static_BADLAD_trained.h5" \
#-e PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby \
BARD_CONF="$FAR $INTTIME $CLOUD_VSN $PANDA_VSN $PANDA_USR $PANDA_PWD"
sudo pluginctl-dev deploy --name panda-bard \
	--resource request.cpu=200m,limit.cpu=2000m,request.memory=100Mi,limit.memory=1Gi \
	--selector zone=core \
	--volume /media/plugin-data/sensors:/workspace/mount \
	-e PANDA_BKGFILES="W01A_Passby_static_BADFM_trained.h5 W01A_Passby_static_BADLAD_trained.h5" \
	-e PANDA_BKGDIR=/workspace/mount/bkg_models/W01A-Passby \
	${BARD_CONF} \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:latest
echo '... done'
#END

#: <<'END'
#
# ODT-SOA plugin
#
echo 'Launching ODT-SOA plugin'
# W01A
CAM_IP="-e IP=10.31.81.15"
CAM_CALIB_FILE="-e CALIB_FILE=cam_panda_calib_W01A.dat"
# W022
#CAM_IP="-e IP=10.31.81.14"
#CAM_CALIB_FILE="-e CALIB_FILE=cam_panda_calib_W01A.dat"
OD_MODEL_FILE="-e MODEL_FILE=yolov7-tiny_fp16.trt"
OD_LABEL_FILE="-e LABEL_FILE=yolo_coco_labels.txt"
OD_CONF_THR="-e CONF_THR=0.25"
OD_NMS_THR="-e NMS_THR=0.45"
ODT_SOA_CONF="$CAM_IP $CAM_CALIB_FILE $OD_MODEL_FILE $OD_LABEL_FILE $OD_CONF_THR $OD_NMS_THR"
sudo pluginctl-dev deploy --name panda-odt-soa \
        --selector zone=agent \
	--develop \
	--resource request.cpu=1000m,limit.cpu=5000m,request.memory=100Mi,limit.memory=5Gi \
	${ODT_SOA_CONF} \
        registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa-replay:latest
echo '... done'
#END


#: <<'END'
#
# ROSBoard
#
echo 'Launching rosboard plugin'
sudo pluginctl-dev deploy --develop --name panda-rosboard \
	--resource request.cpu=100m,limit.cpu=1000m,request.memory=100Mi,limit.memory=500Mi \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosboard:latest
echo '... done'
#END
#sudo kubectl port-forward --address 0.0.0.0 pod/panda-rosboard 8888:8888

#: <<'END'
echo '... Sleeping for 60s before replay'
for i in `seq 1 60`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'
ROSPLAY_TOPICS="TOPICS=/sensors/raingauge /calib/listmode /bag/sensors/camera/image_color"
ROSPLAY_BAGS="BAGS="
#for file in "/media/plugin-data/sensors/bags/W01A-Drive-by/Co60"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Ba133"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Co60_1"/*
#for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Co60_2"/*
for file in "/media/plugin-data/sensors/bags/W01A-Pass-by/Cs137"/*
do
  ROSPLAY_BAGS+="${file##*/} "
done
echo $ROSPLAY_TOPICS
echo $ROSPLAY_BAGS
#REPLAY_DIR="-e REPLAYDIR=/workspace/mount/bags/W01A-Drive-by/Co60"
#REPLAY_DIR="-e REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Ba133"
#REPLAY_DIR="-e REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Co60_1"
#REPLAY_DIR="-e REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Co60_2"
REPLAY_DIR="-e REPLAYDIR=/workspace/mount/bags/W01A-Pass-by/Cs137"
ROSPLAY_CONF="$REPLAY_DIR"
echo 'Launching rosbag play'
sudo pluginctl-dev deploy --name panda-rosplay \
	--resource request.cpu=100m,limit.cpu=1000m,request.memory=100Mi,limit.memory=500Mi \
	--selector zone=core \
	--volume /media/plugin-data/sensors:/workspace/mount \
	${ROSPLAY_CONF} \
	-e "$ROSPLAY_BAGS" \
	-e "$ROSPLAY_TOPICS" \
	registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosplay:v1.0.1
echo '... done'
#END
