#!/bin/bash
echo 'Launching script' ${0##*/} 'with PID:' $$

#///////////////////////////////////////////////////////////
#
# Node settings
#
#///////////////////////////////////////////////////////////
# Node ID
VSN=$( cat /etc/waggle/vsn )

# Network
NETWORK_CONF="--net host --env ROS_HOSTNAME=localhost --env ROS_MASTER_URI=http://localhost:11311"

# Logging
LOGGING_CONF="--log-opt mode=non-blocking --log-opt max-buffer-size=4m"

# Shared volumes
MOUNT_CONF="--mount src=/media/plugin-data/sensors,target=/home/panda/mount,type=bind"

# Access kubernetes data streams for metsense and raingauge
ip=$(sudo kubectl describe service wes-rabbitmq | grep IP: | tr -d ' ' | cut -d ':' -f 2)
WAGGLE_USR="--env WAGGLE_PLUGIN_USERNAME=plugin"
WAGGLE_PWD="--env WAGGLE_PLUGIN_PASSWORD=plugin"
WAGGLE_HOST="--env WAGGLE_PLUGIN_HOST=${ip}"
WAGGLE_CONF="$WAGGLE_USR $WAGGLE_PWD $WAGGLE_HOST"

# NVIDIA
NVIDIA_VIS_DEVICES="--env NVIDIA_VISIBLE_DEVICES=all"
NVIDIA_DRIVER_CAPS="--env NVIDIA_DRIVER_CAPABILITIES=compute,video,utility"
NVIDIA_CONF="--runtime=nvidia"


#///////////////////////////////////////////////////////////
#
# roscore plugin
#
#///////////////////////////////////////////////////////////
echo 'Launching roscore plugin'
sudo docker run -dit --name panda-roscore \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
	    ${MOUNT_CONF} \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-roscore:v1.0.1
echo '... Sleeping for 3s to make sure all containers will see roscore up and running'
for i in `seq 1 3`; do
    sleep 1
    echo -n '.'
done
echo ''
echo '... done'


#///////////////////////////////////////////////////////////
#
# sensor plugins
#
#///////////////////////////////////////////////////////////
echo 'Launching raingauge plugin'
sudo docker run -dit --name panda-raingauge \
	    ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-raingauge:v1.0.2
echo '... done'
echo 'Launching env plugin'
sudo docker run -dit --name panda-metsense \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${WAGGLE_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-metsense:v1.0.3
echo '... done'


#///////////////////////////////////////////////////////////
#
# dbaserh plugin
#
#///////////////////////////////////////////////////////////
# W022 DEMO
#CRYSTAL_SER="--env CRYSTAL_SER=65008-00951"
#DBASERH_SER="--env DBASERH_SER=0"
#DBASERH_HV="--env DBASERH_HV=900"
#DBASERH_FGN="--env DBASERH_FGN=0.75"
#DBASERH_LLD="--env DBASERH_LLD=12"
# back up bar for W022 DEMO
CRYSTAL_SER="--env CRYSTAL_SER=65008-01003"
DBASERH_SER="--env DBASERH_SER=0"
DBASERH_HV="--env DBASERH_HV=950"
DBASERH_FGN="--env DBASERH_FGN=0.85"
DBASERH_LLD="--env DBASERH_LLD=12"
DBASERH_CONF="$CRYSTAL_SER $DBASERH_SER $DBASERH_HV $DBASERH_FGN $DBASERH_LLD"
echo '... Launching dbaserh plugin'
sudo docker run -dit --name panda-dbaserh \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
	    ${DBASERH_CONF} \
	    --device /dev/bus/usb \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-dbaserh:v1.0.12
echo '... done'
DBASERH_AGG_CONF="--env MODE=daq_listmode_aggregator"
echo '... Launching listmode-agg plugin'
sudo docker run -dit --name panda-dbaserh-agg \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${DBASERH_AGG_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-listmode-agg:v2.0.0
echo '... done'


#///////////////////////////////////////////////////////////
#
# calibration plugin
#
#///////////////////////////////////////////////////////////
INTTIME="--env INTTIME=240"
OVERLAP="--env OVERLAP=180"
FAR="--env FAR=0.125"
CALIB_CONF="$INTTIME $OVERLAP $FAR"
echo '... Launching calibration plugin'
sudo docker run -dit --name panda-calib \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
	    ${CALIB_CONF} \
            registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-calib:v2.0.0
echo '... done'


#///////////////////////////////////////////////////////////
#
# rosbag plugin
#
#///////////////////////////////////////////////////////////
METSENSE_TOPICS="/sensors/env/temperature /sensors/env/pressure /sensors/env/humidity"
RAINGAUGE_TOPICS="/sensors/raingauge"
DBASERH_TOPICS="/dbaserh/listmode"
CALIB_TOPICS="/calib/listmode /calib/params"
BAG_SIZE="--env SIZE=256"
BAG_DIR="--env BAGDIR=/home/panda/mount"
RADENV_BAG_PRFX="--env PRFX=panda-radenv-stream"
RADENV_PLUGINS=( "METSENSE" "RAINGAUGE" "DBASERH" "CALIB" )
RADENV_BAG_TOPICS="TOPICS="
for plugin in "${RADENV_PLUGINS[@]}"; do
    #echo ".. $plugin bagging is ON"
    topics=$plugin"_TOPICS"
    RADENV_BAG_TOPICS+="${!topics} "
done
RADENV_BAG_CONF="$BAG_SIZE $BAG_DIR $RADENV_BAG_PRFX"
echo 'Launching rosbag plugin'
sudo docker run -dit --name panda-radenv-rosbag \
            ${NETWORK_CONF} \
            ${LOGGING_CONF} \
            ${MOUNT_CONF} \
            ${RADENV_BAG_CONF} \
	    --env "$RADENV_BAG_TOPICS" \
	    registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-rosbag-host:v1.0.1
echo '... done'

