# waggle/sage
import waggle.plugin as waggle
import sage_data_client as sage
# generic
from datetime import datetime
from dateutil import relativedelta
from collections import defaultdict
import subprocess
import os.path
import time
import pandas as pd
import threading
# panda
from model import load_model

#
# Helper class for file transfer from node to cloud
#
class NodeToCloudModelXfer():
    def __init__(self, model=None):
        self.model = model
        # get node/cloud VSN, beehive credentials, and model dir from env
        self.REMOTE_NODE = os.getenv("PANDA_VSN")
        self.CLOUD_NODE = os.getenv("CLOUD_VSN")
        self.PANDA_USR, self.PANDA_PWD = os.getenv("PANDA_USR"), os.getenv("PANDA_PWD")
        self.XFER_DIR = os.getenv('BKGMODELDIR')

    def upload_model(self, filepath, meta):
    
        print(f"xfer: uploading file {filepath}, src_fileid {meta['src_fileid']}", flush=True)
        
        with waggle.Plugin() as plugin:
            plugin.upload_file(filepath, meta=meta)

        print(f"xfer: spawning waiter thread", flush=True)

        self.wait_for_model(meta["src_fileid"])

    def wait_for_model(self, src_fileid):
        thread = threading.Thread(target=self.query_model, args=[src_fileid])
        thread.start()

    def query_model(self, src_fileid):
        #
        # Periodically query for models uploaded by cloud plugin
        # Look for match with node VSN and original model file ID
        # Download from matched url and load model file
        #

        print(f"xfer: starting queries in waiting thread for src_fileid {src_fileid}", flush=True)

        wait_time = 30
        date = datetime.today()
        start_date = date - relativedelta.relativedelta(days=1)
        end_date = date + relativedelta.relativedelta(days=1)
        while True:

            print("xfer: querying", flush=True)

            df = sage.query(
                start=start_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
                end=end_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
                filter={"name": "upload",                  # name associated to Plugin.upload_file method
                        "vsn": self.CLOUD_NODE,            # VSN of cloud node
                        "task": "panda-training"})         # name of plugin running on cloud node
            if df.empty:

                print("xfer: no match, sleeping ...", flush=True)
                
                time.sleep(wait_time)
                continue
            match = df.loc[(df["meta.dst_vsn"] == self.REMOTE_NODE) & (df["meta.src_fileid"] == src_fileid)]
            if not match.empty:
                matched_url = match["value"].values[0]
                break
            else:

                print("xfer: no match, sleeping ...", flush=True)

                time.sleep(wait_time)
                
        filepath = self.XFER_DIR + '/' + matched_url.split('/')[-1]

        print(f"xfer: downloading from matched url {matched_url} to filepath {filepath}", flush=True)

        while True:
            try:
                subprocess.check_call(["wget", "--user="+self.PANDA_USR, "--password="+self.PANDA_PWD,
                                       "--directory-prefix="+self.XFER_DIR, matched_url])
            except subprocess.CalledProcessError as e:
                print(f"xfer: exception raised {e.output}")
                print("xfer: download not staged yet, sleeping ...", flush=True)
                
                time.sleep(wait_time)
            
            if os.path.isfile(filepath): 
                print(f"xfer: download completed", flush=True)
                break

        print(f"xfer: ready to load model {filepath}", flush=True)
        load_model(self.model, filepath)

        time.sleep(wait_time)

