# generic
import numpy as np
from collections import namedtuple
from scipy.stats import chi2
# PANDA
import radai
#from radai.algorithms import KSigma, NSCRAD
#from radai.filters import EWMAFilter
#from radai.result import Results

#
# rad filters
#
class RadonProxyFilter:
    def __init__(self, diff_time=30., thr=0.05):
        """
        Proxy is based on the instantaneuous precipitation value which
        is computed as the difference between two consecutive total accumulation
        values from the raingauge
        diff_time: search for ts within that range  
        thr: proxy threshold
        """
        self.diff_time = diff_time
        self.thr = thr
        self.decay = np.log(2) / 27.06 / 60 # decay constant of Pb-214 (1/s)
        self.prev_ts, self.prev_acc = 0., 0.
        self.ts, self.prec, self.proxy = 0., 0., 0.
        
    def update(self, ts, acc):
        #print(f"radon proxy update: ts {ts}")
        self.precipitation = acc - self.prev_acc if self.prev_acc else 0.
        self.proxy = self.proxy * np.exp(-self.decay * (ts - self.prev_ts))
        self.proxy += self.precipitation
        self.prev_acc = acc
        self.prev_ts = ts
        self.ts = ts

    def get_proxy(self):
        return self.proxy

    def analyze(self, ts):
        #print(f"radon proxy analyze: ts {ts}, state ts {self.ts}, proxy {self.proxy}")
        if abs(ts - self.ts) < self.diff_time and self.proxy > self.thr:
            return True
        return False
    
class KSigmaFilter:
    def __init__(self, int_time=1., tscale=3600., far=0.125, fpr=2):
        """
        int_time: integration time in sec
        tscale: filter time scale in sec
        far: false alarm rate in /h
        fpr: filter false positive rate in units of sigma
        """
        fpr = chi2.sf(fpr**2, 1)
        filter_far = fpr * 3600. / int_time
        self.filter = radai.algorithms.KSigma(
            far=far,
            int_time=int_time,
            filter=radai.filters.EWMAFilter(lam=int_time/tscale),
            filter_far=filter_far)
        self.thr = self.filter.threshold

    def analyze(self, data, ts, lt):
        results = self.filter.analyze(data, ts, lt)
        mean = self.filter.filter.x_k[0]
        metric = results[0].alarm_metric
        alarm = results[0].is_alarm
        rate = sum(data) / lt
        alarm_up = alarm & (rate >= mean)
        alarm_down = alarm & (rate < mean)
        return mean, metric, alarm_up, alarm_down

class EWMAFilter:
    def __init__(self, int_time=1., tscale=3600.):
        self.filter = radai.filters.EWMAFilter(lam=int_time/tscale)

    def analyze(self, data, ts, lt):
        self.filter.update(data, ts, lt)
        mean = self.filter.x_k[0]
        #dev = np.sqrt(self.filter.P_k[0, 0])
        return mean
    
class EWMADerivativeFilter:
    def __init__(self, int_time=1., tscale=3600., mean=0., thr=0.1):
        self.int_time = int_time
        self.filter = radai.filters.EWMAFilter(lam=int_time/tscale, x_k=mean, P_k=0.)
        self.thr = thr
        self.prev_mean = 0.

    def analyze(self, data, ts, lt):
        self.filter.update(data, ts, lt)
        mean = self.filter.x_k[0]
        #dev = np.sqrt(self.filter.P_k[0, 0])
        metric = (mean - self.prev_mean) / self.int_time
        alarm = abs(metric) > self.thr
        alarm_up = alarm and (metric > 0)
        alarm_down = alarm and (metric <= 0)
        self.prev_mean = mean
        return mean, metric, alarm_up, alarm_down
        
class NSCRADFilter:
    def __init__(self, int_time=1., tscale=3600, far=0.125, bin_edges=None):
        """
        int_time: integration time in sec
        tscale: filter time scale in sec
        far: flase alarm rate in /h
        bin_edges: spectrum bin edges
        """
        self.filter = radai.algorithms.NSCRAD(
            far=far,
            int_time=int_time,
            lam=int_time/tscale, 
            bin_edges=bin_edges,
            nuisance_spectra=[],
            spectral_windows=[
                [bin_edges[0], 64.],
                [195., 1530.],
                [83., 2511.]])
        """
        nuisance_spectra=[
        [2.19234513e-02, 2.68507037e-02, 3.11000546e-02, 3.27340381e-02,
        4.18285851e-02, 3.69966456e-02, 3.14862744e-02, 2.73278035e-02,
        2.70422788e-02, 2.70397514e-02, 2.75794957e-02, 2.32226464e-02,
        2.59110640e-02, 2.72943540e-02, 2.51563789e-02, 2.58287870e-02,
        2.13575539e-02, 2.08774375e-02, 2.10859341e-02, 1.92997193e-02,
        1.64075303e-02, 1.82276303e-02, 1.98859003e-02, 1.56163319e-02,
        1.24367265e-02, 1.45220680e-02, 1.71527585e-02, 1.76447743e-02,
        1.45930908e-02, 1.28375528e-02, 1.56927998e-02, 1.89370276e-02,
        1.52316292e-02, 9.74879152e-03, 6.53460272e-03, 6.96143546e-03,
        5.37814959e-03, 5.81301759e-03, 6.57471110e-03, 6.14287885e-03,
        5.80505138e-03, 4.51265033e-03, 4.74703930e-03, 4.71121096e-03,
        5.04972532e-03, 8.26975367e-03, 1.34769742e-02, 1.51019007e-02,
        1.19611895e-02, 5.45311621e-03, 2.59836001e-03, 2.83907870e-03,
        2.78833426e-03, 3.37289057e-03, 3.70933499e-03, 3.78885449e-03,
        3.33691443e-03, 2.72043328e-03, 3.19232869e-03, 2.56476683e-03,
        2.60624099e-03, 2.99748092e-03, 3.50021515e-03, 2.66473811e-03,
        2.59262428e-03, 1.70823346e-03, 1.75389528e-03, 1.84848306e-03,
        1.70145165e-03, 2.77810651e-03, 4.61078354e-03, 4.83198940e-03,
        4.08125882e-03, 1.64475579e-03, 2.43572043e-03, 2.37616395e-03,
        2.48098291e-03, 1.68916050e-03, 1.76945847e-03, 1.81568053e-03,
        2.66096291e-03, 2.84903835e-03, 3.04913771e-03, 2.09922751e-03,
        2.48927900e-03, 1.41064600e-03, 1.17800445e-03, 8.06495737e-04,
        9.11760467e-04, 1.19023790e-03, 9.71378115e-04, 1.47879123e-03,
        2.32806982e-03, 3.15275671e-03, 2.59112237e-03, 1.67284757e-03,
        1.09870163e-03, 5.01038118e-04, 4.61892623e-04, 3.28878273e-04,
        6.13737369e-04, 1.00000000e-06, 2.31151869e-04, 5.08893298e-04,
        2.45780461e-04, 5.50983166e-04, 8.93350156e-04, 7.68767452e-04,
        4.76427702e-04, 9.87852518e-05, 2.59352219e-05, 8.44368231e-05,
        9.43747870e-05, 3.17579111e-04, 3.09110577e-04, 5.27057350e-05,
        1.00000000e-06, 8.27160762e-05, 6.96099409e-04, 3.31649530e-04,
        1.86327808e-04, 1.00000000e-06, 3.00613553e-05, 2.59016340e-05,
        9.66090334e-06, 1.16545024e-05, 6.78541876e-05, 1.18489763e-05]],
        """
        self.thr = self.filter.threshold
        
    def analyze(self, spectrum, ts, lt):
        results = self.filter.analyze(spectrum, ts, lt)
        metric = results[0].alarm_metric
        alarm = results[0].is_alarm
        return metric, alarm
