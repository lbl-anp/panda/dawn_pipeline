# generic
import time
import numpy as np
from collections import defaultdict, namedtuple

#
# binning
#
def sqrt_n_bins(nmin=0, nmax=3000, nbins=128):
    """ Returns quadratically-spaced energy bins."""
    bin_edges = np.linspace(np.sqrt(nmin), np.sqrt(nmax), nbins+1)
    bin_edges = bin_edges**2
    bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
    return bin_edges, bin_centers

#
# helper classes
#
RadDataBinMode = namedtuple("RadDataBinMode", "ts lt spectrum")
class RadDataRollingBinModeAggregator:
    def __init__(self, int_time=1., nbins=128, bin_edges=None):
        self.int_time = int_time
        self.nbins = nbins
        self.bin_edges = bin_edges
        self.n_buf = 2
        self.buf = np.zeros((self.n_buf, nbins))
        self.lt, self.buf_lt = 0., 0.
        self.tf, self.ts, self.buf_ts = 0., 0., 0.
        self.ready = False
        
    def append(self, energies, tf, tl):
        if tf <= self.tf:
            print(f"error: past timestamp in rad series: current {tf}, prev {self.ts}")
            return
        self.tf = tf
        if self.lt == 0.:
            self.ts = tf
        self.lt += tl - tf
        self.buf[0] += np.histogram(energies, bins=self.bin_edges)[0]
        if self.lt >= self.int_time:
            self.buf_ts = self.ts
            self.buf_lt = self.lt
            # roll aggregator
            self.buf = np.roll(self.buf, self.n_buf - 1, axis=0)
            # reset aggregator
            self.lt = 0.
            self.buf[0] = np.zeros(self.nbins)
            self.ready = True

    def get(self):
        while not self.ready:
            time.sleep(0.1)
        self.ready = False
        return RadDataBinMode(ts=self.buf_ts, lt=self.buf_lt, spectrum=self.buf[1])

BinMode = namedtuple("BinMode", "spectrum")
ListMode = namedtuple("ListMode", "data timestamps")
RadData = namedtuple("RadData", "ts lt binmode listmode")
class RadDataRollingAggregator:
    def __init__(self, int_time=1., bin_edges=None):
        self.int_time = int_time
        self.bin_edges = bin_edges
        self.data_buf, self.ts_buf = [[], []], [[], []]
        self.lt, self.tf, self.ts = 0., 0., 0. 
        self.agg_lt, self.agg_ts = 0., 0.
        self.ready = False

    def append(self, data, timestamps):
        if not np.any(data):
            print(f"error: empty rad data msg")
            return
        tf, tl = timestamps[0], timestamps[-1]
        if tf <= self.tf:
            print(f"error: past timestamp in rad series: current {tf}, prev {self.ts}")
            return
        self.tf = tf
        if self.lt == 0.:
            self.ts = tf
        self.lt += tl - tf
        self.data_buf[0].append(data)
        self.ts_buf[0].append(timestamps)
        if self.lt >= self.int_time:
            self.agg_ts = self.ts
            self.agg_lt = self.lt
            # roll aggregators
            self.data_buf[0], self.data_buf[1] = self.data_buf[1], self.data_buf[0]
            self.ts_buf[0], self.ts_buf[1] = self.ts_buf[1], self.ts_buf[0]
            # reset aggregator
            self.lt = 0.
            self.data_buf[0], self.ts_buf[0] = [], []
            self.ready = True

    def get(self):
        while not self.ready:
            time.sleep(0.1) # make this much shorter than integration time
        self.ready = False
        data = np.hstack(self.data_buf[1])
        timestamps = np.hstack(self.ts_buf[1])
        spectrum = np.histogram(data, bins=self.bin_edges)[0]
        return RadData(ts=self.agg_ts, lt=self.agg_lt, binmode=BinMode(spectrum=spectrum), listmode=ListMode(data=data, timestamps=timestamps))


