# ROS
import rospy
from bard_ros_msgs.msg import EncounterInfo, ModelInfo
# generic
import os
import numpy as np
import h5py
from collections import namedtuple, defaultdict, Counter
import time
import sys
import math
from datetime import datetime
from dateutil import relativedelta
# PANDA
from bad import BADLAD, BADMF
from filters import EWMAFilter, EWMADerivativeFilter, KSigmaFilter, NSCRADFilter, RadonProxyFilter
from cloud import NodeToCloudModelXfer


#Triage = namedtuple("Triage", "radon_proxy mean_slow metric_slow mean_fast metric_fast metric_nscrad static radon anomaly none flavor")
class SpectralTriage:
    #
    # Spectra are classified into 4 buckets: "static", "radon", "anomaly" (and "none")
    # Triage is based on the combination of count rate filters, NSCRAD filter and radon proxy filter
    # For each spectrum returns:
    # - all filter metrics
    # - flavor
    #
    def __init__(self, int_time=1., far=0.125,
                 tscale_slow=2*3600, fpr_slow=2,
                 tscale_fast=30, fpr_fast=3,
                 tscale_nscrad=2*3600, bin_edges=None,
                 radon_proxy_thr=0.05):
        self.slowCountRateFilter = KSigmaFilter(int_time=int_time, tscale=tscale_slow, far=far, fpr=fpr_slow)
        self.fastCountRateFilter = KSigmaFilter(int_time=int_time, tscale=tscale_fast, far=far, fpr=fpr_fast)
        self.nscradFilter = NSCRADFilter(int_time=int_time, tscale=tscale_nscrad, far=far, bin_edges=bin_edges)
        self.radonProxyFilter = RadonProxyFilter(thr=radon_proxy_thr)

    def updateRadonProxyFilter(self, ts, acc):
        return self.radonProxyFilter.update(ts, acc)
        
    def analyze(self, spectInfo):
        radon_proxy = None
        alarm_radon_proxy = False
        radon_proxy = self.radonProxyFilter.get_proxy()
        alarm_radon_proxy = self.radonProxyFilter.analyze(spectInfo.timestamp)
        mean_slow, metric_slow, alarm_slow_up, alarm_slow_down = self.slowCountRateFilter.analyze(spectInfo.spectrum, spectInfo.timestamp, spectInfo.livetime)
        mean_fast, metric_fast, alarm_fast_up, alarm_fast_down = self.fastCountRateFilter.analyze(spectInfo.spectrum, spectInfo.timestamp, spectInfo.livetime)
        metric_nscrad, alarm_nscrad = self.nscradFilter.analyze(spectInfo.spectrum, spectInfo.timestamp, spectInfo.livetime)
        static = not (alarm_slow_up or alarm_fast_up or alarm_nscrad or alarm_radon_proxy)
        radon = alarm_radon_proxy
        anomaly_weak = alarm_slow_up or alarm_fast_up or alarm_nscrad
        anomaly_strong = alarm_slow_up and (alarm_nscrad or alarm_fast_up)
        anomaly = anomaly_weak or anomaly_strong
        if radon and anomaly_strong: radon = False
        if radon and anomaly_weak: anomaly = False
        none = False
        if not (static or radon or anomaly): none = True
        flavor = ''
        if static: flavor = "static"
        elif radon: flavor = "radon"

        spectInfo.triage_radon_proxy = radon_proxy
        spectInfo.triage_metric_slow = metric_slow
        spectInfo.triage_mean_slow = mean_slow
        spectInfo.triage_metric_fast = metric_fast
        spectInfo.triage_mean_fast = mean_fast
        spectInfo.triage_metric_nscrad = metric_nscrad
        spectInfo.triage_static = static
        spectInfo.triage_radon = radon
        spectInfo.triage_anom = anomaly
        spectInfo.triage_none = none
        spectInfo.triage_flavor = flavor

class ModelBootstrap:
    #
    # Builds the initial untrained model to start anomaly detection as soon as possible 
    # If model files are passed in, bootstrap is marked as done for corresponding flavors
    #
    def __init__(self, minutes=30, model=None):
        self.model = model
        nmax = int(minutes * 60. / self.model.int_time)
        self.spectrum, self.count, self.nmax = {}, {}, {}
        self.ready = {}
        #self.in_process = {}
        self.done = {}
        for flavor in ["static", "radon"]:
            self.spectrum[flavor] = np.zeros(self.model.nbins)
            self.count[flavor] = 0
            self.nmax[flavor] = (1 + 3 * (flavor == "radon")) * nmax # 4x stats for radon
            self.ready[flavor] = False 
            #self.in_process[flavor] = False
            self.done[flavor] = False
        if self.model.flavors:
            self.done["static"] = True
            if "radon" in self.model.flavors:
                if self.model.order["radon"]:
                    self.done["radon"] = True

    def append(self, spectInfo):
        flavor = spectInfo.triage_flavor
        # move this down, to continue building up stats
        # for static flavor after ready for boostrap
        #if self.ready[flavor]: return
        self.spectrum[flavor] += spectInfo.spectrum
        self.count[flavor] += 1
        if self.ready[flavor]: return
        if self.count[flavor] == self.nmax[flavor]:
            self.ready[flavor] = True

    def static_is_done(self):
        if self.done["static"]: return True
        if not self.ready["static"]: return False
        if not self.done["static"]:
            self.bootstrap_model("static")
            return True
        """
        # Async version:
        # in_process would keep from repeated bootstrapping
        if not (self.done["static"] or self.in_process["static"]):
            self.in_process["static"] = True
            self.bootstrap_model("static") 
        return False
        """

    def radon_is_done(self):
        if self.done["radon"]: return True
        if not (self.ready["static"] and self.ready["radon"]): return False
        if not self.done["radon"]:
            self.bootstrap_model("radon")
            return True
    
    def is_done(self):
        return self.done["static"] and self.done["radon"]
        
    def bootstrap_model(self, flavor):
        print(f"bootstrapping {flavor} flavor")
        start = time.time()

        self.model.impl[flavor] = {}
        if flavor == "static":
            static_comp = self.spectrum["static"] / self.count["static"]
            static_comp /= static_comp.sum()
            np.clip(static_comp, 1.E-6, None, out=static_comp)
            ncomp = 1
            basis = np.asarray([static_comp])
            self.model.flavors.append("static")
            self.model.iter["static"] = 0
            self.model.order["static"] = 0
            self.model.flavor = "static"
        else:
           static_comp = self.spectrum["static"] / self.count["static"]
           radon_comp = self.spectrum["radon"] / self.count["radon"]
           np.clip(radon_comp - static_comp, 1.E-6, None, out=radon_comp) 
           radon_comp /= radon_comp.sum()
           np.clip(radon_comp, 1.E-6, None, out=radon_comp)
           static_comp /= static_comp.sum()
           np.clip(static_comp, 1.E-6, None, out=static_comp)
           ncomp = 2
           basis = np.asarray([static_comp, radon_comp])
           self.model.flavors.append("radon")
           self.model.iter["radon"] = 0
           self.model.order["radon"] = 0
           self.model.flavor = "radon"
        now = datetime.utcnow()
        timestamp = time.mktime(now.timetuple())
        fileID = now.strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badlad"] = fileID
        ID = fileID if self.model.ID is None else self.model.ID
        # BADLAD
        print(f"badlad implementation of {flavor} flavor")
        self.model.impl[flavor]["badlad"] = BADLAD(n_components=ncomp,
                                                   nmf='pnmf_nnls',
                                                   n_steps_iter=100,
                                                   num_bins=self.model.nbins,
                                                   bincenters=self.model.bin_centers,
                                                   binedges=self.model.bin_edges,
                                                   mean_inttime=self.model.int_time,
                                                   steptime=self.model.int_time,
                                                   V=basis)
        self.model.impl[flavor]["badlad"].far = self.model.far
        self.model.impl[flavor]["badlad"].calculate_threshold(far=self.model.far)
        filename = fileID + ".h5"
        filepath = os.getenv("BKGMODELDIR") + '/' + filename
        print(f"writing model (badlad / {flavor}) to {filepath}")
        self.model.impl[flavor]["badlad"].write_model(filepath)
        with h5py.File(filepath, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["fileID"] = fileID
            h5in.attrs["ID"] = ID
            h5in.attrs["impl"] = "badlad"
            h5in.attrs["iter"] = 0
            h5in.attrs["order"] = 0
            h5in.attrs["timestamp"] = timestamp
        # BADFM model
        print(f"badfm implementation of {flavor} flavor")
        templates = {}
        #with h5py.File(os.getenv("PKGDIR")+"/templates/radai_source_templates.h5", 'r') as h5in:
        with h5py.File(os.getenv("PKGDIR")+"/templates/radai_source_templates_no_snm.h5", 'r') as h5in:
            for src, template in list(h5in['templates'].items()):
                templates[src] = np.reshape(template, (1,self.model.nbins))
        self.model.impl[flavor]["badfm"] = BADMF(n_components=ncomp,
                                                 nmf='pnmf_nnls',
                                                 templates=templates,
                                                 n_steps_iter=100,
                                                 num_bins=self.model.nbins,
                                                 bincenters=self.model.bin_centers,
                                                 binedges=self.model.bin_edges,
                                                 mean_inttime=self.model.int_time,
                                                 steptime=self.model.int_time,
                                                 V=basis)
        self.model.impl[flavor]["badfm"].far = self.model.far
        self.model.impl[flavor]["badfm"].calculate_threshold_analytically(far=self.model.far, inttime=self.model.int_time)
        fileID = (now + relativedelta.relativedelta(seconds=1)).strftime("%Y%m%d-%H%M%S")
        self.model.fileID[flavor]["badfm"] = fileID
        filename = fileID + ".h5"
        filepath = os.getenv("BKGMODELDIR") + '/' + filename
        print(f"writing model (badmf / {flavor}) to {filepath}")
        self.model.impl[flavor]["badfm"].write_model(filepath)
        with h5py.File(filepath, "r+") as h5in:
            h5in.attrs["flavor"] = flavor
            h5in.attrs["ID"] = ID
            h5in.attrs["fileID"] = fileID
            h5in.attrs["impl"] = "badfm"
            h5in.attrs["iter"] = 0
            h5in.attrs["order"] = 0
            h5in.attrs["timestamp"] = timestamp
        self.model.ID = ID
        self.done[flavor] = True
        #self.in_process[flavor] = False
        
        stop = time.time()
        print(f"bootstrapping done in {(stop - start):.3f}s")

class ModelTraining:
    #
    # Takes care of training the bootstrap model, as well as performing any training request
    # Marks boostrapped model as trained if corresponding model files were passed
    #
    def __init__(self, model=None):
        self.model = model
        self.nmax, self.halfnmax = {}, {}
        self.training_set, self.ts, self.index = {}, {}, {}
        self.half, self.once = {}, {}
        self.ready, self.ready_ts = {}, {}
        self.in_process = {}
        self.nmax["static"] = int(3600. / self.model.far / self.model.int_time)
        self.nmax["radon"] = self.nmax["static"] // 2
        for flavor in ["static", "radon"]:
            self.halfnmax[flavor] = self.nmax[flavor] // 2
            self.training_set[flavor] = np.zeros((self.nmax[flavor], self.model.nbins))
            self.ts[flavor] = np.zeros(self.nmax[flavor])
            self.index[flavor] = 0
            self.half[flavor] = False
            self.once[flavor] = False
            self.ready[flavor] = False
            self.in_process[flavor] = False
        self.model_xfer = NodeToCloudModelXfer(model=self.model)

    def append(self, spectInfo):
        flavor = spectInfo.triage_flavor
        if not self.half[flavor]:
            if self.index[flavor] == (self.halfnmax[flavor] - 1): self.half[flavor] = True
        if not self.once[flavor]:
            if self.index[flavor] == (self.nmax[flavor] - 1): self.once[flavor] = True
        if not self.ready[flavor]:
            if flavor == "static" and self.once[flavor]:
                self.ready[flavor] = True
                self.ready_ts[flavor] = spectInfo.timestamp
            if flavor == "radon" and self.once[flavor] and self.half["static"]:
                    self.ready[flavor] = True
                    self.ready_ts[flavor] = spectInfo.timestamp
        if self.index[flavor] == self.nmax[flavor]: self.index[flavor] = 0
        self.training_set[flavor][self.index[flavor]] = spectInfo.spectrum
        self.ts[flavor][self.index[flavor]] = spectInfo.timestamp
        self.index[flavor] += 1

    def static_is_done(self):
        if self.model.order["static"]: return True
        else:
            #if (self.ready["static"] and not (self.model.order["static"] or self.in_process["static"])): 
            if (self.ready["static"] and not self.in_process["static"]):
                self.in_process["static"] = True
                self.train_model("static")
            return False

    def radon_is_done(self):
        if self.model.order["radon"]: return True
        else:
            if (self.ready["radon"] and not self.in_process["radon"]):
                self.in_process["radon"] = True
                self.train_model("radon")
            return False

    def train_model(self, flavor):
        #
        # training is offloaded to cloud node
        #
        # TO-DO
        # account for model order properly 
        print(f"training: {flavor}")
        start = time.time()

        # prep training set
        if flavor == "static":
            training = self.training_set["static"]
        else:
            training = np.vstack((self.training_set["radon"],
                                  self.training_set["static"][:self.nmax["radon"]]))
        # add training set to model files and upload for cloud training
        for impl in ["badlad", "badfm"]:
            fileID = self.model.fileID[flavor][impl]
            filename = fileID + ".h5"
            filepath = self.model_xfer.XFER_DIR + '/' + filename
            meta = {}
            meta["src_fileid"] = fileID
            with h5py.File(filepath, 'r+') as h5out:
                h5out.create_dataset("training", data=training)
            self.model_xfer.upload_model(filepath, meta)

class ModelMonitoring:
    #
    # Based on BADLAD implementation of model to compute AIC score
    # on spectra not classified as "anomaly"
    # For each spectrum, returns:
    # - number of bkg components
    # - bkg components and weights
    # - metric and threshold
    # - alarm flag
    #
    # TO-DO:
    # We need field data to see the actual behavior of that metric in the field
    # and design alarm and criteria for model re-training
    #
    def __init__(self, model=None, int_time=1., tscale=3600.):
        self.model = model
        self.AICFilter = EWMAFilter(int_time=int_time, tscale=tscale)

    def check(self, spectInfo):
        impl = self.model.impl[self.model.flavor]["badlad"]
        spect_info = impl.analyze(spectInfo.spectrum, index=[0]).loc[0, :]
        spectInfo.bkg_n_components = impl.n_components
        spectInfo.bkg_components = np.asarray([impl.basis[i] for i in range(spectInfo.bkg_n_components)]).flatten()
        spectInfo.bkg_weights = np.zeros(spectInfo.bkg_n_components)
        for i in range(spectInfo.bkg_n_components):
            spectInfo.bkg_weights[i] = spect_info['b'+str(i)]
        spectInfo.bkg_threshold = impl.threshold
        spectInfo.bkg_alarm = spect_info["alarm"]
        # Issue of infinite metric for too large value of deviance:
        # Metric is computed as -1 * np.log(distribution.sf(deviance)), with distribution=Gamma
        # FIX till we propagate change from distribution.sf to distribution.logsf in BAD
        #spectInfo.bkg_metric = spect_info["alarm_metric"]
        metric = spect_info["alarm_metric"]
        if np.isinf(metric): metric = 1000.
        spectInfo.bkg_metric = metric
        #
        aic = 2 * spectInfo.bkg_n_components + spect_info["deviance"]
        spectInfo.bkg_mean_aic = self.AICFilter.analyze([aic], spectInfo.timestamp, 1.) # does not scale with live time, so enter 1.
        spectInfo.bkg_trained = self.model.order[self.model.flavor] > 0
        spectInfo.bkg_flavor = self.model.flavor

class AnomalyDetection:
    #
    # Uses the BADFM implementation of the models for anomaly detection
    # For each spectrum, returns:
    # - top 3 isotope IDs, metrics, src thresholds, alarms, src weights, src templates
    # - alarm flag
    # - bkg weights
    # Genrates encounters for consecutive alarms within encounter_split value. 
    # Encounter buffer includes encouter_split long pre- and post-buffers for:
    # - listmode data/timestamps
    # - spectra info: timestamp, top ID, alarm
    #
    # Update: switch from buffering to streaming for encounters
    #
    #
    def __init__(self, model=None, int_time=1., encounter_split=10):
        self.model = model
        self.srcs = ["Am-241", "Ba-133", "Co-57", "Co-60", "Cs-137", "Cu-67",
                     "F-18", "I-131", "Ir-192", "K-40", "Lu-177", "NatU", "Ra-226",
                     "Sr-90", "Tc-99m", "Th-232", "Tl-201", "Xe-133"]
        # for encounter generation
        self.encounter_split = math.ceil(encounter_split / int_time)
        self.split_count = 0
        self.in_encounter = False
        self.consecutive = False
        self.encounter_id = 0
        #self.buf_size = self.encounter_split
        #self.data_buf = [None] * self.buf_size
        #self.ts_buf = [None] * self.buf_size
        #self.buf_idx = 0
        #self.buf_full = False
        self.pub = rospy.Publisher("/bard/encounter_info", EncounterInfo, queue_size=1)

    def detect(self, spectInfo, listmode):
        impl = self.model.impl[self.model.flavor]["badfm"]
        results = impl.analyze(spectInfo.spectrum, index=[0]).loc[0, :]
        results = results[results['alarm'].values==True]
        alarm = not results.empty
        n_alarms = 0
        if alarm:
            n_alarms = results.shape[0]
            srcs = results.index.values.tolist()
            spectInfo.anom_src_ids = np.asarray(srcs).flatten()
            # See comment above about inf metrics issue coming from large deviance values
            #spectInfo.anom_src_metrics = np.asarray(results['alarm_metric'].tolist()).flatten()
            metrics = np.asarray(results['alarm_metric'].tolist())
            metrics[np.isinf(metrics)] = 1000.
            spectInfo.anom_src_metrics = metrics.flatten()
            spectInfo.anom_src_thresholds = np.asarray([impl.threshold[src] for src in srcs]).flatten()
            spectInfo.anom_src_templates = np.asarray([impl.templates[src][0] for src in srcs]).flatten()
            spectInfo.anom_src_weights = np.asarray(results["s00"].tolist()).flatten()
            # get all bkg weights
            bkg_weights = np.zeros((n_alarms,impl.n_components))
            for i in range(n_alarms):
                for j in range(impl.n_components):
                    bkg_weights[i][j] = results.iloc[i]['b'+str(j)]
            #spectInfo.anom_bkg_weights = np.asarray(bkg_weights).flatten()
            spectInfo.anom_bkg_weights = bkg_weights.flatten()
            # get all src weight std errors
            # x = spectrum, (n_bins,)
            # x_rec = V.T @ weights, V is the model including both background and source template
            # F = (V * (1 / x_rec)[None, :]) @ V.T, Fisher info for the fit (n_comp, n_comp)
            # cov = np.linalg.inv(F), estimated covariance matrix of estimated weights (n_comp, n_comp)
            # src weight_std = np.sqrt(cov[-1, -1]), src template on last row of model
            src_weights_std = np.zeros(n_alarms)
            V = np.zeros((impl.n_components+1, len(spectInfo.spectrum)))
            for i in range(impl.n_components):
                V[i] = impl.basis[i]
            for i in range(n_alarms):
                # model: bkg components first, then src template
                src = spectInfo.anom_src_ids[i]
                V[-1] = impl.templates[src][0]
                # weights: bkg weights, then src weights
                weights = np.zeros(impl.n_components+1)
                for j in range(impl.n_components):
                    weights[j] = spectInfo.anom_bkg_weights[impl.n_components*i+j]
                weights[-1] = spectInfo.anom_src_weights[i]
                # reconstructed spectrum
                spectrum_rec = V.T @ weights
                # Fisher info
                F = (V * (1 / spectrum_rec)[None, :]) @ V.T
                # covariance
                cov = np.linalg.inv(F)
                # std deviation
                src_weights_std[i] = np.sqrt(cov[-1, -1])
            spectInfo.anom_src_weights_std = src_weights_std.flatten()
        spectInfo.anom_n_alarms = n_alarms

        if self.in_encounter:
            # publish data with same encounter id
            encounterInfo = EncounterInfo()
            encounterInfo.header.stamp = rospy.Time.now()
            encounterInfo.id = self.encounter_id
            encounterInfo.ts = listmode.timestamps[0]
            encounterInfo.tl = listmode.timestamps[-1]
            encounterInfo.n_alarms = n_alarms
            #encounterInfo.data = listmode.data.flatten()
            #encounterInfo.timestamps = listmode.timestamps.flatten()
            if alarm:
                encounterInfo.ids = spectInfo.anom_src_ids
                encounterInfo.weights = spectInfo.anom_src_weights
                encounterInfo.weights_std = spectInfo.anom_src_weights_std
                #encounterInfo.ids = np.asarray(srcs).flatten()
                #encounterInfo.weights = np.asarray(results["s00"].tolist()).flatten()
            # check if we need to split
            if (not alarm) and (not self.consecutive):
                self.split_count += 1
            else:
                self.split_count = 1
            if self.split_count == self.encounter_split:
                # reset encounter
                #self.data_buf = [None] * self.buf_size
                #self.ts_buf = [None] * self.buf_size
                #self.buf_idx = 0
                #self.buf_full = False
                self.in_encounter = False
                self.consecutive = False
                self.split_count = 1
                # increment encounter id
                self.encounter_id += 1
            self.consecutive = alarm
            encounterInfo.live = self.in_encounter
            self.pub.publish(encounterInfo)
        else:
            # fill pre-alarm buffer
            #if not alarm:
            #    self.data_buf[self.buf_idx] = listmode.data
            #    self.ts_buf[self.buf_idx] = listmode.timestamps
            #    self.buf_idx += 1
            #    if self.buf_idx == self.buf_size:
            #        self.buf_full = True
            #        self.buf_idx = 0
            #else:
            if alarm:
                # start encounter
                self.in_encounter = True
                self.consecutive = True
                encounterInfo = EncounterInfo()
                encounterInfo.header.stamp = rospy.Time.now()
                encounterInfo.id = self.encounter_id
                encounterInfo.ts = listmode.timestamps[0]
                encounterInfo.tl = listmode.timestamps[-1]
                encounterInfo.n_alarms = n_alarms
                encounterInfo.ids = spectInfo.anom_src_ids
                encounterInfo.weights = spectInfo.anom_src_weights
                encounterInfo.weights_std = spectInfo.anom_src_weights_std
                encounterInfo.live = self.in_encounter
                #print(encounterInfo)
                self.pub.publish(encounterInfo)

                """
                # order pre-alarm buffer
                if self.buf_idx:
                    if self.buf_full:
                        for i in range(self.buf_idx):
                            self.data_buf.append(self.data_buf.pop(0))
                            self.ts_buf.append(self.ts_buf.pop(0))
                    else:
                        self.data_buf = self.data_buf[:self.buf_idx]
                        self.ts_buf = self.ts_buf[:self.buf_idx]
                else:
                    if not self.buf_full:
                        self.data_buf = []
                        self.ts_buf = []
                # append first alarm of encounter
                self.data_buf.append(listmode.data)
                self.ts_buf.append(listmode.timestamps)
                # publish encounter data with model info for first alarm
                encounterInfo = EncounterInfo()
                encounterInfo.header.stamp = rospy.Time.now()
                encounterInfo.id = self.encounter_id
                encounterInfo.data = np.hstack(self.data_buf).flatten()
                encounterInfo.timestamps = np.hstack(self.ts_buf).flatten()
                encounterInfo.n_alarms = n_alarms
                encounterInfo.alarm_ids = np.asarray(srcs).flatten()
                modelInfo = ModelInfo()
                modelInfo.nmf = "pnmf_nnls"
                modelInfo.n_steps_iter = 100
                modelInfo.num_bins = self.model.nbins
                modelInfo.bincenters = self.model.bin_centers
                modelInfo.binedges = self.model.bin_edges
                modelInfo.mean_inttime = self.model.int_time
                modelInfo.steptime = self.model.int_time
                modelInfo.basis = self.model.impl[self.model.flavor]["badlad"].basis.flatten()
                modelInfo.far = self.model.far
                modelInfo.threshold = self.model.impl[self.model.flavor]["badlad"].threshold
                modelInfo.srcs = np.asarray(self.srcs).flatten()
                modelInfo.thresholds = np.asarray([self.model.impl[self.model.flavor]["badfm"].threshold[src] for src in self.srcs]).flatten()
                modelInfo.templates = np.asarray([self.model.impl[self.model.flavor]["badfm"].templates[src][0] for src in self.srcs]).flatten()
                modelInfo.flavor = self.model.flavor
                modelInfo.is_trained = self.model.order[self.model.flavor] > 0
                encounterInfo.model_info = modelInfo
                self.pub.publish(encounterInfo)
                """
        """
        if self.in_encounter:
            # append to encounter buffer
            self.data_buf.append(listmode.data)
            self.ts_buf.append(listmode.timestamps)
            self.alarm_ts_buf.append(spectInfo.timestamp)
            self.alarm_id_buf.append(spectInfo.anom_src_ids[0])
            self.alarm_flag_buf.append(spectInfo.anom_alarm)
            self.encounter_id = self.encounter_id

            # check if we need to split
            if (not spectInfo.anom_alarm) and (not self.consecutive):
                self.split_count += 1
            else:
                self.split_count = 1
            if self.split_count == self.encounter_split:
                # publish encounter info msg
                encounterInfo = EncounterInfo()
                encounterInfo.header.stamp = rospy.Time.now()
                encounterInfo.data = np.hstack(self.data_buf).flatten()
                encounterInfo.timestamps = np.hstack(self.ts_buf).flatten()
                encounterInfo.alarm_ts = np.hstack(self.alarm_ts_buf).flatten()
                encounterInfo.alarm_ids = np.hstack(self.alarm_id_buf).flatten()
                encounterInfo.alarm_flags = np.hstack(self.alarm_flag_buf).flatten()
                modelInfo = ModelInfo()
                modelInfo.nmf = "pnmf_nnls"
                modelInfo.n_steps_iter = 100
                modelInfo.num_bins = self.model.nbins
                modelInfo.bincenters = self.model.bin_centers
                modelInfo.binedges = self.model.bin_edges
                modelInfo.mean_inttime = self.model.int_time
                modelInfo.steptime = self.model.int_time
                modelInfo.basis = self.model.impl[self.model.flavor]["badlad"].basis.flatten()
                modelInfo.far = self.model.far
                modelInfo.threshold = self.model.impl[self.model.flavor]["badlad"].threshold
                modelInfo.srcs = np.asarray(self.srcs).flatten()
                modelInfo.thresholds = np.asarray([self.model.impl[self.model.flavor]["badfm"].threshold[src] for src in self.srcs]).flatten()
                modelInfo.templates = np.asarray([self.model.impl[self.model.flavor]["badfm"].templates[src][0] for src in self.srcs]).flatten()
                modelInfo.flavor = self.model.flavor
                modelInfo.is_trained = self.model.order[self.model.flavor] > 0
                encounterInfo.model_info = modelInfo
                self.pub.publish(encounterInfo)
                # reset encounter
                self.data_buf = [None] * self.buf_size
                self.ts_buf = [None] * self.buf_size
                self.alarm_ts_buf = [None] * self.buf_size
                self.alarm_id_buf = [None] * self.buf_size
                self.alarm_flag_buf = [None] * self.buf_size
                self.buf_idx = 0
                self.buf_full = False
                self.in_encounter = False
                self.consecutive = False
                self.split_count = 1
            self.consecutive = spectInfo.anom_alarm
        else:
            # fill pre-alarm buffer
            if not spectInfo.anom_alarm:
                self.data_buf[self.buf_idx] = listmode.data
                self.ts_buf[self.buf_idx] = listmode.timestamps
                self.alarm_ts_buf[self.buf_idx] = spectInfo.timestamp
                self.alarm_id_buf[self.buf_idx] = spectInfo.anom_src_ids[0]
                self.alarm_flag_buf[self.buf_idx] = spectInfo.anom_alarm
                self.buf_idx += 1
                if self.buf_idx == self.buf_size: 
                    self.buf_full = True
                    self.buf_idx = 0
            else:
                # start encounter
                self.in_encounter = True
                self.consecutive = True
                # order pre-alarm buffer
                if self.buf_idx: 
                    if self.buf_full:
                        for i in range(self.buf_idx):
                            self.data_buf.append(self.data_buf.pop(0))
                            self.ts_buf.append(self.ts_buf.pop(0))
                            self.alarm_ts_buf.append(self.alarm_ts_buf.pop(0))
                            self.alarm_id_buf.append(self.alarm_id_buf.pop(0))
                            self.alarm_flag_buf.append(self.alarm_flag_buf.pop(0))
                    else:
                        self.data_buf = self.data_buf[:self.buf_idx]
                        self.ts_buf = self.ts_buf[:self.buf_idx]
                        self.alarm_ts_buf = self.alarm_ts_buf[:self.buf_idx]
                        self.alarm_id_buf = self.alarm_id_buf[:self.buf_idx]
                        self.alarm_flag_buf = self.alarm_flag_buf[:self.buf_idx]
                else:
                    if not self.buf_full:
                        self.data_buf = [] 
                        self.ts_buf = []
                        self.alarm_ts_buf = []
                        self.alarm_id_buf = []
                        self.alarm_flag_buf = []
                # append first alarm of encounter
                self.data_buf.append(listmode.data)
                self.ts_buf.append(listmode.timestamps)
                self.alarm_ts_buf.append(spectInfo.timestamp)
                self.alarm_id_buf.append(spectInfo.anom_src_ids[0])
                self.alarm_flag_buf.append(spectInfo.anom_alarm)
        """
#SelectionResult = namedtuple("SelectionResult", "flavor ts n_radon mean_radon metric_radon")
class ModelSelection:
    #
    # Based on detection of direct and indirect radon enhancement in the data
    # Detection of indirect radon enhancement assumens that the radon (Ra-226)
    # template should not trigger anomalies when the radon is used.
    #
    def __init__(self, model=None, int_time=1., tscale=3600., thr=0.1):
        self.model = model
        self.int_time = int_time
        self.radonFilter = EWMADerivativeFilter(int_time=int_time, tscale=tscale, thr=thr)
        self.thr = thr
        self.live_time = 0.
        self.n_direct_radon, self.n_indirect_radon = 0., 0.
        #self.flavor = ''

    def select(self, spectInfo):
        self.n_direct_radon += spectInfo.triage_radon
        self.n_indirect_radon += spectInfo.anom_alarm and ("Ra-226" in spectInfo.anom_src_ids)
        self.live_time += msg.livetime
        if self.live_time >= self.int_time:
            n_radon = self.n_direct_radon + self.n_indirect_radon
            mean, metric, alarm_up, alarm_down = self.radonFilter.analyze([n_radon], spectInfo.timestamp, 1.)
            radon_trigger = alarm_up
            static_trigger = alarm_down
            if radon_trigger:
                if self.model.flavor == "static":
                    print("switch to radon model")
                    self.model.flavor = "radon"
                else:
                    print("radon trigger while using radon model")
            if static_trigger:
                if self.model.flavor == "radon":
                    print("switch to static model")
                    self.model.flavor = "static"
                else:
                    print("static trigger while using static model")
            # reset aggregator
            self.live_time = 0.
            self.n_direct_radon, self.n_indirect_radon = 0., 0.
            #return True
        #else:
        #    return False
        spectInfo.sel_flavor = self.model.flavor

    #def get_flavor(self):
    #    return self.flavor

"""
MonitoringResults = namedtuple("MonitoringResults", "label metric_bad weights alarm_aic metric_aic aic mean_aic")
class ModelMonitoring:
    #
    # Based on BADLAD implementation of model to compute AIC score
    # on spectra not classified as "anomaly"
    #
    # TO-DO:
    # We need field data to see the actual behavior of that metric in the field
    # and design alarm and criteria for model re-training
    #
    def __init__(self, model=None, int_time=1., tscale=3600., mean=0., thr=0.1):
        self.model = model
        self.int_time = int_time
        self.AICScoreFilter = EWMADerivativeFilter(int_time=int_time, tscale=tscale, mean=mean, thr=thr)
        self.thr = thr
        self.live_time, self.aic = 0., 0.
        self.flavor = None
        self.prev_alarm = False
        self.monitoringResults = None
        # debug
        self.alarm_ts = []

    def aggregate(self, data, msg):
        impl = self.model.impl[self.model.flavor]["badlad"]
        label = "monitoring_" + self.model.flavor + '_' + str(impl.n_components)
        spect_info = impl.analyze(data.spectrum, index=[0]).loc[0, :]
        alarm_bad = spect_info["alarm"]
        metric_bad = spect_info["alarm_metric"]
        weights = [spect_info['b'+str(i)] for i in range(impl.n_components)]        
        
        
        msg.bkg_n_components = impl.n_components
        msg.bkg_components = np.asarray([impl.basis[i] for i in range(msg.bkg_n_components)]).flatten()
        
        
        # make sure we aggregate continuously for the same flavor,
        # model selection could switch to a different model
        if (self.flavor is None) or (self.model.flavor == self.flavor):
            self.live_time += data.lt
            self.aic += 2 * impl.n_components + spect_info["deviance"]
            self.flavor = self.model.flavor
        else:
            self.live_time = 0.
            self.aic = 0.
            self.prev_alarm = False
        if self.live_time >= self.int_time:
            mean_aic, metric_aic, alarm_up, _ = self.AICScoreFilter.analyze([self.aic], data.ts, 1.)
            aic_trigger = alarm_up and self.prev_alarm
            self.prev_alarm = alarm_up
            self.monitoringResults = MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
                                                       alarm_aic=aic_trigger, metric_aic=metric_aic, aic=self.aic, mean_aic=mean_aic)
            if aic_trigger: self.alarm_ts.append(data.ts)
            # reset aggregator
            self.live_time = 0.
            self.aic = 0.
            return True
        else:
            self.monitoringResults = MonitoringResults(label=label, metric_bad=metric_bad, weights=weights,
                                                       alarm_aic=False, metric_aic=0., aic=0., mean_aic=0.)
            return False
                
    def monitoring(self):
        return self.monitoringResults

    def on_alarm(self):
        #
        # TO-DO
        # Consecutive trainings:
        # - first re-training: as is
        # - if second re-training required: add one static comp if not already done
        # - if third training required
        #
        # NOTE: this strategy will likely change based on the actual behavior of field data
        pass
"""
