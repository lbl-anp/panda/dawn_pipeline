# generic
import h5py
import numpy as np
from collections import defaultdict, namedtuple
# panda
from bad import BADLAD, BADMF

ModelInfo = namedtuple("ModelInfo", "ID fileID flavor impl iter order ts")
def model_info(filename):
    with h5py.File(filename, 'r') as h5in:
        return ModelInfo(ID=h5in.attrs["ID"], fileID=h5in.attrs["fileID"], flavor=h5in.attrs["flavor"],
                         impl=h5in.attrs["impl"], iter=h5in.attrs["iter"],
                         order=h5in.attrs["order"], ts=h5in.attrs["timestamp"])

def load_model(model, filepath):
    #
    # called on training return from cloud node
    #
    info = model_info(filepath)
    print(f"Loading model ID {info.ID}, file ID {info.fileID}, flavor {info.flavor}, impl {info.impl}, order {info.order}, iter {info.iter}, ts {info.ts}", flush=True)
    model.ID = info.ID
    model.fileID[info.flavor][info.impl] = info.fileID
    model.impl[info.flavor][info.impl] = BADLAD.read_model(filepath) if info.impl == "badlad" else BADMF.read_model(filepath)
    model.iter[info.flavor] = info.iter
    model.order[info.flavor] = info.order

class BackgroundModel:
    #
    # Background model comes in two flavors, i.e. static and radon
    # Each flavor has a BADLAD and a BADFM implemention
    # - BADLAD impl for AIC monitoring
    # - BADFM impl for anomaly detection
    # Model iteration refers to the training count
    # Model order is defined as:
    # - 0: bootstrapped model
    # - 1: first order model, i.e. 1 static comp. or 1 static + 1 radon comp.
    # - 2: second order model, same as first order with one additional static comp.
    # Model ID is the same for all flavors/implementations
    # Model file ID refers to flavor/implementation specific model files
    #
    def __init__(self, int_time=1., far=0.125, bin_edges=np.linspace(0., 3000, 301), files=None):
        self.ID = None
        self.fileID = defaultdict(dict)
        self.impl = defaultdict(dict)
        self.iter = defaultdict(int)
        self.order = defaultdict(bool)
        self.flavors = []
        self.flavor = None
        if files is not None:
            #
            # init model with exitsting model files
            #
            flavors = []
            for file in files:
                info = model_info(file)
                print(f"Loading model ID {info.ID}, file ID {info.fileID}, flavor {info.flavor}, impl {info.impl}, order {info.order}, iter {info.iter}, ts {info.ts}")
                self.ID = info.ID
                self.fileID[info.flavor][info.impl] = info.fileID
                self.impl[info.flavor][info.impl] = BADLAD.read_model(file) if info.impl == "badlad" else BADMF.read_model(file)
                self.iter[info.flavor] = info.iter
                self.order[info.flavor] = info.order
                flavors.append(info.flavor)
            flavors = list(np.unique(np.asarray(flavors)))
            if "static" not in flavors:
                print("A static model needs to be specified")
                sys.exit(1)
            self.int_time = self.impl["static"]["badlad"].mean_inttime
            self.far = self.impl["static"]["badlad"].far
            self.bin_edges = self.impl["static"]["badlad"].binedges
            self.bin_centers = self.impl["static"]["badlad"].bincenters
            self.nbins = self.impl["static"]["badlad"].num_bins
            self.flavors = flavors
            self.flavor = "static"
        else:
            self.int_time = int_time
            self.far = far
            self.bin_edges = bin_edges
            self.bin_centers = 0.5 * (self.bin_edges[1:] + self.bin_edges[:-1])
            self.nbins = len(self.bin_centers)
