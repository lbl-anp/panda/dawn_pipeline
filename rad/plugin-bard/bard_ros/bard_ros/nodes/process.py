#!/usr/bin/env python
import warnings
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"The frame.append method is deprecated*")
warnings.filterwarnings(action='ignore', category=FutureWarning, message=r"arrays to stack*")
# ROS
import rospy
import curie_ros as ci_ros
from bard_ros_msgs.msg import SpectInfo
from raingauge_ros_msgs.msg import RainGauge
from curie_ros_msgs.msg import RadData
from stark_ros_msgs.msg import StructuredArray
# BARD
from utils import sqrt_n_bins
from utils import RadDataRollingAggregator
from model import BackgroundModel
from background import SpectralTriage, ModelBootstrap, ModelTraining
from background import AnomalyDetection, ModelSelection, ModelMonitoring
# generic
import os
import numpy as np
import h5py
import time

class BARDNode(object):

    def __init__(self):
        #
        # init node/params
        #
        rospy.init_node("bard", anonymous=False)
        FAR = float(os.getenv("FAR")) # in 1/h
        #INT_TIME = 2.
        INT_TIME = float(os.getenv("INTTIME")) # in s
        NBINS = 128
        BIN_EDGES, BIN_CENTERS = sqrt_n_bins(nmin=50, nmax=3000, nbins=NBINS)
        SLOW_SCALE = 2*3600.
        FAST_SCALE = 30.
        NSCRAD_SCALE = 2*3600.
        PROXY_THR = 0.05
        BOOTSTRAP_TIME= 30. # in min
        ENCOUNTER_SPLIT = 10. 
        SELECTION_TIME = 30.
        SELECTION_SCALE = 60.
        MONITORING_SCALE = 1800.
        #
        # OVERRIDE DEFAULTS, FOR TESTING ONLY
        #
        #BOOTSTRAP_TIME = 1
        #FAR = 90 # this corresponds to a training set size of 20 2s integrated spectra
        #
        # subscribers
        #
        self.raingauge_sub = rospy.Subscriber('/sensors/raingauge', RainGauge, self.on_raingauge_msg)
        self.cal_lm_sub = rospy.Subscriber('/calib/listmode', RadData, self.on_listmode_msg)

        # rad data aggregator   
        self.radDataAgg = RadDataRollingAggregator(int_time=INT_TIME, bin_edges=BIN_EDGES)
        
        # bkg model
        BKGFILES = os.getenv("PANDA_BKGFILES")
        if BKGFILES:
            print("Loading background models:")
            BKGDIR = os.getenv("PANDA_BKGDIR")
            delimiter = ',' if ',' in BKGFILES else ' '
            files = []
            for f in BKGFILES.split(delimiter):
                files.append(BKGDIR+'/'+f)
            print(files)
            #files = ["/workspace/mount/bkg_models/W01A-Passby/W01A_Passby_static_BADLAD_trained.h5",
            #         "/workspace/mount/bkg_models/W01A-Passby/W01A_Passby_static_BADFM_trained.h5"]
            self.model = BackgroundModel(int_time=INT_TIME, far=FAR, bin_edges=BIN_EDGES, files=files)
            FAR = self.model.far
        else:
            print("Initializing background model")
            self.model = BackgroundModel(int_time=INT_TIME, far=FAR, bin_edges=BIN_EDGES)
        """
        print("Loading background models:")
        files = ["/workspace/mount/bkg_models/W01A-Passby/W01A_Passby_static_BADLAD_trained.h5",
                 "/workspace/mount/bkg_models/W01A-Passby/W01A_Passby_static_BADFM_trained.h5"]
        print(files)
        self.model = BackgroundModel(int_time=INT_TIME, far=FAR, bin_edges=BIN_EDGES, files=files)
        """

        # spectral triage
        self.triage = SpectralTriage(int_time=INT_TIME, far=FAR,
                                     tscale_slow=SLOW_SCALE, fpr_slow=1,
                                     tscale_fast=FAST_SCALE, fpr_fast=2,
                                     tscale_nscrad=NSCRAD_SCALE, bin_edges=BIN_EDGES,
                                     radon_proxy_thr=PROXY_THR)
        # model bootstrap
        self.bootstrap = ModelBootstrap(minutes=BOOTSTRAP_TIME, model=self.model)
        # model training
        self.training = ModelTraining(model=self.model)
        # anomaly detection
        self.detection = AnomalyDetection(model=self.model, int_time=INT_TIME, encounter_split=ENCOUNTER_SPLIT)
        # model selection
        self.selection = ModelSelection(model=self.model, int_time=SELECTION_TIME, tscale=SELECTION_SCALE, thr=0.1)
        # model monitoring
        self.monitoring = ModelMonitoring(model=self.model, int_time=INT_TIME, tscale=MONITORING_SCALE)
        #
        # TO-DO: add source template generation from anomaly clustering
        #

        self.pub_spectinfo = rospy.Publisher("/bard/spect_info", SpectInfo, queue_size=60)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def on_raingauge_msg(self, msg):
        ts = msg.header.stamp.secs + 1E-9 * msg.header.stamp.nsecs
        val = msg.total_accumulated
        self.triage.updateRadonProxyFilter(ts, val)

    def on_listmode_msg(self, msg):
        lm = ci_ros.converters.from_ros_msg(msg)
        ts = lm.arrays["timestamp_det"]
        data = lm.arrays["energy"]
        #self.radDataAgg.append(data, ts[0], ts[-1])
        self.radDataAgg.append(data, ts)

    def run(self):
        while not rospy.is_shutdown():  
            data = self.radDataAgg.get()
            #
            start = time.time()
            print(f"Rad data ready: ts {data.ts}, lt {data.lt}")

            # create spectral info msg
            # gets filled in by each BARD component
            spectInfo = SpectInfo()
            spectInfo.header.stamp = rospy.Time.now()
            spectInfo.spectrum = data.binmode.spectrum.astype(np.float32).flatten()
            spectInfo.timestamp = data.ts
            spectInfo.livetime = data.lt

            # run spectral triage
            self.triage.analyze(spectInfo)
            #
            stop = time.time()
            print(f"Spectral triage {(stop - start):.3f}s")
            
            # block data flow till bootstrap of static flavor is done
            # after that we start checking against the background model
            # and performing anomaly detection, even if the model is 
            # not trained yet 
            if not self.bootstrap.static_is_done():
                if not (spectInfo.triage_anom or spectInfo.triage_none):
                    self.bootstrap.append(spectInfo)
                    #
                    stop = time.time()
                    print(f"Appending to static bootstrap {(stop - start):.3f}s")
                continue
            
            # bootstrap radon model if still needed,
            # and append to training sets
            if not (spectInfo.triage_anom or spectInfo.triage_none):
                if not self.bootstrap.radon_is_done():
                    self.bootstrap.append(spectInfo)
                self.training.append(spectInfo)
            #
            stop = time.time()
            print(f"Appending to radon bootstrap and/or training {(stop - start):.3f}s")

            # check against background
            self.monitoring.check(spectInfo)
            #
            stop = time.time()
            print(f"Checking bkg model {(stop - start):.3f}s")

            # perform anomaly detection
            self.detection.detect(spectInfo, data.listmode)
            #
            stop = time.time()
            print(f"Anomaly detection {(stop - start):.3f}s")

            # start model selection when both flavors are bootstrapped
            if self.bootstrap.is_done():
                if not (spectInfo.triage_anom or spectInfo.triage_none):
                    self.selection.select(spectInfo)
                    #if self.selection.select(spectInfo):
                    #    self.model.flavor = self.selection.get_flavor()
                    #    
                    stop = time.time()
                    print(f"Model selection {(stop - start):.3f}s")

            # pub spectral info
            self.pub_spectinfo.publish(spectInfo)
            #
            stop = time.time()
            print(f"BARD done in {(stop - start):.3f}s")

            # train bootstrapped flavors when possible
            if not (self.training.static_is_done() | self.training.radon_is_done()):
                continue

if __name__ == '__main__':
    with BARDNode() as node:
        node.run()  

