#!/bin/bash

rm -rf bad
rm -rf curie
rm -rf poisson-gof
rm -rf radai
rm -rf radmf
rm -rf stark
rm -rf curie_ros
rm -rf stark_ros

cp -r ../../dependencies/bad .         # dev_panda branch (inclues pnmf_nnls model)
cp -r ../../dependencies/curie .       # detached branch, original commit
cp -r ../../dependencies/poisson-gof . # detached branch
cp -r ../../dependencies/radai .       # dev_panda (clone of improve-k-sigma) branch
cp -r ../../dependencies/radmf .       # dev_panda branch (includes pnmf_nnls model)
cp -r ../../dependencies/stark .       # detached branch, original commit
cp -r ../../dependencies/curie_ros .   # dev_panda branch
cp -r ../../dependencies/stark_ros .   # dev_panda branch

rm -rf raingauge_ros_msgs
cp -r ../../sensors/plugin-raingauge/raingauge_ros/raingauge_ros_msgs .

version=$1
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:${version}
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-bard:${version}
