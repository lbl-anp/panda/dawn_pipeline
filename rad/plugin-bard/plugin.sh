#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

source /home/panda/panda_ws/devel/setup.bash

export BKGMODELDIR="/home/panda/mount/bkg_models"
export BADDATADIR=${BKGMODELDIR}
export PKGDIR="/home/panda/panda_ws/src/bard_ros"
export PYTHONPATH="$PATH:$PYTHONPATH"

roslaunch bard_ros bard.launch
