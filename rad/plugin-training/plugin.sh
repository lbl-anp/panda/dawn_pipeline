#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

export BKGMODELDIR="/home/panda/mount/bkg_models"
export BADDATADIR=${BKGMODELDIR}
export PYTHONPATH="$PATH:$PYTHONPATH"

python3 node.py
