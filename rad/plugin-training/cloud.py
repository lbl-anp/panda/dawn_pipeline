# waggle/sage
import waggle.plugin as waggle
import sage_data_client as sage
# generic
from datetime import datetime
from dateutil import relativedelta
from collections import defaultdict
import subprocess
import os.path
import time
import numpy as np
import pandas as pd
import threading
# panda
from training import train_model

#
# deployed nodes
#
NODES = ["W01A", # LBNL 
        "W01C",
        "W059", "W05A", "W05B", "W05C", "W05D",
        "W072", "W073", "W074", "W075", "W076", "W077"]

#
# Helper class for file transfer from cloud to node
#
class CloudToNodeModelXfer():
    def __init__(self):
        # get beehive credentials, and model dir from env
        self.PANDA_USR, self.PANDA_PWD = os.getenv("PANDA_USR"), os.getenv("PANDA_PWD")
        self.XFER_DIR = os.getenv('BKGMODELDIR')
        self.ts_record = defaultdict() # holds timestamp of last processed model file
        now = time.time_ns()
        for node in NODES:
            self.ts_record[node] = np.int64(now)

        print(f"USR: {self.PANDA_USR}, XFER DIR: {self.XFER_DIR}", flush=True)

        print(f"PYTHONPATH: {os.getenv('PYTHONPATH')}", flush=True)

    def wait_for_model(self):

        print(f"xfer: starting queries in main thread", flush=True)

        wait_time = 30
        date = datetime.today()
        start_date = date - relativedelta.relativedelta(days=1)
        end_date = date + relativedelta.relativedelta(days=1)
        while True:

            for NODE in NODES:

                print(f"xfer: querying for node {NODE}", flush=True)

                df = sage.query(
                    start=start_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
                    end=end_date.strftime("%Y-%m-%d")+" 00:00:00.000000000+00:0",
                    filter={"name": "upload",            # name associated to Plugin.upload_file method
                            "vsn": NODE,                 # VSN of remote node
                            "task": "panda-bard"})       # name of plugin running on remote node
                if df.empty:

                    print(f"xfer: nothing for node {NODE}", flush=True)
                    
                    continue

                # find first entry with timestamp larger than last one
                match = df.loc[np.int64(df["timestamp"]) > self.ts_record[NODE]] # implicit conversion to np.datetime64
                if not match.empty:
                    # update record
                    self.ts_record[NODE] = np.int64(match["timestamp"].values[0])
                    meta = {}
                    meta["src_fileid"] = match["meta.src_fileid"].values[0]
                    meta["dst_vsn"] = NODE
                    matched_url = match["value"].values[0]

                    filepath = self.XFER_DIR + '/' + matched_url.split('/')[-1]

                    print(f"xfer: match for node {NODE}, src_fileid = {meta['src_fileid']}", flush=True) 
                    print(f"at ts {self.ts_record[NODE]}, downloading from matched url {matched_url} to filepath {filepath}", flush=True)
                    
                    while True:
                        try:
                            subprocess.check_call(["wget", "--user="+self.PANDA_USR, "--password="+self.PANDA_PWD,
                                                   "--directory-prefix="+self.XFER_DIR, matched_url])
                        except subprocess.CalledProcessError as e:
                            print(f"xfer: exception raised {e.output}")
                            print("xfer: download not staged yet, sleeping ...", flush=True)
                            
                            time.sleep(wait_time)
                        
                        if os.path.isfile(filepath): break

                    # spawn training and upload threads
                    now = datetime.utcnow()
                    timestamp = time.mktime(now.timetuple())
                    fileID = now.strftime("%Y%m%d-%H%M%S")
                    self.start_train_model(filepath, meta, timestamp, fileID, self.XFER_DIR)
                    filename = fileID + ".h5"
                    filepath = self.XFER_DIR + '/' + filename 
                    self.start_upload_model(filepath, meta)
                else:

                    print(f"xfer: no match for node {NODE}", flush=True)


            print("Looped over all nodes, sleeping ...", flush=True)

            time.sleep(wait_time)

    def start_train_model(self, filepath, meta, timestamp, fileID, xfer_dir):

        print(f"xfer: starting training thread, file {filepath}, src_fileid {meta['src_fileid']}", flush=True)

        thread = threading.Thread(target=train_model, args=[filepath, meta, timestamp, fileID, xfer_dir])
        thread.start()

    def start_upload_model(self, filepath, meta):

        print(f"xfer: starting upload thread, file {filepath}, src_fileid {meta['src_fileid']}", flush=True)

        thread = threading.Thread(target=self.upload_model, args=[filepath, meta])
        thread.start()


    def upload_model(self, filepath, meta):
        wait_time = 30
        while True:
            if os.path.isfile(filepath):
                break
            else:
                print(f"xfer: trained model {filepath} not ready for upload yet, sleeping ...", flush=True)

                time.sleep(wait_time)

        with waggle.Plugin() as plugin:
            plugin.upload_file(filepath, meta=meta, keep=True)

