#!/bin/bash

rm -rf bad
rm -rf poisson-gof
rm -rf radmf

cp -r ../../dependencies/bad .       # dev_panda branch (inclues pnmf_nnls model)
cp -r ../../dependencies/poisson-gof .
cp -r ../../dependencies/radmf .     # dev_panda branch (includes pnmf_nnls model)

version=$1
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-training:${version}
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-training:${version}
