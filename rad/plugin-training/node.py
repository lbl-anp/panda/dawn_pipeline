# PANDA
from cloud import CloudToNodeModelXfer


class ModelTrainingNode(object):
    def __init__(self):
        self.model_xfer = CloudToNodeModelXfer()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def run(self):
        while True:
            self.model_xfer.wait_for_model()

if __name__ == '__main__':
    with ModelTrainingNode() as node:
        node.run()
