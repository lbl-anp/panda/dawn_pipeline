# generic
import numpy as np
import h5py
from collections import namedtuple
import time
from datetime import datetime
# PANDA
from bad import BADLAD, BADMF

ModelInfo = namedtuple("ModelInfo", "ID fileID flavor impl iter order ts")
def model_info(filename):
    with h5py.File(filename, 'r') as h5in:
        return ModelInfo(ID=h5in.attrs["ID"], fileID=h5in.attrs["fileID"], flavor=h5in.attrs["flavor"],
                         impl=h5in.attrs["impl"], iter=h5in.attrs["iter"],
                         order=h5in.attrs["order"], ts=h5in.attrs["timestamp"])

def train_model(filepath, meta, timestamp, fileID, xfer_dir):
    start = time.time()

    info = model_info(filepath)
    print(f"train_model: model ID {info.ID}, file ID {info.fileID}, flavor {info.flavor}, impl {info.impl}, order {info.order}, iter {info.iter}, ts {info.ts}", flush=True)

    with h5py.File(filepath, 'r') as h5in:
        training = h5in["training"][()]
    
    print(f"train_model: training set {training.shape}", flush=True)

    impl = BADLAD.read_model(filepath) if info.impl == "badlad" else BADMF.read_model(filepath)
    impl.fit(training=training, n_iter=50, eps=1.E-5)
    if info.impl == "badlad":
        impl.calculate_threshold(training=training, far=impl.far, set_threshold=True, empirical=True)
    else:
        impl.calculate_threshold(training, far=impl.far, set_threshold=True)    
    for i in range(impl.n_components):
        np.clip(impl.basis[i], 1.E-6, None, out=impl.basis[i])
        
    filename = fileID + ".h5"
    filepath = xfer_dir + '/' + filename

    print(f"train_model: write model to {filepath}", flush=True)

    impl.write_model(filepath)
    with h5py.File(filepath, "r+") as h5in:
        h5in.attrs["flavor"] = info.flavor
        h5in.attrs["ID"] = info.ID
        h5in.attrs["fileID"] = fileID
        h5in.attrs["impl"] = info.impl
        h5in.attrs["iter"] = int(info.iter) + 1
        # TO-DO: once monitoring is set up and triggers re-training with addition of one component
        # once should handle order properly here
        h5in.attrs["order"] = 1
        h5in.attrs["timestamp"] = timestamp
    
    stop = time.time()
    print(f"train_model: training done in {(stop - start):.1f}s", flush=True)
