FROM amd64/ubuntu:focal
RUN ln -s /usr/bin/python3.8 /usr/bin/python

# Setup directory structure
RUN mkdir -p /home/panda
WORKDIR /home/panda

# Install dependencies
RUN apt-get update \
  && apt-get install -y python3-pip \
# For h5py
  && apt-get install -y libhdf5-dev \
# For numpy
  && apt-get install -y gfortran libopenblas-dev liblapack-dev \
# LLVM for numba
  && apt-get install -y llvm-10 \
  && ln -s /usr/bin/llvm-config-10 /usr/bin/llvm-config \
  && apt-get install -y swig \
# sage data client
  && apt-get install -y wget
RUN rm -rf /var/lib/apt/lists/*

# Install python packages
RUN rm -rf /usr/lib/python3/dist-packages/numpy
RUN pip3 install --force-reinstall numpy==1.21
RUN pip3 install matplotlib
RUN pip3 install numba
RUN pip3 install healpy
RUN pip3 install h5py
RUN pip3 install deprecated
RUN pip3 install nlopt
RUN pip3 install pytest-cov
RUN pip3 install numexpr
RUN pip3 install statsmodels
RUN pip3 install scikit-learn
RUN pip3 install python-dateutil


RUN pip3 install pywaggle
RUN pip3 install packaging==22.0
RUN pip3 install pandas==1.5.3
RUN pip3 install sage_data_client

COPY poisson-gof /home/panda/poisson-gof
WORKDIR /home/panda/poisson-gof
RUN python3 setup.py install --user
COPY radmf /home/panda/radmf
WORKDIR /home/panda/radmf
RUN python3 setup.py install --user
COPY bad /home/panda/bad
WORKDIR /home/panda/bad
RUN python3 setup.py install --user

WORKDIR /home/panda
RUN mkdir -p mount/bkg_models
COPY node.py .
COPY cloud.py .
COPY training.py .
COPY plugin.sh .

CMD ["/bin/bash", "-c", "./plugin.sh"]
