#!/bin/bash

model=$1
precision=$2

python3 detect_trt.py --source /mount/sample --weights ${model} --p ${precision} --labels yolo_coco_labels.txt --class 0 1 2 3 5 7 --name output
