import os
import time
import argparse
import glob
from pathlib import Path
import cv2
import numpy as np
import torch
from torchvision import transforms
from utils.datasets import letterbox
from utils.general import check_img_size, non_max_suppression, scale_coords
from utils.plots import plot_one_box

def get_device():
    return torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def load_model(device):
    #model = torch.load("yolov7.pt", map_location=device)["model"]
    model = torch.load("yolov7-tiny.pt", map_location=device)["model"]
    model.float().eval()
    if torch.cuda.is_available(): model.half().to(device)
    return model

def color_labels(model, label_file=''):
    if label_file:
        labels = open(label_file).read().strip().split("\n")
    else:
        labels = model.module.names if hasattr(model, 'module') else model.names
    colors = [[np.random.randint(0, 255) for _ in range(3)] for _ in labels]
    return labels, colors

def detect():
    device = get_device()
    model = load_model(device)
    stride = int(model.stride.max())  # model stride
    img_size = 640
    img_size = check_img_size(img_size, s=stride)  # check img_size disible by stride
    if device.type != 'cpu':
        model(torch.zeros(1, 3, img_size, img_size).to(device).type_as(next(model.parameters())))  # run once
    labels, colors = color_labels(model)

    save_dir = "/mount/output"
    if not os.path.exists(save_dir): os.makedirs(save_dir)
    path = str(Path("/mount/sample").absolute())  # os-agnostic absolute path
    if '*' in path:
        filepaths = sorted(glob.glob(path, recursive=True))
    elif os.path.isdir(path):
        filepaths = sorted(glob.glob(os.path.join(path, '*.*')))
    elif os.path.isfile(path):
        filepaths = [path]  # files

    for filepath in filepaths:
        t0 = time.time()

        # Pre-process input
        img0 = cv2.imread(filepath)  # BGR
        img = letterbox(img0, img_size, stride=stride)[0]
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img).to(device)
        if torch.cuda.is_available(): 
            img = img.half().to(device)
        img /= 255.0
        img = img.unsqueeze(0)
        t1 = time.time()
        
        # Run inference
        with torch.no_grad():
            predictions, _ = model(img)
        t2 = time.time()
        
        # Apply NMS
        predictions = non_max_suppression(predictions, 0.25, 0.45, classes=[0, 1, 2, 3, 5, 7], agnostic=False)
        t3 = time.time()

        # Post-process
        for detections in predictions:
            save_path = save_dir + '/' + os.path.basename(filepath)  
            #gn = torch.tensor(img0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            detections[:, :4] = scale_coords(img.shape[2:], detections[:, :4], img0.shape).round()
            s = ""
            for c in detections[:, -1].unique():
                n = (detections[:, -1] == c).sum()
                s += f"{n} {labels[int(c)]}{'s' * (n > 1)}, "
            for *xyxy, conf, cls in reversed(detections):
                label = f'{labels[int(cls)]} {conf:.2f}'
                plot_one_box(xyxy, img0, label=label, color=colors[int(cls)], line_thickness=1)
        cv2.imwrite(save_path, img0)
        t4 = time.time()

        print(f"{s} input: {(1E3 * (t1 - t0)):.1f}ms, inference: {(1E3 * (t2 - t1)):.1f}ms, NMS: {(1E3 * (t3 - t2)):.1f}ms, output: {(1E3 * (t4 - t3)):.1f}ms")


if __name__ == '__main__':
    detect()
