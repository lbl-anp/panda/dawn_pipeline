#!/bin/bash

model=$1
precision=$2

python3 detect_trt_tuned.py --source /mount/sample --weights ${model} --p ${precision} --name output
