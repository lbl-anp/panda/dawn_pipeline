import numpy as np
import cv2
import time
import os
import argparse
from pathlib import Path

def detect(weights, config):
    #CLASSES = ["bicycle", "bus",  "car", "motorbike", "person"]
    CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus",  "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
    COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

    print(f"loading SSD model {weights}, config {config}")
    net = cv2.dnn.readNetFromCaffe(config, weights)
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

    save_dir = "/mount/output"
    if not os.path.exists(save_dir): os.makedirs(save_dir)

    paths = sorted(Path("/mount/sample").iterdir(), key=os.path.getmtime)
    for path in paths:
        t0 = time.time()

        # Pre-process input
        frame = cv2.imread(str(path))
        h, w = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(frame, 0.007843, (300, 300), 127.5)
        net.setInput(blob)
        t1 = time.time()

        # inference
        detections = net.forward()
        t2 = time.time()

        classIDs = []
        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.5:
                idx = int(detections[0, 0, i, 1])
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                label = "{}: {:.2f}%".format(CLASSES[idx],confidence*100)
                cv2.rectangle(frame, (startX, startY), (endX, endY), COLORS[idx], 2)
                y = startY - 15 if startY - 15 > 15 else startY + 15
                cv2.putText(frame, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
                classIDs.append(idx)

        save_path = save_dir + '/' + path.name
        cv2.imwrite(save_path, frame)
        t3 = time.time()

        s = ""
        if len(classIDs):
            classes = np.asarray(classIDs)
            for c in np.unique(classes):
                n = (classes == c).sum()
                s += f"{n} {CLASSES[int(c)]}{'s' * (n > 1)}, "

        print(f"detections: {s}")
        print(f"input: {(1E3 * (t1 - t0)):.1f}ms, inference: {(1E3 * (t2 - t1)):.1f}ms, output: {(1E3 * (t3 - t2)):.1f}ms")


        """
        (H, W) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (640, 640), swapRB=True, crop=False)
        net.setInput(blob)
        t1 = time.time()

        # Run inference
        layerOutputs = net.forward(ln)
        #layerOutputs = net.forward()
        t2 = time.time()

        # NMS
        boxes = []
        confidences = []
        classIDs = []
        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                if classID not in class_mask: continue
                confidence = scores[classID]
                if confidence > 0.25:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(int(classID))
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.45)
        t3 = time.time()

        # Post-process output
        if len(idxs) > 0:
            s = ""
            classes = np.asarray(classIDs)[idxs.flatten()]
            for c in np.unique(classes):
                n = (classes == c).sum()
                s += f"{n} {LABELS[int(c)]}{'s' * (n > 1)}, "
            for i in idxs.flatten():
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
                cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        save_path = save_dir + '/' + path.name
        cv2.imwrite(save_path, frame)
        t4 = time.time()

        print(f"detections: {s}")
        print(f"input: {(1E3 * (t1 - t0)):.1f}ms, inference: {(1E3 * (t2 - t1)):.1f}ms, NMS: {(1E3 * (t3 - t2)):.1f}ms, output: {(1E3 * (t4 - t3)):.1f}ms")
        """

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str)
    parser.add_argument('--config', type=str)
    args = parser.parse_args()
    detect(args.weights, args.config)
