import os
import sys
import time
import argparse
from pathlib import Path
from collections import namedtuple
import cv2
import numpy as np

# OD
from threading import Event
import queue
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt

import torch
from utils.general import increment_path, non_max_suppression, xyxy2xywh, scale_coords
from utils.torch_utils import time_synchronized
from utils.plots import plot_one_box

# Tracker
import norfair
from norfair import Detection, Tracker
DISTANCE_THRESHOLD_BBOX: float = 0.7
DISTANCE_THRESHOLD_CENTROID: int = 30
MAX_DISTANCE: int = 10000

class HostDeviceMem(object):
    """Simple helper data class that's a little nicer to use than a 2-tuple."""     
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem
    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)
    def __repr__(self):
        return self.__str__()

def get_engine(filename, logger):
    """Create TensorRT engine."""
    with open(filename, "rb") as f, trt.Runtime(logger) as runtime:
        buf = f.read()
        engine = runtime.deserialize_cuda_engine(buf)
        return engine

def letterbox(im, new_shape=(640, 640), color=(114, 114, 114), auto=True, scaleup=True, stride=32):
    shape = im.shape[:2]
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better val mAP)
        r = min(r, 1.0)
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    if auto:  # minimum rectangle
        dw, dh = np.mod(dw, stride), np.mod(dh, stride)  # wh padding
    dw /= 2  # divide padding into 2 sides
    dh /= 2
    if shape[::-1] != new_unpad:  # resize
        im = cv2.resize(im, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
    return im, r, (dw, dh)

def process_inputs(inputs, img, shape, precision):
    image, ratio, dwdh = letterbox(img, new_shape=shape, auto=False)
    image = image.transpose((2, 0, 1))
    image = np.expand_dims(image, 0)
    image = np.ascontiguousarray(image)
    if precision == "fp32":
        image = image.astype(np.float32) / 255.0
    else:
        image = image.astype(np.float16) / 255.0
    np.copyto(inputs[0].host, image.ravel())
    return {"ratio": ratio, "dwdh": dwdh, "img_shape": img.shape}


def nms(boxes, scores, nms_thr):
    """Single class NMS implemented in Numpy."""
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]
    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])
        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)
        inds = np.where(ovr <= nms_thr)[0]
        order = order[inds + 1]
    return keep 


def multiclass_nms(boxes, scores, nms_thr, score_thr):
    """Multiclass NMS implemented in Numpy"""
    final_dets = []
    num_classes = scores.shape[1]
    for cls_ind in range(num_classes):
        cls_scores = scores[:, cls_ind]
        valid_score_mask = cls_scores > score_thr
        if valid_score_mask.sum() == 0: continue
        else:
            valid_scores = cls_scores[valid_score_mask]
            valid_boxes = boxes[valid_score_mask]
            keep = nms(valid_boxes, valid_scores, nms_thr)
            if len(keep) > 0:
                cls_inds = np.ones((len(keep), 1)) * cls_ind
                dets = np.concatenate(
                        [valid_boxes[keep], valid_scores[keep, None], cls_inds], 1
                        )
                final_dets.append(dets)
    if len(final_dets) == 0: return None
    return np.concatenate(final_dets, 0)



def detect(save_img=False):
    source, weights, precision, view_img, save_txt, imgsz, trace = opt.source, opt.weights, opt.p, opt.view_img, opt.save_txt, opt.img_size, not opt.no_trace
    save_img = not opt.nosave and not source.endswith('.txt')  # save inference images

    #match model to precision
    weights += '_' + precision + '.trt'
    print("model:", weights)

    # Directories
    save_dir = Path(increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok))  # increment run
    (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir

    # TRT + CUDA context initialization
    cfx = cuda.Device(0).make_context()
    trt_logger = trt.Logger(trt.Logger.INFO)      
    engine = get_engine(weights, trt_logger)
    input_shape = engine.get_binding_shape(0)

    print("input shape", input_shape)

    # Create buffers
    inputs, outputs, bindings = [], [], []
    for binding in engine:
        # Compute size of memory to allocate
        size = (trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size)
        dtype = trt.nptype(engine.get_binding_dtype(binding))
        # Allocate host and device buffers
        host_mem = cuda.pagelocked_empty(size, dtype)
        device_mem = cuda.mem_alloc(host_mem.nbytes)
        # Append the device buffer to device bindings
        bindings.append(int(device_mem))
        # Append to the appropriate list
        if engine.binding_is_input(binding):
            inputs.append(HostDeviceMem(host_mem, device_mem))
        else:
            outputs.append(HostDeviceMem(host_mem, device_mem))
    # Create stream        
    stream = cuda.Stream()

    # Initialize tracker
    #distance_function = "euclidean"
    #distance_threshold = DISTANCE_THRESHOLD_CENTROID
    #tracker = Tracker(
    #    distance_function=distance_function,
    #    distance_threshold=distance_threshold,
    #)

    labels = []
    with open(opt.labels, "r") as f:
        labels = [line.rstrip() for line in f]
    colors = [[np.random.randint(0, 255) for _ in range(3)] for _ in labels]


    with engine.create_execution_context() as context:
        paths = sorted(Path(source).iterdir(), key=os.path.getmtime)
        for path in paths:
            t0 = time.time()

            # pre-process input
            img = cv2.imread(str(path))
            im0 = img
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            meta = process_inputs(inputs, img, (640, 640), precision)
            t1 = time.time()
            
            # transfer data to GPU, run inference, transfer data back
            cfx.push()
            [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]
            #[cuda.memcpy_htod(inp.device, inp.host) for inp in inputs]
            context.execute_async(batch_size=1, bindings=bindings, stream_handle=stream.handle)
            #context.execute(batch_size=1, bindings=bindings)
            [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]
            #[cuda.memcpy_dtoh(out.host, out.device) for out in outputs]
            stream.synchronize()
            cfx.pop()
            t2 = time.time()

            # post-process output
            n_bboxes = 25200 # for strides of 8 (3x80x80), 16 (3x40x40), and 32 (3x20x20) on 640x640
            #n_bboxes = 18900 # for strides of 8 (3x80x60), 16 (3x40x30), and 32 (3x20x15) on 480x640
            n_features = 85
            predictions = (outputs[0].host).reshape((n_bboxes, n_features))
            mask = [0, 1, 2, 3, 4] # x, y, w, h, conf
            class_mask = [c + 5 for c in opt.classes]
            mask.extend(class_mask)
            predictions = predictions[:, mask]
            t3 = time.time()
            
            # NMS
            bboxes = predictions[:, :4]
            scores = predictions[:, 4:5] * predictions[:, 5:]
            # bboxes are in the xywh format, convert to xyxy format
            bboxes_xyxy = np.ones_like(bboxes)
            bboxes_xyxy[:, 0] = bboxes[:, 0] - bboxes[:, 2] / 2. # xmin = x_center - half_width
            bboxes_xyxy[:, 1] = bboxes[:, 1] - bboxes[:, 3] / 2. # ymin = y_center - half_height
            bboxes_xyxy[:, 2] = bboxes[:, 0] + bboxes[:, 2] / 2. # xmax = x_center + half_width
            bboxes_xyxy[:, 3] = bboxes[:, 1] + bboxes[:, 3] / 2. # ymax = y_center + half_height
            bboxes_xyxy -= np.array([*meta["dwdh"], *meta["dwdh"]])
            bboxes_xyxy /= meta["ratio"]
            predictions = multiclass_nms(bboxes_xyxy, scores, nms_thr=opt.iou_thres, score_thr=opt.conf_thres)
            t4 = time.time()

            print(f'({(1E3 * (t1 - t0)):.1f}ms) input, ({(1E3 * (t2 - t1)):.1f}ms) inference, ({(1E3 * (t3 - t2)):.1f}ms) output, ({(1E3 * (t4 - t3)):.1f}ms) NMS')

            # Process detections
            # NMS format: xyxy, score, class
            if predictions is not None:
                n_detections = predictions.shape[0]
                s = ''
                for c in np.unique(predictions[:, -1]):
                    n = (predictions[:, -1] == c).sum()
                    s += f"{n} {labels[int(c)]}{'s' * (n > 1)}, "
                print(f"predictions for {n_detections} detections: {s}")

                for *xyxy, conf, cls in reversed(predictions):
                    label = f'{labels[int(cls)]} {conf:.2f}'
                    plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=1)
            
            save_path = str(save_dir / path.name)
            cv2.imwrite(save_path, im0)
            
            """
            for i, det in enumerate(detections):
                #p, s, im0, frame = path, f"frame {idx}, ", im0s, getattr(dataset, 'frame', 0)

                p = Path(path)  # to Path
                save_path = str(save_dir / p.name)  # img.jpg
                if len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                    # Print results
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += f"{n} {labels[int(c)]}{'s' * (n > 1)}, "  # add to string

                    #++++++++++++++++++++++++++++++++++++++++++++
                    norfair_detections = []
                    #++++++++++++++++++++++++++++++++++++++++++++
                
                    # Write results
                    for *xyxy, conf, cls in reversed(det):
                        if save_img or view_img:  # Add bbox to image
                            label = f'{labels[int(cls)]} {conf:.2f}'
                            plot_one_box(xyxy, im0, label=label, color=colors[int(cls)], line_thickness=1)


                        xywh = xyxy2xywh(torch.tensor(xyxy).view(1, 4)).view(-1).tolist()
                        centroid = np.array(
                            [xywh[0], xywh[1]]
                        )
                        scores = np.array([conf])
                        norfair_detections.append(
                            Detection(
                                points=centroid,
                                scores=scores,
                                label=int(cls),
                            )
                        )

                    t4 = time_synchronized()
                    tracked_objects = tracker.update(detections=norfair_detections)
                    t5 = time_synchronized()
                
                    n_tracks = len(tracked_objects)
                    s += f"{n_tracks} tracks, "
                
                    norfair.draw_points(im0, tracked_objects , draw_ids=True, draw_points=True, draw_labels=False)
                

                    # Print time (inference + NMS)
                    print(f'{s}Done. ({(1E3 * (t2 - t1)):.1f}ms) Inference, ({(1E3 * (t3 - t2)):.1f}ms) NMS, ({(1E3 * (t5 - t4)):.1f}ms) Tracking')

                    if save_img:
                        cv2.imwrite(save_path, im0)
            """


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='yolov7.pt', help='model.pt path(s)')
    parser.add_argument('--p', type=str, default='fp32', help='model precision')
    parser.add_argument('--labels', type=str)
    parser.add_argument('--source', type=str, default='inference/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')
    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    parser.add_argument('--save-conf', action='store_true', help='save confidences in --save-txt labels')
    parser.add_argument('--nosave', action='store_true', help='do not save images/videos')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--project', default='runs/detect', help='save results to project/name')
    parser.add_argument('--name', default='exp', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    parser.add_argument('--no-trace', action='store_true', help='don`t trace model')
    opt = parser.parse_args()
    print(opt)
    #check_requirements(exclude=('pycocotools', 'thop'))

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov7.pt']:
                detect()
                strip_optimizer(opt.weights)
        else:
            detect()
