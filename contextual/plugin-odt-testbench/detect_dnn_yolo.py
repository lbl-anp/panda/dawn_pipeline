import os
import time
import argparse
from pathlib import Path
import cv2
import numpy as np


def detect(weights, config):
    LABELS = open("yolo_coco_labels.txt").read().strip().split("\n")
    np.random.seed(42)
    COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),dtype="uint8")
    
    class_mask = [0, 1, 2, 3, 5, 7]

    print(f"Loading weights {weights} and config {config}")
    net = cv2.dnn.readNetFromDarknet(config, weights)
    
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
    ln = net.getLayerNames()
    #ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    ln = [ln[i - 1] for i in net.getUnconnectedOutLayers()]

    save_dir = "/mount/output"
    if not os.path.exists(save_dir): os.makedirs(save_dir)

    paths = sorted(Path("/mount/sample").iterdir(), key=os.path.getmtime)
    for path in paths:
        t0 = time.time()

        # Pre-process input
        frame = cv2.imread(str(path))
        (H, W) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (640, 640), swapRB=True, crop=False)
        net.setInput(blob)
        t1 = time.time()
        
        # Run inference
        layerOutputs = net.forward(ln)
        #layerOutputs = net.forward()
        t2 = time.time()

        # NMS
        boxes = []
        confidences = []
        classIDs = []
        for output in layerOutputs:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                if classID not in class_mask: continue
                confidence = scores[classID]
                if confidence > 0.25:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(int(classID))
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.45)
        t3 = time.time()

        # Post-process output
        if len(idxs) > 0:
            s = ""
            classes = np.asarray(classIDs)[idxs.flatten()]
            for c in np.unique(classes):
                n = (classes == c).sum()
                s += f"{n} {LABELS[int(c)]}{'s' * (n > 1)}, "
            for i in idxs.flatten():
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                color = [int(c) for c in COLORS[classIDs[i]]]
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
                cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    
        save_path = save_dir + '/' + path.name
        cv2.imwrite(save_path, frame)      
        t4 = time.time()

        print(f"detections: {s}")
        print(f"input: {(1E3 * (t1 - t0)):.1f}ms, inference: {(1E3 * (t2 - t1)):.1f}ms, NMS: {(1E3 * (t3 - t2)):.1f}ms, output: {(1E3 * (t4 - t3)):.1f}ms")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', type=str, default='yolov7.weights')
    parser.add_argument('--config', type=str, default='yolov7.cfg')
    args = parser.parse_args()
    detect(args.weights, args.config)
