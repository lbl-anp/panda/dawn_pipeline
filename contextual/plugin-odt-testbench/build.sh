#!/bin/bash

rm -rf yolov7
cp -r ../../dependencies/yolov7 .

version=$1
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-testbench:${version}

