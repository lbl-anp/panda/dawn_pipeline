#!/bin/bash

rm -rf tensorrt-od-model-zoo
rm -rf tensorrt_od_ros
rm -rf object_perception_msgs
rm -rf vision_opencv

cp -r ../../dependencies/tensorrt-od-model-zoo .  # dev_panda branch
cp -r ../../dependencies/tensorrt_od_ros .        # dev_panda branch
cp -r ../../dependencies/object_perception_msgs . # main branch
cp -r ../../dependencies/vision_opencv .          # noetic branch

version=$1
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-detection:${version}
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-detection:${version}
