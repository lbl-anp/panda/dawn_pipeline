#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM


. /opt/ros/noetic/setup.sh
. /workspace/ros/devel/setup.sh

export ROS_LOG_DIR="/home/panda/mount/logs"

#export OD_MIN_PERIOD="1" #1 Hz inference for streaming camera images
#export OD_ENGINE="${YOLOV7_WORKSPACE}/yolov7-models/yolov7-tiny.trt"
#export OD_LIBRARY="${YOLOV7_WORKSPACE}/yolov7-libs/lib_yolov7_nms.py"
#export OD_LABELS="${YOLOV7_WORKSPACE}/yolov7-labels/yolo_coco_labels.txt"

roslaunch tensorrt_od_ros detect.launch

