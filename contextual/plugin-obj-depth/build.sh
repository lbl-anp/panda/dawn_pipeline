#!/bin/bash

rm -rf sort
rm -rf stark
rm -rf trajan
rm -rf sort_tracker_ros
rm -rf object_perception_msgs


cp -r ../../dependencies/sort .                   # detached branch, original commit
cp -r ../../dependencies/stark .                  # detached branch, original commit
cp -r ../../dependencies/trajan .                 # dev_panda (main branch now delegates base stuff to stark, 
                                                              #so stick to previous /tags/v1.3.0, 
                                                              #i.e. git checkout tags/v1.3.0 -b dev_panda)
cp -r ../../dependencies/sort_tracker_ros .       # dev_panda branch
cp -r ../../dependencies/object_perception_msgs . # main branch


version=$1
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-depth:${version}
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-obj-depth:${version}
