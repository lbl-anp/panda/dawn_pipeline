FROM registry.gitlab.com/lbl-anp/docker_central/ros_base_noetic_arm64v8

# Install extra dependencies
RUN apt-get update \
    && apt-get install -q -y --no-install-recommends \
        python3-pip \
        python3-sklearn \
        python3-skimage \
        python3-numpy \
        python3-opencv \
        python3-h5py \
        python3-numba \
        ros-$ROS_DISTRO-cv-bridge \
        ros-$ROS_DISTRO-geometry2 \
    && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install pip -U \
    && python3 -m pip install --no-cache-dir --force \
        opencv-python \
        scipy \
        python-dateutil>=2.8.2 

COPY sort /home/panda/sort
WORKDIR /home/panda/sort
RUN python3 setup.py install --user

COPY stark /home/panda/stark
WORKDIR /home/panda/stark
RUN python3 setup.py install --user

COPY trajan /home/panda/trajan
WORKDIR /home/panda/trajan
RUN python3 setup.py install --user

# Some other libs in base image call for numpy.typeDict, deprecated above 1.21
RUN pip3 install numpy==1.21

WORKDIR $ROS_WORKSPACE
RUN mkdir -p src
COPY object_perception_msgs src/object_perception_msgs

RUN . /opt/ros/noetic/setup.sh && catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3
RUN rm -rf src/object_perception_msgs

COPY sort_tracker_ros src/sort_tracker_ros
COPY sort_tracker.launch src/sort_tracker_ros/launch/sort_tracker.launch
COPY sort_tracker_params.yaml src/sort_tracker_ros/launch/sort_tracker_params.yaml
RUN . /opt/ros/noetic/setup.sh && catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3

# PANDA
RUN mkdir -p /home/panda
WORKDIR /home/panda
RUN mkdir mount
RUN mkdir plugin-scripts
COPY plugin.sh plugin-scripts/plugin.sh
RUN . /opt/ros/noetic/setup.sh && . ${ROS_WORKSPACE}/devel/setup.sh
CMD ["/bin/bash", "-c", "./plugin-scripts/plugin.sh"]
