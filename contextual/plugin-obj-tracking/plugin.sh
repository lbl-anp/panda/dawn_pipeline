#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

source /opt/ros/noetic/setup.bash
source /workspace/ros/devel/setup.bash

export ROS_LOG_DIR="/home/panda/mount/logs"

roslaunch sort_tracker_ros sort_tracker.launch
