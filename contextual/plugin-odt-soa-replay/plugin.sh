#!/bin/bash
function control_c {
    echo -en "\n*** Caught SIGINT; Clean up for script "${0##*/}" and Exit ***\n"
    # No real cleanup needed there
    exit $?
}
trap control_c SIGINT
trap control_c SIGTERM

export CAMERA_DIR="/workspace/panda/camera"
export OD_DIR="/workspace/panda/od/yolov7"
export TRACKER_DIR="/workspace/panda/tracker/norfair"

export ODT_MODEL_PATH="/workspace/panda/od/yolov7"
export ODT_SOA_PATH="/workspace/panda_ws/src/odt_soa_ros/nodes"
export PYTHONPATH="$ODT_SOA_PATH:$ODT_MODEL_PATH:$PYTHONPATH"

source /workspace/panda_ws/devel/setup.bash

roslaunch odt_soa_ros odt_soa.launch
