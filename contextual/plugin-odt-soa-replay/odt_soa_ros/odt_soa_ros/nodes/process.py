#!/usr/bin/env python
# ROS
import rospy
from sensor_msgs.msg import Image, CompressedImage
from bard_ros_msgs.msg import EncounterInfo
# OpenCV
import cv2
from cv_bridge import CvBridge
# pywaggle
#import waggle.plugin as plg
# camera
from camera import Camera
# object detection
from yoloTRT import YoloTRT
import torch
# 3D positions
from metric import estimate_positions
# tracking
from tracker import Tracker
# drawing
from drawing import draw_boxes
# generic
import numpy as np
import time
import copy
from collections import defaultdict
from bisect import bisect
from scipy.optimize import nnls
from scipy.stats import chi2


class ODTSOANode(object):
    #
    # Node performs object detection and tracking + source-object attribution
    # on unrectified camera frames
    #
    def __init__(self):
        #
        # intialize node
        #
        rospy.loginfo("Initializing node")
        rospy.init_node('odt_soa', anonymous=False)
        # read params
        params = self.get_parameters()

        # init camera -> intialization is modified for replay
        cam_IP = params["IP"]
        calib_file = params["calib_file"]
        self.camera = Camera(cam_IP, calib_file)
        self.img = None
        self.img_ts = 0.
        self.cvbr = CvBridge() # make sure this is instantiated before any sub/callback or pub using it
        self.sub_cam = rospy.Subscriber("/bag/sensors/camera/image_color", CompressedImage, self.on_camera_frame)

        # init OD
        model_file = params["model_file"]
        label_file = params["label_file"]
        conf_thr = params["conf_thr"]
        nms_thr = params["nms_thr"]
        self.yoloTRT = YoloTRT(
            model_file=model_file, label_file=label_file,
            input_w=self.camera.res_x, input_h=self.camera.res_y,
            conf_thr=conf_thr, nms_thr=nms_thr
        )
        
        # init tracking
        self.tracker = Tracker()
        
        # init ODT+SOA
        self.encounter = None
        self.in_encounter = False
        self.encounter_ts_buffer = []
        self.encounter_dt_buffer = []
        self.encounter_data_buffer = []
        self.encounter_err_buffer = []
        self.odt_ts_buffer = defaultdict(list)
        self.odt_r2_buffer = defaultdict(list)
        self.odt_label_buffer = defaultdict(int)
        self.odt_killed_ids = []
        self.in_encounter_soa = False
        self.soa_init_buffer = defaultdict(bool)
        self.soa_enc_idx_buffer = defaultdict(int)
        self.soa_odt_idx_buffer = defaultdict(int)
        self.soa_signal_buffer = defaultdict(list)
        self.soa_signal_std_buffer = defaultdict(list)
        self.soa_response_buffer = defaultdict(list)
        self.soa_fom_buffer = defaultdict(float) 

        # ROS
        #self.cvbr = CvBridge()
        self.pub_img = rospy.Publisher("/odt/camera/image_color", Image, queue_size=60)
        self.sub_bard = rospy.Subscriber("/bard/encounter_info", EncounterInfo, self.on_encounter)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        pass
    
    def get_parameters(self):
        params = {}
        params["IP"] = rospy.get_param("~IP", default="")
        params["calib_file"] = rospy.get_param("~calib_file", default="")
        params["model_file"] = rospy.get_param("~model_file", default="")
        params["label_file"] = rospy.get_param("~label_file", default="")
        params["conf_thr"] = rospy.get_param("~conf_thr", default=0.25)
        params["nms_thr"] = rospy.get_param("~nms_thr", default=0.45)
        return params

    def publish_img(self, img, ts):
        #msg = self.cvbr.cv2_to_imgmsg(img)
        msg = Image()
        msg.data = np.ascontiguousarray(img).tobytes()
        msg.width = img.shape[1]
        msg.height = img.shape[0]
        msg.encoding = "bgr8"
        msg.step = 3 * img.shape[1]
        msg.header.stamp = rospy.Time(ts * 1E-9)
        self.pub_img.publish(msg)

    def on_encounter(self, msg):
        self.encounter = msg
        self.in_encounter = self.encounter.live

    def on_camera_frame(self, msg):
        self.img = cv2.cvtColor(self.cvbr.compressed_imgmsg_to_cv2(msg), cv2.COLOR_RGB2BGR)
        self.img_ts = msg.header.stamp.to_nsec()

    def get_camera_frame(self):
        while not self.img_ts: time.sleep(0.01)
        return self.img, self.img_ts

    def run(self):
        prev_img_ts = time.time() * 1E9 #ns
        prev_enc_ts = time.time()       #s

        # run within TensorRT context
        with self.yoloTRT.engine.create_execution_context() as context:
            while not rospy.is_shutdown():
                img, img_ts = self.get_camera_frame()
                if img_ts == prev_img_ts: 
                    continue
                #rospy.loginfo(f"reading frame at {img_ts * 1E-9}")


                if self.in_encounter:
                    encounter = copy.deepcopy(self.encounter)
                    # buffer encounter data
                    if encounter.ts != prev_enc_ts:
                        
                        self.in_encounter_soa = True
                        
                        #rospy.loginfo(f"--> encounter {encounter.id}, [{encounter.ts} , {encounter.tl}], dt {img_ts*1E-9 - encounter.tl}")
                        #rospy.loginfo(f"--> {encounter.n_alarms} alarms: {encounter.ids}, {encounter.weights}")
                        if encounter.n_alarms:
                            # use mean of integration range for ts
                            self.encounter_ts_buffer.append(0.5 *(encounter.ts + encounter.tl))
                            self.encounter_dt_buffer.append(encounter.tl - encounter.ts)
                            # for now only first alarm
                            self.encounter_data_buffer.append(encounter.weights[0])
                            self.encounter_err_buffer.append(encounter.weights_std[0])
                    prev_enc_ts = encounter.ts
                

                # run OD inference
                data = self.yoloTRT.format_input(img)
                self.yoloTRT.infer(data)
                predictions = self.yoloTRT.process_output()
                if predictions is None: 
                    rospy.loginfo("No predictions on current frame")
                    # still needed to update the Kalman filter
                    self.tracker.track(None)
                else:
                    # format predictions to detections for tracking
                    detections = self.tracker.format(predictions)
                    
                    # get tracked objects
                    objects = self.tracker.track(detections)
                    
                    tracked_objects = [obj for obj in objects if obj.live_points[0]]
                    #tracked_objects = objects
                    
                    # keep track of killed objects
                    self.odt_killed_ids.extend([obj.id for obj in objects if (not obj.live_points[0] and not obj.hit_counter)])
                    # keep only detections matched with a tracked object
                    # for those only retrieve tracked id, no need for tracked point (obj.estimate[0])
                    # as below we use the detection xyxy for position estimation and display
                    tracked_detections = []
                    for det in detections:
                        for obj in tracked_objects:
                            if obj.data == det.data:
                                tracked_detections.append([*det.data, int(100*det.scores[0]), det.label, obj.id])
                                tracked_objects.remove(obj)
                                break
                    
                    if tracked_detections:
                        tracked_detections = np.asarray(tracked_detections)
                        # estimate positions
                        tracked_positions = estimate_positions(tracked_detections, self.camera)
                    

                        boxes = tracked_detections[:,:4]
                        foms, labels = [], []
                        for i in range(tracked_detections.shape[0]):
                            label = self.yoloTRT.classes[tracked_detections[i][5]]
                            tracked_id = tracked_detections[i][6]
                            distance = np.sqrt(tracked_positions[i][3])
                            if tracked_id in self.soa_fom_buffer:
                                fom = self.soa_fom_buffer[tracked_id]
                                foms.append(fom)
                                labels.append(f"{label} {tracked_id}, {distance:.1f}m, {fom:.2f}")
                            else:
                                foms.append(None)
                                labels.append(f"{label} {tracked_id}, {distance:.1f}m")
                        draw_boxes(img, boxes, foms=foms, labels=labels)




                        # SOA
                        if self.in_encounter_soa:
                            self.in_encounter_soa = False
                            ts = img_ts * 1E-9
                            for tracked_id, label, r2 in zip(tracked_detections[:,6], tracked_detections[:,5], tracked_positions[:,3]):
                                self.odt_ts_buffer[tracked_id].append(ts)
                                self.odt_r2_buffer[tracked_id].append(r2)
                                self.odt_label_buffer[tracked_id] = label


                            #rospy.loginfo(f"--> ODT buffer size: {len(self.odt_ts_buffer)}, ({tracked_detections.shape[0]} detections)") 
                                
                            # perform SOA
                            for k in self.odt_ts_buffer.keys():
                                    
                                #rospy.loginfo(f"--> SOA for id {k}")
                                    
                                if len(self.odt_ts_buffer[k]) < 2: continue

                                #rospy.loginfo(f"--> len odt {len(self.odt_ts_buffer[k])}")

                                if not self.soa_init_buffer[k]:

                                    #rospy.loginfo("--> init SOA")

                                    odt_ahead = bisect(self.encounter_ts_buffer, self.odt_ts_buffer[k][-1]) == 0
                                    odt_behind = bisect(self.encounter_ts_buffer, self.odt_ts_buffer[k][0]) == len(self.encounter_ts_buffer)
                                    if odt_ahead or odt_behind: 
                                        
                                        #rospy.loginfo("--> no overlap")
                                        
                                        continue
                                    if not odt_ahead and not odt_behind:
                                        if self.encounter_ts_buffer[0] < self.odt_ts_buffer[k][0]:
                                            self.soa_enc_idx_buffer[k] = bisect(self.encounter_ts_buffer, self.odt_ts_buffer[k][0])
                                            self.soa_odt_idx_buffer[k] = 1
                                        else:
                                            self.soa_enc_idx_buffer[k] = 0
                                            self.soa_odt_idx_buffer[k] = bisect(self.odt_ts_buffer[k], self.encounter_ts_buffer[0])
                    

                                        self.soa_init_buffer[k] = True

                                        data = self.encounter_data_buffer[self.soa_enc_idx_buffer[k]]
                                        err = self.encounter_err_buffer[self.soa_enc_idx_buffer[k]]
                                        dt = self.encounter_dt_buffer[self.soa_enc_idx_buffer[k]]
                                        self.soa_signal_buffer[k].append(data)
                                        self.soa_signal_std_buffer[k].append(err)
                                        resp = dt / (0.5 * (self.odt_r2_buffer[k][self.soa_odt_idx_buffer[k]-1] + self.odt_r2_buffer[k][self.soa_odt_idx_buffer[k]]))
                                        self.soa_response_buffer[k].append(resp)
                                        continue

                                if encounter.n_alarms:
                                    self.soa_enc_idx_buffer[k] += 1
                                    enc_ts = self.encounter_ts_buffer[self.soa_enc_idx_buffer[k]]
                                    
                                    # for now search full list of odt ts
                                    idx = bisect(self.odt_ts_buffer[k], enc_ts)
                                    
                                    # here idx cannot be zero
                                    if idx == len(self.odt_ts_buffer[k]):
                                        self.soa_enc_idx_buffer[k] -= 1
                                        continue
                                    else:
                                        data = self.encounter_data_buffer[self.soa_enc_idx_buffer[k]]
                                        err = self.encounter_err_buffer[self.soa_enc_idx_buffer[k]]
                                        dt = self.encounter_dt_buffer[self.soa_enc_idx_buffer[k]]
                                        self.soa_signal_buffer[k].append(data)
                                        self.soa_signal_std_buffer[k].append(err)
                                        resp = dt / (0.5 * (self.odt_r2_buffer[k][idx-1] + self.odt_r2_buffer[k][idx]))
                                        self.soa_response_buffer[k].append(resp)

                                        
                                        if len(self.soa_signal_buffer[k]) > 2:
                                            signal = np.asarray(self.soa_signal_buffer[k])
                                            signal_std = np.asarray(self.soa_signal_std_buffer[k])
                                            response = np.asarray(self.soa_response_buffer[k])
                                            scale = nnls((response / signal_std)[:, None], signal / signal_std)[0]
                                            
                                            chi2_value = np.sum((response * scale - signal)**2 / signal_std**2)
                                            N = len(self.soa_signal_buffer[k])
                                            # use reduced chi2 as fom
                                            #fom = chi2_value / (N - 1) 
                                            # use cdf of chi2 distribution with corresponding number of dof as fom
                                            #fom = chi2.cdf(chi2_value, df=(N - 1)) 
                                            # use chi2 converted to mean 0 and variance 1
                                            fom = (chi2_value - (N-1)) / np.sqrt(2*(N-1))
                                            self.soa_fom_buffer[k] = fom
                                            #rospy.loginfo(f"{self.yoloTRT.classes[self.odt_label_buffer[k]]}, {chi2_value}, {len(self.soa_signal_buffer[k]) - 1}, {fom}")

                            # discard killed objects from buffer
                            # can we reid instead?
                            for k in self.odt_killed_ids:
                                if k in self.odt_ts_buffer:
                                    del self.odt_ts_buffer[k]
                                    del self.odt_r2_buffer[k]
                                    del self.odt_label_buffer[k]
                                if k in self.soa_init_buffer:
                                    del self.soa_init_buffer[k]
                                if k in self.soa_enc_idx_buffer:
                                    del self.soa_enc_idx_buffer[k]
                                if k in self.soa_odt_idx_buffer:
                                    del self.soa_odt_idx_buffer[k]
                                if k in self.soa_signal_buffer:
                                    del self.soa_signal_buffer[k]
                                    del self.soa_signal_std_buffer[k]
                                    del self.soa_response_buffer[k]
                                    if k in self.soa_fom_buffer:
                                        del self.soa_fom_buffer[k]
                            self.odt_killed_ids.clear()        


                if not self.in_encounter:
                    # reset buffers at end of encounter
                    self.encounter_ts_buffer.clear()
                    self.encounter_dt_buffer.clear()
                    self.encounter_data_buffer.clear()
                    self.odt_ts_buffer.clear()
                    self.odt_r2_buffer.clear()
                    self.odt_label_buffer.clear()
                    self.odt_killed_ids.clear()
                    self.soa_init_buffer.clear()
                    self.soa_enc_idx_buffer.clear()
                    self.soa_odt_idx_buffer.clear()
                    self.soa_signal_buffer.clear()
                    self.soa_signal_std_buffer.clear()
                    self.soa_response_buffer.clear()
                    self.soa_fom_buffer.clear()

                # publish img
                self.publish_img(img, img_ts)
                
                #tl = img_ts * 1E-9
                rate = 1E9 / (img_ts - prev_img_ts)
                rospy.loginfo(f"processing at fps: {rate} Hz")
                prev_img_ts = img_ts
        
        #with plg.Plugin() as plugin:
        #    plugin.upload_file(bagfilename)
        #    time.sleep(10)
	

if __name__ == '__main__':
    with ODTSOANode() as node:
        #node.run()  
        with torch.no_grad():
            node.run()
