import cv2

def draw_boxes(img, boxes, labels=None):
    tl = 2
    tf = 1
    for box, label in zip(boxes, labels):
        c1, c2 = (box[0], box[1]), (box[2], box[3])
        cv2.rectangle(img, c1, c2, [255, 255, 255], thickness=tl, lineType=cv2.LINE_AA)
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, [255, 255, 255], -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [0, 0, 0], thickness=tf, lineType=cv2.LINE_AA)

def draw_boxes(img, boxes, foms=None, labels=None):
    tl = 2
    tf = 1
    for box, fom, label in zip(boxes, foms, labels):
        if fom is None: color = [255, 255, 255] #BGR
        else:
            # reduced chi2
            if fom < 4.: color = [0, 69, 255]     # red-orange 
            elif fom < 10.: color = [0, 215, 255] # gold 
            else: color = [170, 232, 238]         # pale gold
            # chi2.cdf
            #if fom > 0.9: color = [0, 69, 255]    # red-orange
            #elif fom > 0.5: color = [0, 215, 255] # gold
            #else: color = [170, 232, 238]         # pale gold
        # box
        c1, c2 = (box[0], box[1]), (box[2], box[3])
        cv2.rectangle(img, c1, c2, color, thickness=tl, lineType=cv2.LINE_AA)
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        # label
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, color, -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [0, 0, 0], thickness=tf, lineType=cv2.LINE_AA)


"""
def draw_boxes(img, boxes, labels, colors):
    tl = 2
    tf = 1
    for i, xyxy in enumerate(boxes):
        c1, c2 = (int(xyxy[0]), int(xyxy[1])), (int(xyxy[2]), int(xyxy[3]))
        # c1 = top left corner, c2 = bottom right corner
        cv2.rectangle(img, c1, c2, colors[i], thickness=tl, lineType=cv2.LINE_AA)
        label = labels[i]
        t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
        c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
        cv2.rectangle(img, c1, c2, colors[i], -1, cv2.LINE_AA)  # filled
        cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)

def draw_points(img, points, labels):
    tl = 2
    tf = 1
    for i, xy in enumerate(points):
        c = (int(xy[0]), int(xy[1]))
        cv2.drawMarker(img, (c[0], c[1]),
            color=[0, 0, 255], thickness=tl,
            markerType= cv2.MARKER_CROSS, line_type=cv2.LINE_AA,
            markerSize=30
        )
        cv2.putText(img, labels[i], (c[0] + 10, c[1] + 10), 0, tl / 3, [0, 0, 255], thickness=tf, lineType=cv2.LINE_AA)
"""
