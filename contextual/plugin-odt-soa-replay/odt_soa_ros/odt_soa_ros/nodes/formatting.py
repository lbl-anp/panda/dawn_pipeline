import cv2
import numpy as np

class InputFormat:
    def __init__(self, from_hw=(0, 0), to_hw=(0, 0), RGB=True):
        r_h = to_hw[0] / from_hw[0]
        r_w = to_hw[1] / from_hw[1]
        self.RGB = RGB
        self.ratio = min(r_h, r_w)
        self.th, self.tw = 0, 0
        self.pad_t, self.pad_b = 0, 0
        self.pad_l, self.pad_r = 0, 0
        self.pad_w, self.pad_h = 0, 0
        if r_h > r_w:
            self.tw = to_hw[1]
            self.th = int(r_w * from_hw[0])
            self.pad_t = int((to_hw[0] - self.th) / 2)
            self.pad_b = to_hw[0] - self.th - self.pad_t
            self.pad_h = self.pad_b
        else:
            self.tw = int(r_h * from_hw[1])
            self.th = to_hw[0]
            self.pad_l = int((to_hw[1] - self.tw) / 2)
            self.pad_r = to_hw[1] - self.tw - self.pad_l
            self.pad_w = self.pad_l
        self.resized_img = np.empty((self.tw, self.th, 3))
        self.data = np.empty((to_hw[1], to_hw[0], 3))
        self.dataT = np.transpose(self.data, [2, 0, 1])
        self.array = np.empty((1, 3, to_hw[1], to_hw[0]))
        self.output = np.empty(3*to_hw[1]*to_hw[0])
    
    def format(self, img):
        #
        # Resize img and pad within new dimensions
        #
        if not self.RGB: 
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.resized_img = cv2.resize(img, (self.tw, self.th))
        self.data = cv2.copyMakeBorder(
            self.resized_img, self.pad_t, self.pad_b, self.pad_l, self.pad_r, cv2.BORDER_CONSTANT, None, (128, 128, 128)
        )
        self.data = self.data.astype(np.float16)
        self.data /= 255.0
        # HWC to CHW format:
        self.dataT = np.transpose(self.data, [2, 0, 1])
        # Convert the image to row-major order, also known as "C order":
        self.array = np.expand_dims(self.dataT, axis=0)
        self.array = np.ascontiguousarray(self.array)
        self.output = self.array.ravel()
        return self.output
