import os
TRACKER_DIR = os.getenv('TRACKER_DIR')
import sys
sys.path.insert(1, TRACKER_DIR)
import numpy as np
import norfair
#DISTANCE_THRESHOLD_BBOX: float = 0.7
#DISTANCE_THRESHOLD_CENTROID: int = 30
DISTANCE_THRESHOLD_CENTROID: int = 100
MAX_DISTANCE: int = 10000

class Tracker:
    def __init__(self):
        distance_function = "euclidean"
        distance_threshold = DISTANCE_THRESHOLD_CENTROID
        self.impl = norfair.Tracker(
            distance_function=distance_function,
            distance_threshold=distance_threshold
        )
    
    def format(self, predictions):
        detections = []
        for *xyxy, score, cls in predictions:
            x = int(0.5 * (xyxy[0] + xyxy[2]))
            y = int(0.5 * (xyxy[1] + xyxy[3]))
            detections.append(
                norfair.Detection(
                    points=np.array([x, y]),
                    scores=np.array([score]),
                    label=int(cls),
                    data=[int(v) for v in xyxy],
                )
            )
        return detections

    def track(self, detections):
        return self.impl.update(detections=detections)

    """
    def track(self, predictions):
        if predictions is None:
            self.impl.update(detections=None)
            return
        detections = []
        for *xyxy, score, cls in predictions:
            x = (xyxy[0] + xyxy[2]) / 2
            y = (xyxy[1] + xyxy[3]) / 2
            detections.append(
                norfair.Detection(
                    points=np.array([x, y]),
                    scores=np.array([score]),
                    label=int(cls),
                )
            )
        return self.impl.update(detections=detections)
    """ 
