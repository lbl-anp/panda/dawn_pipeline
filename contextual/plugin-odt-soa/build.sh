#!/bin/bash

#rm -rf norfair
rm -rf vision_opencv
rm -rf yolov7
#cp -r ../../dependencies/norfair .       # detached branch
cp -r ../../dependencies/vision_opencv . # noetic branch
cp -r ../../dependencies/yolov7 .        # detached branch

rm -rf bard_ros_msgs
cp -r ../../rad/plugin-bard/bard_ros/bard_ros_msgs .

version=$1
#sudo docker build . --no-cache -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa:${version}
sudo docker build . -t registry.gitlab.com/lbl-anp/panda/dawn_pipeline/plugin-odt-soa:${version}
