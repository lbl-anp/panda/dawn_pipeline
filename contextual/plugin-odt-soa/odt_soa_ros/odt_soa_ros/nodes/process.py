#!/usr/bin/env python
# ROS
import rospy
from sensor_msgs.msg import Image, CompressedImage
from bard_ros_msgs.msg import EncounterInfo
# OpenCV
import cv2
from cv_bridge import CvBridge
# pywaggle
#import waggle.plugin as plg
# camera
from camera import Camera
# object detection
from yoloTRT import YoloTRT
import torch
# 3D positions
from metric import estimate_pos_from_boxes
# tracking
from tracker import Tracker
# drawing
from drawing import draw_boxes, draw_points
# generic
import numpy as np
import time

class ODTSOANode(object):
    #
    # Node performs object detection and tracking + source-object attribution
    # on unrectified camera frames
    #
    def __init__(self):
        #
        # intialize node
        #
        rospy.loginfo("Initializing node")
        rospy.init_node('odt_soa', anonymous=False)
        # read params
        params = self.get_parameters()

        # init camera -> intialization is modified for replay
        cam_IP = params["IP"]
        calib_file = params["calib_file"]
        self.camera = Camera(cam_IP, calib_file)
        
        # init OD
        model_file = params["model_file"]
        label_file = params["label_file"]
        conf_thr = params["conf_thr"]
        nms_thr = params["nms_thr"]
        self.yoloTRT = YoloTRT(
            model_file=model_file, label_file=label_file,
            input_w=self.camera.res_x, input_h=self.camera.res_y,
            conf_thr=conf_thr, nms_thr=nms_thr
        )
        
        # for replay
        #self.img = None
        #self.timestamp = 0.
        #self.sub_cam = rospy.Subscriber("/bag/sensors/camera/image_color", CompressedImage, self.on_camera_frame)

        # init tracking
        self.tracker = Tracker()
        # init SOA
        #

        # ROS
        self.cvbr = CvBridge()
        self.pub_img = rospy.Publisher("/odt/camera/image_color", Image, queue_size=60)
        self.sub_bard = rospy.Subscriber("/bard/encounter_info", EncounterInfo, self.on_encounter)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def close(self):
        pass
    
    def get_parameters(self):
        params = {}
        params["IP"] = rospy.get_param("~IP", default="")
        params["calib_file"] = rospy.get_param("~calib_file", default="")
        params["model_file"] = rospy.get_param("~model_file", default="")
        params["label_file"] = rospy.get_param("~label_file", default="")
        params["conf_thr"] = rospy.get_param("~conf_thr", default=0.25)
        params["nms_thr"] = rospy.get_param("~nms_thr", default=0.45)
        return params

    def publish_img(self, img, ts):
        #msg = self.cvbr.cv2_to_imgmsg(img)
        msg = Image()
        msg.data = np.ascontiguousarray(img).tobytes()
        msg.width = img.shape[1]
        msg.height = img.shape[0]
        msg.encoding = "bgr8"
        msg.step = 3 * img.shape[1]
        msg.header.stamp = rospy.Time(ts * 1E-9)
        self.pub_img.publish(msg)

    def on_encounter(self, msg):
        """
        encounter_id = msg.id
        tf = msg.timestamps[0]
        tl = msg.timestamps[-1]
        print(f"received encounter: id {encounter_id}, tf {tf}, lt {tl-tf}")
        if msg.model_info:
            print(f"model flavor: {msg.model_info.flavor}, trained: {msg.model_info.is_trained}")
        if msg.n_alarms:
            print(f"alarms for: {msg.alarm_ids}")
        """
        pass

    # for replay
    """
    def on_camera_frame(self, msg):
        self.img = cv2.cvtColor(self.cvbr.compressed_imgmsg_to_cv2(msg), cv2.COLOR_RGB2BGR)
        self.timestamp = msg.header.stamp.to_nsec()

    def get_camera_frame(self):
        while not self.timestamp: time.sleep(0.01)
        return self.img, self.timestamp
    """

    def run(self):
        # comment out for replay
        # start camera capture
        self.camera.capture_start()
        time.sleep(1.)
        
        tf = time.time()

        # run within TensorRT context
        with self.yoloTRT.engine.create_execution_context() as context:
            while not rospy.is_shutdown():
                # commented out for replay
                img, ts = self.camera.capture_read()

                # for replay
                #img, ts = self.get_camera_frame()
                #if ts * 1E-9 == tf: 
                #    print(f"same timestamp, ts {ts}")
                #    continue
                
                data = self.yoloTRT.format_input(img)
                self.yoloTRT.infer(data)
                predictions = self.yoloTRT.process_output()
                if predictions is None: 
                    rospy.loginfo("No predictions from inference on current frame")
                else:
                    # estimate depth
                    boxes = predictions[:,:4]
                    positions = estimate_pos_from_boxes(boxes, predictions[:,5], self.camera)
                    # draw bounding boxes and labels on img
                    #labels = [f'{self.yoloTRT.classes[int(cls)]} {score:.2f}' for score, cls in zip(predictions[:,4], predictions[:,5])]
                    labels = [f'{self.yoloTRT.classes[int(cls)]} {score:.2f}, {d:.2f}m' for score, cls, d in zip(predictions[:,4], predictions[:,5], positions[:,3])]
                    colors = [self.yoloTRT.colors[int(cls)] for cls in predictions[:,5]]
                    draw_boxes(img, boxes, labels, colors)
                    # track detections
                    objects = self.tracker.track(predictions)
                    points = [obj.estimate[0] for obj in objects if obj.live_points[0]]
                    labels = [str(obj.id) for obj in objects if obj.live_points[0]]
                    # draw traking markers and ids on img
                    draw_points(img, points, labels)
                
                
                #cv2.imwrite(f"/workspace/mount/img-{str(count)}.png", img)
                
                # publish img
                #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                self.publish_img(img, ts)
                
                tl = ts * 1E-9
                rate = 1. / (tl - tf)
                rospy.loginfo(f"processing at fps: {rate} Hz")
                tf = tl
        
        #with plg.Plugin() as plugin:
        #    plugin.upload_file(bagfilename)
        #    time.sleep(10)
	
        # comment out for replay
        self.camera.capture_stop()

if __name__ == '__main__':
    with ODTSOANode() as node:
        #node.run()  
        with torch.no_grad():
            node.run()
