# ROS
import rospy
from sensor_msgs.msg import CameraInfo
# pywaggle
from waggle.data.vision import resolve_device
from waggle.data.timestamp import get_timestamp
# generic
import os
import cv2
import time
import threading
import numpy as np

class Camera:
    def __init__(self, cam_IP, calib_file):
        stream = "rtsp://" + cam_IP + ":554/0/profile6/media.smp"
        self.res_x = 1280 
        self.res_y = 960
        rospy.loginfo("Camera params:")
        rospy.loginfo(f".. stream: {stream}")
        rospy.loginfo(f".. resolution: {self.res_x}x{self.res_y}")
        CAMERA_DIR = os.getenv('CAMERA_DIR')
        self.read_calibration(CAMERA_DIR+'/'+calib_file)
        # comment out for replay
        rospy.loginfo("Video capture")
        self.vcap = cv2.VideoCapture(resolve_device(stream))
        if self.vcap.isOpened() is False:
            rospy.loginfo("[Exiting]: Error accessing camera rstp stream.")
            exit(0)
        ok, self.img = self.vcap.read() # initialize with a single read
        if not ok:
            rospy.loginfo("[Exiting]: Error reading frame at initialization.")
            exit(0)
        self.capture_thread = threading.Thread(target=self.capture_update, args=())
        self.capture_thread.daemon = True  # daemon threads run in background
        self.lock = threading.Lock()
        self.stopped = False
        self.img, self.timestamp = None, None

    def read_calibration(self, calib_file):
        #
        # reads in calibration file to init camera matrix/distortion vector
        # prepares CameraInfo msg
        #
        rospy.loginfo(f"Camera calibration file: {calib_file}")
        self.cam_mtx = np.zeros((3, 3))
        self.dist = np.zeros((1,5))
        self.proj_mtx = np.zeros((3, 4))
        f = open(calib_file, 'r')
        for i, line in enumerate(f):
            if i < 3:
                self.cam_mtx[i] = np.asarray(list(map(float, line.split())))
            elif i == 3:
                self.dist[0] = np.asarray(list(map(float, line.split())))
            else:
                self.proj_mtx[i-4] = np.asarray(list(map(float, line.split())))
        # CameraInfo
        # http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html
        self.camInfoMsg = CameraInfo()
        # Header
        # .. timestamp, set at publication
        self.camInfoMsg.header.frame_id = "/sensors/camera"
        # camera resolution
        self.camInfoMsg.height = self.res_y
        self.camInfoMsg.width = self.res_x
        # distortion model
        self.camInfoMsg.distortion_model = "plumb_bob"
        # distortion vector
        self.camInfoMsg.D = [self.dist[0][0], self.dist[0][1], self.dist[0][2], self.dist[0][3], self.dist[0][4]]
        # camera intrinsic matrix (row-major)
        self.camInfoMsg.K = [self.cam_mtx[0][0], self.cam_mtx[0][1], self.cam_mtx[0][2],
                             self.cam_mtx[1][0], self.cam_mtx[1][1], self.cam_mtx[1][2],
                             self.cam_mtx[2][0], self.cam_mtx[2][1], self.cam_mtx[2][2]]
        # rectification matrix for stereos camera only, set to identity for monocular cameras
        self.camInfoMsg.R = [1., 0., 0., 0., 1., 0., 0., 0., 1.]
        # projection matrix
        self.camInfoMsg.P = [self.proj_mtx[0][0], self.proj_mtx[0][1], self.proj_mtx[0][2], self.proj_mtx[0][3],
                             self.proj_mtx[1][0], self.proj_mtx[1][1], self.proj_mtx[1][2], self.proj_mtx[1][3],
                             self.proj_mtx[2][0], self.proj_mtx[2][1], self.proj_mtx[2][2], self.proj_mtx[2][3]]
        rospy.loginfo(".. camera intrinsics:")
        rospy.loginfo(f"{self.camInfoMsg.K[0]}, {self.camInfoMsg.K[1]}, {self.camInfoMsg.K[2]}")
        rospy.loginfo(f"{self.camInfoMsg.K[3]}, {self.camInfoMsg.K[4]}, {self.camInfoMsg.K[5]}")
        rospy.loginfo(f"{self.camInfoMsg.K[6]}, {self.camInfoMsg.K[7]}, {self.camInfoMsg.K[8]}")
        rospy.loginfo(".. distortion vector:")
        rospy.loginfo(f"{self.camInfoMsg.D[0]}, {self.camInfoMsg.D[1]}, {self.camInfoMsg.D[2]}, {self.camInfoMsg.D[3]}, {self.camInfoMsg.D[4]}")
        rospy.loginfo(".. projection matrix:")
        rospy.loginfo(f"{self.camInfoMsg.P[0]}, {self.camInfoMsg.P[1]}, {self.camInfoMsg.P[2]}, {self.camInfoMsg.P[3]}")
        rospy.loginfo(f"{self.camInfoMsg.P[4]}, {self.camInfoMsg.P[5]}, {self.camInfoMsg.P[6]}, {self.camInfoMsg.P[7]}")
        rospy.loginfo(f"{self.camInfoMsg.P[8]}, {self.camInfoMsg.P[9]}, {self.camInfoMsg.P[10]}, {self.camInfoMsg.P[11]}")

    # comment out for replay
    def capture_start(self):
        self.capture_thread.start()

    def capture_update(self):
        sleep = 0.01 # sleeping 10ms between grabs
        while True:
            if self.stopped: break
            self.lock.acquire()
            ok = self.vcap.grab()
            if not ok:
                print("[Exiting]: Error grabbing frame.")
                exit(0) 
            self.timestamp = get_timestamp()
            ok, self.img = self.vcap.retrieve()
            if not ok:
                print("[Exiting]: Error retrieving frame.")
                exit(0)
            self.lock.release() 
            time.sleep(sleep)
        self.vcap.release()
    
    def capture_read(self):
        self.lock.acquire()
        img = self.img
        timestamp = self.timestamp
        self.lock.release()
        #img = cv2.undistort(img, self.cam_mtx, self.dist, None, self.proj_mtx)
        #return cv2.undistort(img, self.cam_mtx, self.dist), timestamp
        return img, timestamp

    def capture_stop(self):
        self.stopped = True
