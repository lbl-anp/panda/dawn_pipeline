# ROS
import rospy
# YOLO + TRT
from utils.general import increment_path, non_max_suppression, xyxy2xywh, scale_coords
from utils.torch_utils import time_synchronized
from utils.plots import plot_one_box
import pycuda.autoinit
import pycuda.driver as cuda
import tensorrt as trt
from formatting import InputFormat
# generic
import os
import sys
import numpy as np
from collections import defaultdict

#
# For inference post-processing
#
def nms(boxes, scores, nms_thr):
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]
    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])
        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)
        inds = np.where(ovr <= nms_thr)[0]
        order = order[inds + 1]
    return keep

def multiclass_nms(boxes, scores, nms_thr, score_thr):
    final_dets = []
    num_classes = scores.shape[1]
    for cls_ind in range(num_classes):
        cls_scores = scores[:, cls_ind]
        valid_score_mask = cls_scores > score_thr
        if valid_score_mask.sum() == 0: continue
        else:
            valid_scores = cls_scores[valid_score_mask]
            valid_boxes = boxes[valid_score_mask]
            keep = nms(valid_boxes, valid_scores, nms_thr)
            if len(keep) > 0:
                cls_inds = np.ones((len(keep), 1)) * cls_ind
                dets = np.concatenate(
                        [valid_boxes[keep], valid_scores[keep, None], cls_inds], 1
                        )
                final_dets.append(dets)
    if len(final_dets) == 0: return None
    return np.concatenate(final_dets, 0)

#
# Node implements yolov7-tiny inference with TensorRT engine
#
class YoloTRT:
    def __init__(self, model_file="", label_file="", input_w=0, input_h=0, conf_thr=0, nms_thr=0):
        rospy.loginfo("OD params:")
        rospy.loginfo(f".. model: {model_file}")
        rospy.loginfo(f".. labels: {label_file}")   
        rospy.loginfo(f".. input width: {input_w}")
        rospy.loginfo(f".. input height: {input_h}")
        rospy.loginfo(f".. score thr: {conf_thr}")
        rospy.loginfo(f".. NMS thr: {nms_thr}")
        ids = [0, 1, 2, 3, 4, 5, 6, 7] # coco label ids for: person, bicycle, car, motorcycle, airplane, bus, train, truck
        OD_DIR = os.getenv('OD_DIR')
        with open(OD_DIR+'/'+label_file, "r") as f:
            labels = np.asarray([line.rstrip() for line in f])[ids]
        self.n_classes = len(ids)
        self.classes, self.colors = {}, {}
        for i in range(self.n_classes):
            self.classes[ids[i]] = labels[i]
            self.colors[ids[i]] = [np.random.randint(0, 255) for _ in range(3)]
        self.conf_thr = conf_thr
        self.nms_thr = nms_thr
        rospy.loginfo("Initializing CUDA context and stream")
        self.cuda_ctx = cuda.Device(0).make_context()
        self.cuda_str = cuda.Stream()
        rospy.loginfo("Initializing TensorRT engine")
        TRT_LOGGER = trt.Logger(trt.Logger.INFO)      
        #self.engine = get_engine(model_file, trt_logger)
        runtime = trt.Runtime(TRT_LOGGER)
        with open(OD_DIR+'/'+model_file, "rb") as f:
            self.engine = runtime.deserialize_cuda_engine(f.read())
        self.context = self.engine.create_execution_context()
        self.batch_size = self.engine.max_batch_size
        rospy.loginfo(f"Initializing host and device buffers, max batch size: {self.engine.max_batch_size}")
        self.host_inputs, self.cuda_inputs = [], []
        self.host_outputs, self.cuda_outputs = [], []
        self.bindings = []
        for binding in self.engine:
            # Compute size of memory to allocate
            size = (trt.volume(self.engine.get_binding_shape(binding)) * self.batch_size)
            dtype = trt.nptype(self.engine.get_binding_dtype(binding))
            # allocate host and device buffers
            host_mem = cuda.pagelocked_empty(size, dtype)
            cuda_mem = cuda.mem_alloc(host_mem.nbytes)
            # append the device buffer to device bindings
            self.bindings.append(int(cuda_mem))
            # append to the appropriate list
            if self.engine.binding_is_input(binding):
                self.input_w = self.engine.get_binding_shape(binding)[-1]
                self.input_h = self.engine.get_binding_shape(binding)[-2]
                self.host_inputs.append(host_mem)
                self.cuda_inputs.append(cuda_mem)
            else:
                self.host_outputs.append(host_mem)
                self.cuda_outputs.append(cuda_mem)
        rospy.loginfo("Initializing input formatting")
        self.inputFormat = InputFormat(
            from_hw=(input_h, input_w),
            to_hw=(self.input_h, self.input_w),
            RGB=True
        )
        rospy.loginfo("Initializing output formatting")
        self.output_size = 25200
        #self.n_preds = 25200  # for strides of 8 (3x80x80), 16 (3x40x40), and 32 (3x20x20) on 640x640
        #self.n_preds = 19200  # for strides of 8 (3x80x80)
        self.n_preds = 6000   # for strides of 16 (3x40x40), and 32 (3x20x20) on 640x640

        self.n_features = 13 # x w y h score p0 ... p7
        self.stride = 85     # inference on 80 classes total
        self.bboxes_xywh = np.zeros((self.n_preds, 4))
        self.bboxes_xyxy = np.zeros((self.n_preds, 4))
        self.scores = np.zeros((self.n_preds, self.n_classes))

        self.predictions = np.zeros((self.n_preds, self.stride))


    def format_input(self, img):
        return self.inputFormat.format(img)
    
    def infer(self, data):
        self.cuda_ctx.push()
        np.copyto(self.host_inputs[0], data)
        cuda.memcpy_htod_async(self.cuda_inputs[0], self.host_inputs[0], self.cuda_str)
        self.context.execute_async(
            batch_size=self.batch_size, bindings=self.bindings, stream_handle=self.cuda_str.handle
        )
        cuda.memcpy_dtoh_async(self.host_outputs[0], self.cuda_outputs[0], self.cuda_str)
        self.cuda_str.synchronize()
        self.cuda_ctx.pop()

    def process_output(self):
        # get inference results for considered classes only
        #self.predictions = (self.host_outputs[0]).reshape((self.n_preds, self.stride))[:, :self.n_features]
        self.predictions = (self.host_outputs[0][(self.output_size - self.n_preds)*self.stride:]).reshape((self.n_preds, self.stride))[:, :self.n_features]
        self.bboxes_xywh = self.predictions[:, :4]
        self.scores = self.predictions[:, 4:5] * self.predictions[:, 5:]

        # transform xywh default bboxes to xyxy bboxes, correct for resizing (ratio and padding)
        self.bboxes_xyxy[:, 0] = self.bboxes_xywh[:, 0] - self.bboxes_xywh[:, 2] / 2. # xmin = x_center - half_width
        self.bboxes_xyxy[:, 1] = self.bboxes_xywh[:, 1] - self.bboxes_xywh[:, 3] / 2. # ymin = y_center - half_height
        self.bboxes_xyxy[:, 2] = self.bboxes_xywh[:, 0] + self.bboxes_xywh[:, 2] / 2. # xmax = x_center + half_width
        self.bboxes_xyxy[:, 3] = self.bboxes_xywh[:, 1] + self.bboxes_xywh[:, 3] / 2. # ymax = y_center + half_height
        self.bboxes_xyxy -= np.array([self.inputFormat.pad_w, self.inputFormat.pad_h, self.inputFormat.pad_w, self.inputFormat.pad_h])
        self.bboxes_xyxy /= self.inputFormat.ratio
        # run NMS
        #return np.array([]), np.array([]), np.array([])
        return multiclass_nms(self.bboxes_xyxy, self.scores, nms_thr=self.nms_thr, score_thr=self.conf_thr)





