import numpy as np
import cv2

nominal_heights = np.array(
        [1.8,  # class 0, person
         1.5,  # class 1, bicycle
         1.5,  # class 2, car
         1.5,  # class 3, motorcycle
         0.,   # class 4, airplane
         2.0,  # class 5, bus
         0.,   # class 6, train
         2.0   # class 7, truck
         ])
    
def estimate_pos_from_boxes(boxes, classes, camera):
    """
    boxes: in xyxy (top left corner, bottom right corner)
    classes: class idx
    camera: camera model

    Estimates depth based on camera intrinsics and distortion vectors
    and scaling of box heights with nominal heights per class
    """
    N = boxes.shape[0]
    # x coordinate at center of boxes 
    x = 0.5 * (boxes[:,0] + boxes[:,2]).reshape((N,1))
    # y coordinate at top, center, and bottom of boxes
    y_t = boxes[:,1].reshape((N,1))
    y_c = 0.5 * (boxes[:,1] + boxes[:,3]).reshape((N,1))
    y_b = boxes[:,3].reshape((N,1))
    # define 3 points per box along center
    points = np.zeros((3*N,1,2))
    points[:N,:,0] = x
    points[:N,:,1] = y_t
    points[N:2*N,:,0] = x
    points[N:2*N,:,1] = y_c
    points[2*N:,:,0] = x
    points[2*N:,:,1] = y_b
    # undistort points
    undistorted_points = cv2.undistortPoints(points, camera.cam_mtx, camera.dist, P=camera.proj_mtx)
    # undistorted box height values
    # if it comes back negative we re-assign original height value
    heights = (points[2*N:,:,1] - points[:N,:,1]).flatten()
    undistorted_heights = (undistorted_points[2*N:,:,1] - undistorted_points[:N,:,1]).flatten()
    mask = (undistorted_heights < 0)
    undistorted_heights[mask] = heights[mask]
    # estimate positions at center of boxes
    # pos_z = fx * nominal_height / height
    # pos_x = (x - cx) * z / fx
    # pos_y = (y - cy) * z / fy
    positions = np.zeros((N,4))
    fx = camera.proj_mtx[0][0]
    fy = camera.proj_mtx[1][1]
    cx = camera.proj_mtx[0][2]
    cy = camera.proj_mtx[1][2]
    positions[:,2] = nominal_heights[classes.astype(int)] * fx * np.reciprocal(undistorted_heights) 
    positions[:,0] = (undistorted_points[N:2*N,:,0].flatten() - cx) * positions[:,2] / fx
    positions[:,1] = (undistorted_points[N:2*N,:,1].flatten() - cy) * positions[:,2] / fy
    positions[:,3] = np.sqrt(np.square(positions[:,0]) + np.square(positions[:,1]) + np.square(positions[:,2]))
    print(positions)

    return positions

